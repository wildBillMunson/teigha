

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 8.00.0603 */
/* at Tue Mar 21 02:46:13 2017
 */
/* Compiler settings for Z:\B\2\C\C524\Core\ActiveX\OdaX\OdaX2.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 8.00.0603 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __OdaX2_h__
#define __OdaX2_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IAcadTable_FWD_DEFINED__
#define __IAcadTable_FWD_DEFINED__
typedef interface IAcadTable IAcadTable;

#endif 	/* __IAcadTable_FWD_DEFINED__ */


#ifndef __IAcadOle_FWD_DEFINED__
#define __IAcadOle_FWD_DEFINED__
typedef interface IAcadOle IAcadOle;

#endif 	/* __IAcadOle_FWD_DEFINED__ */


#ifndef __IOdaOle_FWD_DEFINED__
#define __IOdaOle_FWD_DEFINED__
typedef interface IOdaOle IOdaOle;

#endif 	/* __IOdaOle_FWD_DEFINED__ */


#ifndef __IAcadDynamicBlockReferenceProperty_FWD_DEFINED__
#define __IAcadDynamicBlockReferenceProperty_FWD_DEFINED__
typedef interface IAcadDynamicBlockReferenceProperty IAcadDynamicBlockReferenceProperty;

#endif 	/* __IAcadDynamicBlockReferenceProperty_FWD_DEFINED__ */


#ifndef __IOdaMLineStyles_FWD_DEFINED__
#define __IOdaMLineStyles_FWD_DEFINED__
typedef interface IOdaMLineStyles IOdaMLineStyles;

#endif 	/* __IOdaMLineStyles_FWD_DEFINED__ */


#ifndef __IOdaMLineStyle_FWD_DEFINED__
#define __IOdaMLineStyle_FWD_DEFINED__
typedef interface IOdaMLineStyle IOdaMLineStyle;

#endif 	/* __IOdaMLineStyle_FWD_DEFINED__ */


#ifndef __IAcadSurface_FWD_DEFINED__
#define __IAcadSurface_FWD_DEFINED__
typedef interface IAcadSurface IAcadSurface;

#endif 	/* __IAcadSurface_FWD_DEFINED__ */


#ifndef __IAcadPlaneSurface_FWD_DEFINED__
#define __IAcadPlaneSurface_FWD_DEFINED__
typedef interface IAcadPlaneSurface IAcadPlaneSurface;

#endif 	/* __IAcadPlaneSurface_FWD_DEFINED__ */


#ifndef __IAcadExtrudedSurface_FWD_DEFINED__
#define __IAcadExtrudedSurface_FWD_DEFINED__
typedef interface IAcadExtrudedSurface IAcadExtrudedSurface;

#endif 	/* __IAcadExtrudedSurface_FWD_DEFINED__ */


#ifndef __IAcadRevolvedSurface_FWD_DEFINED__
#define __IAcadRevolvedSurface_FWD_DEFINED__
typedef interface IAcadRevolvedSurface IAcadRevolvedSurface;

#endif 	/* __IAcadRevolvedSurface_FWD_DEFINED__ */


#ifndef __IAcadSweptSurface_FWD_DEFINED__
#define __IAcadSweptSurface_FWD_DEFINED__
typedef interface IAcadSweptSurface IAcadSweptSurface;

#endif 	/* __IAcadSweptSurface_FWD_DEFINED__ */


#ifndef __IAcadLoftedSurface_FWD_DEFINED__
#define __IAcadLoftedSurface_FWD_DEFINED__
typedef interface IAcadLoftedSurface IAcadLoftedSurface;

#endif 	/* __IAcadLoftedSurface_FWD_DEFINED__ */


#ifndef __IAcadHelix_FWD_DEFINED__
#define __IAcadHelix_FWD_DEFINED__
typedef interface IAcadHelix IAcadHelix;

#endif 	/* __IAcadHelix_FWD_DEFINED__ */


#ifndef __IAcadUnderlay_FWD_DEFINED__
#define __IAcadUnderlay_FWD_DEFINED__
typedef interface IAcadUnderlay IAcadUnderlay;

#endif 	/* __IAcadUnderlay_FWD_DEFINED__ */


#ifndef __IAcadDwfUnderlay_FWD_DEFINED__
#define __IAcadDwfUnderlay_FWD_DEFINED__
typedef interface IAcadDwfUnderlay IAcadDwfUnderlay;

#endif 	/* __IAcadDwfUnderlay_FWD_DEFINED__ */


#ifndef __IOdaPolyfaceMesh_FWD_DEFINED__
#define __IOdaPolyfaceMesh_FWD_DEFINED__
typedef interface IOdaPolyfaceMesh IOdaPolyfaceMesh;

#endif 	/* __IOdaPolyfaceMesh_FWD_DEFINED__ */


#ifndef __IAcadSubEntity_FWD_DEFINED__
#define __IAcadSubEntity_FWD_DEFINED__
typedef interface IAcadSubEntity IAcadSubEntity;

#endif 	/* __IAcadSubEntity_FWD_DEFINED__ */


#ifndef __IAcadMLeaderLeader_FWD_DEFINED__
#define __IAcadMLeaderLeader_FWD_DEFINED__
typedef interface IAcadMLeaderLeader IAcadMLeaderLeader;

#endif 	/* __IAcadMLeaderLeader_FWD_DEFINED__ */


#ifndef __IAcadSubEntSolidFace_FWD_DEFINED__
#define __IAcadSubEntSolidFace_FWD_DEFINED__
typedef interface IAcadSubEntSolidFace IAcadSubEntSolidFace;

#endif 	/* __IAcadSubEntSolidFace_FWD_DEFINED__ */


#ifndef __IAcadSubEntSolidEdge_FWD_DEFINED__
#define __IAcadSubEntSolidEdge_FWD_DEFINED__
typedef interface IAcadSubEntSolidEdge IAcadSubEntSolidEdge;

#endif 	/* __IAcadSubEntSolidEdge_FWD_DEFINED__ */


#ifndef __IAcadSubEntSolidVertex_FWD_DEFINED__
#define __IAcadSubEntSolidVertex_FWD_DEFINED__
typedef interface IAcadSubEntSolidVertex IAcadSubEntSolidVertex;

#endif 	/* __IAcadSubEntSolidVertex_FWD_DEFINED__ */


#ifndef __IAcadWipeout_FWD_DEFINED__
#define __IAcadWipeout_FWD_DEFINED__
typedef interface IAcadWipeout IAcadWipeout;

#endif 	/* __IAcadWipeout_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"
#include "OdaX.h"

#ifdef __cplusplus
extern "C"{
#endif 


/* interface __MIDL_itf_OdaX2_0000_0000 */
/* [local] */ 
























































































































extern RPC_IF_HANDLE __MIDL_itf_OdaX2_0000_0000_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_OdaX2_0000_0000_v0_0_s_ifspec;

#ifndef __IAcadTable_INTERFACE_DEFINED__
#define __IAcadTable_INTERFACE_DEFINED__

/* interface IAcadTable */
/* [object][oleautomation][dual][helpstring][uuid] */ 


EXTERN_C const IID IID_IAcadTable;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("62AE6596-74E9-4fc1-9F31-CB9567E550F9")
    IAcadTable : public IAcadEntity
    {
    public:
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_StyleName( 
            /* [retval][out] */ BSTR *bstrName) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_StyleName( 
            /* [in] */ BSTR bstrName) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_Rows( 
            /* [retval][out] */ int *pRows) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_Rows( 
            /* [in] */ int pRows) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_Columns( 
            /* [retval][out] */ int *pColumns) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_Columns( 
            /* [in] */ int pColumns) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_FlowDirection( 
            /* [retval][out] */ AcTableDirection *pFlow) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_FlowDirection( 
            /* [in] */ AcTableDirection pFlow) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_Width( 
            /* [retval][out] */ double *pWidth) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_Width( 
            /* [in] */ double pWidth) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_Height( 
            /* [retval][out] */ double *pHeight) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_Height( 
            /* [in] */ double pHeight) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_VertCellMargin( 
            /* [retval][out] */ double *pGap) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_VertCellMargin( 
            /* [in] */ double pGap) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_HorzCellMargin( 
            /* [retval][out] */ double *pGap) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_HorzCellMargin( 
            /* [in] */ double pGap) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_InsertionPoint( 
            /* [retval][out] */ VARIANT *insPoint) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_InsertionPoint( 
            /* [in] */ VARIANT insPoint) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetColumnWidth( 
            /* [in] */ int col,
            /* [retval][out] */ double *pWidth) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetColumnWidth( 
            /* [in] */ int col,
            /* [in] */ double Width) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_ColumnWidth( 
            /* [in] */ double rhs) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetRowHeight( 
            /* [in] */ int row,
            /* [retval][out] */ double *pHeight) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetRowHeight( 
            /* [in] */ int row,
            /* [in] */ double Height) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_RowHeight( 
            /* [in] */ double rhs) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetMinimumColumnWidth( 
            /* [in] */ int col,
            /* [retval][out] */ double *pWidth) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetMinimumRowHeight( 
            /* [in] */ int row,
            /* [retval][out] */ double *pHeight) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_MinimumTableWidth( 
            /* [retval][out] */ double *pWidth) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_MinimumTableHeight( 
            /* [retval][out] */ double *pHeight) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_Direction( 
            /* [retval][out] */ VARIANT *DirectionVector) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_Direction( 
            /* [in] */ VARIANT DirectionVector) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_TitleSuppressed( 
            /* [retval][out] */ VARIANT_BOOL *bValue) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_TitleSuppressed( 
            /* [in] */ VARIANT_BOOL bValue) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_HeaderSuppressed( 
            /* [retval][out] */ VARIANT_BOOL *bValue) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_HeaderSuppressed( 
            /* [in] */ VARIANT_BOOL bValue) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetAlignment( 
            /* [in] */ AcRowType rowType,
            /* [retval][out] */ AcCellAlignment *pCellAlignment) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetAlignment( 
            /* [in] */ int rowTypes,
            /* [in] */ AcCellAlignment cellAlignment) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetBackgroundColorNone( 
            /* [in] */ AcRowType rowType,
            /* [retval][out] */ VARIANT_BOOL *bValue) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetBackgroundColorNone( 
            /* [in] */ int rowTypes,
            /* [in] */ VARIANT_BOOL bValue) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetBackgroundColor( 
            /* [in] */ AcRowType rowType,
            /* [retval][out] */ IAcadAcCmColor **pColor) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetBackgroundColor( 
            /* [in] */ int rowTypes,
            /* [in] */ IAcadAcCmColor *pColor) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetContentColor( 
            /* [in] */ AcRowType rowType,
            /* [retval][out] */ IAcadAcCmColor **pColor) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetContentColor( 
            /* [in] */ int rowTypes,
            /* [in] */ IAcadAcCmColor *pColor) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetTextStyle( 
            /* [in] */ AcRowType rowType,
            /* [retval][out] */ BSTR *bstrName) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetTextStyle( 
            /* [in] */ int rowTypes,
            /* [in] */ BSTR bstrName) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetTextHeight( 
            /* [in] */ AcRowType rowType,
            /* [retval][out] */ double *pTextHeight) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetTextHeight( 
            /* [in] */ int rowTypes,
            /* [in] */ double TextHeight) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetGridLineWeight( 
            /* [in] */ AcGridLineType gridLineType,
            /* [in] */ AcRowType rowType,
            /* [retval][out] */ ACAD_LWEIGHT *Lineweight) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetGridLineWeight( 
            /* [in] */ int gridLineTypes,
            /* [in] */ int rowTypes,
            /* [in] */ ACAD_LWEIGHT Lineweight) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetGridColor( 
            /* [in] */ AcGridLineType gridLineType,
            /* [in] */ AcRowType rowType,
            /* [retval][out] */ IAcadAcCmColor **pColor) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetGridColor( 
            /* [in] */ int gridLineTypes,
            /* [in] */ int rowTypes,
            /* [in] */ IAcadAcCmColor *pColor) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetGridVisibility( 
            /* [in] */ AcGridLineType gridLineType,
            /* [in] */ AcRowType rowType,
            /* [retval][out] */ VARIANT_BOOL *bValue) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetGridVisibility( 
            /* [in] */ int gridLineTypes,
            /* [in] */ int rowTypes,
            /* [in] */ VARIANT_BOOL bValue) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_TableStyleOverrides( 
            /* [retval][out] */ VARIANT *pIntArray) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ClearTableStyleOverrides( 
            /* [in] */ int flag) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetCellType( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [retval][out] */ AcCellType *pCellType) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetCellType( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ AcCellType CellType) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetCellExtents( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ VARIANT_BOOL bOuterCell,
            /* [retval][out] */ VARIANT *pPts) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetAttachmentPoint( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [retval][out] */ VARIANT *pAttachmentPoint) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetCellAlignment( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [retval][out] */ AcCellAlignment *pCellAlignment) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetCellAlignment( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ AcCellAlignment cellAlignment) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetCellBackgroundColorNone( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [retval][out] */ VARIANT_BOOL *bValue) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetCellBackgroundColorNone( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ VARIANT_BOOL bValue) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetCellBackgroundColor( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [retval][out] */ IAcadAcCmColor **pColor) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetCellBackgroundColor( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ IAcadAcCmColor *pColor) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetCellContentColor( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [retval][out] */ IAcadAcCmColor **pColor) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetCellContentColor( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ IAcadAcCmColor *pColor) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetCellStyleOverrides( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [retval][out] */ VARIANT *pIntArray) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE DeleteCellContent( 
            /* [in] */ int row,
            /* [in] */ int col) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetRowType( 
            /* [in] */ int row,
            /* [retval][out] */ AcRowType *pRowType) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetText( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [retval][out] */ BSTR *pStr) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetText( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ BSTR pStr) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetCellTextStyle( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [retval][out] */ BSTR *bstrName) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetCellTextStyle( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ BSTR bstrName) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetCellTextHeight( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [retval][out] */ double *pTextHeight) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetCellTextHeight( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ double TextHeight) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetTextRotation( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [retval][out] */ AcRotationAngle *TextRotation) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetTextRotation( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ AcRotationAngle TextRotation) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetAutoScale( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [retval][out] */ VARIANT_BOOL *bValue) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetAutoScale( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ VARIANT_BOOL bValue) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetBlockTableRecordId( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [retval][out] */ LONG_PTR *blkId) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetBlockTableRecordId( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ LONG_PTR blkId,
            /* [in] */ VARIANT_BOOL bAutoFit) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetBlockScale( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [retval][out] */ double *blkScale) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetBlockScale( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ double blkScale) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetBlockRotation( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [retval][out] */ double *blkRotation) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetBlockRotation( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ double blkRotation) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetBlockAttributeValue( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ LONG_PTR attdefId,
            /* [retval][out] */ BSTR *bstrValue) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetBlockAttributeValue( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ LONG_PTR attdefId,
            /* [in] */ BSTR bstrValue) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetCellGridLineWeight( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ AcCellEdgeMask edge,
            /* [retval][out] */ ACAD_LWEIGHT *plineweight) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetCellGridLineWeight( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ int edges,
            /* [in] */ ACAD_LWEIGHT Lineweight) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetCellGridColor( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ AcCellEdgeMask edge,
            /* [retval][out] */ IAcadAcCmColor **pColor) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetCellGridColor( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ int edges,
            /* [in] */ IAcadAcCmColor *pColor) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetCellGridVisibility( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ AcCellEdgeMask edge,
            /* [retval][out] */ VARIANT_BOOL *bValue) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetCellGridVisibility( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ int edges,
            /* [in] */ VARIANT_BOOL bValue) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE InsertColumns( 
            /* [in] */ int col,
            /* [in] */ double Width,
            /* [in] */ int cols) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE DeleteColumns( 
            /* [in] */ int col,
            /* [in] */ int cols) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE InsertRows( 
            /* [in] */ int row,
            /* [in] */ double Height,
            /* [in] */ int Rows) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE DeleteRows( 
            /* [in] */ int row,
            /* [in] */ int Rows) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE MergeCells( 
            /* [in] */ int minRow,
            /* [in] */ int maxRow,
            /* [in] */ int minCol,
            /* [in] */ int maxCol) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE UnmergeCells( 
            /* [in] */ int minRow,
            /* [in] */ int maxRow,
            /* [in] */ int minCol,
            /* [in] */ int maxCol) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IsMergedCell( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [out] */ int *minRow,
            /* [out] */ int *maxRow,
            /* [out] */ int *minCol,
            /* [out] */ int *maxCol,
            /* [retval][out] */ VARIANT_BOOL *pbValue) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFieldId( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [retval][out] */ LONG_PTR *fieldId) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetFieldId( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ LONG_PTR fieldId) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GenerateLayout( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RecomputeTableBlock( 
            /* [in] */ VARIANT_BOOL bForceUpdate) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE HitTest( 
            /* [in] */ VARIANT wpt,
            /* [in] */ VARIANT wviewVec,
            /* [out] */ int *resultRowIndex,
            /* [out] */ int *resultColumnIndex,
            /* [retval][out] */ VARIANT_BOOL *bReturn) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Select( 
            /* [in] */ VARIANT wpt,
            /* [in] */ VARIANT wvwVec,
            /* [in] */ VARIANT wvwxVec,
            /* [in] */ double wxaper,
            /* [in] */ double wyaper,
            /* [in] */ VARIANT_BOOL allowOutside,
            /* [out] */ int *resultRowIndex,
            /* [out] */ int *resultColumnIndex,
            /* [retval][out] */ VARIANT *pPaths) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SelectSubRegion( 
            /* [in] */ VARIANT wpt1,
            /* [in] */ VARIANT wpt2,
            /* [in] */ VARIANT wvwVec,
            /* [in] */ VARIANT wvwxVec,
            /* [in] */ AcSelectType seltype,
            /* [in] */ VARIANT_BOOL bIncludeCurrentSelection,
            /* [out] */ int *rowMin,
            /* [out] */ int *rowMax,
            /* [out] */ int *colMin,
            /* [out] */ int *colMax,
            /* [retval][out] */ VARIANT *pPaths) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ReselectSubRegion( 
            /* [retval][out] */ VARIANT *pPaths) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetSubSelection( 
            /* [out] */ int *rowMin,
            /* [out] */ int *rowMax,
            /* [out] */ int *colMin,
            /* [out] */ int *colMax) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetSubSelection( 
            /* [in] */ int rowMin,
            /* [in] */ int rowMax,
            /* [in] */ int colMin,
            /* [in] */ int colMax) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ClearSubSelection( void) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_HasSubSelection( 
            /* [retval][out] */ VARIANT_BOOL *pbValue) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_RegenerateTableSuppressed( 
            /* [retval][out] */ VARIANT_BOOL *bValue) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_RegenerateTableSuppressed( 
            /* [in] */ VARIANT_BOOL bValue) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetDataType( 
            /* [in] */ AcRowType rowType,
            /* [out] */ AcValueDataType *pDataType,
            /* [out] */ AcValueUnitType *pUnitType) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetDataType( 
            /* [in] */ int rowTypes,
            /* [in] */ AcValueDataType dataType,
            /* [in] */ AcValueUnitType unitType) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFormat( 
            /* [in] */ AcRowType rowType,
            /* [retval][out] */ BSTR *pFormat) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetFormat( 
            /* [in] */ int rowTypes,
            BSTR pFormat) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE FormatValue( 
            /* [in] */ int row,
            /* [in] */ int col,
            AcFormatOption nOption,
            BSTR *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetCellDataType( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [out] */ AcValueDataType *pDataType,
            /* [out] */ AcValueUnitType *pUnitType) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetCellDataType( 
            /* [in] */ int row,
            /* [in] */ int col,
            AcValueDataType dataType,
            AcValueUnitType unitType) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetCellFormat( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [retval][out] */ BSTR *pFormat) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetCellFormat( 
            /* [in] */ int row,
            /* [in] */ int col,
            BSTR pFormat) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetCellValue( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [retval][out] */ VARIANT *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetCellValue( 
            /* [in] */ int row,
            /* [in] */ int col,
            VARIANT val) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetCellValueFromText( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ BSTR val,
            /* [in] */ AcParseOption nOption) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ResetCellValue( 
            /* [in] */ int row,
            /* [in] */ int col) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IsEmpty( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [retval][out] */ VARIANT_BOOL *bValue) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CreateContent( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nIndex,
            /* [retval][out] */ int *pInt) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE MoveContent( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nFromIndex,
            /* [in] */ int nToIndex) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE DeleteContent( 
            /* [in] */ int nRow,
            /* [in] */ int nCol) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetValue( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [retval][out] */ VARIANT *pAcValue) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetValue( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [in] */ VARIANT acValue) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetValueFromText( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [in] */ BSTR szText,
            /* [in] */ AcParseOption nOption) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetDataFormat( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [retval][out] */ BSTR *pValue) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetDataFormat( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [in] */ BSTR szFormat) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetTextString( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [retval][out] */ BSTR *pTextString) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetTextString( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [in] */ BSTR Text) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFieldId2( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [retval][out] */ LONG_PTR *pAcDbObjectId) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetFieldId2( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [in] */ LONG_PTR acDbObjectId,
            /* [in] */ AcCellOption nflag) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetBlockTableRecordId2( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [retval][out] */ LONG_PTR *pAcDbObjectId) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetBlockTableRecordId2( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [in] */ LONG_PTR blkId,
            /* [in] */ VARIANT_BOOL autoFit) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetBlockAttributeValue2( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [in] */ LONG_PTR blkId,
            /* [retval][out] */ BSTR *Value) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetBlockAttributeValue2( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [in] */ LONG_PTR blkId,
            /* [in] */ BSTR Value) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetCustomData( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ BSTR szKey,
            /* [out] */ VARIANT *pData) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetCustomData( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ BSTR szKey,
            /* [in] */ VARIANT data) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetCellStyle( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [retval][out] */ BSTR *pCellStyle) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetCellStyle( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ BSTR szCellStyle) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetContentColor2( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [retval][out] */ IAcadAcCmColor **pColor) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetContentColor2( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [in] */ IAcadAcCmColor *pColor) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetDataType2( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [out] */ AcValueDataType *pDataType,
            /* [out] */ AcValueUnitType *pUnitType) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetDataType2( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [in] */ AcValueDataType dataType,
            /* [in] */ AcValueUnitType unitType) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetTextStyle2( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [retval][out] */ BSTR *pbstrStyleName) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetTextStyle2( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [in] */ BSTR bstrStyleName) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetTextHeight2( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [retval][out] */ double *pHeight) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetTextHeight2( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [in] */ double Height) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetRotation( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [retval][out] */ double *pValue) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetRotation( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [in] */ double Value) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetAutoScale2( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [retval][out] */ VARIANT_BOOL *bAutoScale) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetAutoScale2( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [in] */ VARIANT_BOOL bAutoFit) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetScale( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [retval][out] */ double *pScale) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetScale( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [in] */ double scale) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RemoveAllOverrides( 
            /* [in] */ int nRow,
            /* [in] */ int nCol) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetGridLineWeight2( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ AcGridLineType nGridLineType,
            /* [retval][out] */ ACAD_LWEIGHT *plineweight) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetGridLineWeight2( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ AcGridLineType nGridLineType,
            /* [in] */ ACAD_LWEIGHT Lineweight) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetGridLinetype( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ AcGridLineType nGridLineType,
            /* [retval][out] */ LONG_PTR *pacDbObjId) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetGridLinetype( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ AcGridLineType nGridLineType,
            /* [in] */ LONG_PTR idLinetype) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetGridColor2( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ AcGridLineType nGridLineType,
            /* [retval][out] */ IAcadAcCmColor **pColor) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetGridColor2( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ AcGridLineType nGridLineType,
            /* [in] */ IAcadAcCmColor *pColor) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetGridVisibility2( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ AcGridLineType nGridLineType,
            /* [retval][out] */ VARIANT_BOOL *bVisible) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetGridVisibility2( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ AcGridLineType nGridLineType,
            /* [in] */ VARIANT_BOOL bVisible) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetGridDoubleLineSpacing( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ AcGridLineType nGridLineType,
            /* [retval][out] */ double *pValue) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetGridDoubleLineSpacing( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ AcGridLineType nGridLineType,
            /* [in] */ double fSpacing) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_EnableBreak( 
            /* [in] */ VARIANT_BOOL rhs) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetBreakHeight( 
            /* [in] */ int nIndex,
            /* [retval][out] */ double *pHeight) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetBreakHeight( 
            /* [in] */ int nIndex,
            /* [in] */ double Height) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetContentType( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [retval][out] */ AcCellContentType *pType) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetMargin( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ AcCellMargin nMargin,
            /* [retval][out] */ double *pValue) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetMargin( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ AcCellMargin nMargins,
            /* [in] */ double fMargin) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetContentLayout( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [retval][out] */ AcCellContentLayout *pLayout) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetContentLayout( 
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ AcCellContentLayout nLayout) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetOverride( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [retval][out] */ AcCellProperty *pValue) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetOverride( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [in] */ AcCellProperty nProp) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetGridLineStyle( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ AcGridLineType nGridLineType,
            /* [retval][out] */ AcGridLineStyle *pStyle) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetGridLineStyle( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ AcGridLineType nGridLineTypes,
            /* [in] */ AcGridLineStyle nLineStyle) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE InsertRowsAndInherit( 
            /* [in] */ int nIndex,
            /* [in] */ int nInheritFrom,
            /* [in] */ int nNumRows) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE InsertColumnsAndInherit( 
            /* [in] */ int col,
            /* [in] */ int nInheritFrom,
            /* [in] */ int nNumCols) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetHasFormula( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [retval][out] */ VARIANT_BOOL *bValue) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFormula( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [retval][out] */ BSTR *pszFormula) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetFormula( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [in] */ BSTR pszFormula) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IsContentEditable( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [retval][out] */ VARIANT_BOOL *bValue) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IsFormatEditable( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [retval][out] */ VARIANT_BOOL *bValue) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetCellState( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [retval][out] */ AcCellState *pCellState) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetCellState( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ AcCellState nLock) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE EnableMergeAll( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ VARIANT_BOOL bEnable) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IsMergeAllEnabled( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [retval][out] */ VARIANT_BOOL *bValue) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_BreaksEnabled( 
            /* [retval][out] */ VARIANT_BOOL *bEnabled) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_BreaksEnabled( 
            /* [in] */ VARIANT_BOOL bEnabled) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_RepeatTopLabels( 
            /* [retval][out] */ VARIANT_BOOL *bEnabled) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_RepeatTopLabels( 
            /* [in] */ VARIANT_BOOL bEnabled) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_RepeatBottomLabels( 
            /* [retval][out] */ VARIANT_BOOL *bEnabled) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_RepeatBottomLabels( 
            /* [in] */ VARIANT_BOOL bEnabled) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_TableBreakFlowDirection( 
            /* [retval][out] */ AcTableFlowDirection *pDir) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_TableBreakFlowDirection( 
            /* [in] */ AcTableFlowDirection pDir) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_AllowManualPositions( 
            /* [retval][out] */ VARIANT_BOOL *bEnabled) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_AllowManualPositions( 
            /* [in] */ VARIANT_BOOL bEnabled) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_AllowManualHeights( 
            /* [retval][out] */ VARIANT_BOOL *bEnabled) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_AllowManualHeights( 
            /* [in] */ VARIANT_BOOL bEnabled) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_TableBreakHeight( 
            /* [retval][out] */ double *pHeight) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_TableBreakHeight( 
            /* [in] */ double pHeight) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_BreakSpacing( 
            /* [retval][out] */ double *pSpacing) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_BreakSpacing( 
            /* [in] */ double pSpacing) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetColumnName( 
            /* [in] */ int nIndex,
            /* [retval][out] */ BSTR *Name) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetColumnName( 
            /* [in] */ int nIndex,
            /* [in] */ BSTR Name) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetToolTip( 
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ BSTR tip) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct IAcadTableVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAcadTable * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAcadTable * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAcadTable * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IAcadTable * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IAcadTable * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IAcadTable * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IAcadTable * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Handle )( 
            IAcadTable * This,
            /* [retval][out] */ BSTR *Handle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ObjectName )( 
            IAcadTable * This,
            /* [retval][out] */ BSTR *ObjectName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetXData )( 
            IAcadTable * This,
            /* [in] */ BSTR AppName,
            /* [out] */ VARIANT *XDataType,
            /* [out] */ VARIANT *XDataValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetXData )( 
            IAcadTable * This,
            /* [in] */ VARIANT XDataType,
            /* [in] */ VARIANT XDataValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Delete )( 
            IAcadTable * This);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ObjectID )( 
            IAcadTable * This,
            /* [retval][out] */ LONG_PTR *ObjectID);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Application )( 
            IAcadTable * This,
            /* [retval][out] */ IDispatch **ApplicationObject);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Database )( 
            IAcadTable * This,
            /* [retval][out] */ IAcadDatabase **pDatabase);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_HasExtensionDictionary )( 
            IAcadTable * This,
            /* [retval][out] */ VARIANT_BOOL *bHasDictionary);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetExtensionDictionary )( 
            IAcadTable * This,
            /* [retval][out] */ IAcadDictionary **pExtDictionary);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_OwnerID )( 
            IAcadTable * This,
            /* [retval][out] */ LONG_PTR *OwnerID);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Document )( 
            IAcadTable * This,
            /* [retval][out] */ IDispatch **pDocument);
        
        /* [helpstring][hidden][id] */ HRESULT ( STDMETHODCALLTYPE *Erase )( 
            IAcadTable * This);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_TrueColor )( 
            IAcadTable * This,
            /* [retval][out] */ IAcadAcCmColor **pColor);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_TrueColor )( 
            IAcadTable * This,
            /* [in] */ IAcadAcCmColor *pColor);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Layer )( 
            IAcadTable * This,
            /* [retval][out] */ BSTR *Layer);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Layer )( 
            IAcadTable * This,
            /* [in] */ BSTR Layer);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Linetype )( 
            IAcadTable * This,
            /* [retval][out] */ BSTR *Linetype);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Linetype )( 
            IAcadTable * This,
            /* [in] */ BSTR Linetype);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_LinetypeScale )( 
            IAcadTable * This,
            /* [retval][out] */ ACAD_NOUNITS *ltScale);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_LinetypeScale )( 
            IAcadTable * This,
            /* [in] */ ACAD_NOUNITS ltScale);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Visible )( 
            IAcadTable * This,
            /* [retval][out] */ VARIANT_BOOL *bVisible);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Visible )( 
            IAcadTable * This,
            /* [in] */ VARIANT_BOOL bVisible);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ArrayPolar )( 
            IAcadTable * This,
            /* [in] */ int NumberOfObjects,
            /* [in] */ double AngleToFill,
            /* [in] */ VARIANT CenterPoint,
            /* [retval][out] */ VARIANT *pArrayObjs);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ArrayRectangular )( 
            IAcadTable * This,
            /* [in] */ int NumberOfRows,
            /* [in] */ int NumberOfColumns,
            /* [in] */ int NumberOfLevels,
            /* [in] */ double DistBetweenRows,
            /* [in] */ double DistBetweenCols,
            /* [in] */ double DistBetweenLevels,
            /* [retval][out] */ VARIANT *pArrayObjs);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Highlight )( 
            IAcadTable * This,
            /* [in] */ VARIANT_BOOL HighlightFlag);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Copy )( 
            IAcadTable * This,
            /* [retval][out] */ IDispatch **pCopyObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Move )( 
            IAcadTable * This,
            /* [in] */ VARIANT FromPoint,
            /* [in] */ VARIANT ToPoint);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Rotate )( 
            IAcadTable * This,
            /* [in] */ VARIANT BasePoint,
            /* [in] */ double RotationAngle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Rotate3D )( 
            IAcadTable * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [in] */ double RotationAngle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Mirror )( 
            IAcadTable * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [retval][out] */ IDispatch **pMirrorObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Mirror3D )( 
            IAcadTable * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [in] */ VARIANT point3,
            /* [retval][out] */ IDispatch **pMirrorObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ScaleEntity )( 
            IAcadTable * This,
            /* [in] */ VARIANT BasePoint,
            /* [in] */ double ScaleFactor);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *TransformBy )( 
            IAcadTable * This,
            /* [in] */ VARIANT TransformationMatrix);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Update )( 
            IAcadTable * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetBoundingBox )( 
            IAcadTable * This,
            /* [out] */ VARIANT *MinPoint,
            /* [out] */ VARIANT *MaxPoint);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *IntersectWith )( 
            IAcadTable * This,
            /* [in] */ IDispatch *IntersectObject,
            /* [in] */ AcExtendOption option,
            /* [retval][out] */ VARIANT *intPoints);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_PlotStyleName )( 
            IAcadTable * This,
            /* [retval][out] */ BSTR *plotStyle);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_PlotStyleName )( 
            IAcadTable * This,
            /* [in] */ BSTR plotStyle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Lineweight )( 
            IAcadTable * This,
            /* [retval][out] */ ACAD_LWEIGHT *Lineweight);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Lineweight )( 
            IAcadTable * This,
            /* [in] */ ACAD_LWEIGHT Lineweight);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Hyperlinks )( 
            IAcadTable * This,
            /* [retval][out] */ IAcadHyperlinks **Hyperlinks);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Material )( 
            IAcadTable * This,
            /* [retval][out] */ BSTR *Material);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Material )( 
            IAcadTable * This,
            /* [in] */ BSTR Material);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EntityName )( 
            IAcadTable * This,
            /* [retval][out] */ BSTR *EntityName);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EntityType )( 
            IAcadTable * This,
            /* [retval][out] */ long *entType);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_color )( 
            IAcadTable * This,
            /* [retval][out] */ ACAD_COLOR *color);
        
        /* [helpstring][hidden][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_color )( 
            IAcadTable * This,
            /* [in] */ ACAD_COLOR color);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_StyleName )( 
            IAcadTable * This,
            /* [retval][out] */ BSTR *bstrName);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_StyleName )( 
            IAcadTable * This,
            /* [in] */ BSTR bstrName);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Rows )( 
            IAcadTable * This,
            /* [retval][out] */ int *pRows);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Rows )( 
            IAcadTable * This,
            /* [in] */ int pRows);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Columns )( 
            IAcadTable * This,
            /* [retval][out] */ int *pColumns);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Columns )( 
            IAcadTable * This,
            /* [in] */ int pColumns);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_FlowDirection )( 
            IAcadTable * This,
            /* [retval][out] */ AcTableDirection *pFlow);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_FlowDirection )( 
            IAcadTable * This,
            /* [in] */ AcTableDirection pFlow);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Width )( 
            IAcadTable * This,
            /* [retval][out] */ double *pWidth);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Width )( 
            IAcadTable * This,
            /* [in] */ double pWidth);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Height )( 
            IAcadTable * This,
            /* [retval][out] */ double *pHeight);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Height )( 
            IAcadTable * This,
            /* [in] */ double pHeight);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_VertCellMargin )( 
            IAcadTable * This,
            /* [retval][out] */ double *pGap);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_VertCellMargin )( 
            IAcadTable * This,
            /* [in] */ double pGap);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_HorzCellMargin )( 
            IAcadTable * This,
            /* [retval][out] */ double *pGap);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_HorzCellMargin )( 
            IAcadTable * This,
            /* [in] */ double pGap);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_InsertionPoint )( 
            IAcadTable * This,
            /* [retval][out] */ VARIANT *insPoint);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_InsertionPoint )( 
            IAcadTable * This,
            /* [in] */ VARIANT insPoint);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetColumnWidth )( 
            IAcadTable * This,
            /* [in] */ int col,
            /* [retval][out] */ double *pWidth);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetColumnWidth )( 
            IAcadTable * This,
            /* [in] */ int col,
            /* [in] */ double Width);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ColumnWidth )( 
            IAcadTable * This,
            /* [in] */ double rhs);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetRowHeight )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [retval][out] */ double *pHeight);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetRowHeight )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ double Height);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_RowHeight )( 
            IAcadTable * This,
            /* [in] */ double rhs);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetMinimumColumnWidth )( 
            IAcadTable * This,
            /* [in] */ int col,
            /* [retval][out] */ double *pWidth);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetMinimumRowHeight )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [retval][out] */ double *pHeight);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_MinimumTableWidth )( 
            IAcadTable * This,
            /* [retval][out] */ double *pWidth);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_MinimumTableHeight )( 
            IAcadTable * This,
            /* [retval][out] */ double *pHeight);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Direction )( 
            IAcadTable * This,
            /* [retval][out] */ VARIANT *DirectionVector);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Direction )( 
            IAcadTable * This,
            /* [in] */ VARIANT DirectionVector);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_TitleSuppressed )( 
            IAcadTable * This,
            /* [retval][out] */ VARIANT_BOOL *bValue);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_TitleSuppressed )( 
            IAcadTable * This,
            /* [in] */ VARIANT_BOOL bValue);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_HeaderSuppressed )( 
            IAcadTable * This,
            /* [retval][out] */ VARIANT_BOOL *bValue);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_HeaderSuppressed )( 
            IAcadTable * This,
            /* [in] */ VARIANT_BOOL bValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetAlignment )( 
            IAcadTable * This,
            /* [in] */ AcRowType rowType,
            /* [retval][out] */ AcCellAlignment *pCellAlignment);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetAlignment )( 
            IAcadTable * This,
            /* [in] */ int rowTypes,
            /* [in] */ AcCellAlignment cellAlignment);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetBackgroundColorNone )( 
            IAcadTable * This,
            /* [in] */ AcRowType rowType,
            /* [retval][out] */ VARIANT_BOOL *bValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetBackgroundColorNone )( 
            IAcadTable * This,
            /* [in] */ int rowTypes,
            /* [in] */ VARIANT_BOOL bValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetBackgroundColor )( 
            IAcadTable * This,
            /* [in] */ AcRowType rowType,
            /* [retval][out] */ IAcadAcCmColor **pColor);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetBackgroundColor )( 
            IAcadTable * This,
            /* [in] */ int rowTypes,
            /* [in] */ IAcadAcCmColor *pColor);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetContentColor )( 
            IAcadTable * This,
            /* [in] */ AcRowType rowType,
            /* [retval][out] */ IAcadAcCmColor **pColor);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetContentColor )( 
            IAcadTable * This,
            /* [in] */ int rowTypes,
            /* [in] */ IAcadAcCmColor *pColor);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetTextStyle )( 
            IAcadTable * This,
            /* [in] */ AcRowType rowType,
            /* [retval][out] */ BSTR *bstrName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetTextStyle )( 
            IAcadTable * This,
            /* [in] */ int rowTypes,
            /* [in] */ BSTR bstrName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetTextHeight )( 
            IAcadTable * This,
            /* [in] */ AcRowType rowType,
            /* [retval][out] */ double *pTextHeight);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetTextHeight )( 
            IAcadTable * This,
            /* [in] */ int rowTypes,
            /* [in] */ double TextHeight);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetGridLineWeight )( 
            IAcadTable * This,
            /* [in] */ AcGridLineType gridLineType,
            /* [in] */ AcRowType rowType,
            /* [retval][out] */ ACAD_LWEIGHT *Lineweight);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetGridLineWeight )( 
            IAcadTable * This,
            /* [in] */ int gridLineTypes,
            /* [in] */ int rowTypes,
            /* [in] */ ACAD_LWEIGHT Lineweight);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetGridColor )( 
            IAcadTable * This,
            /* [in] */ AcGridLineType gridLineType,
            /* [in] */ AcRowType rowType,
            /* [retval][out] */ IAcadAcCmColor **pColor);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetGridColor )( 
            IAcadTable * This,
            /* [in] */ int gridLineTypes,
            /* [in] */ int rowTypes,
            /* [in] */ IAcadAcCmColor *pColor);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetGridVisibility )( 
            IAcadTable * This,
            /* [in] */ AcGridLineType gridLineType,
            /* [in] */ AcRowType rowType,
            /* [retval][out] */ VARIANT_BOOL *bValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetGridVisibility )( 
            IAcadTable * This,
            /* [in] */ int gridLineTypes,
            /* [in] */ int rowTypes,
            /* [in] */ VARIANT_BOOL bValue);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_TableStyleOverrides )( 
            IAcadTable * This,
            /* [retval][out] */ VARIANT *pIntArray);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ClearTableStyleOverrides )( 
            IAcadTable * This,
            /* [in] */ int flag);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetCellType )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [retval][out] */ AcCellType *pCellType);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetCellType )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ AcCellType CellType);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetCellExtents )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ VARIANT_BOOL bOuterCell,
            /* [retval][out] */ VARIANT *pPts);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetAttachmentPoint )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [retval][out] */ VARIANT *pAttachmentPoint);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetCellAlignment )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [retval][out] */ AcCellAlignment *pCellAlignment);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetCellAlignment )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ AcCellAlignment cellAlignment);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetCellBackgroundColorNone )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [retval][out] */ VARIANT_BOOL *bValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetCellBackgroundColorNone )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ VARIANT_BOOL bValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetCellBackgroundColor )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [retval][out] */ IAcadAcCmColor **pColor);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetCellBackgroundColor )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ IAcadAcCmColor *pColor);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetCellContentColor )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [retval][out] */ IAcadAcCmColor **pColor);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetCellContentColor )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ IAcadAcCmColor *pColor);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetCellStyleOverrides )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [retval][out] */ VARIANT *pIntArray);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DeleteCellContent )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetRowType )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [retval][out] */ AcRowType *pRowType);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetText )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [retval][out] */ BSTR *pStr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetText )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ BSTR pStr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetCellTextStyle )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [retval][out] */ BSTR *bstrName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetCellTextStyle )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ BSTR bstrName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetCellTextHeight )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [retval][out] */ double *pTextHeight);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetCellTextHeight )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ double TextHeight);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetTextRotation )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [retval][out] */ AcRotationAngle *TextRotation);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetTextRotation )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ AcRotationAngle TextRotation);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetAutoScale )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [retval][out] */ VARIANT_BOOL *bValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetAutoScale )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ VARIANT_BOOL bValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetBlockTableRecordId )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [retval][out] */ LONG_PTR *blkId);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetBlockTableRecordId )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ LONG_PTR blkId,
            /* [in] */ VARIANT_BOOL bAutoFit);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetBlockScale )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [retval][out] */ double *blkScale);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetBlockScale )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ double blkScale);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetBlockRotation )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [retval][out] */ double *blkRotation);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetBlockRotation )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ double blkRotation);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetBlockAttributeValue )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ LONG_PTR attdefId,
            /* [retval][out] */ BSTR *bstrValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetBlockAttributeValue )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ LONG_PTR attdefId,
            /* [in] */ BSTR bstrValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetCellGridLineWeight )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ AcCellEdgeMask edge,
            /* [retval][out] */ ACAD_LWEIGHT *plineweight);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetCellGridLineWeight )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ int edges,
            /* [in] */ ACAD_LWEIGHT Lineweight);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetCellGridColor )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ AcCellEdgeMask edge,
            /* [retval][out] */ IAcadAcCmColor **pColor);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetCellGridColor )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ int edges,
            /* [in] */ IAcadAcCmColor *pColor);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetCellGridVisibility )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ AcCellEdgeMask edge,
            /* [retval][out] */ VARIANT_BOOL *bValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetCellGridVisibility )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ int edges,
            /* [in] */ VARIANT_BOOL bValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *InsertColumns )( 
            IAcadTable * This,
            /* [in] */ int col,
            /* [in] */ double Width,
            /* [in] */ int cols);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DeleteColumns )( 
            IAcadTable * This,
            /* [in] */ int col,
            /* [in] */ int cols);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *InsertRows )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ double Height,
            /* [in] */ int Rows);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DeleteRows )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int Rows);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *MergeCells )( 
            IAcadTable * This,
            /* [in] */ int minRow,
            /* [in] */ int maxRow,
            /* [in] */ int minCol,
            /* [in] */ int maxCol);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *UnmergeCells )( 
            IAcadTable * This,
            /* [in] */ int minRow,
            /* [in] */ int maxRow,
            /* [in] */ int minCol,
            /* [in] */ int maxCol);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *IsMergedCell )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [out] */ int *minRow,
            /* [out] */ int *maxRow,
            /* [out] */ int *minCol,
            /* [out] */ int *maxCol,
            /* [retval][out] */ VARIANT_BOOL *pbValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetFieldId )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [retval][out] */ LONG_PTR *fieldId);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetFieldId )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ LONG_PTR fieldId);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GenerateLayout )( 
            IAcadTable * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RecomputeTableBlock )( 
            IAcadTable * This,
            /* [in] */ VARIANT_BOOL bForceUpdate);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *HitTest )( 
            IAcadTable * This,
            /* [in] */ VARIANT wpt,
            /* [in] */ VARIANT wviewVec,
            /* [out] */ int *resultRowIndex,
            /* [out] */ int *resultColumnIndex,
            /* [retval][out] */ VARIANT_BOOL *bReturn);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Select )( 
            IAcadTable * This,
            /* [in] */ VARIANT wpt,
            /* [in] */ VARIANT wvwVec,
            /* [in] */ VARIANT wvwxVec,
            /* [in] */ double wxaper,
            /* [in] */ double wyaper,
            /* [in] */ VARIANT_BOOL allowOutside,
            /* [out] */ int *resultRowIndex,
            /* [out] */ int *resultColumnIndex,
            /* [retval][out] */ VARIANT *pPaths);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SelectSubRegion )( 
            IAcadTable * This,
            /* [in] */ VARIANT wpt1,
            /* [in] */ VARIANT wpt2,
            /* [in] */ VARIANT wvwVec,
            /* [in] */ VARIANT wvwxVec,
            /* [in] */ AcSelectType seltype,
            /* [in] */ VARIANT_BOOL bIncludeCurrentSelection,
            /* [out] */ int *rowMin,
            /* [out] */ int *rowMax,
            /* [out] */ int *colMin,
            /* [out] */ int *colMax,
            /* [retval][out] */ VARIANT *pPaths);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ReselectSubRegion )( 
            IAcadTable * This,
            /* [retval][out] */ VARIANT *pPaths);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetSubSelection )( 
            IAcadTable * This,
            /* [out] */ int *rowMin,
            /* [out] */ int *rowMax,
            /* [out] */ int *colMin,
            /* [out] */ int *colMax);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetSubSelection )( 
            IAcadTable * This,
            /* [in] */ int rowMin,
            /* [in] */ int rowMax,
            /* [in] */ int colMin,
            /* [in] */ int colMax);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ClearSubSelection )( 
            IAcadTable * This);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_HasSubSelection )( 
            IAcadTable * This,
            /* [retval][out] */ VARIANT_BOOL *pbValue);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_RegenerateTableSuppressed )( 
            IAcadTable * This,
            /* [retval][out] */ VARIANT_BOOL *bValue);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_RegenerateTableSuppressed )( 
            IAcadTable * This,
            /* [in] */ VARIANT_BOOL bValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetDataType )( 
            IAcadTable * This,
            /* [in] */ AcRowType rowType,
            /* [out] */ AcValueDataType *pDataType,
            /* [out] */ AcValueUnitType *pUnitType);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetDataType )( 
            IAcadTable * This,
            /* [in] */ int rowTypes,
            /* [in] */ AcValueDataType dataType,
            /* [in] */ AcValueUnitType unitType);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetFormat )( 
            IAcadTable * This,
            /* [in] */ AcRowType rowType,
            /* [retval][out] */ BSTR *pFormat);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetFormat )( 
            IAcadTable * This,
            /* [in] */ int rowTypes,
            BSTR pFormat);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *FormatValue )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            AcFormatOption nOption,
            BSTR *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetCellDataType )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [out] */ AcValueDataType *pDataType,
            /* [out] */ AcValueUnitType *pUnitType);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetCellDataType )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            AcValueDataType dataType,
            AcValueUnitType unitType);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetCellFormat )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [retval][out] */ BSTR *pFormat);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetCellFormat )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            BSTR pFormat);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetCellValue )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [retval][out] */ VARIANT *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetCellValue )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            VARIANT val);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetCellValueFromText )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ BSTR val,
            /* [in] */ AcParseOption nOption);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ResetCellValue )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *IsEmpty )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [retval][out] */ VARIANT_BOOL *bValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CreateContent )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nIndex,
            /* [retval][out] */ int *pInt);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *MoveContent )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nFromIndex,
            /* [in] */ int nToIndex);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DeleteContent )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetValue )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [retval][out] */ VARIANT *pAcValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetValue )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [in] */ VARIANT acValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetValueFromText )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [in] */ BSTR szText,
            /* [in] */ AcParseOption nOption);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetDataFormat )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [retval][out] */ BSTR *pValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetDataFormat )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [in] */ BSTR szFormat);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetTextString )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [retval][out] */ BSTR *pTextString);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetTextString )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [in] */ BSTR Text);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetFieldId2 )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [retval][out] */ LONG_PTR *pAcDbObjectId);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetFieldId2 )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [in] */ LONG_PTR acDbObjectId,
            /* [in] */ AcCellOption nflag);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetBlockTableRecordId2 )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [retval][out] */ LONG_PTR *pAcDbObjectId);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetBlockTableRecordId2 )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [in] */ LONG_PTR blkId,
            /* [in] */ VARIANT_BOOL autoFit);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetBlockAttributeValue2 )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [in] */ LONG_PTR blkId,
            /* [retval][out] */ BSTR *Value);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetBlockAttributeValue2 )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [in] */ LONG_PTR blkId,
            /* [in] */ BSTR Value);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetCustomData )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ BSTR szKey,
            /* [out] */ VARIANT *pData);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetCustomData )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ BSTR szKey,
            /* [in] */ VARIANT data);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetCellStyle )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [retval][out] */ BSTR *pCellStyle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetCellStyle )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ BSTR szCellStyle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetContentColor2 )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [retval][out] */ IAcadAcCmColor **pColor);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetContentColor2 )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [in] */ IAcadAcCmColor *pColor);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetDataType2 )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [out] */ AcValueDataType *pDataType,
            /* [out] */ AcValueUnitType *pUnitType);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetDataType2 )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [in] */ AcValueDataType dataType,
            /* [in] */ AcValueUnitType unitType);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetTextStyle2 )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [retval][out] */ BSTR *pbstrStyleName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetTextStyle2 )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [in] */ BSTR bstrStyleName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetTextHeight2 )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [retval][out] */ double *pHeight);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetTextHeight2 )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [in] */ double Height);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetRotation )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [retval][out] */ double *pValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetRotation )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [in] */ double Value);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetAutoScale2 )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [retval][out] */ VARIANT_BOOL *bAutoScale);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetAutoScale2 )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [in] */ VARIANT_BOOL bAutoFit);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetScale )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [retval][out] */ double *pScale);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetScale )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [in] */ double scale);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RemoveAllOverrides )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetGridLineWeight2 )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ AcGridLineType nGridLineType,
            /* [retval][out] */ ACAD_LWEIGHT *plineweight);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetGridLineWeight2 )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ AcGridLineType nGridLineType,
            /* [in] */ ACAD_LWEIGHT Lineweight);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetGridLinetype )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ AcGridLineType nGridLineType,
            /* [retval][out] */ LONG_PTR *pacDbObjId);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetGridLinetype )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ AcGridLineType nGridLineType,
            /* [in] */ LONG_PTR idLinetype);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetGridColor2 )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ AcGridLineType nGridLineType,
            /* [retval][out] */ IAcadAcCmColor **pColor);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetGridColor2 )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ AcGridLineType nGridLineType,
            /* [in] */ IAcadAcCmColor *pColor);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetGridVisibility2 )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ AcGridLineType nGridLineType,
            /* [retval][out] */ VARIANT_BOOL *bVisible);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetGridVisibility2 )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ AcGridLineType nGridLineType,
            /* [in] */ VARIANT_BOOL bVisible);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetGridDoubleLineSpacing )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ AcGridLineType nGridLineType,
            /* [retval][out] */ double *pValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetGridDoubleLineSpacing )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ AcGridLineType nGridLineType,
            /* [in] */ double fSpacing);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_EnableBreak )( 
            IAcadTable * This,
            /* [in] */ VARIANT_BOOL rhs);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetBreakHeight )( 
            IAcadTable * This,
            /* [in] */ int nIndex,
            /* [retval][out] */ double *pHeight);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetBreakHeight )( 
            IAcadTable * This,
            /* [in] */ int nIndex,
            /* [in] */ double Height);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetContentType )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [retval][out] */ AcCellContentType *pType);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetMargin )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ AcCellMargin nMargin,
            /* [retval][out] */ double *pValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetMargin )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ AcCellMargin nMargins,
            /* [in] */ double fMargin);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetContentLayout )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [retval][out] */ AcCellContentLayout *pLayout);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetContentLayout )( 
            IAcadTable * This,
            /* [in] */ int row,
            /* [in] */ int col,
            /* [in] */ AcCellContentLayout nLayout);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetOverride )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [retval][out] */ AcCellProperty *pValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetOverride )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [in] */ AcCellProperty nProp);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetGridLineStyle )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ AcGridLineType nGridLineType,
            /* [retval][out] */ AcGridLineStyle *pStyle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetGridLineStyle )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ AcGridLineType nGridLineTypes,
            /* [in] */ AcGridLineStyle nLineStyle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *InsertRowsAndInherit )( 
            IAcadTable * This,
            /* [in] */ int nIndex,
            /* [in] */ int nInheritFrom,
            /* [in] */ int nNumRows);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *InsertColumnsAndInherit )( 
            IAcadTable * This,
            /* [in] */ int col,
            /* [in] */ int nInheritFrom,
            /* [in] */ int nNumCols);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetHasFormula )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [retval][out] */ VARIANT_BOOL *bValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetFormula )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [retval][out] */ BSTR *pszFormula);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetFormula )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ int nContent,
            /* [in] */ BSTR pszFormula);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *IsContentEditable )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [retval][out] */ VARIANT_BOOL *bValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *IsFormatEditable )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [retval][out] */ VARIANT_BOOL *bValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetCellState )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [retval][out] */ AcCellState *pCellState);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetCellState )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ AcCellState nLock);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *EnableMergeAll )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ VARIANT_BOOL bEnable);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *IsMergeAllEnabled )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [retval][out] */ VARIANT_BOOL *bValue);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_BreaksEnabled )( 
            IAcadTable * This,
            /* [retval][out] */ VARIANT_BOOL *bEnabled);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_BreaksEnabled )( 
            IAcadTable * This,
            /* [in] */ VARIANT_BOOL bEnabled);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_RepeatTopLabels )( 
            IAcadTable * This,
            /* [retval][out] */ VARIANT_BOOL *bEnabled);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_RepeatTopLabels )( 
            IAcadTable * This,
            /* [in] */ VARIANT_BOOL bEnabled);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_RepeatBottomLabels )( 
            IAcadTable * This,
            /* [retval][out] */ VARIANT_BOOL *bEnabled);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_RepeatBottomLabels )( 
            IAcadTable * This,
            /* [in] */ VARIANT_BOOL bEnabled);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_TableBreakFlowDirection )( 
            IAcadTable * This,
            /* [retval][out] */ AcTableFlowDirection *pDir);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_TableBreakFlowDirection )( 
            IAcadTable * This,
            /* [in] */ AcTableFlowDirection pDir);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AllowManualPositions )( 
            IAcadTable * This,
            /* [retval][out] */ VARIANT_BOOL *bEnabled);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AllowManualPositions )( 
            IAcadTable * This,
            /* [in] */ VARIANT_BOOL bEnabled);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AllowManualHeights )( 
            IAcadTable * This,
            /* [retval][out] */ VARIANT_BOOL *bEnabled);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AllowManualHeights )( 
            IAcadTable * This,
            /* [in] */ VARIANT_BOOL bEnabled);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_TableBreakHeight )( 
            IAcadTable * This,
            /* [retval][out] */ double *pHeight);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_TableBreakHeight )( 
            IAcadTable * This,
            /* [in] */ double pHeight);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_BreakSpacing )( 
            IAcadTable * This,
            /* [retval][out] */ double *pSpacing);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_BreakSpacing )( 
            IAcadTable * This,
            /* [in] */ double pSpacing);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetColumnName )( 
            IAcadTable * This,
            /* [in] */ int nIndex,
            /* [retval][out] */ BSTR *Name);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetColumnName )( 
            IAcadTable * This,
            /* [in] */ int nIndex,
            /* [in] */ BSTR Name);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetToolTip )( 
            IAcadTable * This,
            /* [in] */ int nRow,
            /* [in] */ int nCol,
            /* [in] */ BSTR tip);
        
        END_INTERFACE
    } IAcadTableVtbl;

    interface IAcadTable
    {
        CONST_VTBL struct IAcadTableVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAcadTable_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAcadTable_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAcadTable_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAcadTable_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IAcadTable_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IAcadTable_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IAcadTable_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IAcadTable_get_Handle(This,Handle)	\
    ( (This)->lpVtbl -> get_Handle(This,Handle) ) 

#define IAcadTable_get_ObjectName(This,ObjectName)	\
    ( (This)->lpVtbl -> get_ObjectName(This,ObjectName) ) 

#define IAcadTable_GetXData(This,AppName,XDataType,XDataValue)	\
    ( (This)->lpVtbl -> GetXData(This,AppName,XDataType,XDataValue) ) 

#define IAcadTable_SetXData(This,XDataType,XDataValue)	\
    ( (This)->lpVtbl -> SetXData(This,XDataType,XDataValue) ) 

#define IAcadTable_Delete(This)	\
    ( (This)->lpVtbl -> Delete(This) ) 

#define IAcadTable_get_ObjectID(This,ObjectID)	\
    ( (This)->lpVtbl -> get_ObjectID(This,ObjectID) ) 

#define IAcadTable_get_Application(This,ApplicationObject)	\
    ( (This)->lpVtbl -> get_Application(This,ApplicationObject) ) 

#define IAcadTable_get_Database(This,pDatabase)	\
    ( (This)->lpVtbl -> get_Database(This,pDatabase) ) 

#define IAcadTable_get_HasExtensionDictionary(This,bHasDictionary)	\
    ( (This)->lpVtbl -> get_HasExtensionDictionary(This,bHasDictionary) ) 

#define IAcadTable_GetExtensionDictionary(This,pExtDictionary)	\
    ( (This)->lpVtbl -> GetExtensionDictionary(This,pExtDictionary) ) 

#define IAcadTable_get_OwnerID(This,OwnerID)	\
    ( (This)->lpVtbl -> get_OwnerID(This,OwnerID) ) 

#define IAcadTable_get_Document(This,pDocument)	\
    ( (This)->lpVtbl -> get_Document(This,pDocument) ) 

#define IAcadTable_Erase(This)	\
    ( (This)->lpVtbl -> Erase(This) ) 


#define IAcadTable_get_TrueColor(This,pColor)	\
    ( (This)->lpVtbl -> get_TrueColor(This,pColor) ) 

#define IAcadTable_put_TrueColor(This,pColor)	\
    ( (This)->lpVtbl -> put_TrueColor(This,pColor) ) 

#define IAcadTable_get_Layer(This,Layer)	\
    ( (This)->lpVtbl -> get_Layer(This,Layer) ) 

#define IAcadTable_put_Layer(This,Layer)	\
    ( (This)->lpVtbl -> put_Layer(This,Layer) ) 

#define IAcadTable_get_Linetype(This,Linetype)	\
    ( (This)->lpVtbl -> get_Linetype(This,Linetype) ) 

#define IAcadTable_put_Linetype(This,Linetype)	\
    ( (This)->lpVtbl -> put_Linetype(This,Linetype) ) 

#define IAcadTable_get_LinetypeScale(This,ltScale)	\
    ( (This)->lpVtbl -> get_LinetypeScale(This,ltScale) ) 

#define IAcadTable_put_LinetypeScale(This,ltScale)	\
    ( (This)->lpVtbl -> put_LinetypeScale(This,ltScale) ) 

#define IAcadTable_get_Visible(This,bVisible)	\
    ( (This)->lpVtbl -> get_Visible(This,bVisible) ) 

#define IAcadTable_put_Visible(This,bVisible)	\
    ( (This)->lpVtbl -> put_Visible(This,bVisible) ) 

#define IAcadTable_ArrayPolar(This,NumberOfObjects,AngleToFill,CenterPoint,pArrayObjs)	\
    ( (This)->lpVtbl -> ArrayPolar(This,NumberOfObjects,AngleToFill,CenterPoint,pArrayObjs) ) 

#define IAcadTable_ArrayRectangular(This,NumberOfRows,NumberOfColumns,NumberOfLevels,DistBetweenRows,DistBetweenCols,DistBetweenLevels,pArrayObjs)	\
    ( (This)->lpVtbl -> ArrayRectangular(This,NumberOfRows,NumberOfColumns,NumberOfLevels,DistBetweenRows,DistBetweenCols,DistBetweenLevels,pArrayObjs) ) 

#define IAcadTable_Highlight(This,HighlightFlag)	\
    ( (This)->lpVtbl -> Highlight(This,HighlightFlag) ) 

#define IAcadTable_Copy(This,pCopyObj)	\
    ( (This)->lpVtbl -> Copy(This,pCopyObj) ) 

#define IAcadTable_Move(This,FromPoint,ToPoint)	\
    ( (This)->lpVtbl -> Move(This,FromPoint,ToPoint) ) 

#define IAcadTable_Rotate(This,BasePoint,RotationAngle)	\
    ( (This)->lpVtbl -> Rotate(This,BasePoint,RotationAngle) ) 

#define IAcadTable_Rotate3D(This,Point1,Point2,RotationAngle)	\
    ( (This)->lpVtbl -> Rotate3D(This,Point1,Point2,RotationAngle) ) 

#define IAcadTable_Mirror(This,Point1,Point2,pMirrorObj)	\
    ( (This)->lpVtbl -> Mirror(This,Point1,Point2,pMirrorObj) ) 

#define IAcadTable_Mirror3D(This,Point1,Point2,point3,pMirrorObj)	\
    ( (This)->lpVtbl -> Mirror3D(This,Point1,Point2,point3,pMirrorObj) ) 

#define IAcadTable_ScaleEntity(This,BasePoint,ScaleFactor)	\
    ( (This)->lpVtbl -> ScaleEntity(This,BasePoint,ScaleFactor) ) 

#define IAcadTable_TransformBy(This,TransformationMatrix)	\
    ( (This)->lpVtbl -> TransformBy(This,TransformationMatrix) ) 

#define IAcadTable_Update(This)	\
    ( (This)->lpVtbl -> Update(This) ) 

#define IAcadTable_GetBoundingBox(This,MinPoint,MaxPoint)	\
    ( (This)->lpVtbl -> GetBoundingBox(This,MinPoint,MaxPoint) ) 

#define IAcadTable_IntersectWith(This,IntersectObject,option,intPoints)	\
    ( (This)->lpVtbl -> IntersectWith(This,IntersectObject,option,intPoints) ) 

#define IAcadTable_get_PlotStyleName(This,plotStyle)	\
    ( (This)->lpVtbl -> get_PlotStyleName(This,plotStyle) ) 

#define IAcadTable_put_PlotStyleName(This,plotStyle)	\
    ( (This)->lpVtbl -> put_PlotStyleName(This,plotStyle) ) 

#define IAcadTable_get_Lineweight(This,Lineweight)	\
    ( (This)->lpVtbl -> get_Lineweight(This,Lineweight) ) 

#define IAcadTable_put_Lineweight(This,Lineweight)	\
    ( (This)->lpVtbl -> put_Lineweight(This,Lineweight) ) 

#define IAcadTable_get_Hyperlinks(This,Hyperlinks)	\
    ( (This)->lpVtbl -> get_Hyperlinks(This,Hyperlinks) ) 

#define IAcadTable_get_Material(This,Material)	\
    ( (This)->lpVtbl -> get_Material(This,Material) ) 

#define IAcadTable_put_Material(This,Material)	\
    ( (This)->lpVtbl -> put_Material(This,Material) ) 

#define IAcadTable_get_EntityName(This,EntityName)	\
    ( (This)->lpVtbl -> get_EntityName(This,EntityName) ) 

#define IAcadTable_get_EntityType(This,entType)	\
    ( (This)->lpVtbl -> get_EntityType(This,entType) ) 

#define IAcadTable_get_color(This,color)	\
    ( (This)->lpVtbl -> get_color(This,color) ) 

#define IAcadTable_put_color(This,color)	\
    ( (This)->lpVtbl -> put_color(This,color) ) 


#define IAcadTable_get_StyleName(This,bstrName)	\
    ( (This)->lpVtbl -> get_StyleName(This,bstrName) ) 

#define IAcadTable_put_StyleName(This,bstrName)	\
    ( (This)->lpVtbl -> put_StyleName(This,bstrName) ) 

#define IAcadTable_get_Rows(This,pRows)	\
    ( (This)->lpVtbl -> get_Rows(This,pRows) ) 

#define IAcadTable_put_Rows(This,pRows)	\
    ( (This)->lpVtbl -> put_Rows(This,pRows) ) 

#define IAcadTable_get_Columns(This,pColumns)	\
    ( (This)->lpVtbl -> get_Columns(This,pColumns) ) 

#define IAcadTable_put_Columns(This,pColumns)	\
    ( (This)->lpVtbl -> put_Columns(This,pColumns) ) 

#define IAcadTable_get_FlowDirection(This,pFlow)	\
    ( (This)->lpVtbl -> get_FlowDirection(This,pFlow) ) 

#define IAcadTable_put_FlowDirection(This,pFlow)	\
    ( (This)->lpVtbl -> put_FlowDirection(This,pFlow) ) 

#define IAcadTable_get_Width(This,pWidth)	\
    ( (This)->lpVtbl -> get_Width(This,pWidth) ) 

#define IAcadTable_put_Width(This,pWidth)	\
    ( (This)->lpVtbl -> put_Width(This,pWidth) ) 

#define IAcadTable_get_Height(This,pHeight)	\
    ( (This)->lpVtbl -> get_Height(This,pHeight) ) 

#define IAcadTable_put_Height(This,pHeight)	\
    ( (This)->lpVtbl -> put_Height(This,pHeight) ) 

#define IAcadTable_get_VertCellMargin(This,pGap)	\
    ( (This)->lpVtbl -> get_VertCellMargin(This,pGap) ) 

#define IAcadTable_put_VertCellMargin(This,pGap)	\
    ( (This)->lpVtbl -> put_VertCellMargin(This,pGap) ) 

#define IAcadTable_get_HorzCellMargin(This,pGap)	\
    ( (This)->lpVtbl -> get_HorzCellMargin(This,pGap) ) 

#define IAcadTable_put_HorzCellMargin(This,pGap)	\
    ( (This)->lpVtbl -> put_HorzCellMargin(This,pGap) ) 

#define IAcadTable_get_InsertionPoint(This,insPoint)	\
    ( (This)->lpVtbl -> get_InsertionPoint(This,insPoint) ) 

#define IAcadTable_put_InsertionPoint(This,insPoint)	\
    ( (This)->lpVtbl -> put_InsertionPoint(This,insPoint) ) 

#define IAcadTable_GetColumnWidth(This,col,pWidth)	\
    ( (This)->lpVtbl -> GetColumnWidth(This,col,pWidth) ) 

#define IAcadTable_SetColumnWidth(This,col,Width)	\
    ( (This)->lpVtbl -> SetColumnWidth(This,col,Width) ) 

#define IAcadTable_put_ColumnWidth(This,rhs)	\
    ( (This)->lpVtbl -> put_ColumnWidth(This,rhs) ) 

#define IAcadTable_GetRowHeight(This,row,pHeight)	\
    ( (This)->lpVtbl -> GetRowHeight(This,row,pHeight) ) 

#define IAcadTable_SetRowHeight(This,row,Height)	\
    ( (This)->lpVtbl -> SetRowHeight(This,row,Height) ) 

#define IAcadTable_put_RowHeight(This,rhs)	\
    ( (This)->lpVtbl -> put_RowHeight(This,rhs) ) 

#define IAcadTable_GetMinimumColumnWidth(This,col,pWidth)	\
    ( (This)->lpVtbl -> GetMinimumColumnWidth(This,col,pWidth) ) 

#define IAcadTable_GetMinimumRowHeight(This,row,pHeight)	\
    ( (This)->lpVtbl -> GetMinimumRowHeight(This,row,pHeight) ) 

#define IAcadTable_get_MinimumTableWidth(This,pWidth)	\
    ( (This)->lpVtbl -> get_MinimumTableWidth(This,pWidth) ) 

#define IAcadTable_get_MinimumTableHeight(This,pHeight)	\
    ( (This)->lpVtbl -> get_MinimumTableHeight(This,pHeight) ) 

#define IAcadTable_get_Direction(This,DirectionVector)	\
    ( (This)->lpVtbl -> get_Direction(This,DirectionVector) ) 

#define IAcadTable_put_Direction(This,DirectionVector)	\
    ( (This)->lpVtbl -> put_Direction(This,DirectionVector) ) 

#define IAcadTable_get_TitleSuppressed(This,bValue)	\
    ( (This)->lpVtbl -> get_TitleSuppressed(This,bValue) ) 

#define IAcadTable_put_TitleSuppressed(This,bValue)	\
    ( (This)->lpVtbl -> put_TitleSuppressed(This,bValue) ) 

#define IAcadTable_get_HeaderSuppressed(This,bValue)	\
    ( (This)->lpVtbl -> get_HeaderSuppressed(This,bValue) ) 

#define IAcadTable_put_HeaderSuppressed(This,bValue)	\
    ( (This)->lpVtbl -> put_HeaderSuppressed(This,bValue) ) 

#define IAcadTable_GetAlignment(This,rowType,pCellAlignment)	\
    ( (This)->lpVtbl -> GetAlignment(This,rowType,pCellAlignment) ) 

#define IAcadTable_SetAlignment(This,rowTypes,cellAlignment)	\
    ( (This)->lpVtbl -> SetAlignment(This,rowTypes,cellAlignment) ) 

#define IAcadTable_GetBackgroundColorNone(This,rowType,bValue)	\
    ( (This)->lpVtbl -> GetBackgroundColorNone(This,rowType,bValue) ) 

#define IAcadTable_SetBackgroundColorNone(This,rowTypes,bValue)	\
    ( (This)->lpVtbl -> SetBackgroundColorNone(This,rowTypes,bValue) ) 

#define IAcadTable_GetBackgroundColor(This,rowType,pColor)	\
    ( (This)->lpVtbl -> GetBackgroundColor(This,rowType,pColor) ) 

#define IAcadTable_SetBackgroundColor(This,rowTypes,pColor)	\
    ( (This)->lpVtbl -> SetBackgroundColor(This,rowTypes,pColor) ) 

#define IAcadTable_GetContentColor(This,rowType,pColor)	\
    ( (This)->lpVtbl -> GetContentColor(This,rowType,pColor) ) 

#define IAcadTable_SetContentColor(This,rowTypes,pColor)	\
    ( (This)->lpVtbl -> SetContentColor(This,rowTypes,pColor) ) 

#define IAcadTable_GetTextStyle(This,rowType,bstrName)	\
    ( (This)->lpVtbl -> GetTextStyle(This,rowType,bstrName) ) 

#define IAcadTable_SetTextStyle(This,rowTypes,bstrName)	\
    ( (This)->lpVtbl -> SetTextStyle(This,rowTypes,bstrName) ) 

#define IAcadTable_GetTextHeight(This,rowType,pTextHeight)	\
    ( (This)->lpVtbl -> GetTextHeight(This,rowType,pTextHeight) ) 

#define IAcadTable_SetTextHeight(This,rowTypes,TextHeight)	\
    ( (This)->lpVtbl -> SetTextHeight(This,rowTypes,TextHeight) ) 

#define IAcadTable_GetGridLineWeight(This,gridLineType,rowType,Lineweight)	\
    ( (This)->lpVtbl -> GetGridLineWeight(This,gridLineType,rowType,Lineweight) ) 

#define IAcadTable_SetGridLineWeight(This,gridLineTypes,rowTypes,Lineweight)	\
    ( (This)->lpVtbl -> SetGridLineWeight(This,gridLineTypes,rowTypes,Lineweight) ) 

#define IAcadTable_GetGridColor(This,gridLineType,rowType,pColor)	\
    ( (This)->lpVtbl -> GetGridColor(This,gridLineType,rowType,pColor) ) 

#define IAcadTable_SetGridColor(This,gridLineTypes,rowTypes,pColor)	\
    ( (This)->lpVtbl -> SetGridColor(This,gridLineTypes,rowTypes,pColor) ) 

#define IAcadTable_GetGridVisibility(This,gridLineType,rowType,bValue)	\
    ( (This)->lpVtbl -> GetGridVisibility(This,gridLineType,rowType,bValue) ) 

#define IAcadTable_SetGridVisibility(This,gridLineTypes,rowTypes,bValue)	\
    ( (This)->lpVtbl -> SetGridVisibility(This,gridLineTypes,rowTypes,bValue) ) 

#define IAcadTable_get_TableStyleOverrides(This,pIntArray)	\
    ( (This)->lpVtbl -> get_TableStyleOverrides(This,pIntArray) ) 

#define IAcadTable_ClearTableStyleOverrides(This,flag)	\
    ( (This)->lpVtbl -> ClearTableStyleOverrides(This,flag) ) 

#define IAcadTable_GetCellType(This,row,col,pCellType)	\
    ( (This)->lpVtbl -> GetCellType(This,row,col,pCellType) ) 

#define IAcadTable_SetCellType(This,row,col,CellType)	\
    ( (This)->lpVtbl -> SetCellType(This,row,col,CellType) ) 

#define IAcadTable_GetCellExtents(This,row,col,bOuterCell,pPts)	\
    ( (This)->lpVtbl -> GetCellExtents(This,row,col,bOuterCell,pPts) ) 

#define IAcadTable_GetAttachmentPoint(This,row,col,pAttachmentPoint)	\
    ( (This)->lpVtbl -> GetAttachmentPoint(This,row,col,pAttachmentPoint) ) 

#define IAcadTable_GetCellAlignment(This,row,col,pCellAlignment)	\
    ( (This)->lpVtbl -> GetCellAlignment(This,row,col,pCellAlignment) ) 

#define IAcadTable_SetCellAlignment(This,row,col,cellAlignment)	\
    ( (This)->lpVtbl -> SetCellAlignment(This,row,col,cellAlignment) ) 

#define IAcadTable_GetCellBackgroundColorNone(This,row,col,bValue)	\
    ( (This)->lpVtbl -> GetCellBackgroundColorNone(This,row,col,bValue) ) 

#define IAcadTable_SetCellBackgroundColorNone(This,row,col,bValue)	\
    ( (This)->lpVtbl -> SetCellBackgroundColorNone(This,row,col,bValue) ) 

#define IAcadTable_GetCellBackgroundColor(This,row,col,pColor)	\
    ( (This)->lpVtbl -> GetCellBackgroundColor(This,row,col,pColor) ) 

#define IAcadTable_SetCellBackgroundColor(This,row,col,pColor)	\
    ( (This)->lpVtbl -> SetCellBackgroundColor(This,row,col,pColor) ) 

#define IAcadTable_GetCellContentColor(This,row,col,pColor)	\
    ( (This)->lpVtbl -> GetCellContentColor(This,row,col,pColor) ) 

#define IAcadTable_SetCellContentColor(This,row,col,pColor)	\
    ( (This)->lpVtbl -> SetCellContentColor(This,row,col,pColor) ) 

#define IAcadTable_GetCellStyleOverrides(This,row,col,pIntArray)	\
    ( (This)->lpVtbl -> GetCellStyleOverrides(This,row,col,pIntArray) ) 

#define IAcadTable_DeleteCellContent(This,row,col)	\
    ( (This)->lpVtbl -> DeleteCellContent(This,row,col) ) 

#define IAcadTable_GetRowType(This,row,pRowType)	\
    ( (This)->lpVtbl -> GetRowType(This,row,pRowType) ) 

#define IAcadTable_GetText(This,row,col,pStr)	\
    ( (This)->lpVtbl -> GetText(This,row,col,pStr) ) 

#define IAcadTable_SetText(This,row,col,pStr)	\
    ( (This)->lpVtbl -> SetText(This,row,col,pStr) ) 

#define IAcadTable_GetCellTextStyle(This,row,col,bstrName)	\
    ( (This)->lpVtbl -> GetCellTextStyle(This,row,col,bstrName) ) 

#define IAcadTable_SetCellTextStyle(This,row,col,bstrName)	\
    ( (This)->lpVtbl -> SetCellTextStyle(This,row,col,bstrName) ) 

#define IAcadTable_GetCellTextHeight(This,row,col,pTextHeight)	\
    ( (This)->lpVtbl -> GetCellTextHeight(This,row,col,pTextHeight) ) 

#define IAcadTable_SetCellTextHeight(This,row,col,TextHeight)	\
    ( (This)->lpVtbl -> SetCellTextHeight(This,row,col,TextHeight) ) 

#define IAcadTable_GetTextRotation(This,row,col,TextRotation)	\
    ( (This)->lpVtbl -> GetTextRotation(This,row,col,TextRotation) ) 

#define IAcadTable_SetTextRotation(This,row,col,TextRotation)	\
    ( (This)->lpVtbl -> SetTextRotation(This,row,col,TextRotation) ) 

#define IAcadTable_GetAutoScale(This,row,col,bValue)	\
    ( (This)->lpVtbl -> GetAutoScale(This,row,col,bValue) ) 

#define IAcadTable_SetAutoScale(This,row,col,bValue)	\
    ( (This)->lpVtbl -> SetAutoScale(This,row,col,bValue) ) 

#define IAcadTable_GetBlockTableRecordId(This,row,col,blkId)	\
    ( (This)->lpVtbl -> GetBlockTableRecordId(This,row,col,blkId) ) 

#define IAcadTable_SetBlockTableRecordId(This,row,col,blkId,bAutoFit)	\
    ( (This)->lpVtbl -> SetBlockTableRecordId(This,row,col,blkId,bAutoFit) ) 

#define IAcadTable_GetBlockScale(This,row,col,blkScale)	\
    ( (This)->lpVtbl -> GetBlockScale(This,row,col,blkScale) ) 

#define IAcadTable_SetBlockScale(This,row,col,blkScale)	\
    ( (This)->lpVtbl -> SetBlockScale(This,row,col,blkScale) ) 

#define IAcadTable_GetBlockRotation(This,row,col,blkRotation)	\
    ( (This)->lpVtbl -> GetBlockRotation(This,row,col,blkRotation) ) 

#define IAcadTable_SetBlockRotation(This,row,col,blkRotation)	\
    ( (This)->lpVtbl -> SetBlockRotation(This,row,col,blkRotation) ) 

#define IAcadTable_GetBlockAttributeValue(This,row,col,attdefId,bstrValue)	\
    ( (This)->lpVtbl -> GetBlockAttributeValue(This,row,col,attdefId,bstrValue) ) 

#define IAcadTable_SetBlockAttributeValue(This,row,col,attdefId,bstrValue)	\
    ( (This)->lpVtbl -> SetBlockAttributeValue(This,row,col,attdefId,bstrValue) ) 

#define IAcadTable_GetCellGridLineWeight(This,row,col,edge,plineweight)	\
    ( (This)->lpVtbl -> GetCellGridLineWeight(This,row,col,edge,plineweight) ) 

#define IAcadTable_SetCellGridLineWeight(This,row,col,edges,Lineweight)	\
    ( (This)->lpVtbl -> SetCellGridLineWeight(This,row,col,edges,Lineweight) ) 

#define IAcadTable_GetCellGridColor(This,row,col,edge,pColor)	\
    ( (This)->lpVtbl -> GetCellGridColor(This,row,col,edge,pColor) ) 

#define IAcadTable_SetCellGridColor(This,row,col,edges,pColor)	\
    ( (This)->lpVtbl -> SetCellGridColor(This,row,col,edges,pColor) ) 

#define IAcadTable_GetCellGridVisibility(This,row,col,edge,bValue)	\
    ( (This)->lpVtbl -> GetCellGridVisibility(This,row,col,edge,bValue) ) 

#define IAcadTable_SetCellGridVisibility(This,row,col,edges,bValue)	\
    ( (This)->lpVtbl -> SetCellGridVisibility(This,row,col,edges,bValue) ) 

#define IAcadTable_InsertColumns(This,col,Width,cols)	\
    ( (This)->lpVtbl -> InsertColumns(This,col,Width,cols) ) 

#define IAcadTable_DeleteColumns(This,col,cols)	\
    ( (This)->lpVtbl -> DeleteColumns(This,col,cols) ) 

#define IAcadTable_InsertRows(This,row,Height,Rows)	\
    ( (This)->lpVtbl -> InsertRows(This,row,Height,Rows) ) 

#define IAcadTable_DeleteRows(This,row,Rows)	\
    ( (This)->lpVtbl -> DeleteRows(This,row,Rows) ) 

#define IAcadTable_MergeCells(This,minRow,maxRow,minCol,maxCol)	\
    ( (This)->lpVtbl -> MergeCells(This,minRow,maxRow,minCol,maxCol) ) 

#define IAcadTable_UnmergeCells(This,minRow,maxRow,minCol,maxCol)	\
    ( (This)->lpVtbl -> UnmergeCells(This,minRow,maxRow,minCol,maxCol) ) 

#define IAcadTable_IsMergedCell(This,row,col,minRow,maxRow,minCol,maxCol,pbValue)	\
    ( (This)->lpVtbl -> IsMergedCell(This,row,col,minRow,maxRow,minCol,maxCol,pbValue) ) 

#define IAcadTable_GetFieldId(This,row,col,fieldId)	\
    ( (This)->lpVtbl -> GetFieldId(This,row,col,fieldId) ) 

#define IAcadTable_SetFieldId(This,row,col,fieldId)	\
    ( (This)->lpVtbl -> SetFieldId(This,row,col,fieldId) ) 

#define IAcadTable_GenerateLayout(This)	\
    ( (This)->lpVtbl -> GenerateLayout(This) ) 

#define IAcadTable_RecomputeTableBlock(This,bForceUpdate)	\
    ( (This)->lpVtbl -> RecomputeTableBlock(This,bForceUpdate) ) 

#define IAcadTable_HitTest(This,wpt,wviewVec,resultRowIndex,resultColumnIndex,bReturn)	\
    ( (This)->lpVtbl -> HitTest(This,wpt,wviewVec,resultRowIndex,resultColumnIndex,bReturn) ) 

#define IAcadTable_Select(This,wpt,wvwVec,wvwxVec,wxaper,wyaper,allowOutside,resultRowIndex,resultColumnIndex,pPaths)	\
    ( (This)->lpVtbl -> Select(This,wpt,wvwVec,wvwxVec,wxaper,wyaper,allowOutside,resultRowIndex,resultColumnIndex,pPaths) ) 

#define IAcadTable_SelectSubRegion(This,wpt1,wpt2,wvwVec,wvwxVec,seltype,bIncludeCurrentSelection,rowMin,rowMax,colMin,colMax,pPaths)	\
    ( (This)->lpVtbl -> SelectSubRegion(This,wpt1,wpt2,wvwVec,wvwxVec,seltype,bIncludeCurrentSelection,rowMin,rowMax,colMin,colMax,pPaths) ) 

#define IAcadTable_ReselectSubRegion(This,pPaths)	\
    ( (This)->lpVtbl -> ReselectSubRegion(This,pPaths) ) 

#define IAcadTable_GetSubSelection(This,rowMin,rowMax,colMin,colMax)	\
    ( (This)->lpVtbl -> GetSubSelection(This,rowMin,rowMax,colMin,colMax) ) 

#define IAcadTable_SetSubSelection(This,rowMin,rowMax,colMin,colMax)	\
    ( (This)->lpVtbl -> SetSubSelection(This,rowMin,rowMax,colMin,colMax) ) 

#define IAcadTable_ClearSubSelection(This)	\
    ( (This)->lpVtbl -> ClearSubSelection(This) ) 

#define IAcadTable_get_HasSubSelection(This,pbValue)	\
    ( (This)->lpVtbl -> get_HasSubSelection(This,pbValue) ) 

#define IAcadTable_get_RegenerateTableSuppressed(This,bValue)	\
    ( (This)->lpVtbl -> get_RegenerateTableSuppressed(This,bValue) ) 

#define IAcadTable_put_RegenerateTableSuppressed(This,bValue)	\
    ( (This)->lpVtbl -> put_RegenerateTableSuppressed(This,bValue) ) 

#define IAcadTable_GetDataType(This,rowType,pDataType,pUnitType)	\
    ( (This)->lpVtbl -> GetDataType(This,rowType,pDataType,pUnitType) ) 

#define IAcadTable_SetDataType(This,rowTypes,dataType,unitType)	\
    ( (This)->lpVtbl -> SetDataType(This,rowTypes,dataType,unitType) ) 

#define IAcadTable_GetFormat(This,rowType,pFormat)	\
    ( (This)->lpVtbl -> GetFormat(This,rowType,pFormat) ) 

#define IAcadTable_SetFormat(This,rowTypes,pFormat)	\
    ( (This)->lpVtbl -> SetFormat(This,rowTypes,pFormat) ) 

#define IAcadTable_FormatValue(This,row,col,nOption,pVal)	\
    ( (This)->lpVtbl -> FormatValue(This,row,col,nOption,pVal) ) 

#define IAcadTable_GetCellDataType(This,row,col,pDataType,pUnitType)	\
    ( (This)->lpVtbl -> GetCellDataType(This,row,col,pDataType,pUnitType) ) 

#define IAcadTable_SetCellDataType(This,row,col,dataType,unitType)	\
    ( (This)->lpVtbl -> SetCellDataType(This,row,col,dataType,unitType) ) 

#define IAcadTable_GetCellFormat(This,row,col,pFormat)	\
    ( (This)->lpVtbl -> GetCellFormat(This,row,col,pFormat) ) 

#define IAcadTable_SetCellFormat(This,row,col,pFormat)	\
    ( (This)->lpVtbl -> SetCellFormat(This,row,col,pFormat) ) 

#define IAcadTable_GetCellValue(This,row,col,pVal)	\
    ( (This)->lpVtbl -> GetCellValue(This,row,col,pVal) ) 

#define IAcadTable_SetCellValue(This,row,col,val)	\
    ( (This)->lpVtbl -> SetCellValue(This,row,col,val) ) 

#define IAcadTable_SetCellValueFromText(This,row,col,val,nOption)	\
    ( (This)->lpVtbl -> SetCellValueFromText(This,row,col,val,nOption) ) 

#define IAcadTable_ResetCellValue(This,row,col)	\
    ( (This)->lpVtbl -> ResetCellValue(This,row,col) ) 

#define IAcadTable_IsEmpty(This,nRow,nCol,bValue)	\
    ( (This)->lpVtbl -> IsEmpty(This,nRow,nCol,bValue) ) 

#define IAcadTable_CreateContent(This,nRow,nCol,nIndex,pInt)	\
    ( (This)->lpVtbl -> CreateContent(This,nRow,nCol,nIndex,pInt) ) 

#define IAcadTable_MoveContent(This,nRow,nCol,nFromIndex,nToIndex)	\
    ( (This)->lpVtbl -> MoveContent(This,nRow,nCol,nFromIndex,nToIndex) ) 

#define IAcadTable_DeleteContent(This,nRow,nCol)	\
    ( (This)->lpVtbl -> DeleteContent(This,nRow,nCol) ) 

#define IAcadTable_GetValue(This,nRow,nCol,nContent,pAcValue)	\
    ( (This)->lpVtbl -> GetValue(This,nRow,nCol,nContent,pAcValue) ) 

#define IAcadTable_SetValue(This,nRow,nCol,nContent,acValue)	\
    ( (This)->lpVtbl -> SetValue(This,nRow,nCol,nContent,acValue) ) 

#define IAcadTable_SetValueFromText(This,nRow,nCol,nContent,szText,nOption)	\
    ( (This)->lpVtbl -> SetValueFromText(This,nRow,nCol,nContent,szText,nOption) ) 

#define IAcadTable_GetDataFormat(This,nRow,nCol,nContent,pValue)	\
    ( (This)->lpVtbl -> GetDataFormat(This,nRow,nCol,nContent,pValue) ) 

#define IAcadTable_SetDataFormat(This,nRow,nCol,nContent,szFormat)	\
    ( (This)->lpVtbl -> SetDataFormat(This,nRow,nCol,nContent,szFormat) ) 

#define IAcadTable_GetTextString(This,nRow,nCol,nContent,pTextString)	\
    ( (This)->lpVtbl -> GetTextString(This,nRow,nCol,nContent,pTextString) ) 

#define IAcadTable_SetTextString(This,nRow,nCol,nContent,Text)	\
    ( (This)->lpVtbl -> SetTextString(This,nRow,nCol,nContent,Text) ) 

#define IAcadTable_GetFieldId2(This,nRow,nCol,nContent,pAcDbObjectId)	\
    ( (This)->lpVtbl -> GetFieldId2(This,nRow,nCol,nContent,pAcDbObjectId) ) 

#define IAcadTable_SetFieldId2(This,nRow,nCol,nContent,acDbObjectId,nflag)	\
    ( (This)->lpVtbl -> SetFieldId2(This,nRow,nCol,nContent,acDbObjectId,nflag) ) 

#define IAcadTable_GetBlockTableRecordId2(This,nRow,nCol,nContent,pAcDbObjectId)	\
    ( (This)->lpVtbl -> GetBlockTableRecordId2(This,nRow,nCol,nContent,pAcDbObjectId) ) 

#define IAcadTable_SetBlockTableRecordId2(This,nRow,nCol,nContent,blkId,autoFit)	\
    ( (This)->lpVtbl -> SetBlockTableRecordId2(This,nRow,nCol,nContent,blkId,autoFit) ) 

#define IAcadTable_GetBlockAttributeValue2(This,nRow,nCol,nContent,blkId,Value)	\
    ( (This)->lpVtbl -> GetBlockAttributeValue2(This,nRow,nCol,nContent,blkId,Value) ) 

#define IAcadTable_SetBlockAttributeValue2(This,nRow,nCol,nContent,blkId,Value)	\
    ( (This)->lpVtbl -> SetBlockAttributeValue2(This,nRow,nCol,nContent,blkId,Value) ) 

#define IAcadTable_GetCustomData(This,nRow,nCol,szKey,pData)	\
    ( (This)->lpVtbl -> GetCustomData(This,nRow,nCol,szKey,pData) ) 

#define IAcadTable_SetCustomData(This,nRow,nCol,szKey,data)	\
    ( (This)->lpVtbl -> SetCustomData(This,nRow,nCol,szKey,data) ) 

#define IAcadTable_GetCellStyle(This,nRow,nCol,pCellStyle)	\
    ( (This)->lpVtbl -> GetCellStyle(This,nRow,nCol,pCellStyle) ) 

#define IAcadTable_SetCellStyle(This,nRow,nCol,szCellStyle)	\
    ( (This)->lpVtbl -> SetCellStyle(This,nRow,nCol,szCellStyle) ) 

#define IAcadTable_GetContentColor2(This,nRow,nCol,nContent,pColor)	\
    ( (This)->lpVtbl -> GetContentColor2(This,nRow,nCol,nContent,pColor) ) 

#define IAcadTable_SetContentColor2(This,nRow,nCol,nContent,pColor)	\
    ( (This)->lpVtbl -> SetContentColor2(This,nRow,nCol,nContent,pColor) ) 

#define IAcadTable_GetDataType2(This,nRow,nCol,nContent,pDataType,pUnitType)	\
    ( (This)->lpVtbl -> GetDataType2(This,nRow,nCol,nContent,pDataType,pUnitType) ) 

#define IAcadTable_SetDataType2(This,nRow,nCol,nContent,dataType,unitType)	\
    ( (This)->lpVtbl -> SetDataType2(This,nRow,nCol,nContent,dataType,unitType) ) 

#define IAcadTable_GetTextStyle2(This,nRow,nCol,nContent,pbstrStyleName)	\
    ( (This)->lpVtbl -> GetTextStyle2(This,nRow,nCol,nContent,pbstrStyleName) ) 

#define IAcadTable_SetTextStyle2(This,nRow,nCol,nContent,bstrStyleName)	\
    ( (This)->lpVtbl -> SetTextStyle2(This,nRow,nCol,nContent,bstrStyleName) ) 

#define IAcadTable_GetTextHeight2(This,nRow,nCol,nContent,pHeight)	\
    ( (This)->lpVtbl -> GetTextHeight2(This,nRow,nCol,nContent,pHeight) ) 

#define IAcadTable_SetTextHeight2(This,nRow,nCol,nContent,Height)	\
    ( (This)->lpVtbl -> SetTextHeight2(This,nRow,nCol,nContent,Height) ) 

#define IAcadTable_GetRotation(This,nRow,nCol,nContent,pValue)	\
    ( (This)->lpVtbl -> GetRotation(This,nRow,nCol,nContent,pValue) ) 

#define IAcadTable_SetRotation(This,nRow,nCol,nContent,Value)	\
    ( (This)->lpVtbl -> SetRotation(This,nRow,nCol,nContent,Value) ) 

#define IAcadTable_GetAutoScale2(This,nRow,nCol,nContent,bAutoScale)	\
    ( (This)->lpVtbl -> GetAutoScale2(This,nRow,nCol,nContent,bAutoScale) ) 

#define IAcadTable_SetAutoScale2(This,nRow,nCol,nContent,bAutoFit)	\
    ( (This)->lpVtbl -> SetAutoScale2(This,nRow,nCol,nContent,bAutoFit) ) 

#define IAcadTable_GetScale(This,nRow,nCol,nContent,pScale)	\
    ( (This)->lpVtbl -> GetScale(This,nRow,nCol,nContent,pScale) ) 

#define IAcadTable_SetScale(This,nRow,nCol,nContent,scale)	\
    ( (This)->lpVtbl -> SetScale(This,nRow,nCol,nContent,scale) ) 

#define IAcadTable_RemoveAllOverrides(This,nRow,nCol)	\
    ( (This)->lpVtbl -> RemoveAllOverrides(This,nRow,nCol) ) 

#define IAcadTable_GetGridLineWeight2(This,nRow,nCol,nGridLineType,plineweight)	\
    ( (This)->lpVtbl -> GetGridLineWeight2(This,nRow,nCol,nGridLineType,plineweight) ) 

#define IAcadTable_SetGridLineWeight2(This,nRow,nCol,nGridLineType,Lineweight)	\
    ( (This)->lpVtbl -> SetGridLineWeight2(This,nRow,nCol,nGridLineType,Lineweight) ) 

#define IAcadTable_GetGridLinetype(This,nRow,nCol,nGridLineType,pacDbObjId)	\
    ( (This)->lpVtbl -> GetGridLinetype(This,nRow,nCol,nGridLineType,pacDbObjId) ) 

#define IAcadTable_SetGridLinetype(This,nRow,nCol,nGridLineType,idLinetype)	\
    ( (This)->lpVtbl -> SetGridLinetype(This,nRow,nCol,nGridLineType,idLinetype) ) 

#define IAcadTable_GetGridColor2(This,nRow,nCol,nGridLineType,pColor)	\
    ( (This)->lpVtbl -> GetGridColor2(This,nRow,nCol,nGridLineType,pColor) ) 

#define IAcadTable_SetGridColor2(This,nRow,nCol,nGridLineType,pColor)	\
    ( (This)->lpVtbl -> SetGridColor2(This,nRow,nCol,nGridLineType,pColor) ) 

#define IAcadTable_GetGridVisibility2(This,nRow,nCol,nGridLineType,bVisible)	\
    ( (This)->lpVtbl -> GetGridVisibility2(This,nRow,nCol,nGridLineType,bVisible) ) 

#define IAcadTable_SetGridVisibility2(This,nRow,nCol,nGridLineType,bVisible)	\
    ( (This)->lpVtbl -> SetGridVisibility2(This,nRow,nCol,nGridLineType,bVisible) ) 

#define IAcadTable_GetGridDoubleLineSpacing(This,nRow,nCol,nGridLineType,pValue)	\
    ( (This)->lpVtbl -> GetGridDoubleLineSpacing(This,nRow,nCol,nGridLineType,pValue) ) 

#define IAcadTable_SetGridDoubleLineSpacing(This,nRow,nCol,nGridLineType,fSpacing)	\
    ( (This)->lpVtbl -> SetGridDoubleLineSpacing(This,nRow,nCol,nGridLineType,fSpacing) ) 

#define IAcadTable_put_EnableBreak(This,rhs)	\
    ( (This)->lpVtbl -> put_EnableBreak(This,rhs) ) 

#define IAcadTable_GetBreakHeight(This,nIndex,pHeight)	\
    ( (This)->lpVtbl -> GetBreakHeight(This,nIndex,pHeight) ) 

#define IAcadTable_SetBreakHeight(This,nIndex,Height)	\
    ( (This)->lpVtbl -> SetBreakHeight(This,nIndex,Height) ) 

#define IAcadTable_GetContentType(This,nRow,nCol,pType)	\
    ( (This)->lpVtbl -> GetContentType(This,nRow,nCol,pType) ) 

#define IAcadTable_GetMargin(This,nRow,nCol,nMargin,pValue)	\
    ( (This)->lpVtbl -> GetMargin(This,nRow,nCol,nMargin,pValue) ) 

#define IAcadTable_SetMargin(This,nRow,nCol,nMargins,fMargin)	\
    ( (This)->lpVtbl -> SetMargin(This,nRow,nCol,nMargins,fMargin) ) 

#define IAcadTable_GetContentLayout(This,row,col,pLayout)	\
    ( (This)->lpVtbl -> GetContentLayout(This,row,col,pLayout) ) 

#define IAcadTable_SetContentLayout(This,row,col,nLayout)	\
    ( (This)->lpVtbl -> SetContentLayout(This,row,col,nLayout) ) 

#define IAcadTable_GetOverride(This,nRow,nCol,nContent,pValue)	\
    ( (This)->lpVtbl -> GetOverride(This,nRow,nCol,nContent,pValue) ) 

#define IAcadTable_SetOverride(This,nRow,nCol,nContent,nProp)	\
    ( (This)->lpVtbl -> SetOverride(This,nRow,nCol,nContent,nProp) ) 

#define IAcadTable_GetGridLineStyle(This,nRow,nCol,nGridLineType,pStyle)	\
    ( (This)->lpVtbl -> GetGridLineStyle(This,nRow,nCol,nGridLineType,pStyle) ) 

#define IAcadTable_SetGridLineStyle(This,nRow,nCol,nGridLineTypes,nLineStyle)	\
    ( (This)->lpVtbl -> SetGridLineStyle(This,nRow,nCol,nGridLineTypes,nLineStyle) ) 

#define IAcadTable_InsertRowsAndInherit(This,nIndex,nInheritFrom,nNumRows)	\
    ( (This)->lpVtbl -> InsertRowsAndInherit(This,nIndex,nInheritFrom,nNumRows) ) 

#define IAcadTable_InsertColumnsAndInherit(This,col,nInheritFrom,nNumCols)	\
    ( (This)->lpVtbl -> InsertColumnsAndInherit(This,col,nInheritFrom,nNumCols) ) 

#define IAcadTable_GetHasFormula(This,nRow,nCol,nContent,bValue)	\
    ( (This)->lpVtbl -> GetHasFormula(This,nRow,nCol,nContent,bValue) ) 

#define IAcadTable_GetFormula(This,nRow,nCol,nContent,pszFormula)	\
    ( (This)->lpVtbl -> GetFormula(This,nRow,nCol,nContent,pszFormula) ) 

#define IAcadTable_SetFormula(This,nRow,nCol,nContent,pszFormula)	\
    ( (This)->lpVtbl -> SetFormula(This,nRow,nCol,nContent,pszFormula) ) 

#define IAcadTable_IsContentEditable(This,nRow,nCol,bValue)	\
    ( (This)->lpVtbl -> IsContentEditable(This,nRow,nCol,bValue) ) 

#define IAcadTable_IsFormatEditable(This,nRow,nCol,bValue)	\
    ( (This)->lpVtbl -> IsFormatEditable(This,nRow,nCol,bValue) ) 

#define IAcadTable_GetCellState(This,nRow,nCol,pCellState)	\
    ( (This)->lpVtbl -> GetCellState(This,nRow,nCol,pCellState) ) 

#define IAcadTable_SetCellState(This,nRow,nCol,nLock)	\
    ( (This)->lpVtbl -> SetCellState(This,nRow,nCol,nLock) ) 

#define IAcadTable_EnableMergeAll(This,nRow,nCol,bEnable)	\
    ( (This)->lpVtbl -> EnableMergeAll(This,nRow,nCol,bEnable) ) 

#define IAcadTable_IsMergeAllEnabled(This,nRow,nCol,bValue)	\
    ( (This)->lpVtbl -> IsMergeAllEnabled(This,nRow,nCol,bValue) ) 

#define IAcadTable_get_BreaksEnabled(This,bEnabled)	\
    ( (This)->lpVtbl -> get_BreaksEnabled(This,bEnabled) ) 

#define IAcadTable_put_BreaksEnabled(This,bEnabled)	\
    ( (This)->lpVtbl -> put_BreaksEnabled(This,bEnabled) ) 

#define IAcadTable_get_RepeatTopLabels(This,bEnabled)	\
    ( (This)->lpVtbl -> get_RepeatTopLabels(This,bEnabled) ) 

#define IAcadTable_put_RepeatTopLabels(This,bEnabled)	\
    ( (This)->lpVtbl -> put_RepeatTopLabels(This,bEnabled) ) 

#define IAcadTable_get_RepeatBottomLabels(This,bEnabled)	\
    ( (This)->lpVtbl -> get_RepeatBottomLabels(This,bEnabled) ) 

#define IAcadTable_put_RepeatBottomLabels(This,bEnabled)	\
    ( (This)->lpVtbl -> put_RepeatBottomLabels(This,bEnabled) ) 

#define IAcadTable_get_TableBreakFlowDirection(This,pDir)	\
    ( (This)->lpVtbl -> get_TableBreakFlowDirection(This,pDir) ) 

#define IAcadTable_put_TableBreakFlowDirection(This,pDir)	\
    ( (This)->lpVtbl -> put_TableBreakFlowDirection(This,pDir) ) 

#define IAcadTable_get_AllowManualPositions(This,bEnabled)	\
    ( (This)->lpVtbl -> get_AllowManualPositions(This,bEnabled) ) 

#define IAcadTable_put_AllowManualPositions(This,bEnabled)	\
    ( (This)->lpVtbl -> put_AllowManualPositions(This,bEnabled) ) 

#define IAcadTable_get_AllowManualHeights(This,bEnabled)	\
    ( (This)->lpVtbl -> get_AllowManualHeights(This,bEnabled) ) 

#define IAcadTable_put_AllowManualHeights(This,bEnabled)	\
    ( (This)->lpVtbl -> put_AllowManualHeights(This,bEnabled) ) 

#define IAcadTable_get_TableBreakHeight(This,pHeight)	\
    ( (This)->lpVtbl -> get_TableBreakHeight(This,pHeight) ) 

#define IAcadTable_put_TableBreakHeight(This,pHeight)	\
    ( (This)->lpVtbl -> put_TableBreakHeight(This,pHeight) ) 

#define IAcadTable_get_BreakSpacing(This,pSpacing)	\
    ( (This)->lpVtbl -> get_BreakSpacing(This,pSpacing) ) 

#define IAcadTable_put_BreakSpacing(This,pSpacing)	\
    ( (This)->lpVtbl -> put_BreakSpacing(This,pSpacing) ) 

#define IAcadTable_GetColumnName(This,nIndex,Name)	\
    ( (This)->lpVtbl -> GetColumnName(This,nIndex,Name) ) 

#define IAcadTable_SetColumnName(This,nIndex,Name)	\
    ( (This)->lpVtbl -> SetColumnName(This,nIndex,Name) ) 

#define IAcadTable_SetToolTip(This,nRow,nCol,tip)	\
    ( (This)->lpVtbl -> SetToolTip(This,nRow,nCol,tip) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetCellTextHeight_Proxy( 
    IAcadTable * This,
    /* [in] */ int row,
    /* [in] */ int col,
    /* [retval][out] */ double *pTextHeight);


void __RPC_STUB IAcadTable_GetCellTextHeight_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetCellTextHeight_Proxy( 
    IAcadTable * This,
    /* [in] */ int row,
    /* [in] */ int col,
    /* [in] */ double TextHeight);


void __RPC_STUB IAcadTable_SetCellTextHeight_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetTextRotation_Proxy( 
    IAcadTable * This,
    /* [in] */ int row,
    /* [in] */ int col,
    /* [retval][out] */ AcRotationAngle *TextRotation);


void __RPC_STUB IAcadTable_GetTextRotation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetTextRotation_Proxy( 
    IAcadTable * This,
    /* [in] */ int row,
    /* [in] */ int col,
    /* [in] */ AcRotationAngle TextRotation);


void __RPC_STUB IAcadTable_SetTextRotation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetAutoScale_Proxy( 
    IAcadTable * This,
    /* [in] */ int row,
    /* [in] */ int col,
    /* [retval][out] */ VARIANT_BOOL *bValue);


void __RPC_STUB IAcadTable_GetAutoScale_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetAutoScale_Proxy( 
    IAcadTable * This,
    /* [in] */ int row,
    /* [in] */ int col,
    /* [in] */ VARIANT_BOOL bValue);


void __RPC_STUB IAcadTable_SetAutoScale_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetBlockTableRecordId_Proxy( 
    IAcadTable * This,
    /* [in] */ int row,
    /* [in] */ int col,
    /* [retval][out] */ LONG_PTR *blkId);


void __RPC_STUB IAcadTable_GetBlockTableRecordId_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetBlockTableRecordId_Proxy( 
    IAcadTable * This,
    /* [in] */ int row,
    /* [in] */ int col,
    /* [in] */ LONG_PTR blkId,
    /* [in] */ VARIANT_BOOL bAutoFit);


void __RPC_STUB IAcadTable_SetBlockTableRecordId_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetBlockScale_Proxy( 
    IAcadTable * This,
    /* [in] */ int row,
    /* [in] */ int col,
    /* [retval][out] */ double *blkScale);


void __RPC_STUB IAcadTable_GetBlockScale_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetBlockScale_Proxy( 
    IAcadTable * This,
    /* [in] */ int row,
    /* [in] */ int col,
    /* [in] */ double blkScale);


void __RPC_STUB IAcadTable_SetBlockScale_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetBlockRotation_Proxy( 
    IAcadTable * This,
    /* [in] */ int row,
    /* [in] */ int col,
    /* [retval][out] */ double *blkRotation);


void __RPC_STUB IAcadTable_GetBlockRotation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetBlockRotation_Proxy( 
    IAcadTable * This,
    /* [in] */ int row,
    /* [in] */ int col,
    /* [in] */ double blkRotation);


void __RPC_STUB IAcadTable_SetBlockRotation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetBlockAttributeValue_Proxy( 
    IAcadTable * This,
    /* [in] */ int row,
    /* [in] */ int col,
    /* [in] */ LONG_PTR attdefId,
    /* [retval][out] */ BSTR *bstrValue);


void __RPC_STUB IAcadTable_GetBlockAttributeValue_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetBlockAttributeValue_Proxy( 
    IAcadTable * This,
    /* [in] */ int row,
    /* [in] */ int col,
    /* [in] */ LONG_PTR attdefId,
    /* [in] */ BSTR bstrValue);


void __RPC_STUB IAcadTable_SetBlockAttributeValue_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetCellGridLineWeight_Proxy( 
    IAcadTable * This,
    /* [in] */ int row,
    /* [in] */ int col,
    /* [in] */ AcCellEdgeMask edge,
    /* [retval][out] */ ACAD_LWEIGHT *plineweight);


void __RPC_STUB IAcadTable_GetCellGridLineWeight_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetCellGridLineWeight_Proxy( 
    IAcadTable * This,
    /* [in] */ int row,
    /* [in] */ int col,
    /* [in] */ int edges,
    /* [in] */ ACAD_LWEIGHT Lineweight);


void __RPC_STUB IAcadTable_SetCellGridLineWeight_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetCellGridColor_Proxy( 
    IAcadTable * This,
    /* [in] */ int row,
    /* [in] */ int col,
    /* [in] */ AcCellEdgeMask edge,
    /* [retval][out] */ IAcadAcCmColor **pColor);


void __RPC_STUB IAcadTable_GetCellGridColor_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetCellGridColor_Proxy( 
    IAcadTable * This,
    /* [in] */ int row,
    /* [in] */ int col,
    /* [in] */ int edges,
    /* [in] */ IAcadAcCmColor *pColor);


void __RPC_STUB IAcadTable_SetCellGridColor_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetCellGridVisibility_Proxy( 
    IAcadTable * This,
    /* [in] */ int row,
    /* [in] */ int col,
    /* [in] */ AcCellEdgeMask edge,
    /* [retval][out] */ VARIANT_BOOL *bValue);


void __RPC_STUB IAcadTable_GetCellGridVisibility_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetCellGridVisibility_Proxy( 
    IAcadTable * This,
    /* [in] */ int row,
    /* [in] */ int col,
    /* [in] */ int edges,
    /* [in] */ VARIANT_BOOL bValue);


void __RPC_STUB IAcadTable_SetCellGridVisibility_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_InsertColumns_Proxy( 
    IAcadTable * This,
    /* [in] */ int col,
    /* [in] */ double Width,
    /* [in] */ int cols);


void __RPC_STUB IAcadTable_InsertColumns_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_DeleteColumns_Proxy( 
    IAcadTable * This,
    /* [in] */ int col,
    /* [in] */ int cols);


void __RPC_STUB IAcadTable_DeleteColumns_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_InsertRows_Proxy( 
    IAcadTable * This,
    /* [in] */ int row,
    /* [in] */ double Height,
    /* [in] */ int Rows);


void __RPC_STUB IAcadTable_InsertRows_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_DeleteRows_Proxy( 
    IAcadTable * This,
    /* [in] */ int row,
    /* [in] */ int Rows);


void __RPC_STUB IAcadTable_DeleteRows_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_MergeCells_Proxy( 
    IAcadTable * This,
    /* [in] */ int minRow,
    /* [in] */ int maxRow,
    /* [in] */ int minCol,
    /* [in] */ int maxCol);


void __RPC_STUB IAcadTable_MergeCells_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_UnmergeCells_Proxy( 
    IAcadTable * This,
    /* [in] */ int minRow,
    /* [in] */ int maxRow,
    /* [in] */ int minCol,
    /* [in] */ int maxCol);


void __RPC_STUB IAcadTable_UnmergeCells_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_IsMergedCell_Proxy( 
    IAcadTable * This,
    /* [in] */ int row,
    /* [in] */ int col,
    /* [out] */ int *minRow,
    /* [out] */ int *maxRow,
    /* [out] */ int *minCol,
    /* [out] */ int *maxCol,
    /* [retval][out] */ VARIANT_BOOL *pbValue);


void __RPC_STUB IAcadTable_IsMergedCell_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetFieldId_Proxy( 
    IAcadTable * This,
    /* [in] */ int row,
    /* [in] */ int col,
    /* [retval][out] */ LONG_PTR *fieldId);


void __RPC_STUB IAcadTable_GetFieldId_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetFieldId_Proxy( 
    IAcadTable * This,
    /* [in] */ int row,
    /* [in] */ int col,
    /* [in] */ LONG_PTR fieldId);


void __RPC_STUB IAcadTable_SetFieldId_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GenerateLayout_Proxy( 
    IAcadTable * This);


void __RPC_STUB IAcadTable_GenerateLayout_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_RecomputeTableBlock_Proxy( 
    IAcadTable * This,
    /* [in] */ VARIANT_BOOL bForceUpdate);


void __RPC_STUB IAcadTable_RecomputeTableBlock_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_HitTest_Proxy( 
    IAcadTable * This,
    /* [in] */ VARIANT wpt,
    /* [in] */ VARIANT wviewVec,
    /* [out] */ int *resultRowIndex,
    /* [out] */ int *resultColumnIndex,
    /* [retval][out] */ VARIANT_BOOL *bReturn);


void __RPC_STUB IAcadTable_HitTest_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_Select_Proxy( 
    IAcadTable * This,
    /* [in] */ VARIANT wpt,
    /* [in] */ VARIANT wvwVec,
    /* [in] */ VARIANT wvwxVec,
    /* [in] */ double wxaper,
    /* [in] */ double wyaper,
    /* [in] */ VARIANT_BOOL allowOutside,
    /* [out] */ int *resultRowIndex,
    /* [out] */ int *resultColumnIndex,
    /* [retval][out] */ VARIANT *pPaths);


void __RPC_STUB IAcadTable_Select_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SelectSubRegion_Proxy( 
    IAcadTable * This,
    /* [in] */ VARIANT wpt1,
    /* [in] */ VARIANT wpt2,
    /* [in] */ VARIANT wvwVec,
    /* [in] */ VARIANT wvwxVec,
    /* [in] */ AcSelectType seltype,
    /* [in] */ VARIANT_BOOL bIncludeCurrentSelection,
    /* [out] */ int *rowMin,
    /* [out] */ int *rowMax,
    /* [out] */ int *colMin,
    /* [out] */ int *colMax,
    /* [retval][out] */ VARIANT *pPaths);


void __RPC_STUB IAcadTable_SelectSubRegion_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_ReselectSubRegion_Proxy( 
    IAcadTable * This,
    /* [retval][out] */ VARIANT *pPaths);


void __RPC_STUB IAcadTable_ReselectSubRegion_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetSubSelection_Proxy( 
    IAcadTable * This,
    /* [out] */ int *rowMin,
    /* [out] */ int *rowMax,
    /* [out] */ int *colMin,
    /* [out] */ int *colMax);


void __RPC_STUB IAcadTable_GetSubSelection_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetSubSelection_Proxy( 
    IAcadTable * This,
    /* [in] */ int rowMin,
    /* [in] */ int rowMax,
    /* [in] */ int colMin,
    /* [in] */ int colMax);


void __RPC_STUB IAcadTable_SetSubSelection_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_ClearSubSelection_Proxy( 
    IAcadTable * This);


void __RPC_STUB IAcadTable_ClearSubSelection_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_get_HasSubSelection_Proxy( 
    IAcadTable * This,
    /* [retval][out] */ VARIANT_BOOL *pbValue);


void __RPC_STUB IAcadTable_get_HasSubSelection_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_get_RegenerateTableSuppressed_Proxy( 
    IAcadTable * This,
    /* [retval][out] */ VARIANT_BOOL *bValue);


void __RPC_STUB IAcadTable_get_RegenerateTableSuppressed_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_put_RegenerateTableSuppressed_Proxy( 
    IAcadTable * This,
    /* [in] */ VARIANT_BOOL bValue);


void __RPC_STUB IAcadTable_put_RegenerateTableSuppressed_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetDataType_Proxy( 
    IAcadTable * This,
    /* [in] */ AcRowType rowType,
    /* [out] */ AcValueDataType *pDataType,
    /* [out] */ AcValueUnitType *pUnitType);


void __RPC_STUB IAcadTable_GetDataType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetDataType_Proxy( 
    IAcadTable * This,
    /* [in] */ int rowTypes,
    /* [in] */ AcValueDataType dataType,
    /* [in] */ AcValueUnitType unitType);


void __RPC_STUB IAcadTable_SetDataType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetFormat_Proxy( 
    IAcadTable * This,
    /* [in] */ AcRowType rowType,
    /* [retval][out] */ BSTR *pFormat);


void __RPC_STUB IAcadTable_GetFormat_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetFormat_Proxy( 
    IAcadTable * This,
    /* [in] */ int rowTypes,
    BSTR pFormat);


void __RPC_STUB IAcadTable_SetFormat_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_FormatValue_Proxy( 
    IAcadTable * This,
    /* [in] */ int row,
    /* [in] */ int col,
    AcFormatOption nOption,
    BSTR *pVal);


void __RPC_STUB IAcadTable_FormatValue_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetCellDataType_Proxy( 
    IAcadTable * This,
    /* [in] */ int row,
    /* [in] */ int col,
    /* [out] */ AcValueDataType *pDataType,
    /* [out] */ AcValueUnitType *pUnitType);


void __RPC_STUB IAcadTable_GetCellDataType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetCellDataType_Proxy( 
    IAcadTable * This,
    /* [in] */ int row,
    /* [in] */ int col,
    AcValueDataType dataType,
    AcValueUnitType unitType);


void __RPC_STUB IAcadTable_SetCellDataType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetCellFormat_Proxy( 
    IAcadTable * This,
    /* [in] */ int row,
    /* [in] */ int col,
    /* [retval][out] */ BSTR *pFormat);


void __RPC_STUB IAcadTable_GetCellFormat_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetCellFormat_Proxy( 
    IAcadTable * This,
    /* [in] */ int row,
    /* [in] */ int col,
    BSTR pFormat);


void __RPC_STUB IAcadTable_SetCellFormat_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetCellValue_Proxy( 
    IAcadTable * This,
    /* [in] */ int row,
    /* [in] */ int col,
    /* [retval][out] */ VARIANT *pVal);


void __RPC_STUB IAcadTable_GetCellValue_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetCellValue_Proxy( 
    IAcadTable * This,
    /* [in] */ int row,
    /* [in] */ int col,
    VARIANT val);


void __RPC_STUB IAcadTable_SetCellValue_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetCellValueFromText_Proxy( 
    IAcadTable * This,
    /* [in] */ int row,
    /* [in] */ int col,
    /* [in] */ BSTR val,
    /* [in] */ AcParseOption nOption);


void __RPC_STUB IAcadTable_SetCellValueFromText_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_ResetCellValue_Proxy( 
    IAcadTable * This,
    /* [in] */ int row,
    /* [in] */ int col);


void __RPC_STUB IAcadTable_ResetCellValue_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_IsEmpty_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [retval][out] */ VARIANT_BOOL *bValue);


void __RPC_STUB IAcadTable_IsEmpty_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_CreateContent_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ int nIndex,
    /* [retval][out] */ int *pInt);


void __RPC_STUB IAcadTable_CreateContent_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_MoveContent_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ int nFromIndex,
    /* [in] */ int nToIndex);


void __RPC_STUB IAcadTable_MoveContent_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_DeleteContent_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol);


void __RPC_STUB IAcadTable_DeleteContent_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetValue_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ int nContent,
    /* [retval][out] */ VARIANT *pAcValue);


void __RPC_STUB IAcadTable_GetValue_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetValue_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ int nContent,
    /* [in] */ VARIANT acValue);


void __RPC_STUB IAcadTable_SetValue_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetValueFromText_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ int nContent,
    /* [in] */ BSTR szText,
    /* [in] */ AcParseOption nOption);


void __RPC_STUB IAcadTable_SetValueFromText_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetDataFormat_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ int nContent,
    /* [retval][out] */ BSTR *pValue);


void __RPC_STUB IAcadTable_GetDataFormat_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetDataFormat_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ int nContent,
    /* [in] */ BSTR szFormat);


void __RPC_STUB IAcadTable_SetDataFormat_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetTextString_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ int nContent,
    /* [retval][out] */ BSTR *pTextString);


void __RPC_STUB IAcadTable_GetTextString_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetTextString_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ int nContent,
    /* [in] */ BSTR Text);


void __RPC_STUB IAcadTable_SetTextString_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetFieldId2_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ int nContent,
    /* [retval][out] */ LONG_PTR *pAcDbObjectId);


void __RPC_STUB IAcadTable_GetFieldId2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetFieldId2_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ int nContent,
    /* [in] */ LONG_PTR acDbObjectId,
    /* [in] */ AcCellOption nflag);


void __RPC_STUB IAcadTable_SetFieldId2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetBlockTableRecordId2_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ int nContent,
    /* [retval][out] */ LONG_PTR *pAcDbObjectId);


void __RPC_STUB IAcadTable_GetBlockTableRecordId2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetBlockTableRecordId2_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ int nContent,
    /* [in] */ LONG_PTR blkId,
    /* [in] */ VARIANT_BOOL autoFit);


void __RPC_STUB IAcadTable_SetBlockTableRecordId2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetBlockAttributeValue2_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ int nContent,
    /* [in] */ LONG_PTR blkId,
    /* [retval][out] */ BSTR *Value);


void __RPC_STUB IAcadTable_GetBlockAttributeValue2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetBlockAttributeValue2_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ int nContent,
    /* [in] */ LONG_PTR blkId,
    /* [in] */ BSTR Value);


void __RPC_STUB IAcadTable_SetBlockAttributeValue2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetCustomData_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ BSTR szKey,
    /* [out] */ VARIANT *pData);


void __RPC_STUB IAcadTable_GetCustomData_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetCustomData_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ BSTR szKey,
    /* [in] */ VARIANT data);


void __RPC_STUB IAcadTable_SetCustomData_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetCellStyle_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [retval][out] */ BSTR *pCellStyle);


void __RPC_STUB IAcadTable_GetCellStyle_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetCellStyle_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ BSTR szCellStyle);


void __RPC_STUB IAcadTable_SetCellStyle_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetContentColor2_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ int nContent,
    /* [retval][out] */ IAcadAcCmColor **pColor);


void __RPC_STUB IAcadTable_GetContentColor2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetContentColor2_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ int nContent,
    /* [in] */ IAcadAcCmColor *pColor);


void __RPC_STUB IAcadTable_SetContentColor2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetDataType2_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ int nContent,
    /* [out] */ AcValueDataType *pDataType,
    /* [out] */ AcValueUnitType *pUnitType);


void __RPC_STUB IAcadTable_GetDataType2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetDataType2_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ int nContent,
    /* [in] */ AcValueDataType dataType,
    /* [in] */ AcValueUnitType unitType);


void __RPC_STUB IAcadTable_SetDataType2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetTextStyle2_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ int nContent,
    /* [retval][out] */ BSTR *pbstrStyleName);


void __RPC_STUB IAcadTable_GetTextStyle2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetTextStyle2_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ int nContent,
    /* [in] */ BSTR bstrStyleName);


void __RPC_STUB IAcadTable_SetTextStyle2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetTextHeight2_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ int nContent,
    /* [retval][out] */ double *pHeight);


void __RPC_STUB IAcadTable_GetTextHeight2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetTextHeight2_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ int nContent,
    /* [in] */ double Height);


void __RPC_STUB IAcadTable_SetTextHeight2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetRotation_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ int nContent,
    /* [retval][out] */ double *pValue);


void __RPC_STUB IAcadTable_GetRotation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetRotation_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ int nContent,
    /* [in] */ double Value);


void __RPC_STUB IAcadTable_SetRotation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetAutoScale2_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ int nContent,
    /* [retval][out] */ VARIANT_BOOL *bAutoScale);


void __RPC_STUB IAcadTable_GetAutoScale2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetAutoScale2_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ int nContent,
    /* [in] */ VARIANT_BOOL bAutoFit);


void __RPC_STUB IAcadTable_SetAutoScale2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetScale_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ int nContent,
    /* [retval][out] */ double *pScale);


void __RPC_STUB IAcadTable_GetScale_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetScale_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ int nContent,
    /* [in] */ double scale);


void __RPC_STUB IAcadTable_SetScale_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_RemoveAllOverrides_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol);


void __RPC_STUB IAcadTable_RemoveAllOverrides_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetGridLineWeight2_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ AcGridLineType nGridLineType,
    /* [retval][out] */ ACAD_LWEIGHT *plineweight);


void __RPC_STUB IAcadTable_GetGridLineWeight2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetGridLineWeight2_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ AcGridLineType nGridLineType,
    /* [in] */ ACAD_LWEIGHT Lineweight);


void __RPC_STUB IAcadTable_SetGridLineWeight2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetGridLinetype_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ AcGridLineType nGridLineType,
    /* [retval][out] */ LONG_PTR *pacDbObjId);


void __RPC_STUB IAcadTable_GetGridLinetype_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetGridLinetype_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ AcGridLineType nGridLineType,
    /* [in] */ LONG_PTR idLinetype);


void __RPC_STUB IAcadTable_SetGridLinetype_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetGridColor2_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ AcGridLineType nGridLineType,
    /* [retval][out] */ IAcadAcCmColor **pColor);


void __RPC_STUB IAcadTable_GetGridColor2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetGridColor2_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ AcGridLineType nGridLineType,
    /* [in] */ IAcadAcCmColor *pColor);


void __RPC_STUB IAcadTable_SetGridColor2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetGridVisibility2_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ AcGridLineType nGridLineType,
    /* [retval][out] */ VARIANT_BOOL *bVisible);


void __RPC_STUB IAcadTable_GetGridVisibility2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetGridVisibility2_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ AcGridLineType nGridLineType,
    /* [in] */ VARIANT_BOOL bVisible);


void __RPC_STUB IAcadTable_SetGridVisibility2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetGridDoubleLineSpacing_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ AcGridLineType nGridLineType,
    /* [retval][out] */ double *pValue);


void __RPC_STUB IAcadTable_GetGridDoubleLineSpacing_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetGridDoubleLineSpacing_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ AcGridLineType nGridLineType,
    /* [in] */ double fSpacing);


void __RPC_STUB IAcadTable_SetGridDoubleLineSpacing_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_put_EnableBreak_Proxy( 
    IAcadTable * This,
    /* [in] */ VARIANT_BOOL rhs);


void __RPC_STUB IAcadTable_put_EnableBreak_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetBreakHeight_Proxy( 
    IAcadTable * This,
    /* [in] */ int nIndex,
    /* [retval][out] */ double *pHeight);


void __RPC_STUB IAcadTable_GetBreakHeight_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetBreakHeight_Proxy( 
    IAcadTable * This,
    /* [in] */ int nIndex,
    /* [in] */ double Height);


void __RPC_STUB IAcadTable_SetBreakHeight_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetContentType_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [retval][out] */ AcCellContentType *pType);


void __RPC_STUB IAcadTable_GetContentType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetMargin_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ AcCellMargin nMargin,
    /* [retval][out] */ double *pValue);


void __RPC_STUB IAcadTable_GetMargin_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetMargin_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ AcCellMargin nMargins,
    /* [in] */ double fMargin);


void __RPC_STUB IAcadTable_SetMargin_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetContentLayout_Proxy( 
    IAcadTable * This,
    /* [in] */ int row,
    /* [in] */ int col,
    /* [retval][out] */ AcCellContentLayout *pLayout);


void __RPC_STUB IAcadTable_GetContentLayout_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetContentLayout_Proxy( 
    IAcadTable * This,
    /* [in] */ int row,
    /* [in] */ int col,
    /* [in] */ AcCellContentLayout nLayout);


void __RPC_STUB IAcadTable_SetContentLayout_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetOverride_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ int nContent,
    /* [retval][out] */ AcCellProperty *pValue);


void __RPC_STUB IAcadTable_GetOverride_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetOverride_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ int nContent,
    /* [in] */ AcCellProperty nProp);


void __RPC_STUB IAcadTable_SetOverride_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetGridLineStyle_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ AcGridLineType nGridLineType,
    /* [retval][out] */ AcGridLineStyle *pStyle);


void __RPC_STUB IAcadTable_GetGridLineStyle_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetGridLineStyle_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ AcGridLineType nGridLineTypes,
    /* [in] */ AcGridLineStyle nLineStyle);


void __RPC_STUB IAcadTable_SetGridLineStyle_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_InsertRowsAndInherit_Proxy( 
    IAcadTable * This,
    /* [in] */ int nIndex,
    /* [in] */ int nInheritFrom,
    /* [in] */ int nNumRows);


void __RPC_STUB IAcadTable_InsertRowsAndInherit_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_InsertColumnsAndInherit_Proxy( 
    IAcadTable * This,
    /* [in] */ int col,
    /* [in] */ int nInheritFrom,
    /* [in] */ int nNumCols);


void __RPC_STUB IAcadTable_InsertColumnsAndInherit_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetHasFormula_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ int nContent,
    /* [retval][out] */ VARIANT_BOOL *bValue);


void __RPC_STUB IAcadTable_GetHasFormula_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetFormula_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ int nContent,
    /* [retval][out] */ BSTR *pszFormula);


void __RPC_STUB IAcadTable_GetFormula_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetFormula_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ int nContent,
    /* [in] */ BSTR pszFormula);


void __RPC_STUB IAcadTable_SetFormula_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_IsContentEditable_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [retval][out] */ VARIANT_BOOL *bValue);


void __RPC_STUB IAcadTable_IsContentEditable_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_IsFormatEditable_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [retval][out] */ VARIANT_BOOL *bValue);


void __RPC_STUB IAcadTable_IsFormatEditable_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetCellState_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [retval][out] */ AcCellState *pCellState);


void __RPC_STUB IAcadTable_GetCellState_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetCellState_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ AcCellState nLock);


void __RPC_STUB IAcadTable_SetCellState_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_EnableMergeAll_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ VARIANT_BOOL bEnable);


void __RPC_STUB IAcadTable_EnableMergeAll_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_IsMergeAllEnabled_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [retval][out] */ VARIANT_BOOL *bValue);


void __RPC_STUB IAcadTable_IsMergeAllEnabled_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_get_BreaksEnabled_Proxy( 
    IAcadTable * This,
    /* [retval][out] */ VARIANT_BOOL *bEnabled);


void __RPC_STUB IAcadTable_get_BreaksEnabled_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_put_BreaksEnabled_Proxy( 
    IAcadTable * This,
    /* [in] */ VARIANT_BOOL bEnabled);


void __RPC_STUB IAcadTable_put_BreaksEnabled_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_get_RepeatTopLabels_Proxy( 
    IAcadTable * This,
    /* [retval][out] */ VARIANT_BOOL *bEnabled);


void __RPC_STUB IAcadTable_get_RepeatTopLabels_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_put_RepeatTopLabels_Proxy( 
    IAcadTable * This,
    /* [in] */ VARIANT_BOOL bEnabled);


void __RPC_STUB IAcadTable_put_RepeatTopLabels_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_get_RepeatBottomLabels_Proxy( 
    IAcadTable * This,
    /* [retval][out] */ VARIANT_BOOL *bEnabled);


void __RPC_STUB IAcadTable_get_RepeatBottomLabels_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_put_RepeatBottomLabels_Proxy( 
    IAcadTable * This,
    /* [in] */ VARIANT_BOOL bEnabled);


void __RPC_STUB IAcadTable_put_RepeatBottomLabels_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_get_TableBreakFlowDirection_Proxy( 
    IAcadTable * This,
    /* [retval][out] */ AcTableFlowDirection *pDir);


void __RPC_STUB IAcadTable_get_TableBreakFlowDirection_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_put_TableBreakFlowDirection_Proxy( 
    IAcadTable * This,
    /* [in] */ AcTableFlowDirection pDir);


void __RPC_STUB IAcadTable_put_TableBreakFlowDirection_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_get_AllowManualPositions_Proxy( 
    IAcadTable * This,
    /* [retval][out] */ VARIANT_BOOL *bEnabled);


void __RPC_STUB IAcadTable_get_AllowManualPositions_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_put_AllowManualPositions_Proxy( 
    IAcadTable * This,
    /* [in] */ VARIANT_BOOL bEnabled);


void __RPC_STUB IAcadTable_put_AllowManualPositions_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_get_AllowManualHeights_Proxy( 
    IAcadTable * This,
    /* [retval][out] */ VARIANT_BOOL *bEnabled);


void __RPC_STUB IAcadTable_get_AllowManualHeights_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_put_AllowManualHeights_Proxy( 
    IAcadTable * This,
    /* [in] */ VARIANT_BOOL bEnabled);


void __RPC_STUB IAcadTable_put_AllowManualHeights_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_get_TableBreakHeight_Proxy( 
    IAcadTable * This,
    /* [retval][out] */ double *pHeight);


void __RPC_STUB IAcadTable_get_TableBreakHeight_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_put_TableBreakHeight_Proxy( 
    IAcadTable * This,
    /* [in] */ double pHeight);


void __RPC_STUB IAcadTable_put_TableBreakHeight_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_get_BreakSpacing_Proxy( 
    IAcadTable * This,
    /* [retval][out] */ double *pSpacing);


void __RPC_STUB IAcadTable_get_BreakSpacing_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_put_BreakSpacing_Proxy( 
    IAcadTable * This,
    /* [in] */ double pSpacing);


void __RPC_STUB IAcadTable_put_BreakSpacing_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_GetColumnName_Proxy( 
    IAcadTable * This,
    /* [in] */ int nIndex,
    /* [retval][out] */ BSTR *Name);


void __RPC_STUB IAcadTable_GetColumnName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetColumnName_Proxy( 
    IAcadTable * This,
    /* [in] */ int nIndex,
    /* [in] */ BSTR Name);


void __RPC_STUB IAcadTable_SetColumnName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IAcadTable_SetToolTip_Proxy( 
    IAcadTable * This,
    /* [in] */ int nRow,
    /* [in] */ int nCol,
    /* [in] */ BSTR tip);


void __RPC_STUB IAcadTable_SetToolTip_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IAcadTable_INTERFACE_DEFINED__ */


#ifndef __IAcadOle_INTERFACE_DEFINED__
#define __IAcadOle_INTERFACE_DEFINED__

/* interface IAcadOle */
/* [object][oleautomation][dual][helpstring][uuid] */ 


EXTERN_C const IID IID_IAcadOle;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("5A90AA1A-8525-49B7-9F44-62E6A958DE82")
    IAcadOle : public IAcadEntity
    {
    public:
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_InsertionPoint( 
            /* [retval][out] */ VARIANT *insPoint) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_InsertionPoint( 
            /* [in] */ VARIANT insPoint) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_Rotation( 
            /* [retval][out] */ ACAD_ANGLE *rot) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_Rotation( 
            /* [in] */ ACAD_ANGLE rot) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_Width( 
            /* [retval][out] */ double *Width) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_Width( 
            /* [in] */ double Width) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_Height( 
            /* [retval][out] */ double *Height) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_Height( 
            /* [in] */ double Height) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_ScaleWidth( 
            /* [retval][out] */ double *swidth) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_ScaleWidth( 
            /* [in] */ double swidth) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_ScaleHeight( 
            /* [retval][out] */ double *sheight) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_ScaleHeight( 
            /* [in] */ double sheight) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_LockAspectRatio( 
            /* [retval][out] */ VARIANT_BOOL *aspect) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_LockAspectRatio( 
            /* [in] */ VARIANT_BOOL aspect) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_OleItemType( 
            /* [retval][out] */ AcOleType *pType) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_OleItemType( 
            /* [in] */ AcOleType pType) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_OlePlotQuality( 
            /* [retval][out] */ AcOlePlotQuality *pPQuality) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_OlePlotQuality( 
            /* [in] */ AcOlePlotQuality pPQuality) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_OleSourceApp( 
            /* [retval][out] */ BSTR *srcApp) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_OleSourceApp( 
            /* [in] */ BSTR srcApp) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct IAcadOleVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAcadOle * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAcadOle * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAcadOle * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IAcadOle * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IAcadOle * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IAcadOle * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IAcadOle * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Handle )( 
            IAcadOle * This,
            /* [retval][out] */ BSTR *Handle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ObjectName )( 
            IAcadOle * This,
            /* [retval][out] */ BSTR *ObjectName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetXData )( 
            IAcadOle * This,
            /* [in] */ BSTR AppName,
            /* [out] */ VARIANT *XDataType,
            /* [out] */ VARIANT *XDataValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetXData )( 
            IAcadOle * This,
            /* [in] */ VARIANT XDataType,
            /* [in] */ VARIANT XDataValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Delete )( 
            IAcadOle * This);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ObjectID )( 
            IAcadOle * This,
            /* [retval][out] */ LONG_PTR *ObjectID);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Application )( 
            IAcadOle * This,
            /* [retval][out] */ IDispatch **ApplicationObject);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Database )( 
            IAcadOle * This,
            /* [retval][out] */ IAcadDatabase **pDatabase);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_HasExtensionDictionary )( 
            IAcadOle * This,
            /* [retval][out] */ VARIANT_BOOL *bHasDictionary);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetExtensionDictionary )( 
            IAcadOle * This,
            /* [retval][out] */ IAcadDictionary **pExtDictionary);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_OwnerID )( 
            IAcadOle * This,
            /* [retval][out] */ LONG_PTR *OwnerID);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Document )( 
            IAcadOle * This,
            /* [retval][out] */ IDispatch **pDocument);
        
        /* [helpstring][hidden][id] */ HRESULT ( STDMETHODCALLTYPE *Erase )( 
            IAcadOle * This);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_TrueColor )( 
            IAcadOle * This,
            /* [retval][out] */ IAcadAcCmColor **pColor);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_TrueColor )( 
            IAcadOle * This,
            /* [in] */ IAcadAcCmColor *pColor);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Layer )( 
            IAcadOle * This,
            /* [retval][out] */ BSTR *Layer);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Layer )( 
            IAcadOle * This,
            /* [in] */ BSTR Layer);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Linetype )( 
            IAcadOle * This,
            /* [retval][out] */ BSTR *Linetype);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Linetype )( 
            IAcadOle * This,
            /* [in] */ BSTR Linetype);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_LinetypeScale )( 
            IAcadOle * This,
            /* [retval][out] */ ACAD_NOUNITS *ltScale);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_LinetypeScale )( 
            IAcadOle * This,
            /* [in] */ ACAD_NOUNITS ltScale);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Visible )( 
            IAcadOle * This,
            /* [retval][out] */ VARIANT_BOOL *bVisible);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Visible )( 
            IAcadOle * This,
            /* [in] */ VARIANT_BOOL bVisible);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ArrayPolar )( 
            IAcadOle * This,
            /* [in] */ int NumberOfObjects,
            /* [in] */ double AngleToFill,
            /* [in] */ VARIANT CenterPoint,
            /* [retval][out] */ VARIANT *pArrayObjs);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ArrayRectangular )( 
            IAcadOle * This,
            /* [in] */ int NumberOfRows,
            /* [in] */ int NumberOfColumns,
            /* [in] */ int NumberOfLevels,
            /* [in] */ double DistBetweenRows,
            /* [in] */ double DistBetweenCols,
            /* [in] */ double DistBetweenLevels,
            /* [retval][out] */ VARIANT *pArrayObjs);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Highlight )( 
            IAcadOle * This,
            /* [in] */ VARIANT_BOOL HighlightFlag);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Copy )( 
            IAcadOle * This,
            /* [retval][out] */ IDispatch **pCopyObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Move )( 
            IAcadOle * This,
            /* [in] */ VARIANT FromPoint,
            /* [in] */ VARIANT ToPoint);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Rotate )( 
            IAcadOle * This,
            /* [in] */ VARIANT BasePoint,
            /* [in] */ double RotationAngle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Rotate3D )( 
            IAcadOle * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [in] */ double RotationAngle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Mirror )( 
            IAcadOle * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [retval][out] */ IDispatch **pMirrorObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Mirror3D )( 
            IAcadOle * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [in] */ VARIANT point3,
            /* [retval][out] */ IDispatch **pMirrorObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ScaleEntity )( 
            IAcadOle * This,
            /* [in] */ VARIANT BasePoint,
            /* [in] */ double ScaleFactor);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *TransformBy )( 
            IAcadOle * This,
            /* [in] */ VARIANT TransformationMatrix);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Update )( 
            IAcadOle * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetBoundingBox )( 
            IAcadOle * This,
            /* [out] */ VARIANT *MinPoint,
            /* [out] */ VARIANT *MaxPoint);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *IntersectWith )( 
            IAcadOle * This,
            /* [in] */ IDispatch *IntersectObject,
            /* [in] */ AcExtendOption option,
            /* [retval][out] */ VARIANT *intPoints);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_PlotStyleName )( 
            IAcadOle * This,
            /* [retval][out] */ BSTR *plotStyle);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_PlotStyleName )( 
            IAcadOle * This,
            /* [in] */ BSTR plotStyle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Lineweight )( 
            IAcadOle * This,
            /* [retval][out] */ ACAD_LWEIGHT *Lineweight);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Lineweight )( 
            IAcadOle * This,
            /* [in] */ ACAD_LWEIGHT Lineweight);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Hyperlinks )( 
            IAcadOle * This,
            /* [retval][out] */ IAcadHyperlinks **Hyperlinks);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Material )( 
            IAcadOle * This,
            /* [retval][out] */ BSTR *Material);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Material )( 
            IAcadOle * This,
            /* [in] */ BSTR Material);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EntityName )( 
            IAcadOle * This,
            /* [retval][out] */ BSTR *EntityName);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EntityType )( 
            IAcadOle * This,
            /* [retval][out] */ long *entType);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_color )( 
            IAcadOle * This,
            /* [retval][out] */ ACAD_COLOR *color);
        
        /* [helpstring][hidden][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_color )( 
            IAcadOle * This,
            /* [in] */ ACAD_COLOR color);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_InsertionPoint )( 
            IAcadOle * This,
            /* [retval][out] */ VARIANT *insPoint);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_InsertionPoint )( 
            IAcadOle * This,
            /* [in] */ VARIANT insPoint);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Rotation )( 
            IAcadOle * This,
            /* [retval][out] */ ACAD_ANGLE *rot);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Rotation )( 
            IAcadOle * This,
            /* [in] */ ACAD_ANGLE rot);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Width )( 
            IAcadOle * This,
            /* [retval][out] */ double *Width);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Width )( 
            IAcadOle * This,
            /* [in] */ double Width);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Height )( 
            IAcadOle * This,
            /* [retval][out] */ double *Height);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Height )( 
            IAcadOle * This,
            /* [in] */ double Height);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ScaleWidth )( 
            IAcadOle * This,
            /* [retval][out] */ double *swidth);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ScaleWidth )( 
            IAcadOle * This,
            /* [in] */ double swidth);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ScaleHeight )( 
            IAcadOle * This,
            /* [retval][out] */ double *sheight);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ScaleHeight )( 
            IAcadOle * This,
            /* [in] */ double sheight);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_LockAspectRatio )( 
            IAcadOle * This,
            /* [retval][out] */ VARIANT_BOOL *aspect);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_LockAspectRatio )( 
            IAcadOle * This,
            /* [in] */ VARIANT_BOOL aspect);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_OleItemType )( 
            IAcadOle * This,
            /* [retval][out] */ AcOleType *pType);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_OleItemType )( 
            IAcadOle * This,
            /* [in] */ AcOleType pType);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_OlePlotQuality )( 
            IAcadOle * This,
            /* [retval][out] */ AcOlePlotQuality *pPQuality);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_OlePlotQuality )( 
            IAcadOle * This,
            /* [in] */ AcOlePlotQuality pPQuality);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_OleSourceApp )( 
            IAcadOle * This,
            /* [retval][out] */ BSTR *srcApp);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_OleSourceApp )( 
            IAcadOle * This,
            /* [in] */ BSTR srcApp);
        
        END_INTERFACE
    } IAcadOleVtbl;

    interface IAcadOle
    {
        CONST_VTBL struct IAcadOleVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAcadOle_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAcadOle_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAcadOle_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAcadOle_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IAcadOle_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IAcadOle_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IAcadOle_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IAcadOle_get_Handle(This,Handle)	\
    ( (This)->lpVtbl -> get_Handle(This,Handle) ) 

#define IAcadOle_get_ObjectName(This,ObjectName)	\
    ( (This)->lpVtbl -> get_ObjectName(This,ObjectName) ) 

#define IAcadOle_GetXData(This,AppName,XDataType,XDataValue)	\
    ( (This)->lpVtbl -> GetXData(This,AppName,XDataType,XDataValue) ) 

#define IAcadOle_SetXData(This,XDataType,XDataValue)	\
    ( (This)->lpVtbl -> SetXData(This,XDataType,XDataValue) ) 

#define IAcadOle_Delete(This)	\
    ( (This)->lpVtbl -> Delete(This) ) 

#define IAcadOle_get_ObjectID(This,ObjectID)	\
    ( (This)->lpVtbl -> get_ObjectID(This,ObjectID) ) 

#define IAcadOle_get_Application(This,ApplicationObject)	\
    ( (This)->lpVtbl -> get_Application(This,ApplicationObject) ) 

#define IAcadOle_get_Database(This,pDatabase)	\
    ( (This)->lpVtbl -> get_Database(This,pDatabase) ) 

#define IAcadOle_get_HasExtensionDictionary(This,bHasDictionary)	\
    ( (This)->lpVtbl -> get_HasExtensionDictionary(This,bHasDictionary) ) 

#define IAcadOle_GetExtensionDictionary(This,pExtDictionary)	\
    ( (This)->lpVtbl -> GetExtensionDictionary(This,pExtDictionary) ) 

#define IAcadOle_get_OwnerID(This,OwnerID)	\
    ( (This)->lpVtbl -> get_OwnerID(This,OwnerID) ) 

#define IAcadOle_get_Document(This,pDocument)	\
    ( (This)->lpVtbl -> get_Document(This,pDocument) ) 

#define IAcadOle_Erase(This)	\
    ( (This)->lpVtbl -> Erase(This) ) 


#define IAcadOle_get_TrueColor(This,pColor)	\
    ( (This)->lpVtbl -> get_TrueColor(This,pColor) ) 

#define IAcadOle_put_TrueColor(This,pColor)	\
    ( (This)->lpVtbl -> put_TrueColor(This,pColor) ) 

#define IAcadOle_get_Layer(This,Layer)	\
    ( (This)->lpVtbl -> get_Layer(This,Layer) ) 

#define IAcadOle_put_Layer(This,Layer)	\
    ( (This)->lpVtbl -> put_Layer(This,Layer) ) 

#define IAcadOle_get_Linetype(This,Linetype)	\
    ( (This)->lpVtbl -> get_Linetype(This,Linetype) ) 

#define IAcadOle_put_Linetype(This,Linetype)	\
    ( (This)->lpVtbl -> put_Linetype(This,Linetype) ) 

#define IAcadOle_get_LinetypeScale(This,ltScale)	\
    ( (This)->lpVtbl -> get_LinetypeScale(This,ltScale) ) 

#define IAcadOle_put_LinetypeScale(This,ltScale)	\
    ( (This)->lpVtbl -> put_LinetypeScale(This,ltScale) ) 

#define IAcadOle_get_Visible(This,bVisible)	\
    ( (This)->lpVtbl -> get_Visible(This,bVisible) ) 

#define IAcadOle_put_Visible(This,bVisible)	\
    ( (This)->lpVtbl -> put_Visible(This,bVisible) ) 

#define IAcadOle_ArrayPolar(This,NumberOfObjects,AngleToFill,CenterPoint,pArrayObjs)	\
    ( (This)->lpVtbl -> ArrayPolar(This,NumberOfObjects,AngleToFill,CenterPoint,pArrayObjs) ) 

#define IAcadOle_ArrayRectangular(This,NumberOfRows,NumberOfColumns,NumberOfLevels,DistBetweenRows,DistBetweenCols,DistBetweenLevels,pArrayObjs)	\
    ( (This)->lpVtbl -> ArrayRectangular(This,NumberOfRows,NumberOfColumns,NumberOfLevels,DistBetweenRows,DistBetweenCols,DistBetweenLevels,pArrayObjs) ) 

#define IAcadOle_Highlight(This,HighlightFlag)	\
    ( (This)->lpVtbl -> Highlight(This,HighlightFlag) ) 

#define IAcadOle_Copy(This,pCopyObj)	\
    ( (This)->lpVtbl -> Copy(This,pCopyObj) ) 

#define IAcadOle_Move(This,FromPoint,ToPoint)	\
    ( (This)->lpVtbl -> Move(This,FromPoint,ToPoint) ) 

#define IAcadOle_Rotate(This,BasePoint,RotationAngle)	\
    ( (This)->lpVtbl -> Rotate(This,BasePoint,RotationAngle) ) 

#define IAcadOle_Rotate3D(This,Point1,Point2,RotationAngle)	\
    ( (This)->lpVtbl -> Rotate3D(This,Point1,Point2,RotationAngle) ) 

#define IAcadOle_Mirror(This,Point1,Point2,pMirrorObj)	\
    ( (This)->lpVtbl -> Mirror(This,Point1,Point2,pMirrorObj) ) 

#define IAcadOle_Mirror3D(This,Point1,Point2,point3,pMirrorObj)	\
    ( (This)->lpVtbl -> Mirror3D(This,Point1,Point2,point3,pMirrorObj) ) 

#define IAcadOle_ScaleEntity(This,BasePoint,ScaleFactor)	\
    ( (This)->lpVtbl -> ScaleEntity(This,BasePoint,ScaleFactor) ) 

#define IAcadOle_TransformBy(This,TransformationMatrix)	\
    ( (This)->lpVtbl -> TransformBy(This,TransformationMatrix) ) 

#define IAcadOle_Update(This)	\
    ( (This)->lpVtbl -> Update(This) ) 

#define IAcadOle_GetBoundingBox(This,MinPoint,MaxPoint)	\
    ( (This)->lpVtbl -> GetBoundingBox(This,MinPoint,MaxPoint) ) 

#define IAcadOle_IntersectWith(This,IntersectObject,option,intPoints)	\
    ( (This)->lpVtbl -> IntersectWith(This,IntersectObject,option,intPoints) ) 

#define IAcadOle_get_PlotStyleName(This,plotStyle)	\
    ( (This)->lpVtbl -> get_PlotStyleName(This,plotStyle) ) 

#define IAcadOle_put_PlotStyleName(This,plotStyle)	\
    ( (This)->lpVtbl -> put_PlotStyleName(This,plotStyle) ) 

#define IAcadOle_get_Lineweight(This,Lineweight)	\
    ( (This)->lpVtbl -> get_Lineweight(This,Lineweight) ) 

#define IAcadOle_put_Lineweight(This,Lineweight)	\
    ( (This)->lpVtbl -> put_Lineweight(This,Lineweight) ) 

#define IAcadOle_get_Hyperlinks(This,Hyperlinks)	\
    ( (This)->lpVtbl -> get_Hyperlinks(This,Hyperlinks) ) 

#define IAcadOle_get_Material(This,Material)	\
    ( (This)->lpVtbl -> get_Material(This,Material) ) 

#define IAcadOle_put_Material(This,Material)	\
    ( (This)->lpVtbl -> put_Material(This,Material) ) 

#define IAcadOle_get_EntityName(This,EntityName)	\
    ( (This)->lpVtbl -> get_EntityName(This,EntityName) ) 

#define IAcadOle_get_EntityType(This,entType)	\
    ( (This)->lpVtbl -> get_EntityType(This,entType) ) 

#define IAcadOle_get_color(This,color)	\
    ( (This)->lpVtbl -> get_color(This,color) ) 

#define IAcadOle_put_color(This,color)	\
    ( (This)->lpVtbl -> put_color(This,color) ) 


#define IAcadOle_get_InsertionPoint(This,insPoint)	\
    ( (This)->lpVtbl -> get_InsertionPoint(This,insPoint) ) 

#define IAcadOle_put_InsertionPoint(This,insPoint)	\
    ( (This)->lpVtbl -> put_InsertionPoint(This,insPoint) ) 

#define IAcadOle_get_Rotation(This,rot)	\
    ( (This)->lpVtbl -> get_Rotation(This,rot) ) 

#define IAcadOle_put_Rotation(This,rot)	\
    ( (This)->lpVtbl -> put_Rotation(This,rot) ) 

#define IAcadOle_get_Width(This,Width)	\
    ( (This)->lpVtbl -> get_Width(This,Width) ) 

#define IAcadOle_put_Width(This,Width)	\
    ( (This)->lpVtbl -> put_Width(This,Width) ) 

#define IAcadOle_get_Height(This,Height)	\
    ( (This)->lpVtbl -> get_Height(This,Height) ) 

#define IAcadOle_put_Height(This,Height)	\
    ( (This)->lpVtbl -> put_Height(This,Height) ) 

#define IAcadOle_get_ScaleWidth(This,swidth)	\
    ( (This)->lpVtbl -> get_ScaleWidth(This,swidth) ) 

#define IAcadOle_put_ScaleWidth(This,swidth)	\
    ( (This)->lpVtbl -> put_ScaleWidth(This,swidth) ) 

#define IAcadOle_get_ScaleHeight(This,sheight)	\
    ( (This)->lpVtbl -> get_ScaleHeight(This,sheight) ) 

#define IAcadOle_put_ScaleHeight(This,sheight)	\
    ( (This)->lpVtbl -> put_ScaleHeight(This,sheight) ) 

#define IAcadOle_get_LockAspectRatio(This,aspect)	\
    ( (This)->lpVtbl -> get_LockAspectRatio(This,aspect) ) 

#define IAcadOle_put_LockAspectRatio(This,aspect)	\
    ( (This)->lpVtbl -> put_LockAspectRatio(This,aspect) ) 

#define IAcadOle_get_OleItemType(This,pType)	\
    ( (This)->lpVtbl -> get_OleItemType(This,pType) ) 

#define IAcadOle_put_OleItemType(This,pType)	\
    ( (This)->lpVtbl -> put_OleItemType(This,pType) ) 

#define IAcadOle_get_OlePlotQuality(This,pPQuality)	\
    ( (This)->lpVtbl -> get_OlePlotQuality(This,pPQuality) ) 

#define IAcadOle_put_OlePlotQuality(This,pPQuality)	\
    ( (This)->lpVtbl -> put_OlePlotQuality(This,pPQuality) ) 

#define IAcadOle_get_OleSourceApp(This,srcApp)	\
    ( (This)->lpVtbl -> get_OleSourceApp(This,srcApp) ) 

#define IAcadOle_put_OleSourceApp(This,srcApp)	\
    ( (This)->lpVtbl -> put_OleSourceApp(This,srcApp) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAcadOle_INTERFACE_DEFINED__ */


#ifndef __IOdaOle_INTERFACE_DEFINED__
#define __IOdaOle_INTERFACE_DEFINED__

/* interface IOdaOle */
/* [object][oleautomation][dual][helpstring][uuid] */ 


EXTERN_C const IID IID_IOdaOle;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("39614E96-FFD9-42EA-99F0-FD7DC498D9B6")
    IOdaOle : public IAcadOle
    {
    public:
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_LinkName( 
            /* [retval][out] */ BSTR *srcApp) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_LinkPath( 
            /* [retval][out] */ BSTR *srcApp) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct IOdaOleVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IOdaOle * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IOdaOle * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IOdaOle * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IOdaOle * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IOdaOle * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IOdaOle * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IOdaOle * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Handle )( 
            IOdaOle * This,
            /* [retval][out] */ BSTR *Handle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ObjectName )( 
            IOdaOle * This,
            /* [retval][out] */ BSTR *ObjectName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetXData )( 
            IOdaOle * This,
            /* [in] */ BSTR AppName,
            /* [out] */ VARIANT *XDataType,
            /* [out] */ VARIANT *XDataValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetXData )( 
            IOdaOle * This,
            /* [in] */ VARIANT XDataType,
            /* [in] */ VARIANT XDataValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Delete )( 
            IOdaOle * This);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ObjectID )( 
            IOdaOle * This,
            /* [retval][out] */ LONG_PTR *ObjectID);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Application )( 
            IOdaOle * This,
            /* [retval][out] */ IDispatch **ApplicationObject);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Database )( 
            IOdaOle * This,
            /* [retval][out] */ IAcadDatabase **pDatabase);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_HasExtensionDictionary )( 
            IOdaOle * This,
            /* [retval][out] */ VARIANT_BOOL *bHasDictionary);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetExtensionDictionary )( 
            IOdaOle * This,
            /* [retval][out] */ IAcadDictionary **pExtDictionary);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_OwnerID )( 
            IOdaOle * This,
            /* [retval][out] */ LONG_PTR *OwnerID);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Document )( 
            IOdaOle * This,
            /* [retval][out] */ IDispatch **pDocument);
        
        /* [helpstring][hidden][id] */ HRESULT ( STDMETHODCALLTYPE *Erase )( 
            IOdaOle * This);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_TrueColor )( 
            IOdaOle * This,
            /* [retval][out] */ IAcadAcCmColor **pColor);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_TrueColor )( 
            IOdaOle * This,
            /* [in] */ IAcadAcCmColor *pColor);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Layer )( 
            IOdaOle * This,
            /* [retval][out] */ BSTR *Layer);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Layer )( 
            IOdaOle * This,
            /* [in] */ BSTR Layer);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Linetype )( 
            IOdaOle * This,
            /* [retval][out] */ BSTR *Linetype);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Linetype )( 
            IOdaOle * This,
            /* [in] */ BSTR Linetype);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_LinetypeScale )( 
            IOdaOle * This,
            /* [retval][out] */ ACAD_NOUNITS *ltScale);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_LinetypeScale )( 
            IOdaOle * This,
            /* [in] */ ACAD_NOUNITS ltScale);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Visible )( 
            IOdaOle * This,
            /* [retval][out] */ VARIANT_BOOL *bVisible);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Visible )( 
            IOdaOle * This,
            /* [in] */ VARIANT_BOOL bVisible);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ArrayPolar )( 
            IOdaOle * This,
            /* [in] */ int NumberOfObjects,
            /* [in] */ double AngleToFill,
            /* [in] */ VARIANT CenterPoint,
            /* [retval][out] */ VARIANT *pArrayObjs);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ArrayRectangular )( 
            IOdaOle * This,
            /* [in] */ int NumberOfRows,
            /* [in] */ int NumberOfColumns,
            /* [in] */ int NumberOfLevels,
            /* [in] */ double DistBetweenRows,
            /* [in] */ double DistBetweenCols,
            /* [in] */ double DistBetweenLevels,
            /* [retval][out] */ VARIANT *pArrayObjs);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Highlight )( 
            IOdaOle * This,
            /* [in] */ VARIANT_BOOL HighlightFlag);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Copy )( 
            IOdaOle * This,
            /* [retval][out] */ IDispatch **pCopyObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Move )( 
            IOdaOle * This,
            /* [in] */ VARIANT FromPoint,
            /* [in] */ VARIANT ToPoint);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Rotate )( 
            IOdaOle * This,
            /* [in] */ VARIANT BasePoint,
            /* [in] */ double RotationAngle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Rotate3D )( 
            IOdaOle * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [in] */ double RotationAngle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Mirror )( 
            IOdaOle * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [retval][out] */ IDispatch **pMirrorObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Mirror3D )( 
            IOdaOle * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [in] */ VARIANT point3,
            /* [retval][out] */ IDispatch **pMirrorObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ScaleEntity )( 
            IOdaOle * This,
            /* [in] */ VARIANT BasePoint,
            /* [in] */ double ScaleFactor);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *TransformBy )( 
            IOdaOle * This,
            /* [in] */ VARIANT TransformationMatrix);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Update )( 
            IOdaOle * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetBoundingBox )( 
            IOdaOle * This,
            /* [out] */ VARIANT *MinPoint,
            /* [out] */ VARIANT *MaxPoint);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *IntersectWith )( 
            IOdaOle * This,
            /* [in] */ IDispatch *IntersectObject,
            /* [in] */ AcExtendOption option,
            /* [retval][out] */ VARIANT *intPoints);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_PlotStyleName )( 
            IOdaOle * This,
            /* [retval][out] */ BSTR *plotStyle);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_PlotStyleName )( 
            IOdaOle * This,
            /* [in] */ BSTR plotStyle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Lineweight )( 
            IOdaOle * This,
            /* [retval][out] */ ACAD_LWEIGHT *Lineweight);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Lineweight )( 
            IOdaOle * This,
            /* [in] */ ACAD_LWEIGHT Lineweight);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Hyperlinks )( 
            IOdaOle * This,
            /* [retval][out] */ IAcadHyperlinks **Hyperlinks);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Material )( 
            IOdaOle * This,
            /* [retval][out] */ BSTR *Material);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Material )( 
            IOdaOle * This,
            /* [in] */ BSTR Material);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EntityName )( 
            IOdaOle * This,
            /* [retval][out] */ BSTR *EntityName);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EntityType )( 
            IOdaOle * This,
            /* [retval][out] */ long *entType);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_color )( 
            IOdaOle * This,
            /* [retval][out] */ ACAD_COLOR *color);
        
        /* [helpstring][hidden][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_color )( 
            IOdaOle * This,
            /* [in] */ ACAD_COLOR color);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_InsertionPoint )( 
            IOdaOle * This,
            /* [retval][out] */ VARIANT *insPoint);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_InsertionPoint )( 
            IOdaOle * This,
            /* [in] */ VARIANT insPoint);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Rotation )( 
            IOdaOle * This,
            /* [retval][out] */ ACAD_ANGLE *rot);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Rotation )( 
            IOdaOle * This,
            /* [in] */ ACAD_ANGLE rot);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Width )( 
            IOdaOle * This,
            /* [retval][out] */ double *Width);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Width )( 
            IOdaOle * This,
            /* [in] */ double Width);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Height )( 
            IOdaOle * This,
            /* [retval][out] */ double *Height);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Height )( 
            IOdaOle * This,
            /* [in] */ double Height);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ScaleWidth )( 
            IOdaOle * This,
            /* [retval][out] */ double *swidth);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ScaleWidth )( 
            IOdaOle * This,
            /* [in] */ double swidth);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ScaleHeight )( 
            IOdaOle * This,
            /* [retval][out] */ double *sheight);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ScaleHeight )( 
            IOdaOle * This,
            /* [in] */ double sheight);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_LockAspectRatio )( 
            IOdaOle * This,
            /* [retval][out] */ VARIANT_BOOL *aspect);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_LockAspectRatio )( 
            IOdaOle * This,
            /* [in] */ VARIANT_BOOL aspect);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_OleItemType )( 
            IOdaOle * This,
            /* [retval][out] */ AcOleType *pType);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_OleItemType )( 
            IOdaOle * This,
            /* [in] */ AcOleType pType);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_OlePlotQuality )( 
            IOdaOle * This,
            /* [retval][out] */ AcOlePlotQuality *pPQuality);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_OlePlotQuality )( 
            IOdaOle * This,
            /* [in] */ AcOlePlotQuality pPQuality);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_OleSourceApp )( 
            IOdaOle * This,
            /* [retval][out] */ BSTR *srcApp);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_OleSourceApp )( 
            IOdaOle * This,
            /* [in] */ BSTR srcApp);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_LinkName )( 
            IOdaOle * This,
            /* [retval][out] */ BSTR *srcApp);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_LinkPath )( 
            IOdaOle * This,
            /* [retval][out] */ BSTR *srcApp);
        
        END_INTERFACE
    } IOdaOleVtbl;

    interface IOdaOle
    {
        CONST_VTBL struct IOdaOleVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IOdaOle_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IOdaOle_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IOdaOle_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IOdaOle_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IOdaOle_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IOdaOle_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IOdaOle_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IOdaOle_get_Handle(This,Handle)	\
    ( (This)->lpVtbl -> get_Handle(This,Handle) ) 

#define IOdaOle_get_ObjectName(This,ObjectName)	\
    ( (This)->lpVtbl -> get_ObjectName(This,ObjectName) ) 

#define IOdaOle_GetXData(This,AppName,XDataType,XDataValue)	\
    ( (This)->lpVtbl -> GetXData(This,AppName,XDataType,XDataValue) ) 

#define IOdaOle_SetXData(This,XDataType,XDataValue)	\
    ( (This)->lpVtbl -> SetXData(This,XDataType,XDataValue) ) 

#define IOdaOle_Delete(This)	\
    ( (This)->lpVtbl -> Delete(This) ) 

#define IOdaOle_get_ObjectID(This,ObjectID)	\
    ( (This)->lpVtbl -> get_ObjectID(This,ObjectID) ) 

#define IOdaOle_get_Application(This,ApplicationObject)	\
    ( (This)->lpVtbl -> get_Application(This,ApplicationObject) ) 

#define IOdaOle_get_Database(This,pDatabase)	\
    ( (This)->lpVtbl -> get_Database(This,pDatabase) ) 

#define IOdaOle_get_HasExtensionDictionary(This,bHasDictionary)	\
    ( (This)->lpVtbl -> get_HasExtensionDictionary(This,bHasDictionary) ) 

#define IOdaOle_GetExtensionDictionary(This,pExtDictionary)	\
    ( (This)->lpVtbl -> GetExtensionDictionary(This,pExtDictionary) ) 

#define IOdaOle_get_OwnerID(This,OwnerID)	\
    ( (This)->lpVtbl -> get_OwnerID(This,OwnerID) ) 

#define IOdaOle_get_Document(This,pDocument)	\
    ( (This)->lpVtbl -> get_Document(This,pDocument) ) 

#define IOdaOle_Erase(This)	\
    ( (This)->lpVtbl -> Erase(This) ) 


#define IOdaOle_get_TrueColor(This,pColor)	\
    ( (This)->lpVtbl -> get_TrueColor(This,pColor) ) 

#define IOdaOle_put_TrueColor(This,pColor)	\
    ( (This)->lpVtbl -> put_TrueColor(This,pColor) ) 

#define IOdaOle_get_Layer(This,Layer)	\
    ( (This)->lpVtbl -> get_Layer(This,Layer) ) 

#define IOdaOle_put_Layer(This,Layer)	\
    ( (This)->lpVtbl -> put_Layer(This,Layer) ) 

#define IOdaOle_get_Linetype(This,Linetype)	\
    ( (This)->lpVtbl -> get_Linetype(This,Linetype) ) 

#define IOdaOle_put_Linetype(This,Linetype)	\
    ( (This)->lpVtbl -> put_Linetype(This,Linetype) ) 

#define IOdaOle_get_LinetypeScale(This,ltScale)	\
    ( (This)->lpVtbl -> get_LinetypeScale(This,ltScale) ) 

#define IOdaOle_put_LinetypeScale(This,ltScale)	\
    ( (This)->lpVtbl -> put_LinetypeScale(This,ltScale) ) 

#define IOdaOle_get_Visible(This,bVisible)	\
    ( (This)->lpVtbl -> get_Visible(This,bVisible) ) 

#define IOdaOle_put_Visible(This,bVisible)	\
    ( (This)->lpVtbl -> put_Visible(This,bVisible) ) 

#define IOdaOle_ArrayPolar(This,NumberOfObjects,AngleToFill,CenterPoint,pArrayObjs)	\
    ( (This)->lpVtbl -> ArrayPolar(This,NumberOfObjects,AngleToFill,CenterPoint,pArrayObjs) ) 

#define IOdaOle_ArrayRectangular(This,NumberOfRows,NumberOfColumns,NumberOfLevels,DistBetweenRows,DistBetweenCols,DistBetweenLevels,pArrayObjs)	\
    ( (This)->lpVtbl -> ArrayRectangular(This,NumberOfRows,NumberOfColumns,NumberOfLevels,DistBetweenRows,DistBetweenCols,DistBetweenLevels,pArrayObjs) ) 

#define IOdaOle_Highlight(This,HighlightFlag)	\
    ( (This)->lpVtbl -> Highlight(This,HighlightFlag) ) 

#define IOdaOle_Copy(This,pCopyObj)	\
    ( (This)->lpVtbl -> Copy(This,pCopyObj) ) 

#define IOdaOle_Move(This,FromPoint,ToPoint)	\
    ( (This)->lpVtbl -> Move(This,FromPoint,ToPoint) ) 

#define IOdaOle_Rotate(This,BasePoint,RotationAngle)	\
    ( (This)->lpVtbl -> Rotate(This,BasePoint,RotationAngle) ) 

#define IOdaOle_Rotate3D(This,Point1,Point2,RotationAngle)	\
    ( (This)->lpVtbl -> Rotate3D(This,Point1,Point2,RotationAngle) ) 

#define IOdaOle_Mirror(This,Point1,Point2,pMirrorObj)	\
    ( (This)->lpVtbl -> Mirror(This,Point1,Point2,pMirrorObj) ) 

#define IOdaOle_Mirror3D(This,Point1,Point2,point3,pMirrorObj)	\
    ( (This)->lpVtbl -> Mirror3D(This,Point1,Point2,point3,pMirrorObj) ) 

#define IOdaOle_ScaleEntity(This,BasePoint,ScaleFactor)	\
    ( (This)->lpVtbl -> ScaleEntity(This,BasePoint,ScaleFactor) ) 

#define IOdaOle_TransformBy(This,TransformationMatrix)	\
    ( (This)->lpVtbl -> TransformBy(This,TransformationMatrix) ) 

#define IOdaOle_Update(This)	\
    ( (This)->lpVtbl -> Update(This) ) 

#define IOdaOle_GetBoundingBox(This,MinPoint,MaxPoint)	\
    ( (This)->lpVtbl -> GetBoundingBox(This,MinPoint,MaxPoint) ) 

#define IOdaOle_IntersectWith(This,IntersectObject,option,intPoints)	\
    ( (This)->lpVtbl -> IntersectWith(This,IntersectObject,option,intPoints) ) 

#define IOdaOle_get_PlotStyleName(This,plotStyle)	\
    ( (This)->lpVtbl -> get_PlotStyleName(This,plotStyle) ) 

#define IOdaOle_put_PlotStyleName(This,plotStyle)	\
    ( (This)->lpVtbl -> put_PlotStyleName(This,plotStyle) ) 

#define IOdaOle_get_Lineweight(This,Lineweight)	\
    ( (This)->lpVtbl -> get_Lineweight(This,Lineweight) ) 

#define IOdaOle_put_Lineweight(This,Lineweight)	\
    ( (This)->lpVtbl -> put_Lineweight(This,Lineweight) ) 

#define IOdaOle_get_Hyperlinks(This,Hyperlinks)	\
    ( (This)->lpVtbl -> get_Hyperlinks(This,Hyperlinks) ) 

#define IOdaOle_get_Material(This,Material)	\
    ( (This)->lpVtbl -> get_Material(This,Material) ) 

#define IOdaOle_put_Material(This,Material)	\
    ( (This)->lpVtbl -> put_Material(This,Material) ) 

#define IOdaOle_get_EntityName(This,EntityName)	\
    ( (This)->lpVtbl -> get_EntityName(This,EntityName) ) 

#define IOdaOle_get_EntityType(This,entType)	\
    ( (This)->lpVtbl -> get_EntityType(This,entType) ) 

#define IOdaOle_get_color(This,color)	\
    ( (This)->lpVtbl -> get_color(This,color) ) 

#define IOdaOle_put_color(This,color)	\
    ( (This)->lpVtbl -> put_color(This,color) ) 


#define IOdaOle_get_InsertionPoint(This,insPoint)	\
    ( (This)->lpVtbl -> get_InsertionPoint(This,insPoint) ) 

#define IOdaOle_put_InsertionPoint(This,insPoint)	\
    ( (This)->lpVtbl -> put_InsertionPoint(This,insPoint) ) 

#define IOdaOle_get_Rotation(This,rot)	\
    ( (This)->lpVtbl -> get_Rotation(This,rot) ) 

#define IOdaOle_put_Rotation(This,rot)	\
    ( (This)->lpVtbl -> put_Rotation(This,rot) ) 

#define IOdaOle_get_Width(This,Width)	\
    ( (This)->lpVtbl -> get_Width(This,Width) ) 

#define IOdaOle_put_Width(This,Width)	\
    ( (This)->lpVtbl -> put_Width(This,Width) ) 

#define IOdaOle_get_Height(This,Height)	\
    ( (This)->lpVtbl -> get_Height(This,Height) ) 

#define IOdaOle_put_Height(This,Height)	\
    ( (This)->lpVtbl -> put_Height(This,Height) ) 

#define IOdaOle_get_ScaleWidth(This,swidth)	\
    ( (This)->lpVtbl -> get_ScaleWidth(This,swidth) ) 

#define IOdaOle_put_ScaleWidth(This,swidth)	\
    ( (This)->lpVtbl -> put_ScaleWidth(This,swidth) ) 

#define IOdaOle_get_ScaleHeight(This,sheight)	\
    ( (This)->lpVtbl -> get_ScaleHeight(This,sheight) ) 

#define IOdaOle_put_ScaleHeight(This,sheight)	\
    ( (This)->lpVtbl -> put_ScaleHeight(This,sheight) ) 

#define IOdaOle_get_LockAspectRatio(This,aspect)	\
    ( (This)->lpVtbl -> get_LockAspectRatio(This,aspect) ) 

#define IOdaOle_put_LockAspectRatio(This,aspect)	\
    ( (This)->lpVtbl -> put_LockAspectRatio(This,aspect) ) 

#define IOdaOle_get_OleItemType(This,pType)	\
    ( (This)->lpVtbl -> get_OleItemType(This,pType) ) 

#define IOdaOle_put_OleItemType(This,pType)	\
    ( (This)->lpVtbl -> put_OleItemType(This,pType) ) 

#define IOdaOle_get_OlePlotQuality(This,pPQuality)	\
    ( (This)->lpVtbl -> get_OlePlotQuality(This,pPQuality) ) 

#define IOdaOle_put_OlePlotQuality(This,pPQuality)	\
    ( (This)->lpVtbl -> put_OlePlotQuality(This,pPQuality) ) 

#define IOdaOle_get_OleSourceApp(This,srcApp)	\
    ( (This)->lpVtbl -> get_OleSourceApp(This,srcApp) ) 

#define IOdaOle_put_OleSourceApp(This,srcApp)	\
    ( (This)->lpVtbl -> put_OleSourceApp(This,srcApp) ) 


#define IOdaOle_get_LinkName(This,srcApp)	\
    ( (This)->lpVtbl -> get_LinkName(This,srcApp) ) 

#define IOdaOle_get_LinkPath(This,srcApp)	\
    ( (This)->lpVtbl -> get_LinkPath(This,srcApp) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IOdaOle_INTERFACE_DEFINED__ */


#ifndef __IAcadDynamicBlockReferenceProperty_INTERFACE_DEFINED__
#define __IAcadDynamicBlockReferenceProperty_INTERFACE_DEFINED__

/* interface IAcadDynamicBlockReferenceProperty */
/* [object][oleautomation][dual][helpstring][uuid] */ 


EXTERN_C const IID IID_IAcadDynamicBlockReferenceProperty;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("671D7077-2C99-4D21-A5D3-A5001F1B8F12")
    IAcadDynamicBlockReferenceProperty : public IDispatch
    {
    public:
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_PropertyName( 
            /* [retval][out] */ BSTR *PropertyName) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_ReadOnly( 
            /* [retval][out] */ VARIANT_BOOL *ReadOnly) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_Show( 
            /* [retval][out] */ VARIANT_BOOL *Show) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_Description( 
            /* [retval][out] */ BSTR *Description) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_AllowedValues( 
            /* [retval][out] */ VARIANT *AllowedValues) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_Value( 
            /* [retval][out] */ VARIANT *Value) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_Value( 
            /* [in] */ VARIANT Value) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_UnitsType( 
            /* [retval][out] */ AcDynamicBlockReferencePropertyUnitsType *Units) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct IAcadDynamicBlockReferencePropertyVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAcadDynamicBlockReferenceProperty * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAcadDynamicBlockReferenceProperty * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAcadDynamicBlockReferenceProperty * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IAcadDynamicBlockReferenceProperty * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IAcadDynamicBlockReferenceProperty * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IAcadDynamicBlockReferenceProperty * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IAcadDynamicBlockReferenceProperty * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_PropertyName )( 
            IAcadDynamicBlockReferenceProperty * This,
            /* [retval][out] */ BSTR *PropertyName);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ReadOnly )( 
            IAcadDynamicBlockReferenceProperty * This,
            /* [retval][out] */ VARIANT_BOOL *ReadOnly);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Show )( 
            IAcadDynamicBlockReferenceProperty * This,
            /* [retval][out] */ VARIANT_BOOL *Show);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Description )( 
            IAcadDynamicBlockReferenceProperty * This,
            /* [retval][out] */ BSTR *Description);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AllowedValues )( 
            IAcadDynamicBlockReferenceProperty * This,
            /* [retval][out] */ VARIANT *AllowedValues);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Value )( 
            IAcadDynamicBlockReferenceProperty * This,
            /* [retval][out] */ VARIANT *Value);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Value )( 
            IAcadDynamicBlockReferenceProperty * This,
            /* [in] */ VARIANT Value);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_UnitsType )( 
            IAcadDynamicBlockReferenceProperty * This,
            /* [retval][out] */ AcDynamicBlockReferencePropertyUnitsType *Units);
        
        END_INTERFACE
    } IAcadDynamicBlockReferencePropertyVtbl;

    interface IAcadDynamicBlockReferenceProperty
    {
        CONST_VTBL struct IAcadDynamicBlockReferencePropertyVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAcadDynamicBlockReferenceProperty_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAcadDynamicBlockReferenceProperty_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAcadDynamicBlockReferenceProperty_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAcadDynamicBlockReferenceProperty_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IAcadDynamicBlockReferenceProperty_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IAcadDynamicBlockReferenceProperty_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IAcadDynamicBlockReferenceProperty_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IAcadDynamicBlockReferenceProperty_get_PropertyName(This,PropertyName)	\
    ( (This)->lpVtbl -> get_PropertyName(This,PropertyName) ) 

#define IAcadDynamicBlockReferenceProperty_get_ReadOnly(This,ReadOnly)	\
    ( (This)->lpVtbl -> get_ReadOnly(This,ReadOnly) ) 

#define IAcadDynamicBlockReferenceProperty_get_Show(This,Show)	\
    ( (This)->lpVtbl -> get_Show(This,Show) ) 

#define IAcadDynamicBlockReferenceProperty_get_Description(This,Description)	\
    ( (This)->lpVtbl -> get_Description(This,Description) ) 

#define IAcadDynamicBlockReferenceProperty_get_AllowedValues(This,AllowedValues)	\
    ( (This)->lpVtbl -> get_AllowedValues(This,AllowedValues) ) 

#define IAcadDynamicBlockReferenceProperty_get_Value(This,Value)	\
    ( (This)->lpVtbl -> get_Value(This,Value) ) 

#define IAcadDynamicBlockReferenceProperty_put_Value(This,Value)	\
    ( (This)->lpVtbl -> put_Value(This,Value) ) 

#define IAcadDynamicBlockReferenceProperty_get_UnitsType(This,Units)	\
    ( (This)->lpVtbl -> get_UnitsType(This,Units) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAcadDynamicBlockReferenceProperty_INTERFACE_DEFINED__ */


#ifndef __IOdaMLineStyles_INTERFACE_DEFINED__
#define __IOdaMLineStyles_INTERFACE_DEFINED__

/* interface IOdaMLineStyles */
/* [object][oleautomation][dual][helpstring][uuid] */ 


EXTERN_C const IID IID_IOdaMLineStyles;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("EAB07C99-ABCA-4880-9B1C-4FD13FC3DE3A")
    IOdaMLineStyles : public IAcadObject
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Item( 
            /* [in] */ VARIANT Index,
            /* [retval][out] */ IOdaMLineStyle **pItem) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_Count( 
            /* [retval][out] */ long *pCount) = 0;
        
        virtual /* [hidden][restricted][propget][id] */ HRESULT STDMETHODCALLTYPE get__NewEnum( 
            /* [retval][out] */ IUnknown **pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Add( 
            /* [in] */ BSTR Name,
            /* [retval][out] */ IOdaMLineStyle **pLayout) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct IOdaMLineStylesVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IOdaMLineStyles * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IOdaMLineStyles * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IOdaMLineStyles * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IOdaMLineStyles * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IOdaMLineStyles * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IOdaMLineStyles * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IOdaMLineStyles * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Handle )( 
            IOdaMLineStyles * This,
            /* [retval][out] */ BSTR *Handle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ObjectName )( 
            IOdaMLineStyles * This,
            /* [retval][out] */ BSTR *ObjectName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetXData )( 
            IOdaMLineStyles * This,
            /* [in] */ BSTR AppName,
            /* [out] */ VARIANT *XDataType,
            /* [out] */ VARIANT *XDataValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetXData )( 
            IOdaMLineStyles * This,
            /* [in] */ VARIANT XDataType,
            /* [in] */ VARIANT XDataValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Delete )( 
            IOdaMLineStyles * This);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ObjectID )( 
            IOdaMLineStyles * This,
            /* [retval][out] */ LONG_PTR *ObjectID);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Application )( 
            IOdaMLineStyles * This,
            /* [retval][out] */ IDispatch **ApplicationObject);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Database )( 
            IOdaMLineStyles * This,
            /* [retval][out] */ IAcadDatabase **pDatabase);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_HasExtensionDictionary )( 
            IOdaMLineStyles * This,
            /* [retval][out] */ VARIANT_BOOL *bHasDictionary);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetExtensionDictionary )( 
            IOdaMLineStyles * This,
            /* [retval][out] */ IAcadDictionary **pExtDictionary);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_OwnerID )( 
            IOdaMLineStyles * This,
            /* [retval][out] */ LONG_PTR *OwnerID);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Document )( 
            IOdaMLineStyles * This,
            /* [retval][out] */ IDispatch **pDocument);
        
        /* [helpstring][hidden][id] */ HRESULT ( STDMETHODCALLTYPE *Erase )( 
            IOdaMLineStyles * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Item )( 
            IOdaMLineStyles * This,
            /* [in] */ VARIANT Index,
            /* [retval][out] */ IOdaMLineStyle **pItem);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Count )( 
            IOdaMLineStyles * This,
            /* [retval][out] */ long *pCount);
        
        /* [hidden][restricted][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get__NewEnum )( 
            IOdaMLineStyles * This,
            /* [retval][out] */ IUnknown **pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Add )( 
            IOdaMLineStyles * This,
            /* [in] */ BSTR Name,
            /* [retval][out] */ IOdaMLineStyle **pLayout);
        
        END_INTERFACE
    } IOdaMLineStylesVtbl;

    interface IOdaMLineStyles
    {
        CONST_VTBL struct IOdaMLineStylesVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IOdaMLineStyles_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IOdaMLineStyles_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IOdaMLineStyles_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IOdaMLineStyles_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IOdaMLineStyles_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IOdaMLineStyles_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IOdaMLineStyles_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IOdaMLineStyles_get_Handle(This,Handle)	\
    ( (This)->lpVtbl -> get_Handle(This,Handle) ) 

#define IOdaMLineStyles_get_ObjectName(This,ObjectName)	\
    ( (This)->lpVtbl -> get_ObjectName(This,ObjectName) ) 

#define IOdaMLineStyles_GetXData(This,AppName,XDataType,XDataValue)	\
    ( (This)->lpVtbl -> GetXData(This,AppName,XDataType,XDataValue) ) 

#define IOdaMLineStyles_SetXData(This,XDataType,XDataValue)	\
    ( (This)->lpVtbl -> SetXData(This,XDataType,XDataValue) ) 

#define IOdaMLineStyles_Delete(This)	\
    ( (This)->lpVtbl -> Delete(This) ) 

#define IOdaMLineStyles_get_ObjectID(This,ObjectID)	\
    ( (This)->lpVtbl -> get_ObjectID(This,ObjectID) ) 

#define IOdaMLineStyles_get_Application(This,ApplicationObject)	\
    ( (This)->lpVtbl -> get_Application(This,ApplicationObject) ) 

#define IOdaMLineStyles_get_Database(This,pDatabase)	\
    ( (This)->lpVtbl -> get_Database(This,pDatabase) ) 

#define IOdaMLineStyles_get_HasExtensionDictionary(This,bHasDictionary)	\
    ( (This)->lpVtbl -> get_HasExtensionDictionary(This,bHasDictionary) ) 

#define IOdaMLineStyles_GetExtensionDictionary(This,pExtDictionary)	\
    ( (This)->lpVtbl -> GetExtensionDictionary(This,pExtDictionary) ) 

#define IOdaMLineStyles_get_OwnerID(This,OwnerID)	\
    ( (This)->lpVtbl -> get_OwnerID(This,OwnerID) ) 

#define IOdaMLineStyles_get_Document(This,pDocument)	\
    ( (This)->lpVtbl -> get_Document(This,pDocument) ) 

#define IOdaMLineStyles_Erase(This)	\
    ( (This)->lpVtbl -> Erase(This) ) 


#define IOdaMLineStyles_Item(This,Index,pItem)	\
    ( (This)->lpVtbl -> Item(This,Index,pItem) ) 

#define IOdaMLineStyles_get_Count(This,pCount)	\
    ( (This)->lpVtbl -> get_Count(This,pCount) ) 

#define IOdaMLineStyles_get__NewEnum(This,pVal)	\
    ( (This)->lpVtbl -> get__NewEnum(This,pVal) ) 

#define IOdaMLineStyles_Add(This,Name,pLayout)	\
    ( (This)->lpVtbl -> Add(This,Name,pLayout) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IOdaMLineStyles_INTERFACE_DEFINED__ */


#ifndef __IOdaMLineStyle_INTERFACE_DEFINED__
#define __IOdaMLineStyle_INTERFACE_DEFINED__

/* interface IOdaMLineStyle */
/* [object][oleautomation][dual][helpstring][uuid] */ 


EXTERN_C const IID IID_IOdaMLineStyle;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("3A1DC1C0-D49D-4DC6-A944-A158227BFF03")
    IOdaMLineStyle : public IAcadObject
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Name( 
            /* [retval][out] */ BSTR *pName) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Name( 
            /* [in] */ BSTR pName) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Description( 
            /* [retval][out] */ BSTR *bstrDes) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Description( 
            /* [in] */ BSTR bstrDes) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_StartAngle( 
            /* [retval][out] */ ACAD_ANGLE *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_StartAngle( 
            /* [in] */ ACAD_ANGLE newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_EndAngle( 
            /* [retval][out] */ ACAD_ANGLE *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_EndAngle( 
            /* [in] */ ACAD_ANGLE newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_FillColor( 
            /* [retval][out] */ IAcadAcCmColor **pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_FillColor( 
            /* [in] */ IAcadAcCmColor *newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ElementsCount( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Filled( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Filled( 
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ShowMiters( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ShowMiters( 
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_StartSquareCap( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_StartSquareCap( 
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_StartRoundCap( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_StartRoundCap( 
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_StartInnerArcs( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_StartInnerArcs( 
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_EndSquareCap( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_EndSquareCap( 
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_EndRoundCap( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_EndRoundCap( 
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_EndInnerArcs( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_EndInnerArcs( 
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE AddElement( 
            /* [in] */ double offset,
            /* [in] */ IAcadAcCmColor *Color,
            /* [in] */ IAcadLineType *pLinetype) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_OffsetAt( 
            /* [in] */ int index,
            /* [retval][out] */ double *value) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_OffsetAt( 
            /* [in] */ int index,
            /* [in] */ double value) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ColorAt( 
            /* [in] */ int index,
            /* [retval][out] */ IAcadAcCmColor **Color) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ColorAt( 
            /* [in] */ int index,
            /* [in] */ IAcadAcCmColor *Color) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_LineTypeAt( 
            /* [in] */ int index,
            /* [retval][out] */ IAcadLineType **LineType) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_LineTypeAt( 
            /* [in] */ int index,
            /* [in] */ IAcadLineType *LineType) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct IOdaMLineStyleVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IOdaMLineStyle * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IOdaMLineStyle * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IOdaMLineStyle * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IOdaMLineStyle * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IOdaMLineStyle * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IOdaMLineStyle * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IOdaMLineStyle * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Handle )( 
            IOdaMLineStyle * This,
            /* [retval][out] */ BSTR *Handle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ObjectName )( 
            IOdaMLineStyle * This,
            /* [retval][out] */ BSTR *ObjectName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetXData )( 
            IOdaMLineStyle * This,
            /* [in] */ BSTR AppName,
            /* [out] */ VARIANT *XDataType,
            /* [out] */ VARIANT *XDataValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetXData )( 
            IOdaMLineStyle * This,
            /* [in] */ VARIANT XDataType,
            /* [in] */ VARIANT XDataValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Delete )( 
            IOdaMLineStyle * This);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ObjectID )( 
            IOdaMLineStyle * This,
            /* [retval][out] */ LONG_PTR *ObjectID);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Application )( 
            IOdaMLineStyle * This,
            /* [retval][out] */ IDispatch **ApplicationObject);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Database )( 
            IOdaMLineStyle * This,
            /* [retval][out] */ IAcadDatabase **pDatabase);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_HasExtensionDictionary )( 
            IOdaMLineStyle * This,
            /* [retval][out] */ VARIANT_BOOL *bHasDictionary);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetExtensionDictionary )( 
            IOdaMLineStyle * This,
            /* [retval][out] */ IAcadDictionary **pExtDictionary);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_OwnerID )( 
            IOdaMLineStyle * This,
            /* [retval][out] */ LONG_PTR *OwnerID);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Document )( 
            IOdaMLineStyle * This,
            /* [retval][out] */ IDispatch **pDocument);
        
        /* [helpstring][hidden][id] */ HRESULT ( STDMETHODCALLTYPE *Erase )( 
            IOdaMLineStyle * This);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Name )( 
            IOdaMLineStyle * This,
            /* [retval][out] */ BSTR *pName);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Name )( 
            IOdaMLineStyle * This,
            /* [in] */ BSTR pName);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Description )( 
            IOdaMLineStyle * This,
            /* [retval][out] */ BSTR *bstrDes);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Description )( 
            IOdaMLineStyle * This,
            /* [in] */ BSTR bstrDes);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StartAngle )( 
            IOdaMLineStyle * This,
            /* [retval][out] */ ACAD_ANGLE *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_StartAngle )( 
            IOdaMLineStyle * This,
            /* [in] */ ACAD_ANGLE newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_EndAngle )( 
            IOdaMLineStyle * This,
            /* [retval][out] */ ACAD_ANGLE *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_EndAngle )( 
            IOdaMLineStyle * This,
            /* [in] */ ACAD_ANGLE newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_FillColor )( 
            IOdaMLineStyle * This,
            /* [retval][out] */ IAcadAcCmColor **pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_FillColor )( 
            IOdaMLineStyle * This,
            /* [in] */ IAcadAcCmColor *newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ElementsCount )( 
            IOdaMLineStyle * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Filled )( 
            IOdaMLineStyle * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Filled )( 
            IOdaMLineStyle * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ShowMiters )( 
            IOdaMLineStyle * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ShowMiters )( 
            IOdaMLineStyle * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StartSquareCap )( 
            IOdaMLineStyle * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_StartSquareCap )( 
            IOdaMLineStyle * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StartRoundCap )( 
            IOdaMLineStyle * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_StartRoundCap )( 
            IOdaMLineStyle * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StartInnerArcs )( 
            IOdaMLineStyle * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_StartInnerArcs )( 
            IOdaMLineStyle * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_EndSquareCap )( 
            IOdaMLineStyle * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_EndSquareCap )( 
            IOdaMLineStyle * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_EndRoundCap )( 
            IOdaMLineStyle * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_EndRoundCap )( 
            IOdaMLineStyle * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_EndInnerArcs )( 
            IOdaMLineStyle * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_EndInnerArcs )( 
            IOdaMLineStyle * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *AddElement )( 
            IOdaMLineStyle * This,
            /* [in] */ double offset,
            /* [in] */ IAcadAcCmColor *Color,
            /* [in] */ IAcadLineType *pLinetype);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_OffsetAt )( 
            IOdaMLineStyle * This,
            /* [in] */ int index,
            /* [retval][out] */ double *value);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_OffsetAt )( 
            IOdaMLineStyle * This,
            /* [in] */ int index,
            /* [in] */ double value);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ColorAt )( 
            IOdaMLineStyle * This,
            /* [in] */ int index,
            /* [retval][out] */ IAcadAcCmColor **Color);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ColorAt )( 
            IOdaMLineStyle * This,
            /* [in] */ int index,
            /* [in] */ IAcadAcCmColor *Color);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_LineTypeAt )( 
            IOdaMLineStyle * This,
            /* [in] */ int index,
            /* [retval][out] */ IAcadLineType **LineType);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_LineTypeAt )( 
            IOdaMLineStyle * This,
            /* [in] */ int index,
            /* [in] */ IAcadLineType *LineType);
        
        END_INTERFACE
    } IOdaMLineStyleVtbl;

    interface IOdaMLineStyle
    {
        CONST_VTBL struct IOdaMLineStyleVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IOdaMLineStyle_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IOdaMLineStyle_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IOdaMLineStyle_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IOdaMLineStyle_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IOdaMLineStyle_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IOdaMLineStyle_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IOdaMLineStyle_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IOdaMLineStyle_get_Handle(This,Handle)	\
    ( (This)->lpVtbl -> get_Handle(This,Handle) ) 

#define IOdaMLineStyle_get_ObjectName(This,ObjectName)	\
    ( (This)->lpVtbl -> get_ObjectName(This,ObjectName) ) 

#define IOdaMLineStyle_GetXData(This,AppName,XDataType,XDataValue)	\
    ( (This)->lpVtbl -> GetXData(This,AppName,XDataType,XDataValue) ) 

#define IOdaMLineStyle_SetXData(This,XDataType,XDataValue)	\
    ( (This)->lpVtbl -> SetXData(This,XDataType,XDataValue) ) 

#define IOdaMLineStyle_Delete(This)	\
    ( (This)->lpVtbl -> Delete(This) ) 

#define IOdaMLineStyle_get_ObjectID(This,ObjectID)	\
    ( (This)->lpVtbl -> get_ObjectID(This,ObjectID) ) 

#define IOdaMLineStyle_get_Application(This,ApplicationObject)	\
    ( (This)->lpVtbl -> get_Application(This,ApplicationObject) ) 

#define IOdaMLineStyle_get_Database(This,pDatabase)	\
    ( (This)->lpVtbl -> get_Database(This,pDatabase) ) 

#define IOdaMLineStyle_get_HasExtensionDictionary(This,bHasDictionary)	\
    ( (This)->lpVtbl -> get_HasExtensionDictionary(This,bHasDictionary) ) 

#define IOdaMLineStyle_GetExtensionDictionary(This,pExtDictionary)	\
    ( (This)->lpVtbl -> GetExtensionDictionary(This,pExtDictionary) ) 

#define IOdaMLineStyle_get_OwnerID(This,OwnerID)	\
    ( (This)->lpVtbl -> get_OwnerID(This,OwnerID) ) 

#define IOdaMLineStyle_get_Document(This,pDocument)	\
    ( (This)->lpVtbl -> get_Document(This,pDocument) ) 

#define IOdaMLineStyle_Erase(This)	\
    ( (This)->lpVtbl -> Erase(This) ) 


#define IOdaMLineStyle_get_Name(This,pName)	\
    ( (This)->lpVtbl -> get_Name(This,pName) ) 

#define IOdaMLineStyle_put_Name(This,pName)	\
    ( (This)->lpVtbl -> put_Name(This,pName) ) 

#define IOdaMLineStyle_get_Description(This,bstrDes)	\
    ( (This)->lpVtbl -> get_Description(This,bstrDes) ) 

#define IOdaMLineStyle_put_Description(This,bstrDes)	\
    ( (This)->lpVtbl -> put_Description(This,bstrDes) ) 

#define IOdaMLineStyle_get_StartAngle(This,pVal)	\
    ( (This)->lpVtbl -> get_StartAngle(This,pVal) ) 

#define IOdaMLineStyle_put_StartAngle(This,newVal)	\
    ( (This)->lpVtbl -> put_StartAngle(This,newVal) ) 

#define IOdaMLineStyle_get_EndAngle(This,pVal)	\
    ( (This)->lpVtbl -> get_EndAngle(This,pVal) ) 

#define IOdaMLineStyle_put_EndAngle(This,newVal)	\
    ( (This)->lpVtbl -> put_EndAngle(This,newVal) ) 

#define IOdaMLineStyle_get_FillColor(This,pVal)	\
    ( (This)->lpVtbl -> get_FillColor(This,pVal) ) 

#define IOdaMLineStyle_put_FillColor(This,newVal)	\
    ( (This)->lpVtbl -> put_FillColor(This,newVal) ) 

#define IOdaMLineStyle_get_ElementsCount(This,pVal)	\
    ( (This)->lpVtbl -> get_ElementsCount(This,pVal) ) 

#define IOdaMLineStyle_get_Filled(This,pVal)	\
    ( (This)->lpVtbl -> get_Filled(This,pVal) ) 

#define IOdaMLineStyle_put_Filled(This,newVal)	\
    ( (This)->lpVtbl -> put_Filled(This,newVal) ) 

#define IOdaMLineStyle_get_ShowMiters(This,pVal)	\
    ( (This)->lpVtbl -> get_ShowMiters(This,pVal) ) 

#define IOdaMLineStyle_put_ShowMiters(This,newVal)	\
    ( (This)->lpVtbl -> put_ShowMiters(This,newVal) ) 

#define IOdaMLineStyle_get_StartSquareCap(This,pVal)	\
    ( (This)->lpVtbl -> get_StartSquareCap(This,pVal) ) 

#define IOdaMLineStyle_put_StartSquareCap(This,newVal)	\
    ( (This)->lpVtbl -> put_StartSquareCap(This,newVal) ) 

#define IOdaMLineStyle_get_StartRoundCap(This,pVal)	\
    ( (This)->lpVtbl -> get_StartRoundCap(This,pVal) ) 

#define IOdaMLineStyle_put_StartRoundCap(This,newVal)	\
    ( (This)->lpVtbl -> put_StartRoundCap(This,newVal) ) 

#define IOdaMLineStyle_get_StartInnerArcs(This,pVal)	\
    ( (This)->lpVtbl -> get_StartInnerArcs(This,pVal) ) 

#define IOdaMLineStyle_put_StartInnerArcs(This,newVal)	\
    ( (This)->lpVtbl -> put_StartInnerArcs(This,newVal) ) 

#define IOdaMLineStyle_get_EndSquareCap(This,pVal)	\
    ( (This)->lpVtbl -> get_EndSquareCap(This,pVal) ) 

#define IOdaMLineStyle_put_EndSquareCap(This,newVal)	\
    ( (This)->lpVtbl -> put_EndSquareCap(This,newVal) ) 

#define IOdaMLineStyle_get_EndRoundCap(This,pVal)	\
    ( (This)->lpVtbl -> get_EndRoundCap(This,pVal) ) 

#define IOdaMLineStyle_put_EndRoundCap(This,newVal)	\
    ( (This)->lpVtbl -> put_EndRoundCap(This,newVal) ) 

#define IOdaMLineStyle_get_EndInnerArcs(This,pVal)	\
    ( (This)->lpVtbl -> get_EndInnerArcs(This,pVal) ) 

#define IOdaMLineStyle_put_EndInnerArcs(This,newVal)	\
    ( (This)->lpVtbl -> put_EndInnerArcs(This,newVal) ) 

#define IOdaMLineStyle_AddElement(This,offset,Color,pLinetype)	\
    ( (This)->lpVtbl -> AddElement(This,offset,Color,pLinetype) ) 

#define IOdaMLineStyle_get_OffsetAt(This,index,value)	\
    ( (This)->lpVtbl -> get_OffsetAt(This,index,value) ) 

#define IOdaMLineStyle_put_OffsetAt(This,index,value)	\
    ( (This)->lpVtbl -> put_OffsetAt(This,index,value) ) 

#define IOdaMLineStyle_get_ColorAt(This,index,Color)	\
    ( (This)->lpVtbl -> get_ColorAt(This,index,Color) ) 

#define IOdaMLineStyle_put_ColorAt(This,index,Color)	\
    ( (This)->lpVtbl -> put_ColorAt(This,index,Color) ) 

#define IOdaMLineStyle_get_LineTypeAt(This,index,LineType)	\
    ( (This)->lpVtbl -> get_LineTypeAt(This,index,LineType) ) 

#define IOdaMLineStyle_put_LineTypeAt(This,index,LineType)	\
    ( (This)->lpVtbl -> put_LineTypeAt(This,index,LineType) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IOdaMLineStyle_INTERFACE_DEFINED__ */


#ifndef __IAcadSurface_INTERFACE_DEFINED__
#define __IAcadSurface_INTERFACE_DEFINED__

/* interface IAcadSurface */
/* [object][oleautomation][dual][helpstring][uuid] */ 


EXTERN_C const IID IID_IAcadSurface;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("DB31BDE0-6DD6-4FDF-A289-214EEA6C0072")
    IAcadSurface : public IAcadEntity
    {
    public:
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_SurfaceType( 
            /* [retval][out] */ BSTR *SurfaceType) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_UIsolineDensity( 
            /* [retval][out] */ long *density) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_UIsolineDensity( 
            /* [in] */ long density) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_VIsolineDensity( 
            /* [retval][out] */ long *density) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_VIsolineDensity( 
            /* [in] */ long density) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct IAcadSurfaceVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAcadSurface * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAcadSurface * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAcadSurface * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IAcadSurface * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IAcadSurface * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IAcadSurface * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IAcadSurface * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Handle )( 
            IAcadSurface * This,
            /* [retval][out] */ BSTR *Handle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ObjectName )( 
            IAcadSurface * This,
            /* [retval][out] */ BSTR *ObjectName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetXData )( 
            IAcadSurface * This,
            /* [in] */ BSTR AppName,
            /* [out] */ VARIANT *XDataType,
            /* [out] */ VARIANT *XDataValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetXData )( 
            IAcadSurface * This,
            /* [in] */ VARIANT XDataType,
            /* [in] */ VARIANT XDataValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Delete )( 
            IAcadSurface * This);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ObjectID )( 
            IAcadSurface * This,
            /* [retval][out] */ LONG_PTR *ObjectID);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Application )( 
            IAcadSurface * This,
            /* [retval][out] */ IDispatch **ApplicationObject);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Database )( 
            IAcadSurface * This,
            /* [retval][out] */ IAcadDatabase **pDatabase);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_HasExtensionDictionary )( 
            IAcadSurface * This,
            /* [retval][out] */ VARIANT_BOOL *bHasDictionary);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetExtensionDictionary )( 
            IAcadSurface * This,
            /* [retval][out] */ IAcadDictionary **pExtDictionary);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_OwnerID )( 
            IAcadSurface * This,
            /* [retval][out] */ LONG_PTR *OwnerID);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Document )( 
            IAcadSurface * This,
            /* [retval][out] */ IDispatch **pDocument);
        
        /* [helpstring][hidden][id] */ HRESULT ( STDMETHODCALLTYPE *Erase )( 
            IAcadSurface * This);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_TrueColor )( 
            IAcadSurface * This,
            /* [retval][out] */ IAcadAcCmColor **pColor);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_TrueColor )( 
            IAcadSurface * This,
            /* [in] */ IAcadAcCmColor *pColor);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Layer )( 
            IAcadSurface * This,
            /* [retval][out] */ BSTR *Layer);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Layer )( 
            IAcadSurface * This,
            /* [in] */ BSTR Layer);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Linetype )( 
            IAcadSurface * This,
            /* [retval][out] */ BSTR *Linetype);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Linetype )( 
            IAcadSurface * This,
            /* [in] */ BSTR Linetype);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_LinetypeScale )( 
            IAcadSurface * This,
            /* [retval][out] */ ACAD_NOUNITS *ltScale);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_LinetypeScale )( 
            IAcadSurface * This,
            /* [in] */ ACAD_NOUNITS ltScale);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Visible )( 
            IAcadSurface * This,
            /* [retval][out] */ VARIANT_BOOL *bVisible);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Visible )( 
            IAcadSurface * This,
            /* [in] */ VARIANT_BOOL bVisible);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ArrayPolar )( 
            IAcadSurface * This,
            /* [in] */ int NumberOfObjects,
            /* [in] */ double AngleToFill,
            /* [in] */ VARIANT CenterPoint,
            /* [retval][out] */ VARIANT *pArrayObjs);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ArrayRectangular )( 
            IAcadSurface * This,
            /* [in] */ int NumberOfRows,
            /* [in] */ int NumberOfColumns,
            /* [in] */ int NumberOfLevels,
            /* [in] */ double DistBetweenRows,
            /* [in] */ double DistBetweenCols,
            /* [in] */ double DistBetweenLevels,
            /* [retval][out] */ VARIANT *pArrayObjs);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Highlight )( 
            IAcadSurface * This,
            /* [in] */ VARIANT_BOOL HighlightFlag);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Copy )( 
            IAcadSurface * This,
            /* [retval][out] */ IDispatch **pCopyObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Move )( 
            IAcadSurface * This,
            /* [in] */ VARIANT FromPoint,
            /* [in] */ VARIANT ToPoint);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Rotate )( 
            IAcadSurface * This,
            /* [in] */ VARIANT BasePoint,
            /* [in] */ double RotationAngle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Rotate3D )( 
            IAcadSurface * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [in] */ double RotationAngle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Mirror )( 
            IAcadSurface * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [retval][out] */ IDispatch **pMirrorObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Mirror3D )( 
            IAcadSurface * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [in] */ VARIANT point3,
            /* [retval][out] */ IDispatch **pMirrorObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ScaleEntity )( 
            IAcadSurface * This,
            /* [in] */ VARIANT BasePoint,
            /* [in] */ double ScaleFactor);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *TransformBy )( 
            IAcadSurface * This,
            /* [in] */ VARIANT TransformationMatrix);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Update )( 
            IAcadSurface * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetBoundingBox )( 
            IAcadSurface * This,
            /* [out] */ VARIANT *MinPoint,
            /* [out] */ VARIANT *MaxPoint);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *IntersectWith )( 
            IAcadSurface * This,
            /* [in] */ IDispatch *IntersectObject,
            /* [in] */ AcExtendOption option,
            /* [retval][out] */ VARIANT *intPoints);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_PlotStyleName )( 
            IAcadSurface * This,
            /* [retval][out] */ BSTR *plotStyle);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_PlotStyleName )( 
            IAcadSurface * This,
            /* [in] */ BSTR plotStyle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Lineweight )( 
            IAcadSurface * This,
            /* [retval][out] */ ACAD_LWEIGHT *Lineweight);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Lineweight )( 
            IAcadSurface * This,
            /* [in] */ ACAD_LWEIGHT Lineweight);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Hyperlinks )( 
            IAcadSurface * This,
            /* [retval][out] */ IAcadHyperlinks **Hyperlinks);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Material )( 
            IAcadSurface * This,
            /* [retval][out] */ BSTR *Material);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Material )( 
            IAcadSurface * This,
            /* [in] */ BSTR Material);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EntityName )( 
            IAcadSurface * This,
            /* [retval][out] */ BSTR *EntityName);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EntityType )( 
            IAcadSurface * This,
            /* [retval][out] */ long *entType);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_color )( 
            IAcadSurface * This,
            /* [retval][out] */ ACAD_COLOR *color);
        
        /* [helpstring][hidden][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_color )( 
            IAcadSurface * This,
            /* [in] */ ACAD_COLOR color);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SurfaceType )( 
            IAcadSurface * This,
            /* [retval][out] */ BSTR *SurfaceType);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_UIsolineDensity )( 
            IAcadSurface * This,
            /* [retval][out] */ long *density);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_UIsolineDensity )( 
            IAcadSurface * This,
            /* [in] */ long density);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_VIsolineDensity )( 
            IAcadSurface * This,
            /* [retval][out] */ long *density);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_VIsolineDensity )( 
            IAcadSurface * This,
            /* [in] */ long density);
        
        END_INTERFACE
    } IAcadSurfaceVtbl;

    interface IAcadSurface
    {
        CONST_VTBL struct IAcadSurfaceVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAcadSurface_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAcadSurface_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAcadSurface_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAcadSurface_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IAcadSurface_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IAcadSurface_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IAcadSurface_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IAcadSurface_get_Handle(This,Handle)	\
    ( (This)->lpVtbl -> get_Handle(This,Handle) ) 

#define IAcadSurface_get_ObjectName(This,ObjectName)	\
    ( (This)->lpVtbl -> get_ObjectName(This,ObjectName) ) 

#define IAcadSurface_GetXData(This,AppName,XDataType,XDataValue)	\
    ( (This)->lpVtbl -> GetXData(This,AppName,XDataType,XDataValue) ) 

#define IAcadSurface_SetXData(This,XDataType,XDataValue)	\
    ( (This)->lpVtbl -> SetXData(This,XDataType,XDataValue) ) 

#define IAcadSurface_Delete(This)	\
    ( (This)->lpVtbl -> Delete(This) ) 

#define IAcadSurface_get_ObjectID(This,ObjectID)	\
    ( (This)->lpVtbl -> get_ObjectID(This,ObjectID) ) 

#define IAcadSurface_get_Application(This,ApplicationObject)	\
    ( (This)->lpVtbl -> get_Application(This,ApplicationObject) ) 

#define IAcadSurface_get_Database(This,pDatabase)	\
    ( (This)->lpVtbl -> get_Database(This,pDatabase) ) 

#define IAcadSurface_get_HasExtensionDictionary(This,bHasDictionary)	\
    ( (This)->lpVtbl -> get_HasExtensionDictionary(This,bHasDictionary) ) 

#define IAcadSurface_GetExtensionDictionary(This,pExtDictionary)	\
    ( (This)->lpVtbl -> GetExtensionDictionary(This,pExtDictionary) ) 

#define IAcadSurface_get_OwnerID(This,OwnerID)	\
    ( (This)->lpVtbl -> get_OwnerID(This,OwnerID) ) 

#define IAcadSurface_get_Document(This,pDocument)	\
    ( (This)->lpVtbl -> get_Document(This,pDocument) ) 

#define IAcadSurface_Erase(This)	\
    ( (This)->lpVtbl -> Erase(This) ) 


#define IAcadSurface_get_TrueColor(This,pColor)	\
    ( (This)->lpVtbl -> get_TrueColor(This,pColor) ) 

#define IAcadSurface_put_TrueColor(This,pColor)	\
    ( (This)->lpVtbl -> put_TrueColor(This,pColor) ) 

#define IAcadSurface_get_Layer(This,Layer)	\
    ( (This)->lpVtbl -> get_Layer(This,Layer) ) 

#define IAcadSurface_put_Layer(This,Layer)	\
    ( (This)->lpVtbl -> put_Layer(This,Layer) ) 

#define IAcadSurface_get_Linetype(This,Linetype)	\
    ( (This)->lpVtbl -> get_Linetype(This,Linetype) ) 

#define IAcadSurface_put_Linetype(This,Linetype)	\
    ( (This)->lpVtbl -> put_Linetype(This,Linetype) ) 

#define IAcadSurface_get_LinetypeScale(This,ltScale)	\
    ( (This)->lpVtbl -> get_LinetypeScale(This,ltScale) ) 

#define IAcadSurface_put_LinetypeScale(This,ltScale)	\
    ( (This)->lpVtbl -> put_LinetypeScale(This,ltScale) ) 

#define IAcadSurface_get_Visible(This,bVisible)	\
    ( (This)->lpVtbl -> get_Visible(This,bVisible) ) 

#define IAcadSurface_put_Visible(This,bVisible)	\
    ( (This)->lpVtbl -> put_Visible(This,bVisible) ) 

#define IAcadSurface_ArrayPolar(This,NumberOfObjects,AngleToFill,CenterPoint,pArrayObjs)	\
    ( (This)->lpVtbl -> ArrayPolar(This,NumberOfObjects,AngleToFill,CenterPoint,pArrayObjs) ) 

#define IAcadSurface_ArrayRectangular(This,NumberOfRows,NumberOfColumns,NumberOfLevels,DistBetweenRows,DistBetweenCols,DistBetweenLevels,pArrayObjs)	\
    ( (This)->lpVtbl -> ArrayRectangular(This,NumberOfRows,NumberOfColumns,NumberOfLevels,DistBetweenRows,DistBetweenCols,DistBetweenLevels,pArrayObjs) ) 

#define IAcadSurface_Highlight(This,HighlightFlag)	\
    ( (This)->lpVtbl -> Highlight(This,HighlightFlag) ) 

#define IAcadSurface_Copy(This,pCopyObj)	\
    ( (This)->lpVtbl -> Copy(This,pCopyObj) ) 

#define IAcadSurface_Move(This,FromPoint,ToPoint)	\
    ( (This)->lpVtbl -> Move(This,FromPoint,ToPoint) ) 

#define IAcadSurface_Rotate(This,BasePoint,RotationAngle)	\
    ( (This)->lpVtbl -> Rotate(This,BasePoint,RotationAngle) ) 

#define IAcadSurface_Rotate3D(This,Point1,Point2,RotationAngle)	\
    ( (This)->lpVtbl -> Rotate3D(This,Point1,Point2,RotationAngle) ) 

#define IAcadSurface_Mirror(This,Point1,Point2,pMirrorObj)	\
    ( (This)->lpVtbl -> Mirror(This,Point1,Point2,pMirrorObj) ) 

#define IAcadSurface_Mirror3D(This,Point1,Point2,point3,pMirrorObj)	\
    ( (This)->lpVtbl -> Mirror3D(This,Point1,Point2,point3,pMirrorObj) ) 

#define IAcadSurface_ScaleEntity(This,BasePoint,ScaleFactor)	\
    ( (This)->lpVtbl -> ScaleEntity(This,BasePoint,ScaleFactor) ) 

#define IAcadSurface_TransformBy(This,TransformationMatrix)	\
    ( (This)->lpVtbl -> TransformBy(This,TransformationMatrix) ) 

#define IAcadSurface_Update(This)	\
    ( (This)->lpVtbl -> Update(This) ) 

#define IAcadSurface_GetBoundingBox(This,MinPoint,MaxPoint)	\
    ( (This)->lpVtbl -> GetBoundingBox(This,MinPoint,MaxPoint) ) 

#define IAcadSurface_IntersectWith(This,IntersectObject,option,intPoints)	\
    ( (This)->lpVtbl -> IntersectWith(This,IntersectObject,option,intPoints) ) 

#define IAcadSurface_get_PlotStyleName(This,plotStyle)	\
    ( (This)->lpVtbl -> get_PlotStyleName(This,plotStyle) ) 

#define IAcadSurface_put_PlotStyleName(This,plotStyle)	\
    ( (This)->lpVtbl -> put_PlotStyleName(This,plotStyle) ) 

#define IAcadSurface_get_Lineweight(This,Lineweight)	\
    ( (This)->lpVtbl -> get_Lineweight(This,Lineweight) ) 

#define IAcadSurface_put_Lineweight(This,Lineweight)	\
    ( (This)->lpVtbl -> put_Lineweight(This,Lineweight) ) 

#define IAcadSurface_get_Hyperlinks(This,Hyperlinks)	\
    ( (This)->lpVtbl -> get_Hyperlinks(This,Hyperlinks) ) 

#define IAcadSurface_get_Material(This,Material)	\
    ( (This)->lpVtbl -> get_Material(This,Material) ) 

#define IAcadSurface_put_Material(This,Material)	\
    ( (This)->lpVtbl -> put_Material(This,Material) ) 

#define IAcadSurface_get_EntityName(This,EntityName)	\
    ( (This)->lpVtbl -> get_EntityName(This,EntityName) ) 

#define IAcadSurface_get_EntityType(This,entType)	\
    ( (This)->lpVtbl -> get_EntityType(This,entType) ) 

#define IAcadSurface_get_color(This,color)	\
    ( (This)->lpVtbl -> get_color(This,color) ) 

#define IAcadSurface_put_color(This,color)	\
    ( (This)->lpVtbl -> put_color(This,color) ) 


#define IAcadSurface_get_SurfaceType(This,SurfaceType)	\
    ( (This)->lpVtbl -> get_SurfaceType(This,SurfaceType) ) 

#define IAcadSurface_get_UIsolineDensity(This,density)	\
    ( (This)->lpVtbl -> get_UIsolineDensity(This,density) ) 

#define IAcadSurface_put_UIsolineDensity(This,density)	\
    ( (This)->lpVtbl -> put_UIsolineDensity(This,density) ) 

#define IAcadSurface_get_VIsolineDensity(This,density)	\
    ( (This)->lpVtbl -> get_VIsolineDensity(This,density) ) 

#define IAcadSurface_put_VIsolineDensity(This,density)	\
    ( (This)->lpVtbl -> put_VIsolineDensity(This,density) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAcadSurface_INTERFACE_DEFINED__ */


#ifndef __IAcadPlaneSurface_INTERFACE_DEFINED__
#define __IAcadPlaneSurface_INTERFACE_DEFINED__

/* interface IAcadPlaneSurface */
/* [object][oleautomation][dual][helpstring][uuid] */ 


EXTERN_C const IID IID_IAcadPlaneSurface;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("FFCCA3F9-8BB7-4693-B7F7-07EFDF4DE8A9")
    IAcadPlaneSurface : public IAcadSurface
    {
    public:
    };
    
    
#else 	/* C style interface */

    typedef struct IAcadPlaneSurfaceVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAcadPlaneSurface * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAcadPlaneSurface * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAcadPlaneSurface * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IAcadPlaneSurface * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IAcadPlaneSurface * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IAcadPlaneSurface * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IAcadPlaneSurface * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Handle )( 
            IAcadPlaneSurface * This,
            /* [retval][out] */ BSTR *Handle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ObjectName )( 
            IAcadPlaneSurface * This,
            /* [retval][out] */ BSTR *ObjectName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetXData )( 
            IAcadPlaneSurface * This,
            /* [in] */ BSTR AppName,
            /* [out] */ VARIANT *XDataType,
            /* [out] */ VARIANT *XDataValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetXData )( 
            IAcadPlaneSurface * This,
            /* [in] */ VARIANT XDataType,
            /* [in] */ VARIANT XDataValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Delete )( 
            IAcadPlaneSurface * This);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ObjectID )( 
            IAcadPlaneSurface * This,
            /* [retval][out] */ LONG_PTR *ObjectID);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Application )( 
            IAcadPlaneSurface * This,
            /* [retval][out] */ IDispatch **ApplicationObject);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Database )( 
            IAcadPlaneSurface * This,
            /* [retval][out] */ IAcadDatabase **pDatabase);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_HasExtensionDictionary )( 
            IAcadPlaneSurface * This,
            /* [retval][out] */ VARIANT_BOOL *bHasDictionary);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetExtensionDictionary )( 
            IAcadPlaneSurface * This,
            /* [retval][out] */ IAcadDictionary **pExtDictionary);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_OwnerID )( 
            IAcadPlaneSurface * This,
            /* [retval][out] */ LONG_PTR *OwnerID);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Document )( 
            IAcadPlaneSurface * This,
            /* [retval][out] */ IDispatch **pDocument);
        
        /* [helpstring][hidden][id] */ HRESULT ( STDMETHODCALLTYPE *Erase )( 
            IAcadPlaneSurface * This);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_TrueColor )( 
            IAcadPlaneSurface * This,
            /* [retval][out] */ IAcadAcCmColor **pColor);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_TrueColor )( 
            IAcadPlaneSurface * This,
            /* [in] */ IAcadAcCmColor *pColor);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Layer )( 
            IAcadPlaneSurface * This,
            /* [retval][out] */ BSTR *Layer);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Layer )( 
            IAcadPlaneSurface * This,
            /* [in] */ BSTR Layer);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Linetype )( 
            IAcadPlaneSurface * This,
            /* [retval][out] */ BSTR *Linetype);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Linetype )( 
            IAcadPlaneSurface * This,
            /* [in] */ BSTR Linetype);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_LinetypeScale )( 
            IAcadPlaneSurface * This,
            /* [retval][out] */ ACAD_NOUNITS *ltScale);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_LinetypeScale )( 
            IAcadPlaneSurface * This,
            /* [in] */ ACAD_NOUNITS ltScale);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Visible )( 
            IAcadPlaneSurface * This,
            /* [retval][out] */ VARIANT_BOOL *bVisible);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Visible )( 
            IAcadPlaneSurface * This,
            /* [in] */ VARIANT_BOOL bVisible);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ArrayPolar )( 
            IAcadPlaneSurface * This,
            /* [in] */ int NumberOfObjects,
            /* [in] */ double AngleToFill,
            /* [in] */ VARIANT CenterPoint,
            /* [retval][out] */ VARIANT *pArrayObjs);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ArrayRectangular )( 
            IAcadPlaneSurface * This,
            /* [in] */ int NumberOfRows,
            /* [in] */ int NumberOfColumns,
            /* [in] */ int NumberOfLevels,
            /* [in] */ double DistBetweenRows,
            /* [in] */ double DistBetweenCols,
            /* [in] */ double DistBetweenLevels,
            /* [retval][out] */ VARIANT *pArrayObjs);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Highlight )( 
            IAcadPlaneSurface * This,
            /* [in] */ VARIANT_BOOL HighlightFlag);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Copy )( 
            IAcadPlaneSurface * This,
            /* [retval][out] */ IDispatch **pCopyObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Move )( 
            IAcadPlaneSurface * This,
            /* [in] */ VARIANT FromPoint,
            /* [in] */ VARIANT ToPoint);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Rotate )( 
            IAcadPlaneSurface * This,
            /* [in] */ VARIANT BasePoint,
            /* [in] */ double RotationAngle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Rotate3D )( 
            IAcadPlaneSurface * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [in] */ double RotationAngle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Mirror )( 
            IAcadPlaneSurface * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [retval][out] */ IDispatch **pMirrorObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Mirror3D )( 
            IAcadPlaneSurface * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [in] */ VARIANT point3,
            /* [retval][out] */ IDispatch **pMirrorObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ScaleEntity )( 
            IAcadPlaneSurface * This,
            /* [in] */ VARIANT BasePoint,
            /* [in] */ double ScaleFactor);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *TransformBy )( 
            IAcadPlaneSurface * This,
            /* [in] */ VARIANT TransformationMatrix);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Update )( 
            IAcadPlaneSurface * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetBoundingBox )( 
            IAcadPlaneSurface * This,
            /* [out] */ VARIANT *MinPoint,
            /* [out] */ VARIANT *MaxPoint);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *IntersectWith )( 
            IAcadPlaneSurface * This,
            /* [in] */ IDispatch *IntersectObject,
            /* [in] */ AcExtendOption option,
            /* [retval][out] */ VARIANT *intPoints);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_PlotStyleName )( 
            IAcadPlaneSurface * This,
            /* [retval][out] */ BSTR *plotStyle);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_PlotStyleName )( 
            IAcadPlaneSurface * This,
            /* [in] */ BSTR plotStyle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Lineweight )( 
            IAcadPlaneSurface * This,
            /* [retval][out] */ ACAD_LWEIGHT *Lineweight);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Lineweight )( 
            IAcadPlaneSurface * This,
            /* [in] */ ACAD_LWEIGHT Lineweight);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Hyperlinks )( 
            IAcadPlaneSurface * This,
            /* [retval][out] */ IAcadHyperlinks **Hyperlinks);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Material )( 
            IAcadPlaneSurface * This,
            /* [retval][out] */ BSTR *Material);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Material )( 
            IAcadPlaneSurface * This,
            /* [in] */ BSTR Material);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EntityName )( 
            IAcadPlaneSurface * This,
            /* [retval][out] */ BSTR *EntityName);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EntityType )( 
            IAcadPlaneSurface * This,
            /* [retval][out] */ long *entType);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_color )( 
            IAcadPlaneSurface * This,
            /* [retval][out] */ ACAD_COLOR *color);
        
        /* [helpstring][hidden][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_color )( 
            IAcadPlaneSurface * This,
            /* [in] */ ACAD_COLOR color);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SurfaceType )( 
            IAcadPlaneSurface * This,
            /* [retval][out] */ BSTR *SurfaceType);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_UIsolineDensity )( 
            IAcadPlaneSurface * This,
            /* [retval][out] */ long *density);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_UIsolineDensity )( 
            IAcadPlaneSurface * This,
            /* [in] */ long density);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_VIsolineDensity )( 
            IAcadPlaneSurface * This,
            /* [retval][out] */ long *density);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_VIsolineDensity )( 
            IAcadPlaneSurface * This,
            /* [in] */ long density);
        
        END_INTERFACE
    } IAcadPlaneSurfaceVtbl;

    interface IAcadPlaneSurface
    {
        CONST_VTBL struct IAcadPlaneSurfaceVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAcadPlaneSurface_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAcadPlaneSurface_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAcadPlaneSurface_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAcadPlaneSurface_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IAcadPlaneSurface_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IAcadPlaneSurface_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IAcadPlaneSurface_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IAcadPlaneSurface_get_Handle(This,Handle)	\
    ( (This)->lpVtbl -> get_Handle(This,Handle) ) 

#define IAcadPlaneSurface_get_ObjectName(This,ObjectName)	\
    ( (This)->lpVtbl -> get_ObjectName(This,ObjectName) ) 

#define IAcadPlaneSurface_GetXData(This,AppName,XDataType,XDataValue)	\
    ( (This)->lpVtbl -> GetXData(This,AppName,XDataType,XDataValue) ) 

#define IAcadPlaneSurface_SetXData(This,XDataType,XDataValue)	\
    ( (This)->lpVtbl -> SetXData(This,XDataType,XDataValue) ) 

#define IAcadPlaneSurface_Delete(This)	\
    ( (This)->lpVtbl -> Delete(This) ) 

#define IAcadPlaneSurface_get_ObjectID(This,ObjectID)	\
    ( (This)->lpVtbl -> get_ObjectID(This,ObjectID) ) 

#define IAcadPlaneSurface_get_Application(This,ApplicationObject)	\
    ( (This)->lpVtbl -> get_Application(This,ApplicationObject) ) 

#define IAcadPlaneSurface_get_Database(This,pDatabase)	\
    ( (This)->lpVtbl -> get_Database(This,pDatabase) ) 

#define IAcadPlaneSurface_get_HasExtensionDictionary(This,bHasDictionary)	\
    ( (This)->lpVtbl -> get_HasExtensionDictionary(This,bHasDictionary) ) 

#define IAcadPlaneSurface_GetExtensionDictionary(This,pExtDictionary)	\
    ( (This)->lpVtbl -> GetExtensionDictionary(This,pExtDictionary) ) 

#define IAcadPlaneSurface_get_OwnerID(This,OwnerID)	\
    ( (This)->lpVtbl -> get_OwnerID(This,OwnerID) ) 

#define IAcadPlaneSurface_get_Document(This,pDocument)	\
    ( (This)->lpVtbl -> get_Document(This,pDocument) ) 

#define IAcadPlaneSurface_Erase(This)	\
    ( (This)->lpVtbl -> Erase(This) ) 


#define IAcadPlaneSurface_get_TrueColor(This,pColor)	\
    ( (This)->lpVtbl -> get_TrueColor(This,pColor) ) 

#define IAcadPlaneSurface_put_TrueColor(This,pColor)	\
    ( (This)->lpVtbl -> put_TrueColor(This,pColor) ) 

#define IAcadPlaneSurface_get_Layer(This,Layer)	\
    ( (This)->lpVtbl -> get_Layer(This,Layer) ) 

#define IAcadPlaneSurface_put_Layer(This,Layer)	\
    ( (This)->lpVtbl -> put_Layer(This,Layer) ) 

#define IAcadPlaneSurface_get_Linetype(This,Linetype)	\
    ( (This)->lpVtbl -> get_Linetype(This,Linetype) ) 

#define IAcadPlaneSurface_put_Linetype(This,Linetype)	\
    ( (This)->lpVtbl -> put_Linetype(This,Linetype) ) 

#define IAcadPlaneSurface_get_LinetypeScale(This,ltScale)	\
    ( (This)->lpVtbl -> get_LinetypeScale(This,ltScale) ) 

#define IAcadPlaneSurface_put_LinetypeScale(This,ltScale)	\
    ( (This)->lpVtbl -> put_LinetypeScale(This,ltScale) ) 

#define IAcadPlaneSurface_get_Visible(This,bVisible)	\
    ( (This)->lpVtbl -> get_Visible(This,bVisible) ) 

#define IAcadPlaneSurface_put_Visible(This,bVisible)	\
    ( (This)->lpVtbl -> put_Visible(This,bVisible) ) 

#define IAcadPlaneSurface_ArrayPolar(This,NumberOfObjects,AngleToFill,CenterPoint,pArrayObjs)	\
    ( (This)->lpVtbl -> ArrayPolar(This,NumberOfObjects,AngleToFill,CenterPoint,pArrayObjs) ) 

#define IAcadPlaneSurface_ArrayRectangular(This,NumberOfRows,NumberOfColumns,NumberOfLevels,DistBetweenRows,DistBetweenCols,DistBetweenLevels,pArrayObjs)	\
    ( (This)->lpVtbl -> ArrayRectangular(This,NumberOfRows,NumberOfColumns,NumberOfLevels,DistBetweenRows,DistBetweenCols,DistBetweenLevels,pArrayObjs) ) 

#define IAcadPlaneSurface_Highlight(This,HighlightFlag)	\
    ( (This)->lpVtbl -> Highlight(This,HighlightFlag) ) 

#define IAcadPlaneSurface_Copy(This,pCopyObj)	\
    ( (This)->lpVtbl -> Copy(This,pCopyObj) ) 

#define IAcadPlaneSurface_Move(This,FromPoint,ToPoint)	\
    ( (This)->lpVtbl -> Move(This,FromPoint,ToPoint) ) 

#define IAcadPlaneSurface_Rotate(This,BasePoint,RotationAngle)	\
    ( (This)->lpVtbl -> Rotate(This,BasePoint,RotationAngle) ) 

#define IAcadPlaneSurface_Rotate3D(This,Point1,Point2,RotationAngle)	\
    ( (This)->lpVtbl -> Rotate3D(This,Point1,Point2,RotationAngle) ) 

#define IAcadPlaneSurface_Mirror(This,Point1,Point2,pMirrorObj)	\
    ( (This)->lpVtbl -> Mirror(This,Point1,Point2,pMirrorObj) ) 

#define IAcadPlaneSurface_Mirror3D(This,Point1,Point2,point3,pMirrorObj)	\
    ( (This)->lpVtbl -> Mirror3D(This,Point1,Point2,point3,pMirrorObj) ) 

#define IAcadPlaneSurface_ScaleEntity(This,BasePoint,ScaleFactor)	\
    ( (This)->lpVtbl -> ScaleEntity(This,BasePoint,ScaleFactor) ) 

#define IAcadPlaneSurface_TransformBy(This,TransformationMatrix)	\
    ( (This)->lpVtbl -> TransformBy(This,TransformationMatrix) ) 

#define IAcadPlaneSurface_Update(This)	\
    ( (This)->lpVtbl -> Update(This) ) 

#define IAcadPlaneSurface_GetBoundingBox(This,MinPoint,MaxPoint)	\
    ( (This)->lpVtbl -> GetBoundingBox(This,MinPoint,MaxPoint) ) 

#define IAcadPlaneSurface_IntersectWith(This,IntersectObject,option,intPoints)	\
    ( (This)->lpVtbl -> IntersectWith(This,IntersectObject,option,intPoints) ) 

#define IAcadPlaneSurface_get_PlotStyleName(This,plotStyle)	\
    ( (This)->lpVtbl -> get_PlotStyleName(This,plotStyle) ) 

#define IAcadPlaneSurface_put_PlotStyleName(This,plotStyle)	\
    ( (This)->lpVtbl -> put_PlotStyleName(This,plotStyle) ) 

#define IAcadPlaneSurface_get_Lineweight(This,Lineweight)	\
    ( (This)->lpVtbl -> get_Lineweight(This,Lineweight) ) 

#define IAcadPlaneSurface_put_Lineweight(This,Lineweight)	\
    ( (This)->lpVtbl -> put_Lineweight(This,Lineweight) ) 

#define IAcadPlaneSurface_get_Hyperlinks(This,Hyperlinks)	\
    ( (This)->lpVtbl -> get_Hyperlinks(This,Hyperlinks) ) 

#define IAcadPlaneSurface_get_Material(This,Material)	\
    ( (This)->lpVtbl -> get_Material(This,Material) ) 

#define IAcadPlaneSurface_put_Material(This,Material)	\
    ( (This)->lpVtbl -> put_Material(This,Material) ) 

#define IAcadPlaneSurface_get_EntityName(This,EntityName)	\
    ( (This)->lpVtbl -> get_EntityName(This,EntityName) ) 

#define IAcadPlaneSurface_get_EntityType(This,entType)	\
    ( (This)->lpVtbl -> get_EntityType(This,entType) ) 

#define IAcadPlaneSurface_get_color(This,color)	\
    ( (This)->lpVtbl -> get_color(This,color) ) 

#define IAcadPlaneSurface_put_color(This,color)	\
    ( (This)->lpVtbl -> put_color(This,color) ) 


#define IAcadPlaneSurface_get_SurfaceType(This,SurfaceType)	\
    ( (This)->lpVtbl -> get_SurfaceType(This,SurfaceType) ) 

#define IAcadPlaneSurface_get_UIsolineDensity(This,density)	\
    ( (This)->lpVtbl -> get_UIsolineDensity(This,density) ) 

#define IAcadPlaneSurface_put_UIsolineDensity(This,density)	\
    ( (This)->lpVtbl -> put_UIsolineDensity(This,density) ) 

#define IAcadPlaneSurface_get_VIsolineDensity(This,density)	\
    ( (This)->lpVtbl -> get_VIsolineDensity(This,density) ) 

#define IAcadPlaneSurface_put_VIsolineDensity(This,density)	\
    ( (This)->lpVtbl -> put_VIsolineDensity(This,density) ) 


#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAcadPlaneSurface_INTERFACE_DEFINED__ */


#ifndef __IAcadExtrudedSurface_INTERFACE_DEFINED__
#define __IAcadExtrudedSurface_INTERFACE_DEFINED__

/* interface IAcadExtrudedSurface */
/* [object][oleautomation][dual][helpstring][uuid] */ 


EXTERN_C const IID IID_IAcadExtrudedSurface;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("91D470C0-CAB6-49F8-B13B-83D53527933A")
    IAcadExtrudedSurface : public IAcadSurface
    {
    public:
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_Height( 
            /* [retval][out] */ double *Height) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_Height( 
            /* [in] */ double Height) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_TaperAngle( 
            /* [retval][out] */ ACAD_ANGLE *TaperAngle) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_TaperAngle( 
            /* [in] */ ACAD_ANGLE TaperAngle) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_Direction( 
            /* [retval][out] */ VARIANT *Direction) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct IAcadExtrudedSurfaceVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAcadExtrudedSurface * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAcadExtrudedSurface * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAcadExtrudedSurface * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IAcadExtrudedSurface * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IAcadExtrudedSurface * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IAcadExtrudedSurface * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IAcadExtrudedSurface * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Handle )( 
            IAcadExtrudedSurface * This,
            /* [retval][out] */ BSTR *Handle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ObjectName )( 
            IAcadExtrudedSurface * This,
            /* [retval][out] */ BSTR *ObjectName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetXData )( 
            IAcadExtrudedSurface * This,
            /* [in] */ BSTR AppName,
            /* [out] */ VARIANT *XDataType,
            /* [out] */ VARIANT *XDataValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetXData )( 
            IAcadExtrudedSurface * This,
            /* [in] */ VARIANT XDataType,
            /* [in] */ VARIANT XDataValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Delete )( 
            IAcadExtrudedSurface * This);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ObjectID )( 
            IAcadExtrudedSurface * This,
            /* [retval][out] */ LONG_PTR *ObjectID);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Application )( 
            IAcadExtrudedSurface * This,
            /* [retval][out] */ IDispatch **ApplicationObject);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Database )( 
            IAcadExtrudedSurface * This,
            /* [retval][out] */ IAcadDatabase **pDatabase);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_HasExtensionDictionary )( 
            IAcadExtrudedSurface * This,
            /* [retval][out] */ VARIANT_BOOL *bHasDictionary);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetExtensionDictionary )( 
            IAcadExtrudedSurface * This,
            /* [retval][out] */ IAcadDictionary **pExtDictionary);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_OwnerID )( 
            IAcadExtrudedSurface * This,
            /* [retval][out] */ LONG_PTR *OwnerID);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Document )( 
            IAcadExtrudedSurface * This,
            /* [retval][out] */ IDispatch **pDocument);
        
        /* [helpstring][hidden][id] */ HRESULT ( STDMETHODCALLTYPE *Erase )( 
            IAcadExtrudedSurface * This);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_TrueColor )( 
            IAcadExtrudedSurface * This,
            /* [retval][out] */ IAcadAcCmColor **pColor);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_TrueColor )( 
            IAcadExtrudedSurface * This,
            /* [in] */ IAcadAcCmColor *pColor);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Layer )( 
            IAcadExtrudedSurface * This,
            /* [retval][out] */ BSTR *Layer);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Layer )( 
            IAcadExtrudedSurface * This,
            /* [in] */ BSTR Layer);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Linetype )( 
            IAcadExtrudedSurface * This,
            /* [retval][out] */ BSTR *Linetype);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Linetype )( 
            IAcadExtrudedSurface * This,
            /* [in] */ BSTR Linetype);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_LinetypeScale )( 
            IAcadExtrudedSurface * This,
            /* [retval][out] */ ACAD_NOUNITS *ltScale);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_LinetypeScale )( 
            IAcadExtrudedSurface * This,
            /* [in] */ ACAD_NOUNITS ltScale);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Visible )( 
            IAcadExtrudedSurface * This,
            /* [retval][out] */ VARIANT_BOOL *bVisible);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Visible )( 
            IAcadExtrudedSurface * This,
            /* [in] */ VARIANT_BOOL bVisible);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ArrayPolar )( 
            IAcadExtrudedSurface * This,
            /* [in] */ int NumberOfObjects,
            /* [in] */ double AngleToFill,
            /* [in] */ VARIANT CenterPoint,
            /* [retval][out] */ VARIANT *pArrayObjs);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ArrayRectangular )( 
            IAcadExtrudedSurface * This,
            /* [in] */ int NumberOfRows,
            /* [in] */ int NumberOfColumns,
            /* [in] */ int NumberOfLevels,
            /* [in] */ double DistBetweenRows,
            /* [in] */ double DistBetweenCols,
            /* [in] */ double DistBetweenLevels,
            /* [retval][out] */ VARIANT *pArrayObjs);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Highlight )( 
            IAcadExtrudedSurface * This,
            /* [in] */ VARIANT_BOOL HighlightFlag);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Copy )( 
            IAcadExtrudedSurface * This,
            /* [retval][out] */ IDispatch **pCopyObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Move )( 
            IAcadExtrudedSurface * This,
            /* [in] */ VARIANT FromPoint,
            /* [in] */ VARIANT ToPoint);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Rotate )( 
            IAcadExtrudedSurface * This,
            /* [in] */ VARIANT BasePoint,
            /* [in] */ double RotationAngle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Rotate3D )( 
            IAcadExtrudedSurface * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [in] */ double RotationAngle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Mirror )( 
            IAcadExtrudedSurface * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [retval][out] */ IDispatch **pMirrorObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Mirror3D )( 
            IAcadExtrudedSurface * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [in] */ VARIANT point3,
            /* [retval][out] */ IDispatch **pMirrorObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ScaleEntity )( 
            IAcadExtrudedSurface * This,
            /* [in] */ VARIANT BasePoint,
            /* [in] */ double ScaleFactor);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *TransformBy )( 
            IAcadExtrudedSurface * This,
            /* [in] */ VARIANT TransformationMatrix);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Update )( 
            IAcadExtrudedSurface * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetBoundingBox )( 
            IAcadExtrudedSurface * This,
            /* [out] */ VARIANT *MinPoint,
            /* [out] */ VARIANT *MaxPoint);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *IntersectWith )( 
            IAcadExtrudedSurface * This,
            /* [in] */ IDispatch *IntersectObject,
            /* [in] */ AcExtendOption option,
            /* [retval][out] */ VARIANT *intPoints);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_PlotStyleName )( 
            IAcadExtrudedSurface * This,
            /* [retval][out] */ BSTR *plotStyle);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_PlotStyleName )( 
            IAcadExtrudedSurface * This,
            /* [in] */ BSTR plotStyle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Lineweight )( 
            IAcadExtrudedSurface * This,
            /* [retval][out] */ ACAD_LWEIGHT *Lineweight);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Lineweight )( 
            IAcadExtrudedSurface * This,
            /* [in] */ ACAD_LWEIGHT Lineweight);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Hyperlinks )( 
            IAcadExtrudedSurface * This,
            /* [retval][out] */ IAcadHyperlinks **Hyperlinks);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Material )( 
            IAcadExtrudedSurface * This,
            /* [retval][out] */ BSTR *Material);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Material )( 
            IAcadExtrudedSurface * This,
            /* [in] */ BSTR Material);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EntityName )( 
            IAcadExtrudedSurface * This,
            /* [retval][out] */ BSTR *EntityName);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EntityType )( 
            IAcadExtrudedSurface * This,
            /* [retval][out] */ long *entType);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_color )( 
            IAcadExtrudedSurface * This,
            /* [retval][out] */ ACAD_COLOR *color);
        
        /* [helpstring][hidden][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_color )( 
            IAcadExtrudedSurface * This,
            /* [in] */ ACAD_COLOR color);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SurfaceType )( 
            IAcadExtrudedSurface * This,
            /* [retval][out] */ BSTR *SurfaceType);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_UIsolineDensity )( 
            IAcadExtrudedSurface * This,
            /* [retval][out] */ long *density);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_UIsolineDensity )( 
            IAcadExtrudedSurface * This,
            /* [in] */ long density);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_VIsolineDensity )( 
            IAcadExtrudedSurface * This,
            /* [retval][out] */ long *density);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_VIsolineDensity )( 
            IAcadExtrudedSurface * This,
            /* [in] */ long density);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Height )( 
            IAcadExtrudedSurface * This,
            /* [retval][out] */ double *Height);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Height )( 
            IAcadExtrudedSurface * This,
            /* [in] */ double Height);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_TaperAngle )( 
            IAcadExtrudedSurface * This,
            /* [retval][out] */ ACAD_ANGLE *TaperAngle);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_TaperAngle )( 
            IAcadExtrudedSurface * This,
            /* [in] */ ACAD_ANGLE TaperAngle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Direction )( 
            IAcadExtrudedSurface * This,
            /* [retval][out] */ VARIANT *Direction);
        
        END_INTERFACE
    } IAcadExtrudedSurfaceVtbl;

    interface IAcadExtrudedSurface
    {
        CONST_VTBL struct IAcadExtrudedSurfaceVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAcadExtrudedSurface_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAcadExtrudedSurface_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAcadExtrudedSurface_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAcadExtrudedSurface_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IAcadExtrudedSurface_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IAcadExtrudedSurface_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IAcadExtrudedSurface_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IAcadExtrudedSurface_get_Handle(This,Handle)	\
    ( (This)->lpVtbl -> get_Handle(This,Handle) ) 

#define IAcadExtrudedSurface_get_ObjectName(This,ObjectName)	\
    ( (This)->lpVtbl -> get_ObjectName(This,ObjectName) ) 

#define IAcadExtrudedSurface_GetXData(This,AppName,XDataType,XDataValue)	\
    ( (This)->lpVtbl -> GetXData(This,AppName,XDataType,XDataValue) ) 

#define IAcadExtrudedSurface_SetXData(This,XDataType,XDataValue)	\
    ( (This)->lpVtbl -> SetXData(This,XDataType,XDataValue) ) 

#define IAcadExtrudedSurface_Delete(This)	\
    ( (This)->lpVtbl -> Delete(This) ) 

#define IAcadExtrudedSurface_get_ObjectID(This,ObjectID)	\
    ( (This)->lpVtbl -> get_ObjectID(This,ObjectID) ) 

#define IAcadExtrudedSurface_get_Application(This,ApplicationObject)	\
    ( (This)->lpVtbl -> get_Application(This,ApplicationObject) ) 

#define IAcadExtrudedSurface_get_Database(This,pDatabase)	\
    ( (This)->lpVtbl -> get_Database(This,pDatabase) ) 

#define IAcadExtrudedSurface_get_HasExtensionDictionary(This,bHasDictionary)	\
    ( (This)->lpVtbl -> get_HasExtensionDictionary(This,bHasDictionary) ) 

#define IAcadExtrudedSurface_GetExtensionDictionary(This,pExtDictionary)	\
    ( (This)->lpVtbl -> GetExtensionDictionary(This,pExtDictionary) ) 

#define IAcadExtrudedSurface_get_OwnerID(This,OwnerID)	\
    ( (This)->lpVtbl -> get_OwnerID(This,OwnerID) ) 

#define IAcadExtrudedSurface_get_Document(This,pDocument)	\
    ( (This)->lpVtbl -> get_Document(This,pDocument) ) 

#define IAcadExtrudedSurface_Erase(This)	\
    ( (This)->lpVtbl -> Erase(This) ) 


#define IAcadExtrudedSurface_get_TrueColor(This,pColor)	\
    ( (This)->lpVtbl -> get_TrueColor(This,pColor) ) 

#define IAcadExtrudedSurface_put_TrueColor(This,pColor)	\
    ( (This)->lpVtbl -> put_TrueColor(This,pColor) ) 

#define IAcadExtrudedSurface_get_Layer(This,Layer)	\
    ( (This)->lpVtbl -> get_Layer(This,Layer) ) 

#define IAcadExtrudedSurface_put_Layer(This,Layer)	\
    ( (This)->lpVtbl -> put_Layer(This,Layer) ) 

#define IAcadExtrudedSurface_get_Linetype(This,Linetype)	\
    ( (This)->lpVtbl -> get_Linetype(This,Linetype) ) 

#define IAcadExtrudedSurface_put_Linetype(This,Linetype)	\
    ( (This)->lpVtbl -> put_Linetype(This,Linetype) ) 

#define IAcadExtrudedSurface_get_LinetypeScale(This,ltScale)	\
    ( (This)->lpVtbl -> get_LinetypeScale(This,ltScale) ) 

#define IAcadExtrudedSurface_put_LinetypeScale(This,ltScale)	\
    ( (This)->lpVtbl -> put_LinetypeScale(This,ltScale) ) 

#define IAcadExtrudedSurface_get_Visible(This,bVisible)	\
    ( (This)->lpVtbl -> get_Visible(This,bVisible) ) 

#define IAcadExtrudedSurface_put_Visible(This,bVisible)	\
    ( (This)->lpVtbl -> put_Visible(This,bVisible) ) 

#define IAcadExtrudedSurface_ArrayPolar(This,NumberOfObjects,AngleToFill,CenterPoint,pArrayObjs)	\
    ( (This)->lpVtbl -> ArrayPolar(This,NumberOfObjects,AngleToFill,CenterPoint,pArrayObjs) ) 

#define IAcadExtrudedSurface_ArrayRectangular(This,NumberOfRows,NumberOfColumns,NumberOfLevels,DistBetweenRows,DistBetweenCols,DistBetweenLevels,pArrayObjs)	\
    ( (This)->lpVtbl -> ArrayRectangular(This,NumberOfRows,NumberOfColumns,NumberOfLevels,DistBetweenRows,DistBetweenCols,DistBetweenLevels,pArrayObjs) ) 

#define IAcadExtrudedSurface_Highlight(This,HighlightFlag)	\
    ( (This)->lpVtbl -> Highlight(This,HighlightFlag) ) 

#define IAcadExtrudedSurface_Copy(This,pCopyObj)	\
    ( (This)->lpVtbl -> Copy(This,pCopyObj) ) 

#define IAcadExtrudedSurface_Move(This,FromPoint,ToPoint)	\
    ( (This)->lpVtbl -> Move(This,FromPoint,ToPoint) ) 

#define IAcadExtrudedSurface_Rotate(This,BasePoint,RotationAngle)	\
    ( (This)->lpVtbl -> Rotate(This,BasePoint,RotationAngle) ) 

#define IAcadExtrudedSurface_Rotate3D(This,Point1,Point2,RotationAngle)	\
    ( (This)->lpVtbl -> Rotate3D(This,Point1,Point2,RotationAngle) ) 

#define IAcadExtrudedSurface_Mirror(This,Point1,Point2,pMirrorObj)	\
    ( (This)->lpVtbl -> Mirror(This,Point1,Point2,pMirrorObj) ) 

#define IAcadExtrudedSurface_Mirror3D(This,Point1,Point2,point3,pMirrorObj)	\
    ( (This)->lpVtbl -> Mirror3D(This,Point1,Point2,point3,pMirrorObj) ) 

#define IAcadExtrudedSurface_ScaleEntity(This,BasePoint,ScaleFactor)	\
    ( (This)->lpVtbl -> ScaleEntity(This,BasePoint,ScaleFactor) ) 

#define IAcadExtrudedSurface_TransformBy(This,TransformationMatrix)	\
    ( (This)->lpVtbl -> TransformBy(This,TransformationMatrix) ) 

#define IAcadExtrudedSurface_Update(This)	\
    ( (This)->lpVtbl -> Update(This) ) 

#define IAcadExtrudedSurface_GetBoundingBox(This,MinPoint,MaxPoint)	\
    ( (This)->lpVtbl -> GetBoundingBox(This,MinPoint,MaxPoint) ) 

#define IAcadExtrudedSurface_IntersectWith(This,IntersectObject,option,intPoints)	\
    ( (This)->lpVtbl -> IntersectWith(This,IntersectObject,option,intPoints) ) 

#define IAcadExtrudedSurface_get_PlotStyleName(This,plotStyle)	\
    ( (This)->lpVtbl -> get_PlotStyleName(This,plotStyle) ) 

#define IAcadExtrudedSurface_put_PlotStyleName(This,plotStyle)	\
    ( (This)->lpVtbl -> put_PlotStyleName(This,plotStyle) ) 

#define IAcadExtrudedSurface_get_Lineweight(This,Lineweight)	\
    ( (This)->lpVtbl -> get_Lineweight(This,Lineweight) ) 

#define IAcadExtrudedSurface_put_Lineweight(This,Lineweight)	\
    ( (This)->lpVtbl -> put_Lineweight(This,Lineweight) ) 

#define IAcadExtrudedSurface_get_Hyperlinks(This,Hyperlinks)	\
    ( (This)->lpVtbl -> get_Hyperlinks(This,Hyperlinks) ) 

#define IAcadExtrudedSurface_get_Material(This,Material)	\
    ( (This)->lpVtbl -> get_Material(This,Material) ) 

#define IAcadExtrudedSurface_put_Material(This,Material)	\
    ( (This)->lpVtbl -> put_Material(This,Material) ) 

#define IAcadExtrudedSurface_get_EntityName(This,EntityName)	\
    ( (This)->lpVtbl -> get_EntityName(This,EntityName) ) 

#define IAcadExtrudedSurface_get_EntityType(This,entType)	\
    ( (This)->lpVtbl -> get_EntityType(This,entType) ) 

#define IAcadExtrudedSurface_get_color(This,color)	\
    ( (This)->lpVtbl -> get_color(This,color) ) 

#define IAcadExtrudedSurface_put_color(This,color)	\
    ( (This)->lpVtbl -> put_color(This,color) ) 


#define IAcadExtrudedSurface_get_SurfaceType(This,SurfaceType)	\
    ( (This)->lpVtbl -> get_SurfaceType(This,SurfaceType) ) 

#define IAcadExtrudedSurface_get_UIsolineDensity(This,density)	\
    ( (This)->lpVtbl -> get_UIsolineDensity(This,density) ) 

#define IAcadExtrudedSurface_put_UIsolineDensity(This,density)	\
    ( (This)->lpVtbl -> put_UIsolineDensity(This,density) ) 

#define IAcadExtrudedSurface_get_VIsolineDensity(This,density)	\
    ( (This)->lpVtbl -> get_VIsolineDensity(This,density) ) 

#define IAcadExtrudedSurface_put_VIsolineDensity(This,density)	\
    ( (This)->lpVtbl -> put_VIsolineDensity(This,density) ) 


#define IAcadExtrudedSurface_get_Height(This,Height)	\
    ( (This)->lpVtbl -> get_Height(This,Height) ) 

#define IAcadExtrudedSurface_put_Height(This,Height)	\
    ( (This)->lpVtbl -> put_Height(This,Height) ) 

#define IAcadExtrudedSurface_get_TaperAngle(This,TaperAngle)	\
    ( (This)->lpVtbl -> get_TaperAngle(This,TaperAngle) ) 

#define IAcadExtrudedSurface_put_TaperAngle(This,TaperAngle)	\
    ( (This)->lpVtbl -> put_TaperAngle(This,TaperAngle) ) 

#define IAcadExtrudedSurface_get_Direction(This,Direction)	\
    ( (This)->lpVtbl -> get_Direction(This,Direction) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAcadExtrudedSurface_INTERFACE_DEFINED__ */


#ifndef __IAcadRevolvedSurface_INTERFACE_DEFINED__
#define __IAcadRevolvedSurface_INTERFACE_DEFINED__

/* interface IAcadRevolvedSurface */
/* [object][oleautomation][dual][helpstring][uuid] */ 


EXTERN_C const IID IID_IAcadRevolvedSurface;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("2AA6DCC2-C242-49CB-A576-545E4F500722")
    IAcadRevolvedSurface : public IAcadSurface
    {
    public:
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_RevolutionAngle( 
            /* [retval][out] */ ACAD_ANGLE *revAngle) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_RevolutionAngle( 
            /* [in] */ ACAD_ANGLE revAngle) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_AxisPosition( 
            /* [retval][out] */ VARIANT *AxisPosition) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_AxisPosition( 
            /* [in] */ VARIANT AxisPosition) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_AxisDirection( 
            /* [retval][out] */ VARIANT *AxisDirection) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct IAcadRevolvedSurfaceVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAcadRevolvedSurface * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAcadRevolvedSurface * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAcadRevolvedSurface * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IAcadRevolvedSurface * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IAcadRevolvedSurface * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IAcadRevolvedSurface * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IAcadRevolvedSurface * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Handle )( 
            IAcadRevolvedSurface * This,
            /* [retval][out] */ BSTR *Handle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ObjectName )( 
            IAcadRevolvedSurface * This,
            /* [retval][out] */ BSTR *ObjectName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetXData )( 
            IAcadRevolvedSurface * This,
            /* [in] */ BSTR AppName,
            /* [out] */ VARIANT *XDataType,
            /* [out] */ VARIANT *XDataValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetXData )( 
            IAcadRevolvedSurface * This,
            /* [in] */ VARIANT XDataType,
            /* [in] */ VARIANT XDataValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Delete )( 
            IAcadRevolvedSurface * This);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ObjectID )( 
            IAcadRevolvedSurface * This,
            /* [retval][out] */ LONG_PTR *ObjectID);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Application )( 
            IAcadRevolvedSurface * This,
            /* [retval][out] */ IDispatch **ApplicationObject);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Database )( 
            IAcadRevolvedSurface * This,
            /* [retval][out] */ IAcadDatabase **pDatabase);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_HasExtensionDictionary )( 
            IAcadRevolvedSurface * This,
            /* [retval][out] */ VARIANT_BOOL *bHasDictionary);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetExtensionDictionary )( 
            IAcadRevolvedSurface * This,
            /* [retval][out] */ IAcadDictionary **pExtDictionary);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_OwnerID )( 
            IAcadRevolvedSurface * This,
            /* [retval][out] */ LONG_PTR *OwnerID);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Document )( 
            IAcadRevolvedSurface * This,
            /* [retval][out] */ IDispatch **pDocument);
        
        /* [helpstring][hidden][id] */ HRESULT ( STDMETHODCALLTYPE *Erase )( 
            IAcadRevolvedSurface * This);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_TrueColor )( 
            IAcadRevolvedSurface * This,
            /* [retval][out] */ IAcadAcCmColor **pColor);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_TrueColor )( 
            IAcadRevolvedSurface * This,
            /* [in] */ IAcadAcCmColor *pColor);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Layer )( 
            IAcadRevolvedSurface * This,
            /* [retval][out] */ BSTR *Layer);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Layer )( 
            IAcadRevolvedSurface * This,
            /* [in] */ BSTR Layer);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Linetype )( 
            IAcadRevolvedSurface * This,
            /* [retval][out] */ BSTR *Linetype);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Linetype )( 
            IAcadRevolvedSurface * This,
            /* [in] */ BSTR Linetype);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_LinetypeScale )( 
            IAcadRevolvedSurface * This,
            /* [retval][out] */ ACAD_NOUNITS *ltScale);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_LinetypeScale )( 
            IAcadRevolvedSurface * This,
            /* [in] */ ACAD_NOUNITS ltScale);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Visible )( 
            IAcadRevolvedSurface * This,
            /* [retval][out] */ VARIANT_BOOL *bVisible);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Visible )( 
            IAcadRevolvedSurface * This,
            /* [in] */ VARIANT_BOOL bVisible);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ArrayPolar )( 
            IAcadRevolvedSurface * This,
            /* [in] */ int NumberOfObjects,
            /* [in] */ double AngleToFill,
            /* [in] */ VARIANT CenterPoint,
            /* [retval][out] */ VARIANT *pArrayObjs);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ArrayRectangular )( 
            IAcadRevolvedSurface * This,
            /* [in] */ int NumberOfRows,
            /* [in] */ int NumberOfColumns,
            /* [in] */ int NumberOfLevels,
            /* [in] */ double DistBetweenRows,
            /* [in] */ double DistBetweenCols,
            /* [in] */ double DistBetweenLevels,
            /* [retval][out] */ VARIANT *pArrayObjs);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Highlight )( 
            IAcadRevolvedSurface * This,
            /* [in] */ VARIANT_BOOL HighlightFlag);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Copy )( 
            IAcadRevolvedSurface * This,
            /* [retval][out] */ IDispatch **pCopyObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Move )( 
            IAcadRevolvedSurface * This,
            /* [in] */ VARIANT FromPoint,
            /* [in] */ VARIANT ToPoint);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Rotate )( 
            IAcadRevolvedSurface * This,
            /* [in] */ VARIANT BasePoint,
            /* [in] */ double RotationAngle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Rotate3D )( 
            IAcadRevolvedSurface * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [in] */ double RotationAngle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Mirror )( 
            IAcadRevolvedSurface * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [retval][out] */ IDispatch **pMirrorObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Mirror3D )( 
            IAcadRevolvedSurface * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [in] */ VARIANT point3,
            /* [retval][out] */ IDispatch **pMirrorObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ScaleEntity )( 
            IAcadRevolvedSurface * This,
            /* [in] */ VARIANT BasePoint,
            /* [in] */ double ScaleFactor);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *TransformBy )( 
            IAcadRevolvedSurface * This,
            /* [in] */ VARIANT TransformationMatrix);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Update )( 
            IAcadRevolvedSurface * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetBoundingBox )( 
            IAcadRevolvedSurface * This,
            /* [out] */ VARIANT *MinPoint,
            /* [out] */ VARIANT *MaxPoint);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *IntersectWith )( 
            IAcadRevolvedSurface * This,
            /* [in] */ IDispatch *IntersectObject,
            /* [in] */ AcExtendOption option,
            /* [retval][out] */ VARIANT *intPoints);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_PlotStyleName )( 
            IAcadRevolvedSurface * This,
            /* [retval][out] */ BSTR *plotStyle);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_PlotStyleName )( 
            IAcadRevolvedSurface * This,
            /* [in] */ BSTR plotStyle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Lineweight )( 
            IAcadRevolvedSurface * This,
            /* [retval][out] */ ACAD_LWEIGHT *Lineweight);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Lineweight )( 
            IAcadRevolvedSurface * This,
            /* [in] */ ACAD_LWEIGHT Lineweight);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Hyperlinks )( 
            IAcadRevolvedSurface * This,
            /* [retval][out] */ IAcadHyperlinks **Hyperlinks);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Material )( 
            IAcadRevolvedSurface * This,
            /* [retval][out] */ BSTR *Material);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Material )( 
            IAcadRevolvedSurface * This,
            /* [in] */ BSTR Material);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EntityName )( 
            IAcadRevolvedSurface * This,
            /* [retval][out] */ BSTR *EntityName);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EntityType )( 
            IAcadRevolvedSurface * This,
            /* [retval][out] */ long *entType);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_color )( 
            IAcadRevolvedSurface * This,
            /* [retval][out] */ ACAD_COLOR *color);
        
        /* [helpstring][hidden][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_color )( 
            IAcadRevolvedSurface * This,
            /* [in] */ ACAD_COLOR color);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SurfaceType )( 
            IAcadRevolvedSurface * This,
            /* [retval][out] */ BSTR *SurfaceType);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_UIsolineDensity )( 
            IAcadRevolvedSurface * This,
            /* [retval][out] */ long *density);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_UIsolineDensity )( 
            IAcadRevolvedSurface * This,
            /* [in] */ long density);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_VIsolineDensity )( 
            IAcadRevolvedSurface * This,
            /* [retval][out] */ long *density);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_VIsolineDensity )( 
            IAcadRevolvedSurface * This,
            /* [in] */ long density);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_RevolutionAngle )( 
            IAcadRevolvedSurface * This,
            /* [retval][out] */ ACAD_ANGLE *revAngle);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_RevolutionAngle )( 
            IAcadRevolvedSurface * This,
            /* [in] */ ACAD_ANGLE revAngle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AxisPosition )( 
            IAcadRevolvedSurface * This,
            /* [retval][out] */ VARIANT *AxisPosition);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AxisPosition )( 
            IAcadRevolvedSurface * This,
            /* [in] */ VARIANT AxisPosition);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AxisDirection )( 
            IAcadRevolvedSurface * This,
            /* [retval][out] */ VARIANT *AxisDirection);
        
        END_INTERFACE
    } IAcadRevolvedSurfaceVtbl;

    interface IAcadRevolvedSurface
    {
        CONST_VTBL struct IAcadRevolvedSurfaceVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAcadRevolvedSurface_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAcadRevolvedSurface_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAcadRevolvedSurface_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAcadRevolvedSurface_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IAcadRevolvedSurface_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IAcadRevolvedSurface_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IAcadRevolvedSurface_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IAcadRevolvedSurface_get_Handle(This,Handle)	\
    ( (This)->lpVtbl -> get_Handle(This,Handle) ) 

#define IAcadRevolvedSurface_get_ObjectName(This,ObjectName)	\
    ( (This)->lpVtbl -> get_ObjectName(This,ObjectName) ) 

#define IAcadRevolvedSurface_GetXData(This,AppName,XDataType,XDataValue)	\
    ( (This)->lpVtbl -> GetXData(This,AppName,XDataType,XDataValue) ) 

#define IAcadRevolvedSurface_SetXData(This,XDataType,XDataValue)	\
    ( (This)->lpVtbl -> SetXData(This,XDataType,XDataValue) ) 

#define IAcadRevolvedSurface_Delete(This)	\
    ( (This)->lpVtbl -> Delete(This) ) 

#define IAcadRevolvedSurface_get_ObjectID(This,ObjectID)	\
    ( (This)->lpVtbl -> get_ObjectID(This,ObjectID) ) 

#define IAcadRevolvedSurface_get_Application(This,ApplicationObject)	\
    ( (This)->lpVtbl -> get_Application(This,ApplicationObject) ) 

#define IAcadRevolvedSurface_get_Database(This,pDatabase)	\
    ( (This)->lpVtbl -> get_Database(This,pDatabase) ) 

#define IAcadRevolvedSurface_get_HasExtensionDictionary(This,bHasDictionary)	\
    ( (This)->lpVtbl -> get_HasExtensionDictionary(This,bHasDictionary) ) 

#define IAcadRevolvedSurface_GetExtensionDictionary(This,pExtDictionary)	\
    ( (This)->lpVtbl -> GetExtensionDictionary(This,pExtDictionary) ) 

#define IAcadRevolvedSurface_get_OwnerID(This,OwnerID)	\
    ( (This)->lpVtbl -> get_OwnerID(This,OwnerID) ) 

#define IAcadRevolvedSurface_get_Document(This,pDocument)	\
    ( (This)->lpVtbl -> get_Document(This,pDocument) ) 

#define IAcadRevolvedSurface_Erase(This)	\
    ( (This)->lpVtbl -> Erase(This) ) 


#define IAcadRevolvedSurface_get_TrueColor(This,pColor)	\
    ( (This)->lpVtbl -> get_TrueColor(This,pColor) ) 

#define IAcadRevolvedSurface_put_TrueColor(This,pColor)	\
    ( (This)->lpVtbl -> put_TrueColor(This,pColor) ) 

#define IAcadRevolvedSurface_get_Layer(This,Layer)	\
    ( (This)->lpVtbl -> get_Layer(This,Layer) ) 

#define IAcadRevolvedSurface_put_Layer(This,Layer)	\
    ( (This)->lpVtbl -> put_Layer(This,Layer) ) 

#define IAcadRevolvedSurface_get_Linetype(This,Linetype)	\
    ( (This)->lpVtbl -> get_Linetype(This,Linetype) ) 

#define IAcadRevolvedSurface_put_Linetype(This,Linetype)	\
    ( (This)->lpVtbl -> put_Linetype(This,Linetype) ) 

#define IAcadRevolvedSurface_get_LinetypeScale(This,ltScale)	\
    ( (This)->lpVtbl -> get_LinetypeScale(This,ltScale) ) 

#define IAcadRevolvedSurface_put_LinetypeScale(This,ltScale)	\
    ( (This)->lpVtbl -> put_LinetypeScale(This,ltScale) ) 

#define IAcadRevolvedSurface_get_Visible(This,bVisible)	\
    ( (This)->lpVtbl -> get_Visible(This,bVisible) ) 

#define IAcadRevolvedSurface_put_Visible(This,bVisible)	\
    ( (This)->lpVtbl -> put_Visible(This,bVisible) ) 

#define IAcadRevolvedSurface_ArrayPolar(This,NumberOfObjects,AngleToFill,CenterPoint,pArrayObjs)	\
    ( (This)->lpVtbl -> ArrayPolar(This,NumberOfObjects,AngleToFill,CenterPoint,pArrayObjs) ) 

#define IAcadRevolvedSurface_ArrayRectangular(This,NumberOfRows,NumberOfColumns,NumberOfLevels,DistBetweenRows,DistBetweenCols,DistBetweenLevels,pArrayObjs)	\
    ( (This)->lpVtbl -> ArrayRectangular(This,NumberOfRows,NumberOfColumns,NumberOfLevels,DistBetweenRows,DistBetweenCols,DistBetweenLevels,pArrayObjs) ) 

#define IAcadRevolvedSurface_Highlight(This,HighlightFlag)	\
    ( (This)->lpVtbl -> Highlight(This,HighlightFlag) ) 

#define IAcadRevolvedSurface_Copy(This,pCopyObj)	\
    ( (This)->lpVtbl -> Copy(This,pCopyObj) ) 

#define IAcadRevolvedSurface_Move(This,FromPoint,ToPoint)	\
    ( (This)->lpVtbl -> Move(This,FromPoint,ToPoint) ) 

#define IAcadRevolvedSurface_Rotate(This,BasePoint,RotationAngle)	\
    ( (This)->lpVtbl -> Rotate(This,BasePoint,RotationAngle) ) 

#define IAcadRevolvedSurface_Rotate3D(This,Point1,Point2,RotationAngle)	\
    ( (This)->lpVtbl -> Rotate3D(This,Point1,Point2,RotationAngle) ) 

#define IAcadRevolvedSurface_Mirror(This,Point1,Point2,pMirrorObj)	\
    ( (This)->lpVtbl -> Mirror(This,Point1,Point2,pMirrorObj) ) 

#define IAcadRevolvedSurface_Mirror3D(This,Point1,Point2,point3,pMirrorObj)	\
    ( (This)->lpVtbl -> Mirror3D(This,Point1,Point2,point3,pMirrorObj) ) 

#define IAcadRevolvedSurface_ScaleEntity(This,BasePoint,ScaleFactor)	\
    ( (This)->lpVtbl -> ScaleEntity(This,BasePoint,ScaleFactor) ) 

#define IAcadRevolvedSurface_TransformBy(This,TransformationMatrix)	\
    ( (This)->lpVtbl -> TransformBy(This,TransformationMatrix) ) 

#define IAcadRevolvedSurface_Update(This)	\
    ( (This)->lpVtbl -> Update(This) ) 

#define IAcadRevolvedSurface_GetBoundingBox(This,MinPoint,MaxPoint)	\
    ( (This)->lpVtbl -> GetBoundingBox(This,MinPoint,MaxPoint) ) 

#define IAcadRevolvedSurface_IntersectWith(This,IntersectObject,option,intPoints)	\
    ( (This)->lpVtbl -> IntersectWith(This,IntersectObject,option,intPoints) ) 

#define IAcadRevolvedSurface_get_PlotStyleName(This,plotStyle)	\
    ( (This)->lpVtbl -> get_PlotStyleName(This,plotStyle) ) 

#define IAcadRevolvedSurface_put_PlotStyleName(This,plotStyle)	\
    ( (This)->lpVtbl -> put_PlotStyleName(This,plotStyle) ) 

#define IAcadRevolvedSurface_get_Lineweight(This,Lineweight)	\
    ( (This)->lpVtbl -> get_Lineweight(This,Lineweight) ) 

#define IAcadRevolvedSurface_put_Lineweight(This,Lineweight)	\
    ( (This)->lpVtbl -> put_Lineweight(This,Lineweight) ) 

#define IAcadRevolvedSurface_get_Hyperlinks(This,Hyperlinks)	\
    ( (This)->lpVtbl -> get_Hyperlinks(This,Hyperlinks) ) 

#define IAcadRevolvedSurface_get_Material(This,Material)	\
    ( (This)->lpVtbl -> get_Material(This,Material) ) 

#define IAcadRevolvedSurface_put_Material(This,Material)	\
    ( (This)->lpVtbl -> put_Material(This,Material) ) 

#define IAcadRevolvedSurface_get_EntityName(This,EntityName)	\
    ( (This)->lpVtbl -> get_EntityName(This,EntityName) ) 

#define IAcadRevolvedSurface_get_EntityType(This,entType)	\
    ( (This)->lpVtbl -> get_EntityType(This,entType) ) 

#define IAcadRevolvedSurface_get_color(This,color)	\
    ( (This)->lpVtbl -> get_color(This,color) ) 

#define IAcadRevolvedSurface_put_color(This,color)	\
    ( (This)->lpVtbl -> put_color(This,color) ) 


#define IAcadRevolvedSurface_get_SurfaceType(This,SurfaceType)	\
    ( (This)->lpVtbl -> get_SurfaceType(This,SurfaceType) ) 

#define IAcadRevolvedSurface_get_UIsolineDensity(This,density)	\
    ( (This)->lpVtbl -> get_UIsolineDensity(This,density) ) 

#define IAcadRevolvedSurface_put_UIsolineDensity(This,density)	\
    ( (This)->lpVtbl -> put_UIsolineDensity(This,density) ) 

#define IAcadRevolvedSurface_get_VIsolineDensity(This,density)	\
    ( (This)->lpVtbl -> get_VIsolineDensity(This,density) ) 

#define IAcadRevolvedSurface_put_VIsolineDensity(This,density)	\
    ( (This)->lpVtbl -> put_VIsolineDensity(This,density) ) 


#define IAcadRevolvedSurface_get_RevolutionAngle(This,revAngle)	\
    ( (This)->lpVtbl -> get_RevolutionAngle(This,revAngle) ) 

#define IAcadRevolvedSurface_put_RevolutionAngle(This,revAngle)	\
    ( (This)->lpVtbl -> put_RevolutionAngle(This,revAngle) ) 

#define IAcadRevolvedSurface_get_AxisPosition(This,AxisPosition)	\
    ( (This)->lpVtbl -> get_AxisPosition(This,AxisPosition) ) 

#define IAcadRevolvedSurface_put_AxisPosition(This,AxisPosition)	\
    ( (This)->lpVtbl -> put_AxisPosition(This,AxisPosition) ) 

#define IAcadRevolvedSurface_get_AxisDirection(This,AxisDirection)	\
    ( (This)->lpVtbl -> get_AxisDirection(This,AxisDirection) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAcadRevolvedSurface_INTERFACE_DEFINED__ */


#ifndef __IAcadSweptSurface_INTERFACE_DEFINED__
#define __IAcadSweptSurface_INTERFACE_DEFINED__

/* interface IAcadSweptSurface */
/* [object][oleautomation][dual][helpstring][uuid] */ 


EXTERN_C const IID IID_IAcadSweptSurface;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("A1E46554-B62E-4CEB-A9B8-8C9BC7E8252D")
    IAcadSweptSurface : public IAcadSurface
    {
    public:
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_ProfileRotation( 
            /* [retval][out] */ ACAD_ANGLE *profileRotationAngle) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_ProfileRotation( 
            /* [in] */ ACAD_ANGLE profileRotationAngle) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_Bank( 
            /* [retval][out] */ VARIANT_BOOL *bBank) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_Bank( 
            /* [in] */ VARIANT_BOOL bBank) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_Twist( 
            /* [retval][out] */ ACAD_ANGLE *TwistAngle) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_Twist( 
            /* [in] */ ACAD_ANGLE TwistAngle) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_scale( 
            /* [retval][out] */ double *scale) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_scale( 
            /* [in] */ double scale) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_Length( 
            /* [retval][out] */ double *Length) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct IAcadSweptSurfaceVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAcadSweptSurface * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAcadSweptSurface * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAcadSweptSurface * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IAcadSweptSurface * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IAcadSweptSurface * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IAcadSweptSurface * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IAcadSweptSurface * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Handle )( 
            IAcadSweptSurface * This,
            /* [retval][out] */ BSTR *Handle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ObjectName )( 
            IAcadSweptSurface * This,
            /* [retval][out] */ BSTR *ObjectName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetXData )( 
            IAcadSweptSurface * This,
            /* [in] */ BSTR AppName,
            /* [out] */ VARIANT *XDataType,
            /* [out] */ VARIANT *XDataValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetXData )( 
            IAcadSweptSurface * This,
            /* [in] */ VARIANT XDataType,
            /* [in] */ VARIANT XDataValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Delete )( 
            IAcadSweptSurface * This);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ObjectID )( 
            IAcadSweptSurface * This,
            /* [retval][out] */ LONG_PTR *ObjectID);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Application )( 
            IAcadSweptSurface * This,
            /* [retval][out] */ IDispatch **ApplicationObject);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Database )( 
            IAcadSweptSurface * This,
            /* [retval][out] */ IAcadDatabase **pDatabase);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_HasExtensionDictionary )( 
            IAcadSweptSurface * This,
            /* [retval][out] */ VARIANT_BOOL *bHasDictionary);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetExtensionDictionary )( 
            IAcadSweptSurface * This,
            /* [retval][out] */ IAcadDictionary **pExtDictionary);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_OwnerID )( 
            IAcadSweptSurface * This,
            /* [retval][out] */ LONG_PTR *OwnerID);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Document )( 
            IAcadSweptSurface * This,
            /* [retval][out] */ IDispatch **pDocument);
        
        /* [helpstring][hidden][id] */ HRESULT ( STDMETHODCALLTYPE *Erase )( 
            IAcadSweptSurface * This);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_TrueColor )( 
            IAcadSweptSurface * This,
            /* [retval][out] */ IAcadAcCmColor **pColor);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_TrueColor )( 
            IAcadSweptSurface * This,
            /* [in] */ IAcadAcCmColor *pColor);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Layer )( 
            IAcadSweptSurface * This,
            /* [retval][out] */ BSTR *Layer);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Layer )( 
            IAcadSweptSurface * This,
            /* [in] */ BSTR Layer);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Linetype )( 
            IAcadSweptSurface * This,
            /* [retval][out] */ BSTR *Linetype);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Linetype )( 
            IAcadSweptSurface * This,
            /* [in] */ BSTR Linetype);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_LinetypeScale )( 
            IAcadSweptSurface * This,
            /* [retval][out] */ ACAD_NOUNITS *ltScale);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_LinetypeScale )( 
            IAcadSweptSurface * This,
            /* [in] */ ACAD_NOUNITS ltScale);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Visible )( 
            IAcadSweptSurface * This,
            /* [retval][out] */ VARIANT_BOOL *bVisible);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Visible )( 
            IAcadSweptSurface * This,
            /* [in] */ VARIANT_BOOL bVisible);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ArrayPolar )( 
            IAcadSweptSurface * This,
            /* [in] */ int NumberOfObjects,
            /* [in] */ double AngleToFill,
            /* [in] */ VARIANT CenterPoint,
            /* [retval][out] */ VARIANT *pArrayObjs);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ArrayRectangular )( 
            IAcadSweptSurface * This,
            /* [in] */ int NumberOfRows,
            /* [in] */ int NumberOfColumns,
            /* [in] */ int NumberOfLevels,
            /* [in] */ double DistBetweenRows,
            /* [in] */ double DistBetweenCols,
            /* [in] */ double DistBetweenLevels,
            /* [retval][out] */ VARIANT *pArrayObjs);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Highlight )( 
            IAcadSweptSurface * This,
            /* [in] */ VARIANT_BOOL HighlightFlag);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Copy )( 
            IAcadSweptSurface * This,
            /* [retval][out] */ IDispatch **pCopyObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Move )( 
            IAcadSweptSurface * This,
            /* [in] */ VARIANT FromPoint,
            /* [in] */ VARIANT ToPoint);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Rotate )( 
            IAcadSweptSurface * This,
            /* [in] */ VARIANT BasePoint,
            /* [in] */ double RotationAngle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Rotate3D )( 
            IAcadSweptSurface * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [in] */ double RotationAngle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Mirror )( 
            IAcadSweptSurface * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [retval][out] */ IDispatch **pMirrorObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Mirror3D )( 
            IAcadSweptSurface * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [in] */ VARIANT point3,
            /* [retval][out] */ IDispatch **pMirrorObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ScaleEntity )( 
            IAcadSweptSurface * This,
            /* [in] */ VARIANT BasePoint,
            /* [in] */ double ScaleFactor);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *TransformBy )( 
            IAcadSweptSurface * This,
            /* [in] */ VARIANT TransformationMatrix);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Update )( 
            IAcadSweptSurface * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetBoundingBox )( 
            IAcadSweptSurface * This,
            /* [out] */ VARIANT *MinPoint,
            /* [out] */ VARIANT *MaxPoint);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *IntersectWith )( 
            IAcadSweptSurface * This,
            /* [in] */ IDispatch *IntersectObject,
            /* [in] */ AcExtendOption option,
            /* [retval][out] */ VARIANT *intPoints);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_PlotStyleName )( 
            IAcadSweptSurface * This,
            /* [retval][out] */ BSTR *plotStyle);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_PlotStyleName )( 
            IAcadSweptSurface * This,
            /* [in] */ BSTR plotStyle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Lineweight )( 
            IAcadSweptSurface * This,
            /* [retval][out] */ ACAD_LWEIGHT *Lineweight);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Lineweight )( 
            IAcadSweptSurface * This,
            /* [in] */ ACAD_LWEIGHT Lineweight);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Hyperlinks )( 
            IAcadSweptSurface * This,
            /* [retval][out] */ IAcadHyperlinks **Hyperlinks);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Material )( 
            IAcadSweptSurface * This,
            /* [retval][out] */ BSTR *Material);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Material )( 
            IAcadSweptSurface * This,
            /* [in] */ BSTR Material);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EntityName )( 
            IAcadSweptSurface * This,
            /* [retval][out] */ BSTR *EntityName);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EntityType )( 
            IAcadSweptSurface * This,
            /* [retval][out] */ long *entType);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_color )( 
            IAcadSweptSurface * This,
            /* [retval][out] */ ACAD_COLOR *color);
        
        /* [helpstring][hidden][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_color )( 
            IAcadSweptSurface * This,
            /* [in] */ ACAD_COLOR color);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SurfaceType )( 
            IAcadSweptSurface * This,
            /* [retval][out] */ BSTR *SurfaceType);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_UIsolineDensity )( 
            IAcadSweptSurface * This,
            /* [retval][out] */ long *density);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_UIsolineDensity )( 
            IAcadSweptSurface * This,
            /* [in] */ long density);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_VIsolineDensity )( 
            IAcadSweptSurface * This,
            /* [retval][out] */ long *density);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_VIsolineDensity )( 
            IAcadSweptSurface * This,
            /* [in] */ long density);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ProfileRotation )( 
            IAcadSweptSurface * This,
            /* [retval][out] */ ACAD_ANGLE *profileRotationAngle);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ProfileRotation )( 
            IAcadSweptSurface * This,
            /* [in] */ ACAD_ANGLE profileRotationAngle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Bank )( 
            IAcadSweptSurface * This,
            /* [retval][out] */ VARIANT_BOOL *bBank);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Bank )( 
            IAcadSweptSurface * This,
            /* [in] */ VARIANT_BOOL bBank);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Twist )( 
            IAcadSweptSurface * This,
            /* [retval][out] */ ACAD_ANGLE *TwistAngle);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Twist )( 
            IAcadSweptSurface * This,
            /* [in] */ ACAD_ANGLE TwistAngle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_scale )( 
            IAcadSweptSurface * This,
            /* [retval][out] */ double *scale);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_scale )( 
            IAcadSweptSurface * This,
            /* [in] */ double scale);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Length )( 
            IAcadSweptSurface * This,
            /* [retval][out] */ double *Length);
        
        END_INTERFACE
    } IAcadSweptSurfaceVtbl;

    interface IAcadSweptSurface
    {
        CONST_VTBL struct IAcadSweptSurfaceVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAcadSweptSurface_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAcadSweptSurface_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAcadSweptSurface_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAcadSweptSurface_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IAcadSweptSurface_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IAcadSweptSurface_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IAcadSweptSurface_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IAcadSweptSurface_get_Handle(This,Handle)	\
    ( (This)->lpVtbl -> get_Handle(This,Handle) ) 

#define IAcadSweptSurface_get_ObjectName(This,ObjectName)	\
    ( (This)->lpVtbl -> get_ObjectName(This,ObjectName) ) 

#define IAcadSweptSurface_GetXData(This,AppName,XDataType,XDataValue)	\
    ( (This)->lpVtbl -> GetXData(This,AppName,XDataType,XDataValue) ) 

#define IAcadSweptSurface_SetXData(This,XDataType,XDataValue)	\
    ( (This)->lpVtbl -> SetXData(This,XDataType,XDataValue) ) 

#define IAcadSweptSurface_Delete(This)	\
    ( (This)->lpVtbl -> Delete(This) ) 

#define IAcadSweptSurface_get_ObjectID(This,ObjectID)	\
    ( (This)->lpVtbl -> get_ObjectID(This,ObjectID) ) 

#define IAcadSweptSurface_get_Application(This,ApplicationObject)	\
    ( (This)->lpVtbl -> get_Application(This,ApplicationObject) ) 

#define IAcadSweptSurface_get_Database(This,pDatabase)	\
    ( (This)->lpVtbl -> get_Database(This,pDatabase) ) 

#define IAcadSweptSurface_get_HasExtensionDictionary(This,bHasDictionary)	\
    ( (This)->lpVtbl -> get_HasExtensionDictionary(This,bHasDictionary) ) 

#define IAcadSweptSurface_GetExtensionDictionary(This,pExtDictionary)	\
    ( (This)->lpVtbl -> GetExtensionDictionary(This,pExtDictionary) ) 

#define IAcadSweptSurface_get_OwnerID(This,OwnerID)	\
    ( (This)->lpVtbl -> get_OwnerID(This,OwnerID) ) 

#define IAcadSweptSurface_get_Document(This,pDocument)	\
    ( (This)->lpVtbl -> get_Document(This,pDocument) ) 

#define IAcadSweptSurface_Erase(This)	\
    ( (This)->lpVtbl -> Erase(This) ) 


#define IAcadSweptSurface_get_TrueColor(This,pColor)	\
    ( (This)->lpVtbl -> get_TrueColor(This,pColor) ) 

#define IAcadSweptSurface_put_TrueColor(This,pColor)	\
    ( (This)->lpVtbl -> put_TrueColor(This,pColor) ) 

#define IAcadSweptSurface_get_Layer(This,Layer)	\
    ( (This)->lpVtbl -> get_Layer(This,Layer) ) 

#define IAcadSweptSurface_put_Layer(This,Layer)	\
    ( (This)->lpVtbl -> put_Layer(This,Layer) ) 

#define IAcadSweptSurface_get_Linetype(This,Linetype)	\
    ( (This)->lpVtbl -> get_Linetype(This,Linetype) ) 

#define IAcadSweptSurface_put_Linetype(This,Linetype)	\
    ( (This)->lpVtbl -> put_Linetype(This,Linetype) ) 

#define IAcadSweptSurface_get_LinetypeScale(This,ltScale)	\
    ( (This)->lpVtbl -> get_LinetypeScale(This,ltScale) ) 

#define IAcadSweptSurface_put_LinetypeScale(This,ltScale)	\
    ( (This)->lpVtbl -> put_LinetypeScale(This,ltScale) ) 

#define IAcadSweptSurface_get_Visible(This,bVisible)	\
    ( (This)->lpVtbl -> get_Visible(This,bVisible) ) 

#define IAcadSweptSurface_put_Visible(This,bVisible)	\
    ( (This)->lpVtbl -> put_Visible(This,bVisible) ) 

#define IAcadSweptSurface_ArrayPolar(This,NumberOfObjects,AngleToFill,CenterPoint,pArrayObjs)	\
    ( (This)->lpVtbl -> ArrayPolar(This,NumberOfObjects,AngleToFill,CenterPoint,pArrayObjs) ) 

#define IAcadSweptSurface_ArrayRectangular(This,NumberOfRows,NumberOfColumns,NumberOfLevels,DistBetweenRows,DistBetweenCols,DistBetweenLevels,pArrayObjs)	\
    ( (This)->lpVtbl -> ArrayRectangular(This,NumberOfRows,NumberOfColumns,NumberOfLevels,DistBetweenRows,DistBetweenCols,DistBetweenLevels,pArrayObjs) ) 

#define IAcadSweptSurface_Highlight(This,HighlightFlag)	\
    ( (This)->lpVtbl -> Highlight(This,HighlightFlag) ) 

#define IAcadSweptSurface_Copy(This,pCopyObj)	\
    ( (This)->lpVtbl -> Copy(This,pCopyObj) ) 

#define IAcadSweptSurface_Move(This,FromPoint,ToPoint)	\
    ( (This)->lpVtbl -> Move(This,FromPoint,ToPoint) ) 

#define IAcadSweptSurface_Rotate(This,BasePoint,RotationAngle)	\
    ( (This)->lpVtbl -> Rotate(This,BasePoint,RotationAngle) ) 

#define IAcadSweptSurface_Rotate3D(This,Point1,Point2,RotationAngle)	\
    ( (This)->lpVtbl -> Rotate3D(This,Point1,Point2,RotationAngle) ) 

#define IAcadSweptSurface_Mirror(This,Point1,Point2,pMirrorObj)	\
    ( (This)->lpVtbl -> Mirror(This,Point1,Point2,pMirrorObj) ) 

#define IAcadSweptSurface_Mirror3D(This,Point1,Point2,point3,pMirrorObj)	\
    ( (This)->lpVtbl -> Mirror3D(This,Point1,Point2,point3,pMirrorObj) ) 

#define IAcadSweptSurface_ScaleEntity(This,BasePoint,ScaleFactor)	\
    ( (This)->lpVtbl -> ScaleEntity(This,BasePoint,ScaleFactor) ) 

#define IAcadSweptSurface_TransformBy(This,TransformationMatrix)	\
    ( (This)->lpVtbl -> TransformBy(This,TransformationMatrix) ) 

#define IAcadSweptSurface_Update(This)	\
    ( (This)->lpVtbl -> Update(This) ) 

#define IAcadSweptSurface_GetBoundingBox(This,MinPoint,MaxPoint)	\
    ( (This)->lpVtbl -> GetBoundingBox(This,MinPoint,MaxPoint) ) 

#define IAcadSweptSurface_IntersectWith(This,IntersectObject,option,intPoints)	\
    ( (This)->lpVtbl -> IntersectWith(This,IntersectObject,option,intPoints) ) 

#define IAcadSweptSurface_get_PlotStyleName(This,plotStyle)	\
    ( (This)->lpVtbl -> get_PlotStyleName(This,plotStyle) ) 

#define IAcadSweptSurface_put_PlotStyleName(This,plotStyle)	\
    ( (This)->lpVtbl -> put_PlotStyleName(This,plotStyle) ) 

#define IAcadSweptSurface_get_Lineweight(This,Lineweight)	\
    ( (This)->lpVtbl -> get_Lineweight(This,Lineweight) ) 

#define IAcadSweptSurface_put_Lineweight(This,Lineweight)	\
    ( (This)->lpVtbl -> put_Lineweight(This,Lineweight) ) 

#define IAcadSweptSurface_get_Hyperlinks(This,Hyperlinks)	\
    ( (This)->lpVtbl -> get_Hyperlinks(This,Hyperlinks) ) 

#define IAcadSweptSurface_get_Material(This,Material)	\
    ( (This)->lpVtbl -> get_Material(This,Material) ) 

#define IAcadSweptSurface_put_Material(This,Material)	\
    ( (This)->lpVtbl -> put_Material(This,Material) ) 

#define IAcadSweptSurface_get_EntityName(This,EntityName)	\
    ( (This)->lpVtbl -> get_EntityName(This,EntityName) ) 

#define IAcadSweptSurface_get_EntityType(This,entType)	\
    ( (This)->lpVtbl -> get_EntityType(This,entType) ) 

#define IAcadSweptSurface_get_color(This,color)	\
    ( (This)->lpVtbl -> get_color(This,color) ) 

#define IAcadSweptSurface_put_color(This,color)	\
    ( (This)->lpVtbl -> put_color(This,color) ) 


#define IAcadSweptSurface_get_SurfaceType(This,SurfaceType)	\
    ( (This)->lpVtbl -> get_SurfaceType(This,SurfaceType) ) 

#define IAcadSweptSurface_get_UIsolineDensity(This,density)	\
    ( (This)->lpVtbl -> get_UIsolineDensity(This,density) ) 

#define IAcadSweptSurface_put_UIsolineDensity(This,density)	\
    ( (This)->lpVtbl -> put_UIsolineDensity(This,density) ) 

#define IAcadSweptSurface_get_VIsolineDensity(This,density)	\
    ( (This)->lpVtbl -> get_VIsolineDensity(This,density) ) 

#define IAcadSweptSurface_put_VIsolineDensity(This,density)	\
    ( (This)->lpVtbl -> put_VIsolineDensity(This,density) ) 


#define IAcadSweptSurface_get_ProfileRotation(This,profileRotationAngle)	\
    ( (This)->lpVtbl -> get_ProfileRotation(This,profileRotationAngle) ) 

#define IAcadSweptSurface_put_ProfileRotation(This,profileRotationAngle)	\
    ( (This)->lpVtbl -> put_ProfileRotation(This,profileRotationAngle) ) 

#define IAcadSweptSurface_get_Bank(This,bBank)	\
    ( (This)->lpVtbl -> get_Bank(This,bBank) ) 

#define IAcadSweptSurface_put_Bank(This,bBank)	\
    ( (This)->lpVtbl -> put_Bank(This,bBank) ) 

#define IAcadSweptSurface_get_Twist(This,TwistAngle)	\
    ( (This)->lpVtbl -> get_Twist(This,TwistAngle) ) 

#define IAcadSweptSurface_put_Twist(This,TwistAngle)	\
    ( (This)->lpVtbl -> put_Twist(This,TwistAngle) ) 

#define IAcadSweptSurface_get_scale(This,scale)	\
    ( (This)->lpVtbl -> get_scale(This,scale) ) 

#define IAcadSweptSurface_put_scale(This,scale)	\
    ( (This)->lpVtbl -> put_scale(This,scale) ) 

#define IAcadSweptSurface_get_Length(This,Length)	\
    ( (This)->lpVtbl -> get_Length(This,Length) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAcadSweptSurface_INTERFACE_DEFINED__ */


#ifndef __IAcadLoftedSurface_INTERFACE_DEFINED__
#define __IAcadLoftedSurface_INTERFACE_DEFINED__

/* interface IAcadLoftedSurface */
/* [object][oleautomation][dual][helpstring][uuid] */ 


EXTERN_C const IID IID_IAcadLoftedSurface;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("9A580AC7-FD1D-4D94-A668-B00745FDB153")
    IAcadLoftedSurface : public IAcadSurface
    {
    public:
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_NumCrossSections( 
            /* [retval][out] */ long *NumCrossSections) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_NumGuidePaths( 
            /* [retval][out] */ long *NumGuidePaths) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_SurfaceNormals( 
            /* [retval][out] */ AcLoftedSurfaceNormalType *surfaceNormal) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_SurfaceNormals( 
            /* [in] */ AcLoftedSurfaceNormalType surfaceNormal) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_StartDraftAngle( 
            /* [retval][out] */ ACAD_ANGLE *StartDraftAngle) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_StartDraftAngle( 
            /* [in] */ ACAD_ANGLE StartDraftAngle) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_StartDraftMagnitude( 
            /* [retval][out] */ double *startDraftMag) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_StartDraftMagnitude( 
            /* [in] */ double startDraftMag) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_EndDraftAngle( 
            /* [retval][out] */ ACAD_ANGLE *EndDraftAngle) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_EndDraftAngle( 
            /* [in] */ ACAD_ANGLE EndDraftAngle) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_EndDraftMagnitude( 
            /* [retval][out] */ double *endDraftMag) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_EndDraftMagnitude( 
            /* [in] */ double endDraftMag) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_Closed( 
            /* [retval][out] */ VARIANT_BOOL *bClosed) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_Closed( 
            /* [in] */ VARIANT_BOOL bClosed) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct IAcadLoftedSurfaceVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAcadLoftedSurface * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAcadLoftedSurface * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAcadLoftedSurface * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IAcadLoftedSurface * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IAcadLoftedSurface * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IAcadLoftedSurface * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IAcadLoftedSurface * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Handle )( 
            IAcadLoftedSurface * This,
            /* [retval][out] */ BSTR *Handle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ObjectName )( 
            IAcadLoftedSurface * This,
            /* [retval][out] */ BSTR *ObjectName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetXData )( 
            IAcadLoftedSurface * This,
            /* [in] */ BSTR AppName,
            /* [out] */ VARIANT *XDataType,
            /* [out] */ VARIANT *XDataValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetXData )( 
            IAcadLoftedSurface * This,
            /* [in] */ VARIANT XDataType,
            /* [in] */ VARIANT XDataValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Delete )( 
            IAcadLoftedSurface * This);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ObjectID )( 
            IAcadLoftedSurface * This,
            /* [retval][out] */ LONG_PTR *ObjectID);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Application )( 
            IAcadLoftedSurface * This,
            /* [retval][out] */ IDispatch **ApplicationObject);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Database )( 
            IAcadLoftedSurface * This,
            /* [retval][out] */ IAcadDatabase **pDatabase);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_HasExtensionDictionary )( 
            IAcadLoftedSurface * This,
            /* [retval][out] */ VARIANT_BOOL *bHasDictionary);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetExtensionDictionary )( 
            IAcadLoftedSurface * This,
            /* [retval][out] */ IAcadDictionary **pExtDictionary);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_OwnerID )( 
            IAcadLoftedSurface * This,
            /* [retval][out] */ LONG_PTR *OwnerID);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Document )( 
            IAcadLoftedSurface * This,
            /* [retval][out] */ IDispatch **pDocument);
        
        /* [helpstring][hidden][id] */ HRESULT ( STDMETHODCALLTYPE *Erase )( 
            IAcadLoftedSurface * This);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_TrueColor )( 
            IAcadLoftedSurface * This,
            /* [retval][out] */ IAcadAcCmColor **pColor);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_TrueColor )( 
            IAcadLoftedSurface * This,
            /* [in] */ IAcadAcCmColor *pColor);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Layer )( 
            IAcadLoftedSurface * This,
            /* [retval][out] */ BSTR *Layer);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Layer )( 
            IAcadLoftedSurface * This,
            /* [in] */ BSTR Layer);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Linetype )( 
            IAcadLoftedSurface * This,
            /* [retval][out] */ BSTR *Linetype);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Linetype )( 
            IAcadLoftedSurface * This,
            /* [in] */ BSTR Linetype);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_LinetypeScale )( 
            IAcadLoftedSurface * This,
            /* [retval][out] */ ACAD_NOUNITS *ltScale);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_LinetypeScale )( 
            IAcadLoftedSurface * This,
            /* [in] */ ACAD_NOUNITS ltScale);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Visible )( 
            IAcadLoftedSurface * This,
            /* [retval][out] */ VARIANT_BOOL *bVisible);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Visible )( 
            IAcadLoftedSurface * This,
            /* [in] */ VARIANT_BOOL bVisible);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ArrayPolar )( 
            IAcadLoftedSurface * This,
            /* [in] */ int NumberOfObjects,
            /* [in] */ double AngleToFill,
            /* [in] */ VARIANT CenterPoint,
            /* [retval][out] */ VARIANT *pArrayObjs);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ArrayRectangular )( 
            IAcadLoftedSurface * This,
            /* [in] */ int NumberOfRows,
            /* [in] */ int NumberOfColumns,
            /* [in] */ int NumberOfLevels,
            /* [in] */ double DistBetweenRows,
            /* [in] */ double DistBetweenCols,
            /* [in] */ double DistBetweenLevels,
            /* [retval][out] */ VARIANT *pArrayObjs);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Highlight )( 
            IAcadLoftedSurface * This,
            /* [in] */ VARIANT_BOOL HighlightFlag);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Copy )( 
            IAcadLoftedSurface * This,
            /* [retval][out] */ IDispatch **pCopyObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Move )( 
            IAcadLoftedSurface * This,
            /* [in] */ VARIANT FromPoint,
            /* [in] */ VARIANT ToPoint);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Rotate )( 
            IAcadLoftedSurface * This,
            /* [in] */ VARIANT BasePoint,
            /* [in] */ double RotationAngle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Rotate3D )( 
            IAcadLoftedSurface * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [in] */ double RotationAngle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Mirror )( 
            IAcadLoftedSurface * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [retval][out] */ IDispatch **pMirrorObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Mirror3D )( 
            IAcadLoftedSurface * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [in] */ VARIANT point3,
            /* [retval][out] */ IDispatch **pMirrorObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ScaleEntity )( 
            IAcadLoftedSurface * This,
            /* [in] */ VARIANT BasePoint,
            /* [in] */ double ScaleFactor);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *TransformBy )( 
            IAcadLoftedSurface * This,
            /* [in] */ VARIANT TransformationMatrix);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Update )( 
            IAcadLoftedSurface * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetBoundingBox )( 
            IAcadLoftedSurface * This,
            /* [out] */ VARIANT *MinPoint,
            /* [out] */ VARIANT *MaxPoint);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *IntersectWith )( 
            IAcadLoftedSurface * This,
            /* [in] */ IDispatch *IntersectObject,
            /* [in] */ AcExtendOption option,
            /* [retval][out] */ VARIANT *intPoints);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_PlotStyleName )( 
            IAcadLoftedSurface * This,
            /* [retval][out] */ BSTR *plotStyle);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_PlotStyleName )( 
            IAcadLoftedSurface * This,
            /* [in] */ BSTR plotStyle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Lineweight )( 
            IAcadLoftedSurface * This,
            /* [retval][out] */ ACAD_LWEIGHT *Lineweight);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Lineweight )( 
            IAcadLoftedSurface * This,
            /* [in] */ ACAD_LWEIGHT Lineweight);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Hyperlinks )( 
            IAcadLoftedSurface * This,
            /* [retval][out] */ IAcadHyperlinks **Hyperlinks);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Material )( 
            IAcadLoftedSurface * This,
            /* [retval][out] */ BSTR *Material);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Material )( 
            IAcadLoftedSurface * This,
            /* [in] */ BSTR Material);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EntityName )( 
            IAcadLoftedSurface * This,
            /* [retval][out] */ BSTR *EntityName);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EntityType )( 
            IAcadLoftedSurface * This,
            /* [retval][out] */ long *entType);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_color )( 
            IAcadLoftedSurface * This,
            /* [retval][out] */ ACAD_COLOR *color);
        
        /* [helpstring][hidden][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_color )( 
            IAcadLoftedSurface * This,
            /* [in] */ ACAD_COLOR color);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SurfaceType )( 
            IAcadLoftedSurface * This,
            /* [retval][out] */ BSTR *SurfaceType);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_UIsolineDensity )( 
            IAcadLoftedSurface * This,
            /* [retval][out] */ long *density);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_UIsolineDensity )( 
            IAcadLoftedSurface * This,
            /* [in] */ long density);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_VIsolineDensity )( 
            IAcadLoftedSurface * This,
            /* [retval][out] */ long *density);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_VIsolineDensity )( 
            IAcadLoftedSurface * This,
            /* [in] */ long density);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_NumCrossSections )( 
            IAcadLoftedSurface * This,
            /* [retval][out] */ long *NumCrossSections);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_NumGuidePaths )( 
            IAcadLoftedSurface * This,
            /* [retval][out] */ long *NumGuidePaths);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SurfaceNormals )( 
            IAcadLoftedSurface * This,
            /* [retval][out] */ AcLoftedSurfaceNormalType *surfaceNormal);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_SurfaceNormals )( 
            IAcadLoftedSurface * This,
            /* [in] */ AcLoftedSurfaceNormalType surfaceNormal);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_StartDraftAngle )( 
            IAcadLoftedSurface * This,
            /* [retval][out] */ ACAD_ANGLE *StartDraftAngle);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_StartDraftAngle )( 
            IAcadLoftedSurface * This,
            /* [in] */ ACAD_ANGLE StartDraftAngle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_StartDraftMagnitude )( 
            IAcadLoftedSurface * This,
            /* [retval][out] */ double *startDraftMag);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_StartDraftMagnitude )( 
            IAcadLoftedSurface * This,
            /* [in] */ double startDraftMag);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EndDraftAngle )( 
            IAcadLoftedSurface * This,
            /* [retval][out] */ ACAD_ANGLE *EndDraftAngle);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_EndDraftAngle )( 
            IAcadLoftedSurface * This,
            /* [in] */ ACAD_ANGLE EndDraftAngle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EndDraftMagnitude )( 
            IAcadLoftedSurface * This,
            /* [retval][out] */ double *endDraftMag);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_EndDraftMagnitude )( 
            IAcadLoftedSurface * This,
            /* [in] */ double endDraftMag);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Closed )( 
            IAcadLoftedSurface * This,
            /* [retval][out] */ VARIANT_BOOL *bClosed);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Closed )( 
            IAcadLoftedSurface * This,
            /* [in] */ VARIANT_BOOL bClosed);
        
        END_INTERFACE
    } IAcadLoftedSurfaceVtbl;

    interface IAcadLoftedSurface
    {
        CONST_VTBL struct IAcadLoftedSurfaceVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAcadLoftedSurface_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAcadLoftedSurface_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAcadLoftedSurface_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAcadLoftedSurface_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IAcadLoftedSurface_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IAcadLoftedSurface_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IAcadLoftedSurface_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IAcadLoftedSurface_get_Handle(This,Handle)	\
    ( (This)->lpVtbl -> get_Handle(This,Handle) ) 

#define IAcadLoftedSurface_get_ObjectName(This,ObjectName)	\
    ( (This)->lpVtbl -> get_ObjectName(This,ObjectName) ) 

#define IAcadLoftedSurface_GetXData(This,AppName,XDataType,XDataValue)	\
    ( (This)->lpVtbl -> GetXData(This,AppName,XDataType,XDataValue) ) 

#define IAcadLoftedSurface_SetXData(This,XDataType,XDataValue)	\
    ( (This)->lpVtbl -> SetXData(This,XDataType,XDataValue) ) 

#define IAcadLoftedSurface_Delete(This)	\
    ( (This)->lpVtbl -> Delete(This) ) 

#define IAcadLoftedSurface_get_ObjectID(This,ObjectID)	\
    ( (This)->lpVtbl -> get_ObjectID(This,ObjectID) ) 

#define IAcadLoftedSurface_get_Application(This,ApplicationObject)	\
    ( (This)->lpVtbl -> get_Application(This,ApplicationObject) ) 

#define IAcadLoftedSurface_get_Database(This,pDatabase)	\
    ( (This)->lpVtbl -> get_Database(This,pDatabase) ) 

#define IAcadLoftedSurface_get_HasExtensionDictionary(This,bHasDictionary)	\
    ( (This)->lpVtbl -> get_HasExtensionDictionary(This,bHasDictionary) ) 

#define IAcadLoftedSurface_GetExtensionDictionary(This,pExtDictionary)	\
    ( (This)->lpVtbl -> GetExtensionDictionary(This,pExtDictionary) ) 

#define IAcadLoftedSurface_get_OwnerID(This,OwnerID)	\
    ( (This)->lpVtbl -> get_OwnerID(This,OwnerID) ) 

#define IAcadLoftedSurface_get_Document(This,pDocument)	\
    ( (This)->lpVtbl -> get_Document(This,pDocument) ) 

#define IAcadLoftedSurface_Erase(This)	\
    ( (This)->lpVtbl -> Erase(This) ) 


#define IAcadLoftedSurface_get_TrueColor(This,pColor)	\
    ( (This)->lpVtbl -> get_TrueColor(This,pColor) ) 

#define IAcadLoftedSurface_put_TrueColor(This,pColor)	\
    ( (This)->lpVtbl -> put_TrueColor(This,pColor) ) 

#define IAcadLoftedSurface_get_Layer(This,Layer)	\
    ( (This)->lpVtbl -> get_Layer(This,Layer) ) 

#define IAcadLoftedSurface_put_Layer(This,Layer)	\
    ( (This)->lpVtbl -> put_Layer(This,Layer) ) 

#define IAcadLoftedSurface_get_Linetype(This,Linetype)	\
    ( (This)->lpVtbl -> get_Linetype(This,Linetype) ) 

#define IAcadLoftedSurface_put_Linetype(This,Linetype)	\
    ( (This)->lpVtbl -> put_Linetype(This,Linetype) ) 

#define IAcadLoftedSurface_get_LinetypeScale(This,ltScale)	\
    ( (This)->lpVtbl -> get_LinetypeScale(This,ltScale) ) 

#define IAcadLoftedSurface_put_LinetypeScale(This,ltScale)	\
    ( (This)->lpVtbl -> put_LinetypeScale(This,ltScale) ) 

#define IAcadLoftedSurface_get_Visible(This,bVisible)	\
    ( (This)->lpVtbl -> get_Visible(This,bVisible) ) 

#define IAcadLoftedSurface_put_Visible(This,bVisible)	\
    ( (This)->lpVtbl -> put_Visible(This,bVisible) ) 

#define IAcadLoftedSurface_ArrayPolar(This,NumberOfObjects,AngleToFill,CenterPoint,pArrayObjs)	\
    ( (This)->lpVtbl -> ArrayPolar(This,NumberOfObjects,AngleToFill,CenterPoint,pArrayObjs) ) 

#define IAcadLoftedSurface_ArrayRectangular(This,NumberOfRows,NumberOfColumns,NumberOfLevels,DistBetweenRows,DistBetweenCols,DistBetweenLevels,pArrayObjs)	\
    ( (This)->lpVtbl -> ArrayRectangular(This,NumberOfRows,NumberOfColumns,NumberOfLevels,DistBetweenRows,DistBetweenCols,DistBetweenLevels,pArrayObjs) ) 

#define IAcadLoftedSurface_Highlight(This,HighlightFlag)	\
    ( (This)->lpVtbl -> Highlight(This,HighlightFlag) ) 

#define IAcadLoftedSurface_Copy(This,pCopyObj)	\
    ( (This)->lpVtbl -> Copy(This,pCopyObj) ) 

#define IAcadLoftedSurface_Move(This,FromPoint,ToPoint)	\
    ( (This)->lpVtbl -> Move(This,FromPoint,ToPoint) ) 

#define IAcadLoftedSurface_Rotate(This,BasePoint,RotationAngle)	\
    ( (This)->lpVtbl -> Rotate(This,BasePoint,RotationAngle) ) 

#define IAcadLoftedSurface_Rotate3D(This,Point1,Point2,RotationAngle)	\
    ( (This)->lpVtbl -> Rotate3D(This,Point1,Point2,RotationAngle) ) 

#define IAcadLoftedSurface_Mirror(This,Point1,Point2,pMirrorObj)	\
    ( (This)->lpVtbl -> Mirror(This,Point1,Point2,pMirrorObj) ) 

#define IAcadLoftedSurface_Mirror3D(This,Point1,Point2,point3,pMirrorObj)	\
    ( (This)->lpVtbl -> Mirror3D(This,Point1,Point2,point3,pMirrorObj) ) 

#define IAcadLoftedSurface_ScaleEntity(This,BasePoint,ScaleFactor)	\
    ( (This)->lpVtbl -> ScaleEntity(This,BasePoint,ScaleFactor) ) 

#define IAcadLoftedSurface_TransformBy(This,TransformationMatrix)	\
    ( (This)->lpVtbl -> TransformBy(This,TransformationMatrix) ) 

#define IAcadLoftedSurface_Update(This)	\
    ( (This)->lpVtbl -> Update(This) ) 

#define IAcadLoftedSurface_GetBoundingBox(This,MinPoint,MaxPoint)	\
    ( (This)->lpVtbl -> GetBoundingBox(This,MinPoint,MaxPoint) ) 

#define IAcadLoftedSurface_IntersectWith(This,IntersectObject,option,intPoints)	\
    ( (This)->lpVtbl -> IntersectWith(This,IntersectObject,option,intPoints) ) 

#define IAcadLoftedSurface_get_PlotStyleName(This,plotStyle)	\
    ( (This)->lpVtbl -> get_PlotStyleName(This,plotStyle) ) 

#define IAcadLoftedSurface_put_PlotStyleName(This,plotStyle)	\
    ( (This)->lpVtbl -> put_PlotStyleName(This,plotStyle) ) 

#define IAcadLoftedSurface_get_Lineweight(This,Lineweight)	\
    ( (This)->lpVtbl -> get_Lineweight(This,Lineweight) ) 

#define IAcadLoftedSurface_put_Lineweight(This,Lineweight)	\
    ( (This)->lpVtbl -> put_Lineweight(This,Lineweight) ) 

#define IAcadLoftedSurface_get_Hyperlinks(This,Hyperlinks)	\
    ( (This)->lpVtbl -> get_Hyperlinks(This,Hyperlinks) ) 

#define IAcadLoftedSurface_get_Material(This,Material)	\
    ( (This)->lpVtbl -> get_Material(This,Material) ) 

#define IAcadLoftedSurface_put_Material(This,Material)	\
    ( (This)->lpVtbl -> put_Material(This,Material) ) 

#define IAcadLoftedSurface_get_EntityName(This,EntityName)	\
    ( (This)->lpVtbl -> get_EntityName(This,EntityName) ) 

#define IAcadLoftedSurface_get_EntityType(This,entType)	\
    ( (This)->lpVtbl -> get_EntityType(This,entType) ) 

#define IAcadLoftedSurface_get_color(This,color)	\
    ( (This)->lpVtbl -> get_color(This,color) ) 

#define IAcadLoftedSurface_put_color(This,color)	\
    ( (This)->lpVtbl -> put_color(This,color) ) 


#define IAcadLoftedSurface_get_SurfaceType(This,SurfaceType)	\
    ( (This)->lpVtbl -> get_SurfaceType(This,SurfaceType) ) 

#define IAcadLoftedSurface_get_UIsolineDensity(This,density)	\
    ( (This)->lpVtbl -> get_UIsolineDensity(This,density) ) 

#define IAcadLoftedSurface_put_UIsolineDensity(This,density)	\
    ( (This)->lpVtbl -> put_UIsolineDensity(This,density) ) 

#define IAcadLoftedSurface_get_VIsolineDensity(This,density)	\
    ( (This)->lpVtbl -> get_VIsolineDensity(This,density) ) 

#define IAcadLoftedSurface_put_VIsolineDensity(This,density)	\
    ( (This)->lpVtbl -> put_VIsolineDensity(This,density) ) 


#define IAcadLoftedSurface_get_NumCrossSections(This,NumCrossSections)	\
    ( (This)->lpVtbl -> get_NumCrossSections(This,NumCrossSections) ) 

#define IAcadLoftedSurface_get_NumGuidePaths(This,NumGuidePaths)	\
    ( (This)->lpVtbl -> get_NumGuidePaths(This,NumGuidePaths) ) 

#define IAcadLoftedSurface_get_SurfaceNormals(This,surfaceNormal)	\
    ( (This)->lpVtbl -> get_SurfaceNormals(This,surfaceNormal) ) 

#define IAcadLoftedSurface_put_SurfaceNormals(This,surfaceNormal)	\
    ( (This)->lpVtbl -> put_SurfaceNormals(This,surfaceNormal) ) 

#define IAcadLoftedSurface_get_StartDraftAngle(This,StartDraftAngle)	\
    ( (This)->lpVtbl -> get_StartDraftAngle(This,StartDraftAngle) ) 

#define IAcadLoftedSurface_put_StartDraftAngle(This,StartDraftAngle)	\
    ( (This)->lpVtbl -> put_StartDraftAngle(This,StartDraftAngle) ) 

#define IAcadLoftedSurface_get_StartDraftMagnitude(This,startDraftMag)	\
    ( (This)->lpVtbl -> get_StartDraftMagnitude(This,startDraftMag) ) 

#define IAcadLoftedSurface_put_StartDraftMagnitude(This,startDraftMag)	\
    ( (This)->lpVtbl -> put_StartDraftMagnitude(This,startDraftMag) ) 

#define IAcadLoftedSurface_get_EndDraftAngle(This,EndDraftAngle)	\
    ( (This)->lpVtbl -> get_EndDraftAngle(This,EndDraftAngle) ) 

#define IAcadLoftedSurface_put_EndDraftAngle(This,EndDraftAngle)	\
    ( (This)->lpVtbl -> put_EndDraftAngle(This,EndDraftAngle) ) 

#define IAcadLoftedSurface_get_EndDraftMagnitude(This,endDraftMag)	\
    ( (This)->lpVtbl -> get_EndDraftMagnitude(This,endDraftMag) ) 

#define IAcadLoftedSurface_put_EndDraftMagnitude(This,endDraftMag)	\
    ( (This)->lpVtbl -> put_EndDraftMagnitude(This,endDraftMag) ) 

#define IAcadLoftedSurface_get_Closed(This,bClosed)	\
    ( (This)->lpVtbl -> get_Closed(This,bClosed) ) 

#define IAcadLoftedSurface_put_Closed(This,bClosed)	\
    ( (This)->lpVtbl -> put_Closed(This,bClosed) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAcadLoftedSurface_INTERFACE_DEFINED__ */


#ifndef __IAcadHelix_INTERFACE_DEFINED__
#define __IAcadHelix_INTERFACE_DEFINED__

/* interface IAcadHelix */
/* [object][oleautomation][dual][helpstring][uuid] */ 


EXTERN_C const IID IID_IAcadHelix;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("7480DD11-B890-41B3-BD1A-562D5190DDC2")
    IAcadHelix : public IAcadEntity
    {
    public:
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_Position( 
            /* [retval][out] */ VARIANT *StartPoint) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_Position( 
            /* [in] */ VARIANT StartPoint) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_Constrain( 
            /* [retval][out] */ AcHelixConstrainType *constrainType) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_Constrain( 
            /* [in] */ AcHelixConstrainType constrainType) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_Height( 
            /* [retval][out] */ double *Length) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_Height( 
            /* [in] */ double Length) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_Turns( 
            /* [retval][out] */ ACAD_NOUNITS *Turns) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_Turns( 
            /* [in] */ ACAD_NOUNITS Turns) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_TurnHeight( 
            /* [retval][out] */ double *Distance) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_TurnHeight( 
            /* [in] */ double Distance) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_BaseRadius( 
            /* [retval][out] */ double *Radius) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_BaseRadius( 
            /* [in] */ double Radius) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_TopRadius( 
            /* [retval][out] */ double *Radius) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_TopRadius( 
            /* [in] */ double Radius) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_Twist( 
            /* [retval][out] */ AcHelixTwistType *twistType) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_Twist( 
            /* [in] */ AcHelixTwistType twistType) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_TurnSlope( 
            /* [retval][out] */ ACAD_ANGLE *slopeAngle) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_TotalLength( 
            /* [retval][out] */ double *TotalLength) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct IAcadHelixVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAcadHelix * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAcadHelix * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAcadHelix * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IAcadHelix * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IAcadHelix * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IAcadHelix * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IAcadHelix * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Handle )( 
            IAcadHelix * This,
            /* [retval][out] */ BSTR *Handle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ObjectName )( 
            IAcadHelix * This,
            /* [retval][out] */ BSTR *ObjectName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetXData )( 
            IAcadHelix * This,
            /* [in] */ BSTR AppName,
            /* [out] */ VARIANT *XDataType,
            /* [out] */ VARIANT *XDataValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetXData )( 
            IAcadHelix * This,
            /* [in] */ VARIANT XDataType,
            /* [in] */ VARIANT XDataValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Delete )( 
            IAcadHelix * This);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ObjectID )( 
            IAcadHelix * This,
            /* [retval][out] */ LONG_PTR *ObjectID);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Application )( 
            IAcadHelix * This,
            /* [retval][out] */ IDispatch **ApplicationObject);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Database )( 
            IAcadHelix * This,
            /* [retval][out] */ IAcadDatabase **pDatabase);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_HasExtensionDictionary )( 
            IAcadHelix * This,
            /* [retval][out] */ VARIANT_BOOL *bHasDictionary);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetExtensionDictionary )( 
            IAcadHelix * This,
            /* [retval][out] */ IAcadDictionary **pExtDictionary);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_OwnerID )( 
            IAcadHelix * This,
            /* [retval][out] */ LONG_PTR *OwnerID);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Document )( 
            IAcadHelix * This,
            /* [retval][out] */ IDispatch **pDocument);
        
        /* [helpstring][hidden][id] */ HRESULT ( STDMETHODCALLTYPE *Erase )( 
            IAcadHelix * This);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_TrueColor )( 
            IAcadHelix * This,
            /* [retval][out] */ IAcadAcCmColor **pColor);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_TrueColor )( 
            IAcadHelix * This,
            /* [in] */ IAcadAcCmColor *pColor);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Layer )( 
            IAcadHelix * This,
            /* [retval][out] */ BSTR *Layer);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Layer )( 
            IAcadHelix * This,
            /* [in] */ BSTR Layer);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Linetype )( 
            IAcadHelix * This,
            /* [retval][out] */ BSTR *Linetype);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Linetype )( 
            IAcadHelix * This,
            /* [in] */ BSTR Linetype);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_LinetypeScale )( 
            IAcadHelix * This,
            /* [retval][out] */ ACAD_NOUNITS *ltScale);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_LinetypeScale )( 
            IAcadHelix * This,
            /* [in] */ ACAD_NOUNITS ltScale);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Visible )( 
            IAcadHelix * This,
            /* [retval][out] */ VARIANT_BOOL *bVisible);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Visible )( 
            IAcadHelix * This,
            /* [in] */ VARIANT_BOOL bVisible);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ArrayPolar )( 
            IAcadHelix * This,
            /* [in] */ int NumberOfObjects,
            /* [in] */ double AngleToFill,
            /* [in] */ VARIANT CenterPoint,
            /* [retval][out] */ VARIANT *pArrayObjs);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ArrayRectangular )( 
            IAcadHelix * This,
            /* [in] */ int NumberOfRows,
            /* [in] */ int NumberOfColumns,
            /* [in] */ int NumberOfLevels,
            /* [in] */ double DistBetweenRows,
            /* [in] */ double DistBetweenCols,
            /* [in] */ double DistBetweenLevels,
            /* [retval][out] */ VARIANT *pArrayObjs);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Highlight )( 
            IAcadHelix * This,
            /* [in] */ VARIANT_BOOL HighlightFlag);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Copy )( 
            IAcadHelix * This,
            /* [retval][out] */ IDispatch **pCopyObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Move )( 
            IAcadHelix * This,
            /* [in] */ VARIANT FromPoint,
            /* [in] */ VARIANT ToPoint);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Rotate )( 
            IAcadHelix * This,
            /* [in] */ VARIANT BasePoint,
            /* [in] */ double RotationAngle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Rotate3D )( 
            IAcadHelix * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [in] */ double RotationAngle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Mirror )( 
            IAcadHelix * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [retval][out] */ IDispatch **pMirrorObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Mirror3D )( 
            IAcadHelix * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [in] */ VARIANT point3,
            /* [retval][out] */ IDispatch **pMirrorObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ScaleEntity )( 
            IAcadHelix * This,
            /* [in] */ VARIANT BasePoint,
            /* [in] */ double ScaleFactor);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *TransformBy )( 
            IAcadHelix * This,
            /* [in] */ VARIANT TransformationMatrix);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Update )( 
            IAcadHelix * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetBoundingBox )( 
            IAcadHelix * This,
            /* [out] */ VARIANT *MinPoint,
            /* [out] */ VARIANT *MaxPoint);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *IntersectWith )( 
            IAcadHelix * This,
            /* [in] */ IDispatch *IntersectObject,
            /* [in] */ AcExtendOption option,
            /* [retval][out] */ VARIANT *intPoints);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_PlotStyleName )( 
            IAcadHelix * This,
            /* [retval][out] */ BSTR *plotStyle);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_PlotStyleName )( 
            IAcadHelix * This,
            /* [in] */ BSTR plotStyle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Lineweight )( 
            IAcadHelix * This,
            /* [retval][out] */ ACAD_LWEIGHT *Lineweight);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Lineweight )( 
            IAcadHelix * This,
            /* [in] */ ACAD_LWEIGHT Lineweight);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Hyperlinks )( 
            IAcadHelix * This,
            /* [retval][out] */ IAcadHyperlinks **Hyperlinks);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Material )( 
            IAcadHelix * This,
            /* [retval][out] */ BSTR *Material);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Material )( 
            IAcadHelix * This,
            /* [in] */ BSTR Material);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EntityName )( 
            IAcadHelix * This,
            /* [retval][out] */ BSTR *EntityName);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EntityType )( 
            IAcadHelix * This,
            /* [retval][out] */ long *entType);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_color )( 
            IAcadHelix * This,
            /* [retval][out] */ ACAD_COLOR *color);
        
        /* [helpstring][hidden][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_color )( 
            IAcadHelix * This,
            /* [in] */ ACAD_COLOR color);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Position )( 
            IAcadHelix * This,
            /* [retval][out] */ VARIANT *StartPoint);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Position )( 
            IAcadHelix * This,
            /* [in] */ VARIANT StartPoint);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Constrain )( 
            IAcadHelix * This,
            /* [retval][out] */ AcHelixConstrainType *constrainType);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Constrain )( 
            IAcadHelix * This,
            /* [in] */ AcHelixConstrainType constrainType);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Height )( 
            IAcadHelix * This,
            /* [retval][out] */ double *Length);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Height )( 
            IAcadHelix * This,
            /* [in] */ double Length);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Turns )( 
            IAcadHelix * This,
            /* [retval][out] */ ACAD_NOUNITS *Turns);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Turns )( 
            IAcadHelix * This,
            /* [in] */ ACAD_NOUNITS Turns);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_TurnHeight )( 
            IAcadHelix * This,
            /* [retval][out] */ double *Distance);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_TurnHeight )( 
            IAcadHelix * This,
            /* [in] */ double Distance);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_BaseRadius )( 
            IAcadHelix * This,
            /* [retval][out] */ double *Radius);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_BaseRadius )( 
            IAcadHelix * This,
            /* [in] */ double Radius);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_TopRadius )( 
            IAcadHelix * This,
            /* [retval][out] */ double *Radius);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_TopRadius )( 
            IAcadHelix * This,
            /* [in] */ double Radius);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Twist )( 
            IAcadHelix * This,
            /* [retval][out] */ AcHelixTwistType *twistType);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Twist )( 
            IAcadHelix * This,
            /* [in] */ AcHelixTwistType twistType);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_TurnSlope )( 
            IAcadHelix * This,
            /* [retval][out] */ ACAD_ANGLE *slopeAngle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_TotalLength )( 
            IAcadHelix * This,
            /* [retval][out] */ double *TotalLength);
        
        END_INTERFACE
    } IAcadHelixVtbl;

    interface IAcadHelix
    {
        CONST_VTBL struct IAcadHelixVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAcadHelix_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAcadHelix_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAcadHelix_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAcadHelix_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IAcadHelix_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IAcadHelix_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IAcadHelix_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IAcadHelix_get_Handle(This,Handle)	\
    ( (This)->lpVtbl -> get_Handle(This,Handle) ) 

#define IAcadHelix_get_ObjectName(This,ObjectName)	\
    ( (This)->lpVtbl -> get_ObjectName(This,ObjectName) ) 

#define IAcadHelix_GetXData(This,AppName,XDataType,XDataValue)	\
    ( (This)->lpVtbl -> GetXData(This,AppName,XDataType,XDataValue) ) 

#define IAcadHelix_SetXData(This,XDataType,XDataValue)	\
    ( (This)->lpVtbl -> SetXData(This,XDataType,XDataValue) ) 

#define IAcadHelix_Delete(This)	\
    ( (This)->lpVtbl -> Delete(This) ) 

#define IAcadHelix_get_ObjectID(This,ObjectID)	\
    ( (This)->lpVtbl -> get_ObjectID(This,ObjectID) ) 

#define IAcadHelix_get_Application(This,ApplicationObject)	\
    ( (This)->lpVtbl -> get_Application(This,ApplicationObject) ) 

#define IAcadHelix_get_Database(This,pDatabase)	\
    ( (This)->lpVtbl -> get_Database(This,pDatabase) ) 

#define IAcadHelix_get_HasExtensionDictionary(This,bHasDictionary)	\
    ( (This)->lpVtbl -> get_HasExtensionDictionary(This,bHasDictionary) ) 

#define IAcadHelix_GetExtensionDictionary(This,pExtDictionary)	\
    ( (This)->lpVtbl -> GetExtensionDictionary(This,pExtDictionary) ) 

#define IAcadHelix_get_OwnerID(This,OwnerID)	\
    ( (This)->lpVtbl -> get_OwnerID(This,OwnerID) ) 

#define IAcadHelix_get_Document(This,pDocument)	\
    ( (This)->lpVtbl -> get_Document(This,pDocument) ) 

#define IAcadHelix_Erase(This)	\
    ( (This)->lpVtbl -> Erase(This) ) 


#define IAcadHelix_get_TrueColor(This,pColor)	\
    ( (This)->lpVtbl -> get_TrueColor(This,pColor) ) 

#define IAcadHelix_put_TrueColor(This,pColor)	\
    ( (This)->lpVtbl -> put_TrueColor(This,pColor) ) 

#define IAcadHelix_get_Layer(This,Layer)	\
    ( (This)->lpVtbl -> get_Layer(This,Layer) ) 

#define IAcadHelix_put_Layer(This,Layer)	\
    ( (This)->lpVtbl -> put_Layer(This,Layer) ) 

#define IAcadHelix_get_Linetype(This,Linetype)	\
    ( (This)->lpVtbl -> get_Linetype(This,Linetype) ) 

#define IAcadHelix_put_Linetype(This,Linetype)	\
    ( (This)->lpVtbl -> put_Linetype(This,Linetype) ) 

#define IAcadHelix_get_LinetypeScale(This,ltScale)	\
    ( (This)->lpVtbl -> get_LinetypeScale(This,ltScale) ) 

#define IAcadHelix_put_LinetypeScale(This,ltScale)	\
    ( (This)->lpVtbl -> put_LinetypeScale(This,ltScale) ) 

#define IAcadHelix_get_Visible(This,bVisible)	\
    ( (This)->lpVtbl -> get_Visible(This,bVisible) ) 

#define IAcadHelix_put_Visible(This,bVisible)	\
    ( (This)->lpVtbl -> put_Visible(This,bVisible) ) 

#define IAcadHelix_ArrayPolar(This,NumberOfObjects,AngleToFill,CenterPoint,pArrayObjs)	\
    ( (This)->lpVtbl -> ArrayPolar(This,NumberOfObjects,AngleToFill,CenterPoint,pArrayObjs) ) 

#define IAcadHelix_ArrayRectangular(This,NumberOfRows,NumberOfColumns,NumberOfLevels,DistBetweenRows,DistBetweenCols,DistBetweenLevels,pArrayObjs)	\
    ( (This)->lpVtbl -> ArrayRectangular(This,NumberOfRows,NumberOfColumns,NumberOfLevels,DistBetweenRows,DistBetweenCols,DistBetweenLevels,pArrayObjs) ) 

#define IAcadHelix_Highlight(This,HighlightFlag)	\
    ( (This)->lpVtbl -> Highlight(This,HighlightFlag) ) 

#define IAcadHelix_Copy(This,pCopyObj)	\
    ( (This)->lpVtbl -> Copy(This,pCopyObj) ) 

#define IAcadHelix_Move(This,FromPoint,ToPoint)	\
    ( (This)->lpVtbl -> Move(This,FromPoint,ToPoint) ) 

#define IAcadHelix_Rotate(This,BasePoint,RotationAngle)	\
    ( (This)->lpVtbl -> Rotate(This,BasePoint,RotationAngle) ) 

#define IAcadHelix_Rotate3D(This,Point1,Point2,RotationAngle)	\
    ( (This)->lpVtbl -> Rotate3D(This,Point1,Point2,RotationAngle) ) 

#define IAcadHelix_Mirror(This,Point1,Point2,pMirrorObj)	\
    ( (This)->lpVtbl -> Mirror(This,Point1,Point2,pMirrorObj) ) 

#define IAcadHelix_Mirror3D(This,Point1,Point2,point3,pMirrorObj)	\
    ( (This)->lpVtbl -> Mirror3D(This,Point1,Point2,point3,pMirrorObj) ) 

#define IAcadHelix_ScaleEntity(This,BasePoint,ScaleFactor)	\
    ( (This)->lpVtbl -> ScaleEntity(This,BasePoint,ScaleFactor) ) 

#define IAcadHelix_TransformBy(This,TransformationMatrix)	\
    ( (This)->lpVtbl -> TransformBy(This,TransformationMatrix) ) 

#define IAcadHelix_Update(This)	\
    ( (This)->lpVtbl -> Update(This) ) 

#define IAcadHelix_GetBoundingBox(This,MinPoint,MaxPoint)	\
    ( (This)->lpVtbl -> GetBoundingBox(This,MinPoint,MaxPoint) ) 

#define IAcadHelix_IntersectWith(This,IntersectObject,option,intPoints)	\
    ( (This)->lpVtbl -> IntersectWith(This,IntersectObject,option,intPoints) ) 

#define IAcadHelix_get_PlotStyleName(This,plotStyle)	\
    ( (This)->lpVtbl -> get_PlotStyleName(This,plotStyle) ) 

#define IAcadHelix_put_PlotStyleName(This,plotStyle)	\
    ( (This)->lpVtbl -> put_PlotStyleName(This,plotStyle) ) 

#define IAcadHelix_get_Lineweight(This,Lineweight)	\
    ( (This)->lpVtbl -> get_Lineweight(This,Lineweight) ) 

#define IAcadHelix_put_Lineweight(This,Lineweight)	\
    ( (This)->lpVtbl -> put_Lineweight(This,Lineweight) ) 

#define IAcadHelix_get_Hyperlinks(This,Hyperlinks)	\
    ( (This)->lpVtbl -> get_Hyperlinks(This,Hyperlinks) ) 

#define IAcadHelix_get_Material(This,Material)	\
    ( (This)->lpVtbl -> get_Material(This,Material) ) 

#define IAcadHelix_put_Material(This,Material)	\
    ( (This)->lpVtbl -> put_Material(This,Material) ) 

#define IAcadHelix_get_EntityName(This,EntityName)	\
    ( (This)->lpVtbl -> get_EntityName(This,EntityName) ) 

#define IAcadHelix_get_EntityType(This,entType)	\
    ( (This)->lpVtbl -> get_EntityType(This,entType) ) 

#define IAcadHelix_get_color(This,color)	\
    ( (This)->lpVtbl -> get_color(This,color) ) 

#define IAcadHelix_put_color(This,color)	\
    ( (This)->lpVtbl -> put_color(This,color) ) 


#define IAcadHelix_get_Position(This,StartPoint)	\
    ( (This)->lpVtbl -> get_Position(This,StartPoint) ) 

#define IAcadHelix_put_Position(This,StartPoint)	\
    ( (This)->lpVtbl -> put_Position(This,StartPoint) ) 

#define IAcadHelix_get_Constrain(This,constrainType)	\
    ( (This)->lpVtbl -> get_Constrain(This,constrainType) ) 

#define IAcadHelix_put_Constrain(This,constrainType)	\
    ( (This)->lpVtbl -> put_Constrain(This,constrainType) ) 

#define IAcadHelix_get_Height(This,Length)	\
    ( (This)->lpVtbl -> get_Height(This,Length) ) 

#define IAcadHelix_put_Height(This,Length)	\
    ( (This)->lpVtbl -> put_Height(This,Length) ) 

#define IAcadHelix_get_Turns(This,Turns)	\
    ( (This)->lpVtbl -> get_Turns(This,Turns) ) 

#define IAcadHelix_put_Turns(This,Turns)	\
    ( (This)->lpVtbl -> put_Turns(This,Turns) ) 

#define IAcadHelix_get_TurnHeight(This,Distance)	\
    ( (This)->lpVtbl -> get_TurnHeight(This,Distance) ) 

#define IAcadHelix_put_TurnHeight(This,Distance)	\
    ( (This)->lpVtbl -> put_TurnHeight(This,Distance) ) 

#define IAcadHelix_get_BaseRadius(This,Radius)	\
    ( (This)->lpVtbl -> get_BaseRadius(This,Radius) ) 

#define IAcadHelix_put_BaseRadius(This,Radius)	\
    ( (This)->lpVtbl -> put_BaseRadius(This,Radius) ) 

#define IAcadHelix_get_TopRadius(This,Radius)	\
    ( (This)->lpVtbl -> get_TopRadius(This,Radius) ) 

#define IAcadHelix_put_TopRadius(This,Radius)	\
    ( (This)->lpVtbl -> put_TopRadius(This,Radius) ) 

#define IAcadHelix_get_Twist(This,twistType)	\
    ( (This)->lpVtbl -> get_Twist(This,twistType) ) 

#define IAcadHelix_put_Twist(This,twistType)	\
    ( (This)->lpVtbl -> put_Twist(This,twistType) ) 

#define IAcadHelix_get_TurnSlope(This,slopeAngle)	\
    ( (This)->lpVtbl -> get_TurnSlope(This,slopeAngle) ) 

#define IAcadHelix_get_TotalLength(This,TotalLength)	\
    ( (This)->lpVtbl -> get_TotalLength(This,TotalLength) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAcadHelix_INTERFACE_DEFINED__ */


#ifndef __IAcadUnderlay_INTERFACE_DEFINED__
#define __IAcadUnderlay_INTERFACE_DEFINED__

/* interface IAcadUnderlay */
/* [object][oleautomation][dual][uuid] */ 


EXTERN_C const IID IID_IAcadUnderlay;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("D076BB70-1090-4438-9922-3FD91BA4BB4A")
    IAcadUnderlay : public IAcadEntity
    {
    public:
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_Contrast( 
            /* [retval][out] */ long *Contrast) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_Contrast( 
            /* [in] */ long Contrast) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_Fade( 
            /* [retval][out] */ long *Fade) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_Fade( 
            /* [in] */ long Fade) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_Position( 
            /* [retval][out] */ VARIANT *pos) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_Position( 
            /* [in] */ VARIANT pos) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_Rotation( 
            /* [retval][out] */ ACAD_ANGLE *rotAngle) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_Rotation( 
            /* [in] */ ACAD_ANGLE rotAngle) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_Width( 
            /* [retval][out] */ double *Width) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_Width( 
            /* [in] */ double Width) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_Height( 
            /* [retval][out] */ double *Height) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_Height( 
            /* [in] */ double Height) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_UnderlayName( 
            /* [retval][out] */ BSTR *Name) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_UnderlayName( 
            /* [in] */ BSTR Name) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_ItemName( 
            /* [in] */ BSTR sheetName) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_ItemName( 
            /* [retval][out] */ BSTR *sheetName) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_Monochrome( 
            /* [retval][out] */ VARIANT_BOOL *bMono) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_Monochrome( 
            /* [in] */ VARIANT_BOOL bMono) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_AdjustForBackground( 
            /* [retval][out] */ VARIANT_BOOL *Value) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_AdjustForBackground( 
            /* [in] */ VARIANT_BOOL Value) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ClipBoundary( 
            /* [in] */ VARIANT boundry) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_ScaleFactor( 
            /* [retval][out] */ ACAD_NOUNITS *ScaleFactor) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_ScaleFactor( 
            /* [in] */ ACAD_NOUNITS ScaleFactor) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_File( 
            /* [retval][out] */ BSTR *Name) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_File( 
            /* [in] */ BSTR Name) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_UnderlayVisibility( 
            /* [retval][out] */ VARIANT_BOOL *fVisible) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_UnderlayVisibility( 
            /* [in] */ VARIANT_BOOL fVisible) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_ClippingEnabled( 
            /* [retval][out] */ VARIANT_BOOL *kClip) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_ClippingEnabled( 
            /* [in] */ VARIANT_BOOL kClip) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_UnderlayLayerOverrideApplied( 
            /* [retval][out] */ AcUnderlayLayerOverrideType *bOverride) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_UnderlayLayerOverrideApplied( 
            /* [in] */ AcUnderlayLayerOverrideType bOverride) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct IAcadUnderlayVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAcadUnderlay * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAcadUnderlay * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAcadUnderlay * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IAcadUnderlay * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IAcadUnderlay * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IAcadUnderlay * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IAcadUnderlay * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Handle )( 
            IAcadUnderlay * This,
            /* [retval][out] */ BSTR *Handle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ObjectName )( 
            IAcadUnderlay * This,
            /* [retval][out] */ BSTR *ObjectName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetXData )( 
            IAcadUnderlay * This,
            /* [in] */ BSTR AppName,
            /* [out] */ VARIANT *XDataType,
            /* [out] */ VARIANT *XDataValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetXData )( 
            IAcadUnderlay * This,
            /* [in] */ VARIANT XDataType,
            /* [in] */ VARIANT XDataValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Delete )( 
            IAcadUnderlay * This);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ObjectID )( 
            IAcadUnderlay * This,
            /* [retval][out] */ LONG_PTR *ObjectID);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Application )( 
            IAcadUnderlay * This,
            /* [retval][out] */ IDispatch **ApplicationObject);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Database )( 
            IAcadUnderlay * This,
            /* [retval][out] */ IAcadDatabase **pDatabase);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_HasExtensionDictionary )( 
            IAcadUnderlay * This,
            /* [retval][out] */ VARIANT_BOOL *bHasDictionary);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetExtensionDictionary )( 
            IAcadUnderlay * This,
            /* [retval][out] */ IAcadDictionary **pExtDictionary);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_OwnerID )( 
            IAcadUnderlay * This,
            /* [retval][out] */ LONG_PTR *OwnerID);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Document )( 
            IAcadUnderlay * This,
            /* [retval][out] */ IDispatch **pDocument);
        
        /* [helpstring][hidden][id] */ HRESULT ( STDMETHODCALLTYPE *Erase )( 
            IAcadUnderlay * This);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_TrueColor )( 
            IAcadUnderlay * This,
            /* [retval][out] */ IAcadAcCmColor **pColor);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_TrueColor )( 
            IAcadUnderlay * This,
            /* [in] */ IAcadAcCmColor *pColor);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Layer )( 
            IAcadUnderlay * This,
            /* [retval][out] */ BSTR *Layer);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Layer )( 
            IAcadUnderlay * This,
            /* [in] */ BSTR Layer);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Linetype )( 
            IAcadUnderlay * This,
            /* [retval][out] */ BSTR *Linetype);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Linetype )( 
            IAcadUnderlay * This,
            /* [in] */ BSTR Linetype);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_LinetypeScale )( 
            IAcadUnderlay * This,
            /* [retval][out] */ ACAD_NOUNITS *ltScale);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_LinetypeScale )( 
            IAcadUnderlay * This,
            /* [in] */ ACAD_NOUNITS ltScale);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Visible )( 
            IAcadUnderlay * This,
            /* [retval][out] */ VARIANT_BOOL *bVisible);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Visible )( 
            IAcadUnderlay * This,
            /* [in] */ VARIANT_BOOL bVisible);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ArrayPolar )( 
            IAcadUnderlay * This,
            /* [in] */ int NumberOfObjects,
            /* [in] */ double AngleToFill,
            /* [in] */ VARIANT CenterPoint,
            /* [retval][out] */ VARIANT *pArrayObjs);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ArrayRectangular )( 
            IAcadUnderlay * This,
            /* [in] */ int NumberOfRows,
            /* [in] */ int NumberOfColumns,
            /* [in] */ int NumberOfLevels,
            /* [in] */ double DistBetweenRows,
            /* [in] */ double DistBetweenCols,
            /* [in] */ double DistBetweenLevels,
            /* [retval][out] */ VARIANT *pArrayObjs);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Highlight )( 
            IAcadUnderlay * This,
            /* [in] */ VARIANT_BOOL HighlightFlag);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Copy )( 
            IAcadUnderlay * This,
            /* [retval][out] */ IDispatch **pCopyObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Move )( 
            IAcadUnderlay * This,
            /* [in] */ VARIANT FromPoint,
            /* [in] */ VARIANT ToPoint);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Rotate )( 
            IAcadUnderlay * This,
            /* [in] */ VARIANT BasePoint,
            /* [in] */ double RotationAngle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Rotate3D )( 
            IAcadUnderlay * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [in] */ double RotationAngle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Mirror )( 
            IAcadUnderlay * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [retval][out] */ IDispatch **pMirrorObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Mirror3D )( 
            IAcadUnderlay * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [in] */ VARIANT point3,
            /* [retval][out] */ IDispatch **pMirrorObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ScaleEntity )( 
            IAcadUnderlay * This,
            /* [in] */ VARIANT BasePoint,
            /* [in] */ double ScaleFactor);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *TransformBy )( 
            IAcadUnderlay * This,
            /* [in] */ VARIANT TransformationMatrix);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Update )( 
            IAcadUnderlay * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetBoundingBox )( 
            IAcadUnderlay * This,
            /* [out] */ VARIANT *MinPoint,
            /* [out] */ VARIANT *MaxPoint);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *IntersectWith )( 
            IAcadUnderlay * This,
            /* [in] */ IDispatch *IntersectObject,
            /* [in] */ AcExtendOption option,
            /* [retval][out] */ VARIANT *intPoints);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_PlotStyleName )( 
            IAcadUnderlay * This,
            /* [retval][out] */ BSTR *plotStyle);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_PlotStyleName )( 
            IAcadUnderlay * This,
            /* [in] */ BSTR plotStyle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Lineweight )( 
            IAcadUnderlay * This,
            /* [retval][out] */ ACAD_LWEIGHT *Lineweight);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Lineweight )( 
            IAcadUnderlay * This,
            /* [in] */ ACAD_LWEIGHT Lineweight);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Hyperlinks )( 
            IAcadUnderlay * This,
            /* [retval][out] */ IAcadHyperlinks **Hyperlinks);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Material )( 
            IAcadUnderlay * This,
            /* [retval][out] */ BSTR *Material);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Material )( 
            IAcadUnderlay * This,
            /* [in] */ BSTR Material);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EntityName )( 
            IAcadUnderlay * This,
            /* [retval][out] */ BSTR *EntityName);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EntityType )( 
            IAcadUnderlay * This,
            /* [retval][out] */ long *entType);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_color )( 
            IAcadUnderlay * This,
            /* [retval][out] */ ACAD_COLOR *color);
        
        /* [helpstring][hidden][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_color )( 
            IAcadUnderlay * This,
            /* [in] */ ACAD_COLOR color);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Contrast )( 
            IAcadUnderlay * This,
            /* [retval][out] */ long *Contrast);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Contrast )( 
            IAcadUnderlay * This,
            /* [in] */ long Contrast);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Fade )( 
            IAcadUnderlay * This,
            /* [retval][out] */ long *Fade);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Fade )( 
            IAcadUnderlay * This,
            /* [in] */ long Fade);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Position )( 
            IAcadUnderlay * This,
            /* [retval][out] */ VARIANT *pos);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Position )( 
            IAcadUnderlay * This,
            /* [in] */ VARIANT pos);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Rotation )( 
            IAcadUnderlay * This,
            /* [retval][out] */ ACAD_ANGLE *rotAngle);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Rotation )( 
            IAcadUnderlay * This,
            /* [in] */ ACAD_ANGLE rotAngle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Width )( 
            IAcadUnderlay * This,
            /* [retval][out] */ double *Width);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Width )( 
            IAcadUnderlay * This,
            /* [in] */ double Width);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Height )( 
            IAcadUnderlay * This,
            /* [retval][out] */ double *Height);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Height )( 
            IAcadUnderlay * This,
            /* [in] */ double Height);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_UnderlayName )( 
            IAcadUnderlay * This,
            /* [retval][out] */ BSTR *Name);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_UnderlayName )( 
            IAcadUnderlay * This,
            /* [in] */ BSTR Name);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ItemName )( 
            IAcadUnderlay * This,
            /* [in] */ BSTR sheetName);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ItemName )( 
            IAcadUnderlay * This,
            /* [retval][out] */ BSTR *sheetName);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Monochrome )( 
            IAcadUnderlay * This,
            /* [retval][out] */ VARIANT_BOOL *bMono);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Monochrome )( 
            IAcadUnderlay * This,
            /* [in] */ VARIANT_BOOL bMono);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AdjustForBackground )( 
            IAcadUnderlay * This,
            /* [retval][out] */ VARIANT_BOOL *Value);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AdjustForBackground )( 
            IAcadUnderlay * This,
            /* [in] */ VARIANT_BOOL Value);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ClipBoundary )( 
            IAcadUnderlay * This,
            /* [in] */ VARIANT boundry);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ScaleFactor )( 
            IAcadUnderlay * This,
            /* [retval][out] */ ACAD_NOUNITS *ScaleFactor);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ScaleFactor )( 
            IAcadUnderlay * This,
            /* [in] */ ACAD_NOUNITS ScaleFactor);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_File )( 
            IAcadUnderlay * This,
            /* [retval][out] */ BSTR *Name);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_File )( 
            IAcadUnderlay * This,
            /* [in] */ BSTR Name);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_UnderlayVisibility )( 
            IAcadUnderlay * This,
            /* [retval][out] */ VARIANT_BOOL *fVisible);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_UnderlayVisibility )( 
            IAcadUnderlay * This,
            /* [in] */ VARIANT_BOOL fVisible);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ClippingEnabled )( 
            IAcadUnderlay * This,
            /* [retval][out] */ VARIANT_BOOL *kClip);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ClippingEnabled )( 
            IAcadUnderlay * This,
            /* [in] */ VARIANT_BOOL kClip);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_UnderlayLayerOverrideApplied )( 
            IAcadUnderlay * This,
            /* [retval][out] */ AcUnderlayLayerOverrideType *bOverride);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_UnderlayLayerOverrideApplied )( 
            IAcadUnderlay * This,
            /* [in] */ AcUnderlayLayerOverrideType bOverride);
        
        END_INTERFACE
    } IAcadUnderlayVtbl;

    interface IAcadUnderlay
    {
        CONST_VTBL struct IAcadUnderlayVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAcadUnderlay_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAcadUnderlay_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAcadUnderlay_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAcadUnderlay_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IAcadUnderlay_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IAcadUnderlay_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IAcadUnderlay_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IAcadUnderlay_get_Handle(This,Handle)	\
    ( (This)->lpVtbl -> get_Handle(This,Handle) ) 

#define IAcadUnderlay_get_ObjectName(This,ObjectName)	\
    ( (This)->lpVtbl -> get_ObjectName(This,ObjectName) ) 

#define IAcadUnderlay_GetXData(This,AppName,XDataType,XDataValue)	\
    ( (This)->lpVtbl -> GetXData(This,AppName,XDataType,XDataValue) ) 

#define IAcadUnderlay_SetXData(This,XDataType,XDataValue)	\
    ( (This)->lpVtbl -> SetXData(This,XDataType,XDataValue) ) 

#define IAcadUnderlay_Delete(This)	\
    ( (This)->lpVtbl -> Delete(This) ) 

#define IAcadUnderlay_get_ObjectID(This,ObjectID)	\
    ( (This)->lpVtbl -> get_ObjectID(This,ObjectID) ) 

#define IAcadUnderlay_get_Application(This,ApplicationObject)	\
    ( (This)->lpVtbl -> get_Application(This,ApplicationObject) ) 

#define IAcadUnderlay_get_Database(This,pDatabase)	\
    ( (This)->lpVtbl -> get_Database(This,pDatabase) ) 

#define IAcadUnderlay_get_HasExtensionDictionary(This,bHasDictionary)	\
    ( (This)->lpVtbl -> get_HasExtensionDictionary(This,bHasDictionary) ) 

#define IAcadUnderlay_GetExtensionDictionary(This,pExtDictionary)	\
    ( (This)->lpVtbl -> GetExtensionDictionary(This,pExtDictionary) ) 

#define IAcadUnderlay_get_OwnerID(This,OwnerID)	\
    ( (This)->lpVtbl -> get_OwnerID(This,OwnerID) ) 

#define IAcadUnderlay_get_Document(This,pDocument)	\
    ( (This)->lpVtbl -> get_Document(This,pDocument) ) 

#define IAcadUnderlay_Erase(This)	\
    ( (This)->lpVtbl -> Erase(This) ) 


#define IAcadUnderlay_get_TrueColor(This,pColor)	\
    ( (This)->lpVtbl -> get_TrueColor(This,pColor) ) 

#define IAcadUnderlay_put_TrueColor(This,pColor)	\
    ( (This)->lpVtbl -> put_TrueColor(This,pColor) ) 

#define IAcadUnderlay_get_Layer(This,Layer)	\
    ( (This)->lpVtbl -> get_Layer(This,Layer) ) 

#define IAcadUnderlay_put_Layer(This,Layer)	\
    ( (This)->lpVtbl -> put_Layer(This,Layer) ) 

#define IAcadUnderlay_get_Linetype(This,Linetype)	\
    ( (This)->lpVtbl -> get_Linetype(This,Linetype) ) 

#define IAcadUnderlay_put_Linetype(This,Linetype)	\
    ( (This)->lpVtbl -> put_Linetype(This,Linetype) ) 

#define IAcadUnderlay_get_LinetypeScale(This,ltScale)	\
    ( (This)->lpVtbl -> get_LinetypeScale(This,ltScale) ) 

#define IAcadUnderlay_put_LinetypeScale(This,ltScale)	\
    ( (This)->lpVtbl -> put_LinetypeScale(This,ltScale) ) 

#define IAcadUnderlay_get_Visible(This,bVisible)	\
    ( (This)->lpVtbl -> get_Visible(This,bVisible) ) 

#define IAcadUnderlay_put_Visible(This,bVisible)	\
    ( (This)->lpVtbl -> put_Visible(This,bVisible) ) 

#define IAcadUnderlay_ArrayPolar(This,NumberOfObjects,AngleToFill,CenterPoint,pArrayObjs)	\
    ( (This)->lpVtbl -> ArrayPolar(This,NumberOfObjects,AngleToFill,CenterPoint,pArrayObjs) ) 

#define IAcadUnderlay_ArrayRectangular(This,NumberOfRows,NumberOfColumns,NumberOfLevels,DistBetweenRows,DistBetweenCols,DistBetweenLevels,pArrayObjs)	\
    ( (This)->lpVtbl -> ArrayRectangular(This,NumberOfRows,NumberOfColumns,NumberOfLevels,DistBetweenRows,DistBetweenCols,DistBetweenLevels,pArrayObjs) ) 

#define IAcadUnderlay_Highlight(This,HighlightFlag)	\
    ( (This)->lpVtbl -> Highlight(This,HighlightFlag) ) 

#define IAcadUnderlay_Copy(This,pCopyObj)	\
    ( (This)->lpVtbl -> Copy(This,pCopyObj) ) 

#define IAcadUnderlay_Move(This,FromPoint,ToPoint)	\
    ( (This)->lpVtbl -> Move(This,FromPoint,ToPoint) ) 

#define IAcadUnderlay_Rotate(This,BasePoint,RotationAngle)	\
    ( (This)->lpVtbl -> Rotate(This,BasePoint,RotationAngle) ) 

#define IAcadUnderlay_Rotate3D(This,Point1,Point2,RotationAngle)	\
    ( (This)->lpVtbl -> Rotate3D(This,Point1,Point2,RotationAngle) ) 

#define IAcadUnderlay_Mirror(This,Point1,Point2,pMirrorObj)	\
    ( (This)->lpVtbl -> Mirror(This,Point1,Point2,pMirrorObj) ) 

#define IAcadUnderlay_Mirror3D(This,Point1,Point2,point3,pMirrorObj)	\
    ( (This)->lpVtbl -> Mirror3D(This,Point1,Point2,point3,pMirrorObj) ) 

#define IAcadUnderlay_ScaleEntity(This,BasePoint,ScaleFactor)	\
    ( (This)->lpVtbl -> ScaleEntity(This,BasePoint,ScaleFactor) ) 

#define IAcadUnderlay_TransformBy(This,TransformationMatrix)	\
    ( (This)->lpVtbl -> TransformBy(This,TransformationMatrix) ) 

#define IAcadUnderlay_Update(This)	\
    ( (This)->lpVtbl -> Update(This) ) 

#define IAcadUnderlay_GetBoundingBox(This,MinPoint,MaxPoint)	\
    ( (This)->lpVtbl -> GetBoundingBox(This,MinPoint,MaxPoint) ) 

#define IAcadUnderlay_IntersectWith(This,IntersectObject,option,intPoints)	\
    ( (This)->lpVtbl -> IntersectWith(This,IntersectObject,option,intPoints) ) 

#define IAcadUnderlay_get_PlotStyleName(This,plotStyle)	\
    ( (This)->lpVtbl -> get_PlotStyleName(This,plotStyle) ) 

#define IAcadUnderlay_put_PlotStyleName(This,plotStyle)	\
    ( (This)->lpVtbl -> put_PlotStyleName(This,plotStyle) ) 

#define IAcadUnderlay_get_Lineweight(This,Lineweight)	\
    ( (This)->lpVtbl -> get_Lineweight(This,Lineweight) ) 

#define IAcadUnderlay_put_Lineweight(This,Lineweight)	\
    ( (This)->lpVtbl -> put_Lineweight(This,Lineweight) ) 

#define IAcadUnderlay_get_Hyperlinks(This,Hyperlinks)	\
    ( (This)->lpVtbl -> get_Hyperlinks(This,Hyperlinks) ) 

#define IAcadUnderlay_get_Material(This,Material)	\
    ( (This)->lpVtbl -> get_Material(This,Material) ) 

#define IAcadUnderlay_put_Material(This,Material)	\
    ( (This)->lpVtbl -> put_Material(This,Material) ) 

#define IAcadUnderlay_get_EntityName(This,EntityName)	\
    ( (This)->lpVtbl -> get_EntityName(This,EntityName) ) 

#define IAcadUnderlay_get_EntityType(This,entType)	\
    ( (This)->lpVtbl -> get_EntityType(This,entType) ) 

#define IAcadUnderlay_get_color(This,color)	\
    ( (This)->lpVtbl -> get_color(This,color) ) 

#define IAcadUnderlay_put_color(This,color)	\
    ( (This)->lpVtbl -> put_color(This,color) ) 


#define IAcadUnderlay_get_Contrast(This,Contrast)	\
    ( (This)->lpVtbl -> get_Contrast(This,Contrast) ) 

#define IAcadUnderlay_put_Contrast(This,Contrast)	\
    ( (This)->lpVtbl -> put_Contrast(This,Contrast) ) 

#define IAcadUnderlay_get_Fade(This,Fade)	\
    ( (This)->lpVtbl -> get_Fade(This,Fade) ) 

#define IAcadUnderlay_put_Fade(This,Fade)	\
    ( (This)->lpVtbl -> put_Fade(This,Fade) ) 

#define IAcadUnderlay_get_Position(This,pos)	\
    ( (This)->lpVtbl -> get_Position(This,pos) ) 

#define IAcadUnderlay_put_Position(This,pos)	\
    ( (This)->lpVtbl -> put_Position(This,pos) ) 

#define IAcadUnderlay_get_Rotation(This,rotAngle)	\
    ( (This)->lpVtbl -> get_Rotation(This,rotAngle) ) 

#define IAcadUnderlay_put_Rotation(This,rotAngle)	\
    ( (This)->lpVtbl -> put_Rotation(This,rotAngle) ) 

#define IAcadUnderlay_get_Width(This,Width)	\
    ( (This)->lpVtbl -> get_Width(This,Width) ) 

#define IAcadUnderlay_put_Width(This,Width)	\
    ( (This)->lpVtbl -> put_Width(This,Width) ) 

#define IAcadUnderlay_get_Height(This,Height)	\
    ( (This)->lpVtbl -> get_Height(This,Height) ) 

#define IAcadUnderlay_put_Height(This,Height)	\
    ( (This)->lpVtbl -> put_Height(This,Height) ) 

#define IAcadUnderlay_get_UnderlayName(This,Name)	\
    ( (This)->lpVtbl -> get_UnderlayName(This,Name) ) 

#define IAcadUnderlay_put_UnderlayName(This,Name)	\
    ( (This)->lpVtbl -> put_UnderlayName(This,Name) ) 

#define IAcadUnderlay_put_ItemName(This,sheetName)	\
    ( (This)->lpVtbl -> put_ItemName(This,sheetName) ) 

#define IAcadUnderlay_get_ItemName(This,sheetName)	\
    ( (This)->lpVtbl -> get_ItemName(This,sheetName) ) 

#define IAcadUnderlay_get_Monochrome(This,bMono)	\
    ( (This)->lpVtbl -> get_Monochrome(This,bMono) ) 

#define IAcadUnderlay_put_Monochrome(This,bMono)	\
    ( (This)->lpVtbl -> put_Monochrome(This,bMono) ) 

#define IAcadUnderlay_get_AdjustForBackground(This,Value)	\
    ( (This)->lpVtbl -> get_AdjustForBackground(This,Value) ) 

#define IAcadUnderlay_put_AdjustForBackground(This,Value)	\
    ( (This)->lpVtbl -> put_AdjustForBackground(This,Value) ) 

#define IAcadUnderlay_ClipBoundary(This,boundry)	\
    ( (This)->lpVtbl -> ClipBoundary(This,boundry) ) 

#define IAcadUnderlay_get_ScaleFactor(This,ScaleFactor)	\
    ( (This)->lpVtbl -> get_ScaleFactor(This,ScaleFactor) ) 

#define IAcadUnderlay_put_ScaleFactor(This,ScaleFactor)	\
    ( (This)->lpVtbl -> put_ScaleFactor(This,ScaleFactor) ) 

#define IAcadUnderlay_get_File(This,Name)	\
    ( (This)->lpVtbl -> get_File(This,Name) ) 

#define IAcadUnderlay_put_File(This,Name)	\
    ( (This)->lpVtbl -> put_File(This,Name) ) 

#define IAcadUnderlay_get_UnderlayVisibility(This,fVisible)	\
    ( (This)->lpVtbl -> get_UnderlayVisibility(This,fVisible) ) 

#define IAcadUnderlay_put_UnderlayVisibility(This,fVisible)	\
    ( (This)->lpVtbl -> put_UnderlayVisibility(This,fVisible) ) 

#define IAcadUnderlay_get_ClippingEnabled(This,kClip)	\
    ( (This)->lpVtbl -> get_ClippingEnabled(This,kClip) ) 

#define IAcadUnderlay_put_ClippingEnabled(This,kClip)	\
    ( (This)->lpVtbl -> put_ClippingEnabled(This,kClip) ) 

#define IAcadUnderlay_get_UnderlayLayerOverrideApplied(This,bOverride)	\
    ( (This)->lpVtbl -> get_UnderlayLayerOverrideApplied(This,bOverride) ) 

#define IAcadUnderlay_put_UnderlayLayerOverrideApplied(This,bOverride)	\
    ( (This)->lpVtbl -> put_UnderlayLayerOverrideApplied(This,bOverride) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAcadUnderlay_INTERFACE_DEFINED__ */


#ifndef __IAcadDwfUnderlay_INTERFACE_DEFINED__
#define __IAcadDwfUnderlay_INTERFACE_DEFINED__

/* interface IAcadDwfUnderlay */
/* [object][oleautomation][dual][uuid] */ 


EXTERN_C const IID IID_IAcadDwfUnderlay;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("A095BF5E-B1A5-413f-A6EA-6E057C63CA59")
    IAcadDwfUnderlay : public IAcadUnderlay
    {
    public:
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_DWFFormat( 
            /* [retval][out] */ BSTR *Name) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_DWFFormat( 
            /* [in] */ BSTR Name) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct IAcadDwfUnderlayVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAcadDwfUnderlay * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAcadDwfUnderlay * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAcadDwfUnderlay * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IAcadDwfUnderlay * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IAcadDwfUnderlay * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IAcadDwfUnderlay * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IAcadDwfUnderlay * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Handle )( 
            IAcadDwfUnderlay * This,
            /* [retval][out] */ BSTR *Handle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ObjectName )( 
            IAcadDwfUnderlay * This,
            /* [retval][out] */ BSTR *ObjectName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetXData )( 
            IAcadDwfUnderlay * This,
            /* [in] */ BSTR AppName,
            /* [out] */ VARIANT *XDataType,
            /* [out] */ VARIANT *XDataValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetXData )( 
            IAcadDwfUnderlay * This,
            /* [in] */ VARIANT XDataType,
            /* [in] */ VARIANT XDataValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Delete )( 
            IAcadDwfUnderlay * This);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ObjectID )( 
            IAcadDwfUnderlay * This,
            /* [retval][out] */ LONG_PTR *ObjectID);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Application )( 
            IAcadDwfUnderlay * This,
            /* [retval][out] */ IDispatch **ApplicationObject);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Database )( 
            IAcadDwfUnderlay * This,
            /* [retval][out] */ IAcadDatabase **pDatabase);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_HasExtensionDictionary )( 
            IAcadDwfUnderlay * This,
            /* [retval][out] */ VARIANT_BOOL *bHasDictionary);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetExtensionDictionary )( 
            IAcadDwfUnderlay * This,
            /* [retval][out] */ IAcadDictionary **pExtDictionary);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_OwnerID )( 
            IAcadDwfUnderlay * This,
            /* [retval][out] */ LONG_PTR *OwnerID);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Document )( 
            IAcadDwfUnderlay * This,
            /* [retval][out] */ IDispatch **pDocument);
        
        /* [helpstring][hidden][id] */ HRESULT ( STDMETHODCALLTYPE *Erase )( 
            IAcadDwfUnderlay * This);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_TrueColor )( 
            IAcadDwfUnderlay * This,
            /* [retval][out] */ IAcadAcCmColor **pColor);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_TrueColor )( 
            IAcadDwfUnderlay * This,
            /* [in] */ IAcadAcCmColor *pColor);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Layer )( 
            IAcadDwfUnderlay * This,
            /* [retval][out] */ BSTR *Layer);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Layer )( 
            IAcadDwfUnderlay * This,
            /* [in] */ BSTR Layer);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Linetype )( 
            IAcadDwfUnderlay * This,
            /* [retval][out] */ BSTR *Linetype);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Linetype )( 
            IAcadDwfUnderlay * This,
            /* [in] */ BSTR Linetype);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_LinetypeScale )( 
            IAcadDwfUnderlay * This,
            /* [retval][out] */ ACAD_NOUNITS *ltScale);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_LinetypeScale )( 
            IAcadDwfUnderlay * This,
            /* [in] */ ACAD_NOUNITS ltScale);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Visible )( 
            IAcadDwfUnderlay * This,
            /* [retval][out] */ VARIANT_BOOL *bVisible);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Visible )( 
            IAcadDwfUnderlay * This,
            /* [in] */ VARIANT_BOOL bVisible);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ArrayPolar )( 
            IAcadDwfUnderlay * This,
            /* [in] */ int NumberOfObjects,
            /* [in] */ double AngleToFill,
            /* [in] */ VARIANT CenterPoint,
            /* [retval][out] */ VARIANT *pArrayObjs);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ArrayRectangular )( 
            IAcadDwfUnderlay * This,
            /* [in] */ int NumberOfRows,
            /* [in] */ int NumberOfColumns,
            /* [in] */ int NumberOfLevels,
            /* [in] */ double DistBetweenRows,
            /* [in] */ double DistBetweenCols,
            /* [in] */ double DistBetweenLevels,
            /* [retval][out] */ VARIANT *pArrayObjs);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Highlight )( 
            IAcadDwfUnderlay * This,
            /* [in] */ VARIANT_BOOL HighlightFlag);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Copy )( 
            IAcadDwfUnderlay * This,
            /* [retval][out] */ IDispatch **pCopyObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Move )( 
            IAcadDwfUnderlay * This,
            /* [in] */ VARIANT FromPoint,
            /* [in] */ VARIANT ToPoint);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Rotate )( 
            IAcadDwfUnderlay * This,
            /* [in] */ VARIANT BasePoint,
            /* [in] */ double RotationAngle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Rotate3D )( 
            IAcadDwfUnderlay * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [in] */ double RotationAngle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Mirror )( 
            IAcadDwfUnderlay * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [retval][out] */ IDispatch **pMirrorObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Mirror3D )( 
            IAcadDwfUnderlay * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [in] */ VARIANT point3,
            /* [retval][out] */ IDispatch **pMirrorObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ScaleEntity )( 
            IAcadDwfUnderlay * This,
            /* [in] */ VARIANT BasePoint,
            /* [in] */ double ScaleFactor);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *TransformBy )( 
            IAcadDwfUnderlay * This,
            /* [in] */ VARIANT TransformationMatrix);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Update )( 
            IAcadDwfUnderlay * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetBoundingBox )( 
            IAcadDwfUnderlay * This,
            /* [out] */ VARIANT *MinPoint,
            /* [out] */ VARIANT *MaxPoint);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *IntersectWith )( 
            IAcadDwfUnderlay * This,
            /* [in] */ IDispatch *IntersectObject,
            /* [in] */ AcExtendOption option,
            /* [retval][out] */ VARIANT *intPoints);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_PlotStyleName )( 
            IAcadDwfUnderlay * This,
            /* [retval][out] */ BSTR *plotStyle);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_PlotStyleName )( 
            IAcadDwfUnderlay * This,
            /* [in] */ BSTR plotStyle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Lineweight )( 
            IAcadDwfUnderlay * This,
            /* [retval][out] */ ACAD_LWEIGHT *Lineweight);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Lineweight )( 
            IAcadDwfUnderlay * This,
            /* [in] */ ACAD_LWEIGHT Lineweight);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Hyperlinks )( 
            IAcadDwfUnderlay * This,
            /* [retval][out] */ IAcadHyperlinks **Hyperlinks);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Material )( 
            IAcadDwfUnderlay * This,
            /* [retval][out] */ BSTR *Material);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Material )( 
            IAcadDwfUnderlay * This,
            /* [in] */ BSTR Material);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EntityName )( 
            IAcadDwfUnderlay * This,
            /* [retval][out] */ BSTR *EntityName);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EntityType )( 
            IAcadDwfUnderlay * This,
            /* [retval][out] */ long *entType);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_color )( 
            IAcadDwfUnderlay * This,
            /* [retval][out] */ ACAD_COLOR *color);
        
        /* [helpstring][hidden][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_color )( 
            IAcadDwfUnderlay * This,
            /* [in] */ ACAD_COLOR color);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Contrast )( 
            IAcadDwfUnderlay * This,
            /* [retval][out] */ long *Contrast);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Contrast )( 
            IAcadDwfUnderlay * This,
            /* [in] */ long Contrast);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Fade )( 
            IAcadDwfUnderlay * This,
            /* [retval][out] */ long *Fade);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Fade )( 
            IAcadDwfUnderlay * This,
            /* [in] */ long Fade);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Position )( 
            IAcadDwfUnderlay * This,
            /* [retval][out] */ VARIANT *pos);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Position )( 
            IAcadDwfUnderlay * This,
            /* [in] */ VARIANT pos);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Rotation )( 
            IAcadDwfUnderlay * This,
            /* [retval][out] */ ACAD_ANGLE *rotAngle);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Rotation )( 
            IAcadDwfUnderlay * This,
            /* [in] */ ACAD_ANGLE rotAngle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Width )( 
            IAcadDwfUnderlay * This,
            /* [retval][out] */ double *Width);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Width )( 
            IAcadDwfUnderlay * This,
            /* [in] */ double Width);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Height )( 
            IAcadDwfUnderlay * This,
            /* [retval][out] */ double *Height);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Height )( 
            IAcadDwfUnderlay * This,
            /* [in] */ double Height);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_UnderlayName )( 
            IAcadDwfUnderlay * This,
            /* [retval][out] */ BSTR *Name);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_UnderlayName )( 
            IAcadDwfUnderlay * This,
            /* [in] */ BSTR Name);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ItemName )( 
            IAcadDwfUnderlay * This,
            /* [in] */ BSTR sheetName);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ItemName )( 
            IAcadDwfUnderlay * This,
            /* [retval][out] */ BSTR *sheetName);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Monochrome )( 
            IAcadDwfUnderlay * This,
            /* [retval][out] */ VARIANT_BOOL *bMono);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Monochrome )( 
            IAcadDwfUnderlay * This,
            /* [in] */ VARIANT_BOOL bMono);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_AdjustForBackground )( 
            IAcadDwfUnderlay * This,
            /* [retval][out] */ VARIANT_BOOL *Value);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_AdjustForBackground )( 
            IAcadDwfUnderlay * This,
            /* [in] */ VARIANT_BOOL Value);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ClipBoundary )( 
            IAcadDwfUnderlay * This,
            /* [in] */ VARIANT boundry);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ScaleFactor )( 
            IAcadDwfUnderlay * This,
            /* [retval][out] */ ACAD_NOUNITS *ScaleFactor);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ScaleFactor )( 
            IAcadDwfUnderlay * This,
            /* [in] */ ACAD_NOUNITS ScaleFactor);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_File )( 
            IAcadDwfUnderlay * This,
            /* [retval][out] */ BSTR *Name);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_File )( 
            IAcadDwfUnderlay * This,
            /* [in] */ BSTR Name);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_UnderlayVisibility )( 
            IAcadDwfUnderlay * This,
            /* [retval][out] */ VARIANT_BOOL *fVisible);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_UnderlayVisibility )( 
            IAcadDwfUnderlay * This,
            /* [in] */ VARIANT_BOOL fVisible);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ClippingEnabled )( 
            IAcadDwfUnderlay * This,
            /* [retval][out] */ VARIANT_BOOL *kClip);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ClippingEnabled )( 
            IAcadDwfUnderlay * This,
            /* [in] */ VARIANT_BOOL kClip);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_UnderlayLayerOverrideApplied )( 
            IAcadDwfUnderlay * This,
            /* [retval][out] */ AcUnderlayLayerOverrideType *bOverride);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_UnderlayLayerOverrideApplied )( 
            IAcadDwfUnderlay * This,
            /* [in] */ AcUnderlayLayerOverrideType bOverride);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_DWFFormat )( 
            IAcadDwfUnderlay * This,
            /* [retval][out] */ BSTR *Name);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_DWFFormat )( 
            IAcadDwfUnderlay * This,
            /* [in] */ BSTR Name);
        
        END_INTERFACE
    } IAcadDwfUnderlayVtbl;

    interface IAcadDwfUnderlay
    {
        CONST_VTBL struct IAcadDwfUnderlayVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAcadDwfUnderlay_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAcadDwfUnderlay_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAcadDwfUnderlay_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAcadDwfUnderlay_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IAcadDwfUnderlay_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IAcadDwfUnderlay_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IAcadDwfUnderlay_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IAcadDwfUnderlay_get_Handle(This,Handle)	\
    ( (This)->lpVtbl -> get_Handle(This,Handle) ) 

#define IAcadDwfUnderlay_get_ObjectName(This,ObjectName)	\
    ( (This)->lpVtbl -> get_ObjectName(This,ObjectName) ) 

#define IAcadDwfUnderlay_GetXData(This,AppName,XDataType,XDataValue)	\
    ( (This)->lpVtbl -> GetXData(This,AppName,XDataType,XDataValue) ) 

#define IAcadDwfUnderlay_SetXData(This,XDataType,XDataValue)	\
    ( (This)->lpVtbl -> SetXData(This,XDataType,XDataValue) ) 

#define IAcadDwfUnderlay_Delete(This)	\
    ( (This)->lpVtbl -> Delete(This) ) 

#define IAcadDwfUnderlay_get_ObjectID(This,ObjectID)	\
    ( (This)->lpVtbl -> get_ObjectID(This,ObjectID) ) 

#define IAcadDwfUnderlay_get_Application(This,ApplicationObject)	\
    ( (This)->lpVtbl -> get_Application(This,ApplicationObject) ) 

#define IAcadDwfUnderlay_get_Database(This,pDatabase)	\
    ( (This)->lpVtbl -> get_Database(This,pDatabase) ) 

#define IAcadDwfUnderlay_get_HasExtensionDictionary(This,bHasDictionary)	\
    ( (This)->lpVtbl -> get_HasExtensionDictionary(This,bHasDictionary) ) 

#define IAcadDwfUnderlay_GetExtensionDictionary(This,pExtDictionary)	\
    ( (This)->lpVtbl -> GetExtensionDictionary(This,pExtDictionary) ) 

#define IAcadDwfUnderlay_get_OwnerID(This,OwnerID)	\
    ( (This)->lpVtbl -> get_OwnerID(This,OwnerID) ) 

#define IAcadDwfUnderlay_get_Document(This,pDocument)	\
    ( (This)->lpVtbl -> get_Document(This,pDocument) ) 

#define IAcadDwfUnderlay_Erase(This)	\
    ( (This)->lpVtbl -> Erase(This) ) 


#define IAcadDwfUnderlay_get_TrueColor(This,pColor)	\
    ( (This)->lpVtbl -> get_TrueColor(This,pColor) ) 

#define IAcadDwfUnderlay_put_TrueColor(This,pColor)	\
    ( (This)->lpVtbl -> put_TrueColor(This,pColor) ) 

#define IAcadDwfUnderlay_get_Layer(This,Layer)	\
    ( (This)->lpVtbl -> get_Layer(This,Layer) ) 

#define IAcadDwfUnderlay_put_Layer(This,Layer)	\
    ( (This)->lpVtbl -> put_Layer(This,Layer) ) 

#define IAcadDwfUnderlay_get_Linetype(This,Linetype)	\
    ( (This)->lpVtbl -> get_Linetype(This,Linetype) ) 

#define IAcadDwfUnderlay_put_Linetype(This,Linetype)	\
    ( (This)->lpVtbl -> put_Linetype(This,Linetype) ) 

#define IAcadDwfUnderlay_get_LinetypeScale(This,ltScale)	\
    ( (This)->lpVtbl -> get_LinetypeScale(This,ltScale) ) 

#define IAcadDwfUnderlay_put_LinetypeScale(This,ltScale)	\
    ( (This)->lpVtbl -> put_LinetypeScale(This,ltScale) ) 

#define IAcadDwfUnderlay_get_Visible(This,bVisible)	\
    ( (This)->lpVtbl -> get_Visible(This,bVisible) ) 

#define IAcadDwfUnderlay_put_Visible(This,bVisible)	\
    ( (This)->lpVtbl -> put_Visible(This,bVisible) ) 

#define IAcadDwfUnderlay_ArrayPolar(This,NumberOfObjects,AngleToFill,CenterPoint,pArrayObjs)	\
    ( (This)->lpVtbl -> ArrayPolar(This,NumberOfObjects,AngleToFill,CenterPoint,pArrayObjs) ) 

#define IAcadDwfUnderlay_ArrayRectangular(This,NumberOfRows,NumberOfColumns,NumberOfLevels,DistBetweenRows,DistBetweenCols,DistBetweenLevels,pArrayObjs)	\
    ( (This)->lpVtbl -> ArrayRectangular(This,NumberOfRows,NumberOfColumns,NumberOfLevels,DistBetweenRows,DistBetweenCols,DistBetweenLevels,pArrayObjs) ) 

#define IAcadDwfUnderlay_Highlight(This,HighlightFlag)	\
    ( (This)->lpVtbl -> Highlight(This,HighlightFlag) ) 

#define IAcadDwfUnderlay_Copy(This,pCopyObj)	\
    ( (This)->lpVtbl -> Copy(This,pCopyObj) ) 

#define IAcadDwfUnderlay_Move(This,FromPoint,ToPoint)	\
    ( (This)->lpVtbl -> Move(This,FromPoint,ToPoint) ) 

#define IAcadDwfUnderlay_Rotate(This,BasePoint,RotationAngle)	\
    ( (This)->lpVtbl -> Rotate(This,BasePoint,RotationAngle) ) 

#define IAcadDwfUnderlay_Rotate3D(This,Point1,Point2,RotationAngle)	\
    ( (This)->lpVtbl -> Rotate3D(This,Point1,Point2,RotationAngle) ) 

#define IAcadDwfUnderlay_Mirror(This,Point1,Point2,pMirrorObj)	\
    ( (This)->lpVtbl -> Mirror(This,Point1,Point2,pMirrorObj) ) 

#define IAcadDwfUnderlay_Mirror3D(This,Point1,Point2,point3,pMirrorObj)	\
    ( (This)->lpVtbl -> Mirror3D(This,Point1,Point2,point3,pMirrorObj) ) 

#define IAcadDwfUnderlay_ScaleEntity(This,BasePoint,ScaleFactor)	\
    ( (This)->lpVtbl -> ScaleEntity(This,BasePoint,ScaleFactor) ) 

#define IAcadDwfUnderlay_TransformBy(This,TransformationMatrix)	\
    ( (This)->lpVtbl -> TransformBy(This,TransformationMatrix) ) 

#define IAcadDwfUnderlay_Update(This)	\
    ( (This)->lpVtbl -> Update(This) ) 

#define IAcadDwfUnderlay_GetBoundingBox(This,MinPoint,MaxPoint)	\
    ( (This)->lpVtbl -> GetBoundingBox(This,MinPoint,MaxPoint) ) 

#define IAcadDwfUnderlay_IntersectWith(This,IntersectObject,option,intPoints)	\
    ( (This)->lpVtbl -> IntersectWith(This,IntersectObject,option,intPoints) ) 

#define IAcadDwfUnderlay_get_PlotStyleName(This,plotStyle)	\
    ( (This)->lpVtbl -> get_PlotStyleName(This,plotStyle) ) 

#define IAcadDwfUnderlay_put_PlotStyleName(This,plotStyle)	\
    ( (This)->lpVtbl -> put_PlotStyleName(This,plotStyle) ) 

#define IAcadDwfUnderlay_get_Lineweight(This,Lineweight)	\
    ( (This)->lpVtbl -> get_Lineweight(This,Lineweight) ) 

#define IAcadDwfUnderlay_put_Lineweight(This,Lineweight)	\
    ( (This)->lpVtbl -> put_Lineweight(This,Lineweight) ) 

#define IAcadDwfUnderlay_get_Hyperlinks(This,Hyperlinks)	\
    ( (This)->lpVtbl -> get_Hyperlinks(This,Hyperlinks) ) 

#define IAcadDwfUnderlay_get_Material(This,Material)	\
    ( (This)->lpVtbl -> get_Material(This,Material) ) 

#define IAcadDwfUnderlay_put_Material(This,Material)	\
    ( (This)->lpVtbl -> put_Material(This,Material) ) 

#define IAcadDwfUnderlay_get_EntityName(This,EntityName)	\
    ( (This)->lpVtbl -> get_EntityName(This,EntityName) ) 

#define IAcadDwfUnderlay_get_EntityType(This,entType)	\
    ( (This)->lpVtbl -> get_EntityType(This,entType) ) 

#define IAcadDwfUnderlay_get_color(This,color)	\
    ( (This)->lpVtbl -> get_color(This,color) ) 

#define IAcadDwfUnderlay_put_color(This,color)	\
    ( (This)->lpVtbl -> put_color(This,color) ) 


#define IAcadDwfUnderlay_get_Contrast(This,Contrast)	\
    ( (This)->lpVtbl -> get_Contrast(This,Contrast) ) 

#define IAcadDwfUnderlay_put_Contrast(This,Contrast)	\
    ( (This)->lpVtbl -> put_Contrast(This,Contrast) ) 

#define IAcadDwfUnderlay_get_Fade(This,Fade)	\
    ( (This)->lpVtbl -> get_Fade(This,Fade) ) 

#define IAcadDwfUnderlay_put_Fade(This,Fade)	\
    ( (This)->lpVtbl -> put_Fade(This,Fade) ) 

#define IAcadDwfUnderlay_get_Position(This,pos)	\
    ( (This)->lpVtbl -> get_Position(This,pos) ) 

#define IAcadDwfUnderlay_put_Position(This,pos)	\
    ( (This)->lpVtbl -> put_Position(This,pos) ) 

#define IAcadDwfUnderlay_get_Rotation(This,rotAngle)	\
    ( (This)->lpVtbl -> get_Rotation(This,rotAngle) ) 

#define IAcadDwfUnderlay_put_Rotation(This,rotAngle)	\
    ( (This)->lpVtbl -> put_Rotation(This,rotAngle) ) 

#define IAcadDwfUnderlay_get_Width(This,Width)	\
    ( (This)->lpVtbl -> get_Width(This,Width) ) 

#define IAcadDwfUnderlay_put_Width(This,Width)	\
    ( (This)->lpVtbl -> put_Width(This,Width) ) 

#define IAcadDwfUnderlay_get_Height(This,Height)	\
    ( (This)->lpVtbl -> get_Height(This,Height) ) 

#define IAcadDwfUnderlay_put_Height(This,Height)	\
    ( (This)->lpVtbl -> put_Height(This,Height) ) 

#define IAcadDwfUnderlay_get_UnderlayName(This,Name)	\
    ( (This)->lpVtbl -> get_UnderlayName(This,Name) ) 

#define IAcadDwfUnderlay_put_UnderlayName(This,Name)	\
    ( (This)->lpVtbl -> put_UnderlayName(This,Name) ) 

#define IAcadDwfUnderlay_put_ItemName(This,sheetName)	\
    ( (This)->lpVtbl -> put_ItemName(This,sheetName) ) 

#define IAcadDwfUnderlay_get_ItemName(This,sheetName)	\
    ( (This)->lpVtbl -> get_ItemName(This,sheetName) ) 

#define IAcadDwfUnderlay_get_Monochrome(This,bMono)	\
    ( (This)->lpVtbl -> get_Monochrome(This,bMono) ) 

#define IAcadDwfUnderlay_put_Monochrome(This,bMono)	\
    ( (This)->lpVtbl -> put_Monochrome(This,bMono) ) 

#define IAcadDwfUnderlay_get_AdjustForBackground(This,Value)	\
    ( (This)->lpVtbl -> get_AdjustForBackground(This,Value) ) 

#define IAcadDwfUnderlay_put_AdjustForBackground(This,Value)	\
    ( (This)->lpVtbl -> put_AdjustForBackground(This,Value) ) 

#define IAcadDwfUnderlay_ClipBoundary(This,boundry)	\
    ( (This)->lpVtbl -> ClipBoundary(This,boundry) ) 

#define IAcadDwfUnderlay_get_ScaleFactor(This,ScaleFactor)	\
    ( (This)->lpVtbl -> get_ScaleFactor(This,ScaleFactor) ) 

#define IAcadDwfUnderlay_put_ScaleFactor(This,ScaleFactor)	\
    ( (This)->lpVtbl -> put_ScaleFactor(This,ScaleFactor) ) 

#define IAcadDwfUnderlay_get_File(This,Name)	\
    ( (This)->lpVtbl -> get_File(This,Name) ) 

#define IAcadDwfUnderlay_put_File(This,Name)	\
    ( (This)->lpVtbl -> put_File(This,Name) ) 

#define IAcadDwfUnderlay_get_UnderlayVisibility(This,fVisible)	\
    ( (This)->lpVtbl -> get_UnderlayVisibility(This,fVisible) ) 

#define IAcadDwfUnderlay_put_UnderlayVisibility(This,fVisible)	\
    ( (This)->lpVtbl -> put_UnderlayVisibility(This,fVisible) ) 

#define IAcadDwfUnderlay_get_ClippingEnabled(This,kClip)	\
    ( (This)->lpVtbl -> get_ClippingEnabled(This,kClip) ) 

#define IAcadDwfUnderlay_put_ClippingEnabled(This,kClip)	\
    ( (This)->lpVtbl -> put_ClippingEnabled(This,kClip) ) 

#define IAcadDwfUnderlay_get_UnderlayLayerOverrideApplied(This,bOverride)	\
    ( (This)->lpVtbl -> get_UnderlayLayerOverrideApplied(This,bOverride) ) 

#define IAcadDwfUnderlay_put_UnderlayLayerOverrideApplied(This,bOverride)	\
    ( (This)->lpVtbl -> put_UnderlayLayerOverrideApplied(This,bOverride) ) 


#define IAcadDwfUnderlay_get_DWFFormat(This,Name)	\
    ( (This)->lpVtbl -> get_DWFFormat(This,Name) ) 

#define IAcadDwfUnderlay_put_DWFFormat(This,Name)	\
    ( (This)->lpVtbl -> put_DWFFormat(This,Name) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAcadDwfUnderlay_INTERFACE_DEFINED__ */


#ifndef __IOdaPolyfaceMesh_INTERFACE_DEFINED__
#define __IOdaPolyfaceMesh_INTERFACE_DEFINED__

/* interface IOdaPolyfaceMesh */
/* [object][oleautomation][dual][helpstring][uuid] */ 


EXTERN_C const IID IID_IOdaPolyfaceMesh;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("88F4A330-80BA-4593-975B-BB2ACDC4E465")
    IOdaPolyfaceMesh : public IAcadPolyfaceMesh
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE GetFaces( 
            /* [out] */ VARIANT *__MIDL__IOdaPolyfaceMesh0000) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct IOdaPolyfaceMeshVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IOdaPolyfaceMesh * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IOdaPolyfaceMesh * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IOdaPolyfaceMesh * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IOdaPolyfaceMesh * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IOdaPolyfaceMesh * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IOdaPolyfaceMesh * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IOdaPolyfaceMesh * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Handle )( 
            IOdaPolyfaceMesh * This,
            /* [retval][out] */ BSTR *Handle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ObjectName )( 
            IOdaPolyfaceMesh * This,
            /* [retval][out] */ BSTR *ObjectName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetXData )( 
            IOdaPolyfaceMesh * This,
            /* [in] */ BSTR AppName,
            /* [out] */ VARIANT *XDataType,
            /* [out] */ VARIANT *XDataValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetXData )( 
            IOdaPolyfaceMesh * This,
            /* [in] */ VARIANT XDataType,
            /* [in] */ VARIANT XDataValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Delete )( 
            IOdaPolyfaceMesh * This);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ObjectID )( 
            IOdaPolyfaceMesh * This,
            /* [retval][out] */ LONG_PTR *ObjectID);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Application )( 
            IOdaPolyfaceMesh * This,
            /* [retval][out] */ IDispatch **ApplicationObject);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Database )( 
            IOdaPolyfaceMesh * This,
            /* [retval][out] */ IAcadDatabase **pDatabase);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_HasExtensionDictionary )( 
            IOdaPolyfaceMesh * This,
            /* [retval][out] */ VARIANT_BOOL *bHasDictionary);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetExtensionDictionary )( 
            IOdaPolyfaceMesh * This,
            /* [retval][out] */ IAcadDictionary **pExtDictionary);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_OwnerID )( 
            IOdaPolyfaceMesh * This,
            /* [retval][out] */ LONG_PTR *OwnerID);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Document )( 
            IOdaPolyfaceMesh * This,
            /* [retval][out] */ IDispatch **pDocument);
        
        /* [helpstring][hidden][id] */ HRESULT ( STDMETHODCALLTYPE *Erase )( 
            IOdaPolyfaceMesh * This);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_TrueColor )( 
            IOdaPolyfaceMesh * This,
            /* [retval][out] */ IAcadAcCmColor **pColor);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_TrueColor )( 
            IOdaPolyfaceMesh * This,
            /* [in] */ IAcadAcCmColor *pColor);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Layer )( 
            IOdaPolyfaceMesh * This,
            /* [retval][out] */ BSTR *Layer);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Layer )( 
            IOdaPolyfaceMesh * This,
            /* [in] */ BSTR Layer);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Linetype )( 
            IOdaPolyfaceMesh * This,
            /* [retval][out] */ BSTR *Linetype);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Linetype )( 
            IOdaPolyfaceMesh * This,
            /* [in] */ BSTR Linetype);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_LinetypeScale )( 
            IOdaPolyfaceMesh * This,
            /* [retval][out] */ ACAD_NOUNITS *ltScale);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_LinetypeScale )( 
            IOdaPolyfaceMesh * This,
            /* [in] */ ACAD_NOUNITS ltScale);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Visible )( 
            IOdaPolyfaceMesh * This,
            /* [retval][out] */ VARIANT_BOOL *bVisible);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Visible )( 
            IOdaPolyfaceMesh * This,
            /* [in] */ VARIANT_BOOL bVisible);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ArrayPolar )( 
            IOdaPolyfaceMesh * This,
            /* [in] */ int NumberOfObjects,
            /* [in] */ double AngleToFill,
            /* [in] */ VARIANT CenterPoint,
            /* [retval][out] */ VARIANT *pArrayObjs);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ArrayRectangular )( 
            IOdaPolyfaceMesh * This,
            /* [in] */ int NumberOfRows,
            /* [in] */ int NumberOfColumns,
            /* [in] */ int NumberOfLevels,
            /* [in] */ double DistBetweenRows,
            /* [in] */ double DistBetweenCols,
            /* [in] */ double DistBetweenLevels,
            /* [retval][out] */ VARIANT *pArrayObjs);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Highlight )( 
            IOdaPolyfaceMesh * This,
            /* [in] */ VARIANT_BOOL HighlightFlag);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Copy )( 
            IOdaPolyfaceMesh * This,
            /* [retval][out] */ IDispatch **pCopyObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Move )( 
            IOdaPolyfaceMesh * This,
            /* [in] */ VARIANT FromPoint,
            /* [in] */ VARIANT ToPoint);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Rotate )( 
            IOdaPolyfaceMesh * This,
            /* [in] */ VARIANT BasePoint,
            /* [in] */ double RotationAngle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Rotate3D )( 
            IOdaPolyfaceMesh * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [in] */ double RotationAngle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Mirror )( 
            IOdaPolyfaceMesh * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [retval][out] */ IDispatch **pMirrorObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Mirror3D )( 
            IOdaPolyfaceMesh * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [in] */ VARIANT point3,
            /* [retval][out] */ IDispatch **pMirrorObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ScaleEntity )( 
            IOdaPolyfaceMesh * This,
            /* [in] */ VARIANT BasePoint,
            /* [in] */ double ScaleFactor);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *TransformBy )( 
            IOdaPolyfaceMesh * This,
            /* [in] */ VARIANT TransformationMatrix);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Update )( 
            IOdaPolyfaceMesh * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetBoundingBox )( 
            IOdaPolyfaceMesh * This,
            /* [out] */ VARIANT *MinPoint,
            /* [out] */ VARIANT *MaxPoint);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *IntersectWith )( 
            IOdaPolyfaceMesh * This,
            /* [in] */ IDispatch *IntersectObject,
            /* [in] */ AcExtendOption option,
            /* [retval][out] */ VARIANT *intPoints);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_PlotStyleName )( 
            IOdaPolyfaceMesh * This,
            /* [retval][out] */ BSTR *plotStyle);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_PlotStyleName )( 
            IOdaPolyfaceMesh * This,
            /* [in] */ BSTR plotStyle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Lineweight )( 
            IOdaPolyfaceMesh * This,
            /* [retval][out] */ ACAD_LWEIGHT *Lineweight);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Lineweight )( 
            IOdaPolyfaceMesh * This,
            /* [in] */ ACAD_LWEIGHT Lineweight);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Hyperlinks )( 
            IOdaPolyfaceMesh * This,
            /* [retval][out] */ IAcadHyperlinks **Hyperlinks);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Material )( 
            IOdaPolyfaceMesh * This,
            /* [retval][out] */ BSTR *Material);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Material )( 
            IOdaPolyfaceMesh * This,
            /* [in] */ BSTR Material);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EntityName )( 
            IOdaPolyfaceMesh * This,
            /* [retval][out] */ BSTR *EntityName);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EntityType )( 
            IOdaPolyfaceMesh * This,
            /* [retval][out] */ long *entType);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_color )( 
            IOdaPolyfaceMesh * This,
            /* [retval][out] */ ACAD_COLOR *color);
        
        /* [helpstring][hidden][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_color )( 
            IOdaPolyfaceMesh * This,
            /* [in] */ ACAD_COLOR color);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Coordinates )( 
            IOdaPolyfaceMesh * This,
            /* [in] */ VARIANT Vertices);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Coordinates )( 
            IOdaPolyfaceMesh * This,
            /* [retval][out] */ VARIANT *Vertices);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Coordinate )( 
            IOdaPolyfaceMesh * This,
            /* [in] */ int Index,
            /* [retval][out] */ VARIANT *pVal);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Coordinate )( 
            IOdaPolyfaceMesh * This,
            /* [in] */ int Index,
            /* [in] */ VARIANT pVal);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_NumberOfVertices )( 
            IOdaPolyfaceMesh * This,
            /* [retval][out] */ long *NumVertices);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_NumberOfFaces )( 
            IOdaPolyfaceMesh * This,
            /* [retval][out] */ long *NumFaces);
        
        /* [hidden][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Faces )( 
            IOdaPolyfaceMesh * This,
            /* [in] */ VARIANT rhs);
        
        HRESULT ( STDMETHODCALLTYPE *GetFaces )( 
            IOdaPolyfaceMesh * This,
            /* [out] */ VARIANT *__MIDL__IOdaPolyfaceMesh0000);
        
        END_INTERFACE
    } IOdaPolyfaceMeshVtbl;

    interface IOdaPolyfaceMesh
    {
        CONST_VTBL struct IOdaPolyfaceMeshVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IOdaPolyfaceMesh_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IOdaPolyfaceMesh_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IOdaPolyfaceMesh_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IOdaPolyfaceMesh_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IOdaPolyfaceMesh_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IOdaPolyfaceMesh_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IOdaPolyfaceMesh_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IOdaPolyfaceMesh_get_Handle(This,Handle)	\
    ( (This)->lpVtbl -> get_Handle(This,Handle) ) 

#define IOdaPolyfaceMesh_get_ObjectName(This,ObjectName)	\
    ( (This)->lpVtbl -> get_ObjectName(This,ObjectName) ) 

#define IOdaPolyfaceMesh_GetXData(This,AppName,XDataType,XDataValue)	\
    ( (This)->lpVtbl -> GetXData(This,AppName,XDataType,XDataValue) ) 

#define IOdaPolyfaceMesh_SetXData(This,XDataType,XDataValue)	\
    ( (This)->lpVtbl -> SetXData(This,XDataType,XDataValue) ) 

#define IOdaPolyfaceMesh_Delete(This)	\
    ( (This)->lpVtbl -> Delete(This) ) 

#define IOdaPolyfaceMesh_get_ObjectID(This,ObjectID)	\
    ( (This)->lpVtbl -> get_ObjectID(This,ObjectID) ) 

#define IOdaPolyfaceMesh_get_Application(This,ApplicationObject)	\
    ( (This)->lpVtbl -> get_Application(This,ApplicationObject) ) 

#define IOdaPolyfaceMesh_get_Database(This,pDatabase)	\
    ( (This)->lpVtbl -> get_Database(This,pDatabase) ) 

#define IOdaPolyfaceMesh_get_HasExtensionDictionary(This,bHasDictionary)	\
    ( (This)->lpVtbl -> get_HasExtensionDictionary(This,bHasDictionary) ) 

#define IOdaPolyfaceMesh_GetExtensionDictionary(This,pExtDictionary)	\
    ( (This)->lpVtbl -> GetExtensionDictionary(This,pExtDictionary) ) 

#define IOdaPolyfaceMesh_get_OwnerID(This,OwnerID)	\
    ( (This)->lpVtbl -> get_OwnerID(This,OwnerID) ) 

#define IOdaPolyfaceMesh_get_Document(This,pDocument)	\
    ( (This)->lpVtbl -> get_Document(This,pDocument) ) 

#define IOdaPolyfaceMesh_Erase(This)	\
    ( (This)->lpVtbl -> Erase(This) ) 


#define IOdaPolyfaceMesh_get_TrueColor(This,pColor)	\
    ( (This)->lpVtbl -> get_TrueColor(This,pColor) ) 

#define IOdaPolyfaceMesh_put_TrueColor(This,pColor)	\
    ( (This)->lpVtbl -> put_TrueColor(This,pColor) ) 

#define IOdaPolyfaceMesh_get_Layer(This,Layer)	\
    ( (This)->lpVtbl -> get_Layer(This,Layer) ) 

#define IOdaPolyfaceMesh_put_Layer(This,Layer)	\
    ( (This)->lpVtbl -> put_Layer(This,Layer) ) 

#define IOdaPolyfaceMesh_get_Linetype(This,Linetype)	\
    ( (This)->lpVtbl -> get_Linetype(This,Linetype) ) 

#define IOdaPolyfaceMesh_put_Linetype(This,Linetype)	\
    ( (This)->lpVtbl -> put_Linetype(This,Linetype) ) 

#define IOdaPolyfaceMesh_get_LinetypeScale(This,ltScale)	\
    ( (This)->lpVtbl -> get_LinetypeScale(This,ltScale) ) 

#define IOdaPolyfaceMesh_put_LinetypeScale(This,ltScale)	\
    ( (This)->lpVtbl -> put_LinetypeScale(This,ltScale) ) 

#define IOdaPolyfaceMesh_get_Visible(This,bVisible)	\
    ( (This)->lpVtbl -> get_Visible(This,bVisible) ) 

#define IOdaPolyfaceMesh_put_Visible(This,bVisible)	\
    ( (This)->lpVtbl -> put_Visible(This,bVisible) ) 

#define IOdaPolyfaceMesh_ArrayPolar(This,NumberOfObjects,AngleToFill,CenterPoint,pArrayObjs)	\
    ( (This)->lpVtbl -> ArrayPolar(This,NumberOfObjects,AngleToFill,CenterPoint,pArrayObjs) ) 

#define IOdaPolyfaceMesh_ArrayRectangular(This,NumberOfRows,NumberOfColumns,NumberOfLevels,DistBetweenRows,DistBetweenCols,DistBetweenLevels,pArrayObjs)	\
    ( (This)->lpVtbl -> ArrayRectangular(This,NumberOfRows,NumberOfColumns,NumberOfLevels,DistBetweenRows,DistBetweenCols,DistBetweenLevels,pArrayObjs) ) 

#define IOdaPolyfaceMesh_Highlight(This,HighlightFlag)	\
    ( (This)->lpVtbl -> Highlight(This,HighlightFlag) ) 

#define IOdaPolyfaceMesh_Copy(This,pCopyObj)	\
    ( (This)->lpVtbl -> Copy(This,pCopyObj) ) 

#define IOdaPolyfaceMesh_Move(This,FromPoint,ToPoint)	\
    ( (This)->lpVtbl -> Move(This,FromPoint,ToPoint) ) 

#define IOdaPolyfaceMesh_Rotate(This,BasePoint,RotationAngle)	\
    ( (This)->lpVtbl -> Rotate(This,BasePoint,RotationAngle) ) 

#define IOdaPolyfaceMesh_Rotate3D(This,Point1,Point2,RotationAngle)	\
    ( (This)->lpVtbl -> Rotate3D(This,Point1,Point2,RotationAngle) ) 

#define IOdaPolyfaceMesh_Mirror(This,Point1,Point2,pMirrorObj)	\
    ( (This)->lpVtbl -> Mirror(This,Point1,Point2,pMirrorObj) ) 

#define IOdaPolyfaceMesh_Mirror3D(This,Point1,Point2,point3,pMirrorObj)	\
    ( (This)->lpVtbl -> Mirror3D(This,Point1,Point2,point3,pMirrorObj) ) 

#define IOdaPolyfaceMesh_ScaleEntity(This,BasePoint,ScaleFactor)	\
    ( (This)->lpVtbl -> ScaleEntity(This,BasePoint,ScaleFactor) ) 

#define IOdaPolyfaceMesh_TransformBy(This,TransformationMatrix)	\
    ( (This)->lpVtbl -> TransformBy(This,TransformationMatrix) ) 

#define IOdaPolyfaceMesh_Update(This)	\
    ( (This)->lpVtbl -> Update(This) ) 

#define IOdaPolyfaceMesh_GetBoundingBox(This,MinPoint,MaxPoint)	\
    ( (This)->lpVtbl -> GetBoundingBox(This,MinPoint,MaxPoint) ) 

#define IOdaPolyfaceMesh_IntersectWith(This,IntersectObject,option,intPoints)	\
    ( (This)->lpVtbl -> IntersectWith(This,IntersectObject,option,intPoints) ) 

#define IOdaPolyfaceMesh_get_PlotStyleName(This,plotStyle)	\
    ( (This)->lpVtbl -> get_PlotStyleName(This,plotStyle) ) 

#define IOdaPolyfaceMesh_put_PlotStyleName(This,plotStyle)	\
    ( (This)->lpVtbl -> put_PlotStyleName(This,plotStyle) ) 

#define IOdaPolyfaceMesh_get_Lineweight(This,Lineweight)	\
    ( (This)->lpVtbl -> get_Lineweight(This,Lineweight) ) 

#define IOdaPolyfaceMesh_put_Lineweight(This,Lineweight)	\
    ( (This)->lpVtbl -> put_Lineweight(This,Lineweight) ) 

#define IOdaPolyfaceMesh_get_Hyperlinks(This,Hyperlinks)	\
    ( (This)->lpVtbl -> get_Hyperlinks(This,Hyperlinks) ) 

#define IOdaPolyfaceMesh_get_Material(This,Material)	\
    ( (This)->lpVtbl -> get_Material(This,Material) ) 

#define IOdaPolyfaceMesh_put_Material(This,Material)	\
    ( (This)->lpVtbl -> put_Material(This,Material) ) 

#define IOdaPolyfaceMesh_get_EntityName(This,EntityName)	\
    ( (This)->lpVtbl -> get_EntityName(This,EntityName) ) 

#define IOdaPolyfaceMesh_get_EntityType(This,entType)	\
    ( (This)->lpVtbl -> get_EntityType(This,entType) ) 

#define IOdaPolyfaceMesh_get_color(This,color)	\
    ( (This)->lpVtbl -> get_color(This,color) ) 

#define IOdaPolyfaceMesh_put_color(This,color)	\
    ( (This)->lpVtbl -> put_color(This,color) ) 


#define IOdaPolyfaceMesh_put_Coordinates(This,Vertices)	\
    ( (This)->lpVtbl -> put_Coordinates(This,Vertices) ) 

#define IOdaPolyfaceMesh_get_Coordinates(This,Vertices)	\
    ( (This)->lpVtbl -> get_Coordinates(This,Vertices) ) 

#define IOdaPolyfaceMesh_get_Coordinate(This,Index,pVal)	\
    ( (This)->lpVtbl -> get_Coordinate(This,Index,pVal) ) 

#define IOdaPolyfaceMesh_put_Coordinate(This,Index,pVal)	\
    ( (This)->lpVtbl -> put_Coordinate(This,Index,pVal) ) 

#define IOdaPolyfaceMesh_get_NumberOfVertices(This,NumVertices)	\
    ( (This)->lpVtbl -> get_NumberOfVertices(This,NumVertices) ) 

#define IOdaPolyfaceMesh_get_NumberOfFaces(This,NumFaces)	\
    ( (This)->lpVtbl -> get_NumberOfFaces(This,NumFaces) ) 

#define IOdaPolyfaceMesh_put_Faces(This,rhs)	\
    ( (This)->lpVtbl -> put_Faces(This,rhs) ) 


#define IOdaPolyfaceMesh_GetFaces(This,__MIDL__IOdaPolyfaceMesh0000)	\
    ( (This)->lpVtbl -> GetFaces(This,__MIDL__IOdaPolyfaceMesh0000) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IOdaPolyfaceMesh_INTERFACE_DEFINED__ */


#ifndef __IAcadSubEntity_INTERFACE_DEFINED__
#define __IAcadSubEntity_INTERFACE_DEFINED__

/* interface IAcadSubEntity */
/* [object][oleautomation][dual][helpstring][uuid] */ 


EXTERN_C const IID IID_IAcadSubEntity;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("70991FC5-F814-45a8-BB15-BC703AB5BF63")
    IAcadSubEntity : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE OnModified( void) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_ObjectName( 
            /* [retval][out] */ BSTR *ObjectName) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_color( 
            /* [retval][out] */ IAcadAcCmColor **pColor) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_color( 
            /* [in] */ IAcadAcCmColor *pColor) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_Layer( 
            /* [retval][out] */ BSTR *Layer) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_Linetype( 
            /* [retval][out] */ BSTR *Linetype) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_LinetypeScale( 
            /* [retval][out] */ ACAD_NOUNITS *ltScale) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_PlotStyleName( 
            /* [retval][out] */ BSTR *plotStyle) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_Lineweight( 
            /* [retval][out] */ ACAD_LWEIGHT *Lineweight) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_Hyperlinks( 
            /* [retval][out] */ IAcadHyperlinks **Hyperlinks) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct IAcadSubEntityVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAcadSubEntity * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAcadSubEntity * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAcadSubEntity * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IAcadSubEntity * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IAcadSubEntity * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IAcadSubEntity * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IAcadSubEntity * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *OnModified )( 
            IAcadSubEntity * This);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ObjectName )( 
            IAcadSubEntity * This,
            /* [retval][out] */ BSTR *ObjectName);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_color )( 
            IAcadSubEntity * This,
            /* [retval][out] */ IAcadAcCmColor **pColor);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_color )( 
            IAcadSubEntity * This,
            /* [in] */ IAcadAcCmColor *pColor);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Layer )( 
            IAcadSubEntity * This,
            /* [retval][out] */ BSTR *Layer);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Linetype )( 
            IAcadSubEntity * This,
            /* [retval][out] */ BSTR *Linetype);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_LinetypeScale )( 
            IAcadSubEntity * This,
            /* [retval][out] */ ACAD_NOUNITS *ltScale);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_PlotStyleName )( 
            IAcadSubEntity * This,
            /* [retval][out] */ BSTR *plotStyle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Lineweight )( 
            IAcadSubEntity * This,
            /* [retval][out] */ ACAD_LWEIGHT *Lineweight);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Hyperlinks )( 
            IAcadSubEntity * This,
            /* [retval][out] */ IAcadHyperlinks **Hyperlinks);
        
        END_INTERFACE
    } IAcadSubEntityVtbl;

    interface IAcadSubEntity
    {
        CONST_VTBL struct IAcadSubEntityVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAcadSubEntity_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAcadSubEntity_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAcadSubEntity_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAcadSubEntity_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IAcadSubEntity_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IAcadSubEntity_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IAcadSubEntity_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IAcadSubEntity_OnModified(This)	\
    ( (This)->lpVtbl -> OnModified(This) ) 

#define IAcadSubEntity_get_ObjectName(This,ObjectName)	\
    ( (This)->lpVtbl -> get_ObjectName(This,ObjectName) ) 

#define IAcadSubEntity_get_color(This,pColor)	\
    ( (This)->lpVtbl -> get_color(This,pColor) ) 

#define IAcadSubEntity_put_color(This,pColor)	\
    ( (This)->lpVtbl -> put_color(This,pColor) ) 

#define IAcadSubEntity_get_Layer(This,Layer)	\
    ( (This)->lpVtbl -> get_Layer(This,Layer) ) 

#define IAcadSubEntity_get_Linetype(This,Linetype)	\
    ( (This)->lpVtbl -> get_Linetype(This,Linetype) ) 

#define IAcadSubEntity_get_LinetypeScale(This,ltScale)	\
    ( (This)->lpVtbl -> get_LinetypeScale(This,ltScale) ) 

#define IAcadSubEntity_get_PlotStyleName(This,plotStyle)	\
    ( (This)->lpVtbl -> get_PlotStyleName(This,plotStyle) ) 

#define IAcadSubEntity_get_Lineweight(This,Lineweight)	\
    ( (This)->lpVtbl -> get_Lineweight(This,Lineweight) ) 

#define IAcadSubEntity_get_Hyperlinks(This,Hyperlinks)	\
    ( (This)->lpVtbl -> get_Hyperlinks(This,Hyperlinks) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAcadSubEntity_INTERFACE_DEFINED__ */


#ifndef __IAcadMLeaderLeader_INTERFACE_DEFINED__
#define __IAcadMLeaderLeader_INTERFACE_DEFINED__

/* interface IAcadMLeaderLeader */
/* [object][oleautomation][dual][helpstring][uuid] */ 


EXTERN_C const IID IID_IAcadMLeaderLeader;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("D0AC8EBF-1F76-43ef-9748-D7EE52AABE25")
    IAcadMLeaderLeader : public IAcadSubEntity
    {
    public:
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_LeaderType( 
            /* [retval][out] */ AcMLeaderType *Type) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_LeaderType( 
            /* [in] */ AcMLeaderType Type) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_LeaderLineColor( 
            /* [retval][out] */ IAcadAcCmColor **Type) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_LeaderLineColor( 
            /* [in] */ IAcadAcCmColor *Type) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_LeaderLinetype( 
            /* [retval][out] */ ACAD_LTYPE *Linetype) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_LeaderLinetype( 
            /* [in] */ ACAD_LTYPE Linetype) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_LeaderLineWeight( 
            /* [retval][out] */ ACAD_LWEIGHT *Lineweight) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_LeaderLineWeight( 
            /* [in] */ ACAD_LWEIGHT Lineweight) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_ArrowheadType( 
            /* [retval][out] */ AcDimArrowheadType *BlockName) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_ArrowheadType( 
            /* [in] */ AcDimArrowheadType BlockName) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_ArrowheadSize( 
            /* [retval][out] */ double *size) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_ArrowheadSize( 
            /* [in] */ double size) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_ArrowheadBlock( 
            /* [retval][out] */ BSTR *BlockName) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_ArrowheadBlock( 
            /* [in] */ BSTR BlockName) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct IAcadMLeaderLeaderVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAcadMLeaderLeader * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAcadMLeaderLeader * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAcadMLeaderLeader * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IAcadMLeaderLeader * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IAcadMLeaderLeader * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IAcadMLeaderLeader * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IAcadMLeaderLeader * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *OnModified )( 
            IAcadMLeaderLeader * This);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ObjectName )( 
            IAcadMLeaderLeader * This,
            /* [retval][out] */ BSTR *ObjectName);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_color )( 
            IAcadMLeaderLeader * This,
            /* [retval][out] */ IAcadAcCmColor **pColor);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_color )( 
            IAcadMLeaderLeader * This,
            /* [in] */ IAcadAcCmColor *pColor);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Layer )( 
            IAcadMLeaderLeader * This,
            /* [retval][out] */ BSTR *Layer);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Linetype )( 
            IAcadMLeaderLeader * This,
            /* [retval][out] */ BSTR *Linetype);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_LinetypeScale )( 
            IAcadMLeaderLeader * This,
            /* [retval][out] */ ACAD_NOUNITS *ltScale);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_PlotStyleName )( 
            IAcadMLeaderLeader * This,
            /* [retval][out] */ BSTR *plotStyle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Lineweight )( 
            IAcadMLeaderLeader * This,
            /* [retval][out] */ ACAD_LWEIGHT *Lineweight);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Hyperlinks )( 
            IAcadMLeaderLeader * This,
            /* [retval][out] */ IAcadHyperlinks **Hyperlinks);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_LeaderType )( 
            IAcadMLeaderLeader * This,
            /* [retval][out] */ AcMLeaderType *Type);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_LeaderType )( 
            IAcadMLeaderLeader * This,
            /* [in] */ AcMLeaderType Type);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_LeaderLineColor )( 
            IAcadMLeaderLeader * This,
            /* [retval][out] */ IAcadAcCmColor **Type);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_LeaderLineColor )( 
            IAcadMLeaderLeader * This,
            /* [in] */ IAcadAcCmColor *Type);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_LeaderLinetype )( 
            IAcadMLeaderLeader * This,
            /* [retval][out] */ ACAD_LTYPE *Linetype);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_LeaderLinetype )( 
            IAcadMLeaderLeader * This,
            /* [in] */ ACAD_LTYPE Linetype);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_LeaderLineWeight )( 
            IAcadMLeaderLeader * This,
            /* [retval][out] */ ACAD_LWEIGHT *Lineweight);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_LeaderLineWeight )( 
            IAcadMLeaderLeader * This,
            /* [in] */ ACAD_LWEIGHT Lineweight);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ArrowheadType )( 
            IAcadMLeaderLeader * This,
            /* [retval][out] */ AcDimArrowheadType *BlockName);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ArrowheadType )( 
            IAcadMLeaderLeader * This,
            /* [in] */ AcDimArrowheadType BlockName);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ArrowheadSize )( 
            IAcadMLeaderLeader * This,
            /* [retval][out] */ double *size);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ArrowheadSize )( 
            IAcadMLeaderLeader * This,
            /* [in] */ double size);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ArrowheadBlock )( 
            IAcadMLeaderLeader * This,
            /* [retval][out] */ BSTR *BlockName);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ArrowheadBlock )( 
            IAcadMLeaderLeader * This,
            /* [in] */ BSTR BlockName);
        
        END_INTERFACE
    } IAcadMLeaderLeaderVtbl;

    interface IAcadMLeaderLeader
    {
        CONST_VTBL struct IAcadMLeaderLeaderVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAcadMLeaderLeader_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAcadMLeaderLeader_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAcadMLeaderLeader_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAcadMLeaderLeader_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IAcadMLeaderLeader_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IAcadMLeaderLeader_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IAcadMLeaderLeader_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IAcadMLeaderLeader_OnModified(This)	\
    ( (This)->lpVtbl -> OnModified(This) ) 

#define IAcadMLeaderLeader_get_ObjectName(This,ObjectName)	\
    ( (This)->lpVtbl -> get_ObjectName(This,ObjectName) ) 

#define IAcadMLeaderLeader_get_color(This,pColor)	\
    ( (This)->lpVtbl -> get_color(This,pColor) ) 

#define IAcadMLeaderLeader_put_color(This,pColor)	\
    ( (This)->lpVtbl -> put_color(This,pColor) ) 

#define IAcadMLeaderLeader_get_Layer(This,Layer)	\
    ( (This)->lpVtbl -> get_Layer(This,Layer) ) 

#define IAcadMLeaderLeader_get_Linetype(This,Linetype)	\
    ( (This)->lpVtbl -> get_Linetype(This,Linetype) ) 

#define IAcadMLeaderLeader_get_LinetypeScale(This,ltScale)	\
    ( (This)->lpVtbl -> get_LinetypeScale(This,ltScale) ) 

#define IAcadMLeaderLeader_get_PlotStyleName(This,plotStyle)	\
    ( (This)->lpVtbl -> get_PlotStyleName(This,plotStyle) ) 

#define IAcadMLeaderLeader_get_Lineweight(This,Lineweight)	\
    ( (This)->lpVtbl -> get_Lineweight(This,Lineweight) ) 

#define IAcadMLeaderLeader_get_Hyperlinks(This,Hyperlinks)	\
    ( (This)->lpVtbl -> get_Hyperlinks(This,Hyperlinks) ) 


#define IAcadMLeaderLeader_get_LeaderType(This,Type)	\
    ( (This)->lpVtbl -> get_LeaderType(This,Type) ) 

#define IAcadMLeaderLeader_put_LeaderType(This,Type)	\
    ( (This)->lpVtbl -> put_LeaderType(This,Type) ) 

#define IAcadMLeaderLeader_get_LeaderLineColor(This,Type)	\
    ( (This)->lpVtbl -> get_LeaderLineColor(This,Type) ) 

#define IAcadMLeaderLeader_put_LeaderLineColor(This,Type)	\
    ( (This)->lpVtbl -> put_LeaderLineColor(This,Type) ) 

#define IAcadMLeaderLeader_get_LeaderLinetype(This,Linetype)	\
    ( (This)->lpVtbl -> get_LeaderLinetype(This,Linetype) ) 

#define IAcadMLeaderLeader_put_LeaderLinetype(This,Linetype)	\
    ( (This)->lpVtbl -> put_LeaderLinetype(This,Linetype) ) 

#define IAcadMLeaderLeader_get_LeaderLineWeight(This,Lineweight)	\
    ( (This)->lpVtbl -> get_LeaderLineWeight(This,Lineweight) ) 

#define IAcadMLeaderLeader_put_LeaderLineWeight(This,Lineweight)	\
    ( (This)->lpVtbl -> put_LeaderLineWeight(This,Lineweight) ) 

#define IAcadMLeaderLeader_get_ArrowheadType(This,BlockName)	\
    ( (This)->lpVtbl -> get_ArrowheadType(This,BlockName) ) 

#define IAcadMLeaderLeader_put_ArrowheadType(This,BlockName)	\
    ( (This)->lpVtbl -> put_ArrowheadType(This,BlockName) ) 

#define IAcadMLeaderLeader_get_ArrowheadSize(This,size)	\
    ( (This)->lpVtbl -> get_ArrowheadSize(This,size) ) 

#define IAcadMLeaderLeader_put_ArrowheadSize(This,size)	\
    ( (This)->lpVtbl -> put_ArrowheadSize(This,size) ) 

#define IAcadMLeaderLeader_get_ArrowheadBlock(This,BlockName)	\
    ( (This)->lpVtbl -> get_ArrowheadBlock(This,BlockName) ) 

#define IAcadMLeaderLeader_put_ArrowheadBlock(This,BlockName)	\
    ( (This)->lpVtbl -> put_ArrowheadBlock(This,BlockName) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAcadMLeaderLeader_INTERFACE_DEFINED__ */


#ifndef __IAcadSubEntSolidFace_INTERFACE_DEFINED__
#define __IAcadSubEntSolidFace_INTERFACE_DEFINED__

/* interface IAcadSubEntSolidFace */
/* [object][oleautomation][dual][helpstring][uuid] */ 


EXTERN_C const IID IID_IAcadSubEntSolidFace;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("9D76FD22-F78B-40b5-94A3-B8C0AB17A1E2")
    IAcadSubEntSolidFace : public IAcadSubEntity
    {
    public:
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_Material( 
            /* [retval][out] */ BSTR *Material) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_Material( 
            /* [in] */ BSTR Material) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct IAcadSubEntSolidFaceVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAcadSubEntSolidFace * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAcadSubEntSolidFace * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAcadSubEntSolidFace * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IAcadSubEntSolidFace * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IAcadSubEntSolidFace * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IAcadSubEntSolidFace * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IAcadSubEntSolidFace * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *OnModified )( 
            IAcadSubEntSolidFace * This);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ObjectName )( 
            IAcadSubEntSolidFace * This,
            /* [retval][out] */ BSTR *ObjectName);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_color )( 
            IAcadSubEntSolidFace * This,
            /* [retval][out] */ IAcadAcCmColor **pColor);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_color )( 
            IAcadSubEntSolidFace * This,
            /* [in] */ IAcadAcCmColor *pColor);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Layer )( 
            IAcadSubEntSolidFace * This,
            /* [retval][out] */ BSTR *Layer);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Linetype )( 
            IAcadSubEntSolidFace * This,
            /* [retval][out] */ BSTR *Linetype);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_LinetypeScale )( 
            IAcadSubEntSolidFace * This,
            /* [retval][out] */ ACAD_NOUNITS *ltScale);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_PlotStyleName )( 
            IAcadSubEntSolidFace * This,
            /* [retval][out] */ BSTR *plotStyle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Lineweight )( 
            IAcadSubEntSolidFace * This,
            /* [retval][out] */ ACAD_LWEIGHT *Lineweight);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Hyperlinks )( 
            IAcadSubEntSolidFace * This,
            /* [retval][out] */ IAcadHyperlinks **Hyperlinks);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Material )( 
            IAcadSubEntSolidFace * This,
            /* [retval][out] */ BSTR *Material);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Material )( 
            IAcadSubEntSolidFace * This,
            /* [in] */ BSTR Material);
        
        END_INTERFACE
    } IAcadSubEntSolidFaceVtbl;

    interface IAcadSubEntSolidFace
    {
        CONST_VTBL struct IAcadSubEntSolidFaceVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAcadSubEntSolidFace_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAcadSubEntSolidFace_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAcadSubEntSolidFace_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAcadSubEntSolidFace_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IAcadSubEntSolidFace_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IAcadSubEntSolidFace_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IAcadSubEntSolidFace_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IAcadSubEntSolidFace_OnModified(This)	\
    ( (This)->lpVtbl -> OnModified(This) ) 

#define IAcadSubEntSolidFace_get_ObjectName(This,ObjectName)	\
    ( (This)->lpVtbl -> get_ObjectName(This,ObjectName) ) 

#define IAcadSubEntSolidFace_get_color(This,pColor)	\
    ( (This)->lpVtbl -> get_color(This,pColor) ) 

#define IAcadSubEntSolidFace_put_color(This,pColor)	\
    ( (This)->lpVtbl -> put_color(This,pColor) ) 

#define IAcadSubEntSolidFace_get_Layer(This,Layer)	\
    ( (This)->lpVtbl -> get_Layer(This,Layer) ) 

#define IAcadSubEntSolidFace_get_Linetype(This,Linetype)	\
    ( (This)->lpVtbl -> get_Linetype(This,Linetype) ) 

#define IAcadSubEntSolidFace_get_LinetypeScale(This,ltScale)	\
    ( (This)->lpVtbl -> get_LinetypeScale(This,ltScale) ) 

#define IAcadSubEntSolidFace_get_PlotStyleName(This,plotStyle)	\
    ( (This)->lpVtbl -> get_PlotStyleName(This,plotStyle) ) 

#define IAcadSubEntSolidFace_get_Lineweight(This,Lineweight)	\
    ( (This)->lpVtbl -> get_Lineweight(This,Lineweight) ) 

#define IAcadSubEntSolidFace_get_Hyperlinks(This,Hyperlinks)	\
    ( (This)->lpVtbl -> get_Hyperlinks(This,Hyperlinks) ) 


#define IAcadSubEntSolidFace_get_Material(This,Material)	\
    ( (This)->lpVtbl -> get_Material(This,Material) ) 

#define IAcadSubEntSolidFace_put_Material(This,Material)	\
    ( (This)->lpVtbl -> put_Material(This,Material) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAcadSubEntSolidFace_INTERFACE_DEFINED__ */


#ifndef __IAcadSubEntSolidEdge_INTERFACE_DEFINED__
#define __IAcadSubEntSolidEdge_INTERFACE_DEFINED__

/* interface IAcadSubEntSolidEdge */
/* [object][oleautomation][dual][helpstring][uuid] */ 


EXTERN_C const IID IID_IAcadSubEntSolidEdge;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("EA82FD79-6DE0-445e-ABFB-541E52C91BB5")
    IAcadSubEntSolidEdge : public IAcadSubEntity
    {
    public:
    };
    
    
#else 	/* C style interface */

    typedef struct IAcadSubEntSolidEdgeVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAcadSubEntSolidEdge * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAcadSubEntSolidEdge * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAcadSubEntSolidEdge * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IAcadSubEntSolidEdge * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IAcadSubEntSolidEdge * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IAcadSubEntSolidEdge * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IAcadSubEntSolidEdge * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *OnModified )( 
            IAcadSubEntSolidEdge * This);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ObjectName )( 
            IAcadSubEntSolidEdge * This,
            /* [retval][out] */ BSTR *ObjectName);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_color )( 
            IAcadSubEntSolidEdge * This,
            /* [retval][out] */ IAcadAcCmColor **pColor);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_color )( 
            IAcadSubEntSolidEdge * This,
            /* [in] */ IAcadAcCmColor *pColor);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Layer )( 
            IAcadSubEntSolidEdge * This,
            /* [retval][out] */ BSTR *Layer);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Linetype )( 
            IAcadSubEntSolidEdge * This,
            /* [retval][out] */ BSTR *Linetype);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_LinetypeScale )( 
            IAcadSubEntSolidEdge * This,
            /* [retval][out] */ ACAD_NOUNITS *ltScale);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_PlotStyleName )( 
            IAcadSubEntSolidEdge * This,
            /* [retval][out] */ BSTR *plotStyle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Lineweight )( 
            IAcadSubEntSolidEdge * This,
            /* [retval][out] */ ACAD_LWEIGHT *Lineweight);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Hyperlinks )( 
            IAcadSubEntSolidEdge * This,
            /* [retval][out] */ IAcadHyperlinks **Hyperlinks);
        
        END_INTERFACE
    } IAcadSubEntSolidEdgeVtbl;

    interface IAcadSubEntSolidEdge
    {
        CONST_VTBL struct IAcadSubEntSolidEdgeVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAcadSubEntSolidEdge_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAcadSubEntSolidEdge_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAcadSubEntSolidEdge_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAcadSubEntSolidEdge_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IAcadSubEntSolidEdge_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IAcadSubEntSolidEdge_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IAcadSubEntSolidEdge_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IAcadSubEntSolidEdge_OnModified(This)	\
    ( (This)->lpVtbl -> OnModified(This) ) 

#define IAcadSubEntSolidEdge_get_ObjectName(This,ObjectName)	\
    ( (This)->lpVtbl -> get_ObjectName(This,ObjectName) ) 

#define IAcadSubEntSolidEdge_get_color(This,pColor)	\
    ( (This)->lpVtbl -> get_color(This,pColor) ) 

#define IAcadSubEntSolidEdge_put_color(This,pColor)	\
    ( (This)->lpVtbl -> put_color(This,pColor) ) 

#define IAcadSubEntSolidEdge_get_Layer(This,Layer)	\
    ( (This)->lpVtbl -> get_Layer(This,Layer) ) 

#define IAcadSubEntSolidEdge_get_Linetype(This,Linetype)	\
    ( (This)->lpVtbl -> get_Linetype(This,Linetype) ) 

#define IAcadSubEntSolidEdge_get_LinetypeScale(This,ltScale)	\
    ( (This)->lpVtbl -> get_LinetypeScale(This,ltScale) ) 

#define IAcadSubEntSolidEdge_get_PlotStyleName(This,plotStyle)	\
    ( (This)->lpVtbl -> get_PlotStyleName(This,plotStyle) ) 

#define IAcadSubEntSolidEdge_get_Lineweight(This,Lineweight)	\
    ( (This)->lpVtbl -> get_Lineweight(This,Lineweight) ) 

#define IAcadSubEntSolidEdge_get_Hyperlinks(This,Hyperlinks)	\
    ( (This)->lpVtbl -> get_Hyperlinks(This,Hyperlinks) ) 


#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAcadSubEntSolidEdge_INTERFACE_DEFINED__ */


#ifndef __IAcadSubEntSolidVertex_INTERFACE_DEFINED__
#define __IAcadSubEntSolidVertex_INTERFACE_DEFINED__

/* interface IAcadSubEntSolidVertex */
/* [object][oleautomation][dual][helpstring][uuid] */ 


EXTERN_C const IID IID_IAcadSubEntSolidVertex;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("45CF6C7D-7191-420a-9881-09042DD618ED")
    IAcadSubEntSolidVertex : public IAcadSubEntity
    {
    public:
    };
    
    
#else 	/* C style interface */

    typedef struct IAcadSubEntSolidVertexVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAcadSubEntSolidVertex * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAcadSubEntSolidVertex * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAcadSubEntSolidVertex * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IAcadSubEntSolidVertex * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IAcadSubEntSolidVertex * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IAcadSubEntSolidVertex * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IAcadSubEntSolidVertex * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *OnModified )( 
            IAcadSubEntSolidVertex * This);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ObjectName )( 
            IAcadSubEntSolidVertex * This,
            /* [retval][out] */ BSTR *ObjectName);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_color )( 
            IAcadSubEntSolidVertex * This,
            /* [retval][out] */ IAcadAcCmColor **pColor);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_color )( 
            IAcadSubEntSolidVertex * This,
            /* [in] */ IAcadAcCmColor *pColor);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Layer )( 
            IAcadSubEntSolidVertex * This,
            /* [retval][out] */ BSTR *Layer);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Linetype )( 
            IAcadSubEntSolidVertex * This,
            /* [retval][out] */ BSTR *Linetype);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_LinetypeScale )( 
            IAcadSubEntSolidVertex * This,
            /* [retval][out] */ ACAD_NOUNITS *ltScale);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_PlotStyleName )( 
            IAcadSubEntSolidVertex * This,
            /* [retval][out] */ BSTR *plotStyle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Lineweight )( 
            IAcadSubEntSolidVertex * This,
            /* [retval][out] */ ACAD_LWEIGHT *Lineweight);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Hyperlinks )( 
            IAcadSubEntSolidVertex * This,
            /* [retval][out] */ IAcadHyperlinks **Hyperlinks);
        
        END_INTERFACE
    } IAcadSubEntSolidVertexVtbl;

    interface IAcadSubEntSolidVertex
    {
        CONST_VTBL struct IAcadSubEntSolidVertexVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAcadSubEntSolidVertex_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAcadSubEntSolidVertex_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAcadSubEntSolidVertex_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAcadSubEntSolidVertex_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IAcadSubEntSolidVertex_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IAcadSubEntSolidVertex_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IAcadSubEntSolidVertex_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IAcadSubEntSolidVertex_OnModified(This)	\
    ( (This)->lpVtbl -> OnModified(This) ) 

#define IAcadSubEntSolidVertex_get_ObjectName(This,ObjectName)	\
    ( (This)->lpVtbl -> get_ObjectName(This,ObjectName) ) 

#define IAcadSubEntSolidVertex_get_color(This,pColor)	\
    ( (This)->lpVtbl -> get_color(This,pColor) ) 

#define IAcadSubEntSolidVertex_put_color(This,pColor)	\
    ( (This)->lpVtbl -> put_color(This,pColor) ) 

#define IAcadSubEntSolidVertex_get_Layer(This,Layer)	\
    ( (This)->lpVtbl -> get_Layer(This,Layer) ) 

#define IAcadSubEntSolidVertex_get_Linetype(This,Linetype)	\
    ( (This)->lpVtbl -> get_Linetype(This,Linetype) ) 

#define IAcadSubEntSolidVertex_get_LinetypeScale(This,ltScale)	\
    ( (This)->lpVtbl -> get_LinetypeScale(This,ltScale) ) 

#define IAcadSubEntSolidVertex_get_PlotStyleName(This,plotStyle)	\
    ( (This)->lpVtbl -> get_PlotStyleName(This,plotStyle) ) 

#define IAcadSubEntSolidVertex_get_Lineweight(This,Lineweight)	\
    ( (This)->lpVtbl -> get_Lineweight(This,Lineweight) ) 

#define IAcadSubEntSolidVertex_get_Hyperlinks(This,Hyperlinks)	\
    ( (This)->lpVtbl -> get_Hyperlinks(This,Hyperlinks) ) 


#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAcadSubEntSolidVertex_INTERFACE_DEFINED__ */


#ifndef __IAcadWipeout_INTERFACE_DEFINED__
#define __IAcadWipeout_INTERFACE_DEFINED__

/* interface IAcadWipeout */
/* [object][oleautomation][dual][helpstring][uuid] */ 


EXTERN_C const IID IID_IAcadWipeout;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("977345B6-A313-4f2e-A4EE-1C2BB3C2FE0E")
    IAcadWipeout : public IAcadRasterImage
    {
    public:
    };
    
    
#else 	/* C style interface */

    typedef struct IAcadWipeoutVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAcadWipeout * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAcadWipeout * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAcadWipeout * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IAcadWipeout * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IAcadWipeout * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IAcadWipeout * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IAcadWipeout * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Handle )( 
            IAcadWipeout * This,
            /* [retval][out] */ BSTR *Handle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ObjectName )( 
            IAcadWipeout * This,
            /* [retval][out] */ BSTR *ObjectName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetXData )( 
            IAcadWipeout * This,
            /* [in] */ BSTR AppName,
            /* [out] */ VARIANT *XDataType,
            /* [out] */ VARIANT *XDataValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetXData )( 
            IAcadWipeout * This,
            /* [in] */ VARIANT XDataType,
            /* [in] */ VARIANT XDataValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Delete )( 
            IAcadWipeout * This);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ObjectID )( 
            IAcadWipeout * This,
            /* [retval][out] */ LONG_PTR *ObjectID);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Application )( 
            IAcadWipeout * This,
            /* [retval][out] */ IDispatch **ApplicationObject);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Database )( 
            IAcadWipeout * This,
            /* [retval][out] */ IAcadDatabase **pDatabase);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_HasExtensionDictionary )( 
            IAcadWipeout * This,
            /* [retval][out] */ VARIANT_BOOL *bHasDictionary);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetExtensionDictionary )( 
            IAcadWipeout * This,
            /* [retval][out] */ IAcadDictionary **pExtDictionary);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_OwnerID )( 
            IAcadWipeout * This,
            /* [retval][out] */ LONG_PTR *OwnerID);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Document )( 
            IAcadWipeout * This,
            /* [retval][out] */ IDispatch **pDocument);
        
        /* [helpstring][hidden][id] */ HRESULT ( STDMETHODCALLTYPE *Erase )( 
            IAcadWipeout * This);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_TrueColor )( 
            IAcadWipeout * This,
            /* [retval][out] */ IAcadAcCmColor **pColor);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_TrueColor )( 
            IAcadWipeout * This,
            /* [in] */ IAcadAcCmColor *pColor);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Layer )( 
            IAcadWipeout * This,
            /* [retval][out] */ BSTR *Layer);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Layer )( 
            IAcadWipeout * This,
            /* [in] */ BSTR Layer);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Linetype )( 
            IAcadWipeout * This,
            /* [retval][out] */ BSTR *Linetype);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Linetype )( 
            IAcadWipeout * This,
            /* [in] */ BSTR Linetype);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_LinetypeScale )( 
            IAcadWipeout * This,
            /* [retval][out] */ ACAD_NOUNITS *ltScale);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_LinetypeScale )( 
            IAcadWipeout * This,
            /* [in] */ ACAD_NOUNITS ltScale);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Visible )( 
            IAcadWipeout * This,
            /* [retval][out] */ VARIANT_BOOL *bVisible);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Visible )( 
            IAcadWipeout * This,
            /* [in] */ VARIANT_BOOL bVisible);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ArrayPolar )( 
            IAcadWipeout * This,
            /* [in] */ int NumberOfObjects,
            /* [in] */ double AngleToFill,
            /* [in] */ VARIANT CenterPoint,
            /* [retval][out] */ VARIANT *pArrayObjs);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ArrayRectangular )( 
            IAcadWipeout * This,
            /* [in] */ int NumberOfRows,
            /* [in] */ int NumberOfColumns,
            /* [in] */ int NumberOfLevels,
            /* [in] */ double DistBetweenRows,
            /* [in] */ double DistBetweenCols,
            /* [in] */ double DistBetweenLevels,
            /* [retval][out] */ VARIANT *pArrayObjs);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Highlight )( 
            IAcadWipeout * This,
            /* [in] */ VARIANT_BOOL HighlightFlag);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Copy )( 
            IAcadWipeout * This,
            /* [retval][out] */ IDispatch **pCopyObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Move )( 
            IAcadWipeout * This,
            /* [in] */ VARIANT FromPoint,
            /* [in] */ VARIANT ToPoint);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Rotate )( 
            IAcadWipeout * This,
            /* [in] */ VARIANT BasePoint,
            /* [in] */ double RotationAngle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Rotate3D )( 
            IAcadWipeout * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [in] */ double RotationAngle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Mirror )( 
            IAcadWipeout * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [retval][out] */ IDispatch **pMirrorObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Mirror3D )( 
            IAcadWipeout * This,
            /* [in] */ VARIANT Point1,
            /* [in] */ VARIANT Point2,
            /* [in] */ VARIANT point3,
            /* [retval][out] */ IDispatch **pMirrorObj);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ScaleEntity )( 
            IAcadWipeout * This,
            /* [in] */ VARIANT BasePoint,
            /* [in] */ double ScaleFactor);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *TransformBy )( 
            IAcadWipeout * This,
            /* [in] */ VARIANT TransformationMatrix);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Update )( 
            IAcadWipeout * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetBoundingBox )( 
            IAcadWipeout * This,
            /* [out] */ VARIANT *MinPoint,
            /* [out] */ VARIANT *MaxPoint);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *IntersectWith )( 
            IAcadWipeout * This,
            /* [in] */ IDispatch *IntersectObject,
            /* [in] */ AcExtendOption option,
            /* [retval][out] */ VARIANT *intPoints);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_PlotStyleName )( 
            IAcadWipeout * This,
            /* [retval][out] */ BSTR *plotStyle);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_PlotStyleName )( 
            IAcadWipeout * This,
            /* [in] */ BSTR plotStyle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Lineweight )( 
            IAcadWipeout * This,
            /* [retval][out] */ ACAD_LWEIGHT *Lineweight);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Lineweight )( 
            IAcadWipeout * This,
            /* [in] */ ACAD_LWEIGHT Lineweight);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Hyperlinks )( 
            IAcadWipeout * This,
            /* [retval][out] */ IAcadHyperlinks **Hyperlinks);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Material )( 
            IAcadWipeout * This,
            /* [retval][out] */ BSTR *Material);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Material )( 
            IAcadWipeout * This,
            /* [in] */ BSTR Material);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EntityName )( 
            IAcadWipeout * This,
            /* [retval][out] */ BSTR *EntityName);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_EntityType )( 
            IAcadWipeout * This,
            /* [retval][out] */ long *entType);
        
        /* [helpstring][hidden][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_color )( 
            IAcadWipeout * This,
            /* [retval][out] */ ACAD_COLOR *color);
        
        /* [helpstring][hidden][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_color )( 
            IAcadWipeout * This,
            /* [in] */ ACAD_COLOR color);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Brightness )( 
            IAcadWipeout * This,
            /* [retval][out] */ long *Brightness);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Brightness )( 
            IAcadWipeout * This,
            /* [in] */ long Brightness);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Contrast )( 
            IAcadWipeout * This,
            /* [retval][out] */ long *Contrast);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Contrast )( 
            IAcadWipeout * This,
            /* [in] */ long Contrast);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Fade )( 
            IAcadWipeout * This,
            /* [retval][out] */ long *Fade);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Fade )( 
            IAcadWipeout * This,
            /* [in] */ long Fade);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Origin )( 
            IAcadWipeout * This,
            /* [retval][out] */ VARIANT *Origin);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Origin )( 
            IAcadWipeout * This,
            /* [in] */ VARIANT Origin);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Rotation )( 
            IAcadWipeout * This,
            /* [retval][out] */ ACAD_ANGLE *rotAngle);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Rotation )( 
            IAcadWipeout * This,
            /* [in] */ ACAD_ANGLE rotAngle);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ImageWidth )( 
            IAcadWipeout * This,
            /* [retval][out] */ double *Width);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ImageWidth )( 
            IAcadWipeout * This,
            /* [in] */ double Width);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ImageHeight )( 
            IAcadWipeout * This,
            /* [retval][out] */ double *Height);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ImageHeight )( 
            IAcadWipeout * This,
            /* [in] */ double Height);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Name )( 
            IAcadWipeout * This,
            /* [retval][out] */ BSTR *Name);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Name )( 
            IAcadWipeout * This,
            /* [in] */ BSTR Name);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ImageFile )( 
            IAcadWipeout * This,
            /* [in] */ BSTR imageFileName);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ImageFile )( 
            IAcadWipeout * This,
            /* [retval][out] */ BSTR *imageFileName);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ImageVisibility )( 
            IAcadWipeout * This,
            /* [retval][out] */ VARIANT_BOOL *fVisible);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ImageVisibility )( 
            IAcadWipeout * This,
            /* [in] */ VARIANT_BOOL fVisible);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ClippingEnabled )( 
            IAcadWipeout * This,
            /* [retval][out] */ VARIANT_BOOL *kClip);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ClippingEnabled )( 
            IAcadWipeout * This,
            /* [in] */ VARIANT_BOOL kClip);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Transparency )( 
            IAcadWipeout * This,
            /* [retval][out] */ VARIANT_BOOL *bTransp);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_Transparency )( 
            IAcadWipeout * This,
            /* [in] */ VARIANT_BOOL bTransp);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ClipBoundary )( 
            IAcadWipeout * This,
            /* [in] */ VARIANT boundry);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Height )( 
            IAcadWipeout * This,
            /* [retval][out] */ double *pixelHeight);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Width )( 
            IAcadWipeout * This,
            /* [retval][out] */ double *pixelWidth);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ShowRotation )( 
            IAcadWipeout * This,
            /* [retval][out] */ VARIANT_BOOL *bShow);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ShowRotation )( 
            IAcadWipeout * This,
            /* [in] */ VARIANT_BOOL bShow);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ScaleFactor )( 
            IAcadWipeout * This,
            /* [retval][out] */ ACAD_NOUNITS *ScaleFactor);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_ScaleFactor )( 
            IAcadWipeout * This,
            /* [in] */ ACAD_NOUNITS ScaleFactor);
        
        END_INTERFACE
    } IAcadWipeoutVtbl;

    interface IAcadWipeout
    {
        CONST_VTBL struct IAcadWipeoutVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAcadWipeout_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAcadWipeout_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAcadWipeout_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAcadWipeout_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IAcadWipeout_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IAcadWipeout_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IAcadWipeout_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IAcadWipeout_get_Handle(This,Handle)	\
    ( (This)->lpVtbl -> get_Handle(This,Handle) ) 

#define IAcadWipeout_get_ObjectName(This,ObjectName)	\
    ( (This)->lpVtbl -> get_ObjectName(This,ObjectName) ) 

#define IAcadWipeout_GetXData(This,AppName,XDataType,XDataValue)	\
    ( (This)->lpVtbl -> GetXData(This,AppName,XDataType,XDataValue) ) 

#define IAcadWipeout_SetXData(This,XDataType,XDataValue)	\
    ( (This)->lpVtbl -> SetXData(This,XDataType,XDataValue) ) 

#define IAcadWipeout_Delete(This)	\
    ( (This)->lpVtbl -> Delete(This) ) 

#define IAcadWipeout_get_ObjectID(This,ObjectID)	\
    ( (This)->lpVtbl -> get_ObjectID(This,ObjectID) ) 

#define IAcadWipeout_get_Application(This,ApplicationObject)	\
    ( (This)->lpVtbl -> get_Application(This,ApplicationObject) ) 

#define IAcadWipeout_get_Database(This,pDatabase)	\
    ( (This)->lpVtbl -> get_Database(This,pDatabase) ) 

#define IAcadWipeout_get_HasExtensionDictionary(This,bHasDictionary)	\
    ( (This)->lpVtbl -> get_HasExtensionDictionary(This,bHasDictionary) ) 

#define IAcadWipeout_GetExtensionDictionary(This,pExtDictionary)	\
    ( (This)->lpVtbl -> GetExtensionDictionary(This,pExtDictionary) ) 

#define IAcadWipeout_get_OwnerID(This,OwnerID)	\
    ( (This)->lpVtbl -> get_OwnerID(This,OwnerID) ) 

#define IAcadWipeout_get_Document(This,pDocument)	\
    ( (This)->lpVtbl -> get_Document(This,pDocument) ) 

#define IAcadWipeout_Erase(This)	\
    ( (This)->lpVtbl -> Erase(This) ) 


#define IAcadWipeout_get_TrueColor(This,pColor)	\
    ( (This)->lpVtbl -> get_TrueColor(This,pColor) ) 

#define IAcadWipeout_put_TrueColor(This,pColor)	\
    ( (This)->lpVtbl -> put_TrueColor(This,pColor) ) 

#define IAcadWipeout_get_Layer(This,Layer)	\
    ( (This)->lpVtbl -> get_Layer(This,Layer) ) 

#define IAcadWipeout_put_Layer(This,Layer)	\
    ( (This)->lpVtbl -> put_Layer(This,Layer) ) 

#define IAcadWipeout_get_Linetype(This,Linetype)	\
    ( (This)->lpVtbl -> get_Linetype(This,Linetype) ) 

#define IAcadWipeout_put_Linetype(This,Linetype)	\
    ( (This)->lpVtbl -> put_Linetype(This,Linetype) ) 

#define IAcadWipeout_get_LinetypeScale(This,ltScale)	\
    ( (This)->lpVtbl -> get_LinetypeScale(This,ltScale) ) 

#define IAcadWipeout_put_LinetypeScale(This,ltScale)	\
    ( (This)->lpVtbl -> put_LinetypeScale(This,ltScale) ) 

#define IAcadWipeout_get_Visible(This,bVisible)	\
    ( (This)->lpVtbl -> get_Visible(This,bVisible) ) 

#define IAcadWipeout_put_Visible(This,bVisible)	\
    ( (This)->lpVtbl -> put_Visible(This,bVisible) ) 

#define IAcadWipeout_ArrayPolar(This,NumberOfObjects,AngleToFill,CenterPoint,pArrayObjs)	\
    ( (This)->lpVtbl -> ArrayPolar(This,NumberOfObjects,AngleToFill,CenterPoint,pArrayObjs) ) 

#define IAcadWipeout_ArrayRectangular(This,NumberOfRows,NumberOfColumns,NumberOfLevels,DistBetweenRows,DistBetweenCols,DistBetweenLevels,pArrayObjs)	\
    ( (This)->lpVtbl -> ArrayRectangular(This,NumberOfRows,NumberOfColumns,NumberOfLevels,DistBetweenRows,DistBetweenCols,DistBetweenLevels,pArrayObjs) ) 

#define IAcadWipeout_Highlight(This,HighlightFlag)	\
    ( (This)->lpVtbl -> Highlight(This,HighlightFlag) ) 

#define IAcadWipeout_Copy(This,pCopyObj)	\
    ( (This)->lpVtbl -> Copy(This,pCopyObj) ) 

#define IAcadWipeout_Move(This,FromPoint,ToPoint)	\
    ( (This)->lpVtbl -> Move(This,FromPoint,ToPoint) ) 

#define IAcadWipeout_Rotate(This,BasePoint,RotationAngle)	\
    ( (This)->lpVtbl -> Rotate(This,BasePoint,RotationAngle) ) 

#define IAcadWipeout_Rotate3D(This,Point1,Point2,RotationAngle)	\
    ( (This)->lpVtbl -> Rotate3D(This,Point1,Point2,RotationAngle) ) 

#define IAcadWipeout_Mirror(This,Point1,Point2,pMirrorObj)	\
    ( (This)->lpVtbl -> Mirror(This,Point1,Point2,pMirrorObj) ) 

#define IAcadWipeout_Mirror3D(This,Point1,Point2,point3,pMirrorObj)	\
    ( (This)->lpVtbl -> Mirror3D(This,Point1,Point2,point3,pMirrorObj) ) 

#define IAcadWipeout_ScaleEntity(This,BasePoint,ScaleFactor)	\
    ( (This)->lpVtbl -> ScaleEntity(This,BasePoint,ScaleFactor) ) 

#define IAcadWipeout_TransformBy(This,TransformationMatrix)	\
    ( (This)->lpVtbl -> TransformBy(This,TransformationMatrix) ) 

#define IAcadWipeout_Update(This)	\
    ( (This)->lpVtbl -> Update(This) ) 

#define IAcadWipeout_GetBoundingBox(This,MinPoint,MaxPoint)	\
    ( (This)->lpVtbl -> GetBoundingBox(This,MinPoint,MaxPoint) ) 

#define IAcadWipeout_IntersectWith(This,IntersectObject,option,intPoints)	\
    ( (This)->lpVtbl -> IntersectWith(This,IntersectObject,option,intPoints) ) 

#define IAcadWipeout_get_PlotStyleName(This,plotStyle)	\
    ( (This)->lpVtbl -> get_PlotStyleName(This,plotStyle) ) 

#define IAcadWipeout_put_PlotStyleName(This,plotStyle)	\
    ( (This)->lpVtbl -> put_PlotStyleName(This,plotStyle) ) 

#define IAcadWipeout_get_Lineweight(This,Lineweight)	\
    ( (This)->lpVtbl -> get_Lineweight(This,Lineweight) ) 

#define IAcadWipeout_put_Lineweight(This,Lineweight)	\
    ( (This)->lpVtbl -> put_Lineweight(This,Lineweight) ) 

#define IAcadWipeout_get_Hyperlinks(This,Hyperlinks)	\
    ( (This)->lpVtbl -> get_Hyperlinks(This,Hyperlinks) ) 

#define IAcadWipeout_get_Material(This,Material)	\
    ( (This)->lpVtbl -> get_Material(This,Material) ) 

#define IAcadWipeout_put_Material(This,Material)	\
    ( (This)->lpVtbl -> put_Material(This,Material) ) 

#define IAcadWipeout_get_EntityName(This,EntityName)	\
    ( (This)->lpVtbl -> get_EntityName(This,EntityName) ) 

#define IAcadWipeout_get_EntityType(This,entType)	\
    ( (This)->lpVtbl -> get_EntityType(This,entType) ) 

#define IAcadWipeout_get_color(This,color)	\
    ( (This)->lpVtbl -> get_color(This,color) ) 

#define IAcadWipeout_put_color(This,color)	\
    ( (This)->lpVtbl -> put_color(This,color) ) 


#define IAcadWipeout_get_Brightness(This,Brightness)	\
    ( (This)->lpVtbl -> get_Brightness(This,Brightness) ) 

#define IAcadWipeout_put_Brightness(This,Brightness)	\
    ( (This)->lpVtbl -> put_Brightness(This,Brightness) ) 

#define IAcadWipeout_get_Contrast(This,Contrast)	\
    ( (This)->lpVtbl -> get_Contrast(This,Contrast) ) 

#define IAcadWipeout_put_Contrast(This,Contrast)	\
    ( (This)->lpVtbl -> put_Contrast(This,Contrast) ) 

#define IAcadWipeout_get_Fade(This,Fade)	\
    ( (This)->lpVtbl -> get_Fade(This,Fade) ) 

#define IAcadWipeout_put_Fade(This,Fade)	\
    ( (This)->lpVtbl -> put_Fade(This,Fade) ) 

#define IAcadWipeout_get_Origin(This,Origin)	\
    ( (This)->lpVtbl -> get_Origin(This,Origin) ) 

#define IAcadWipeout_put_Origin(This,Origin)	\
    ( (This)->lpVtbl -> put_Origin(This,Origin) ) 

#define IAcadWipeout_get_Rotation(This,rotAngle)	\
    ( (This)->lpVtbl -> get_Rotation(This,rotAngle) ) 

#define IAcadWipeout_put_Rotation(This,rotAngle)	\
    ( (This)->lpVtbl -> put_Rotation(This,rotAngle) ) 

#define IAcadWipeout_get_ImageWidth(This,Width)	\
    ( (This)->lpVtbl -> get_ImageWidth(This,Width) ) 

#define IAcadWipeout_put_ImageWidth(This,Width)	\
    ( (This)->lpVtbl -> put_ImageWidth(This,Width) ) 

#define IAcadWipeout_get_ImageHeight(This,Height)	\
    ( (This)->lpVtbl -> get_ImageHeight(This,Height) ) 

#define IAcadWipeout_put_ImageHeight(This,Height)	\
    ( (This)->lpVtbl -> put_ImageHeight(This,Height) ) 

#define IAcadWipeout_get_Name(This,Name)	\
    ( (This)->lpVtbl -> get_Name(This,Name) ) 

#define IAcadWipeout_put_Name(This,Name)	\
    ( (This)->lpVtbl -> put_Name(This,Name) ) 

#define IAcadWipeout_put_ImageFile(This,imageFileName)	\
    ( (This)->lpVtbl -> put_ImageFile(This,imageFileName) ) 

#define IAcadWipeout_get_ImageFile(This,imageFileName)	\
    ( (This)->lpVtbl -> get_ImageFile(This,imageFileName) ) 

#define IAcadWipeout_get_ImageVisibility(This,fVisible)	\
    ( (This)->lpVtbl -> get_ImageVisibility(This,fVisible) ) 

#define IAcadWipeout_put_ImageVisibility(This,fVisible)	\
    ( (This)->lpVtbl -> put_ImageVisibility(This,fVisible) ) 

#define IAcadWipeout_get_ClippingEnabled(This,kClip)	\
    ( (This)->lpVtbl -> get_ClippingEnabled(This,kClip) ) 

#define IAcadWipeout_put_ClippingEnabled(This,kClip)	\
    ( (This)->lpVtbl -> put_ClippingEnabled(This,kClip) ) 

#define IAcadWipeout_get_Transparency(This,bTransp)	\
    ( (This)->lpVtbl -> get_Transparency(This,bTransp) ) 

#define IAcadWipeout_put_Transparency(This,bTransp)	\
    ( (This)->lpVtbl -> put_Transparency(This,bTransp) ) 

#define IAcadWipeout_ClipBoundary(This,boundry)	\
    ( (This)->lpVtbl -> ClipBoundary(This,boundry) ) 

#define IAcadWipeout_get_Height(This,pixelHeight)	\
    ( (This)->lpVtbl -> get_Height(This,pixelHeight) ) 

#define IAcadWipeout_get_Width(This,pixelWidth)	\
    ( (This)->lpVtbl -> get_Width(This,pixelWidth) ) 

#define IAcadWipeout_get_ShowRotation(This,bShow)	\
    ( (This)->lpVtbl -> get_ShowRotation(This,bShow) ) 

#define IAcadWipeout_put_ShowRotation(This,bShow)	\
    ( (This)->lpVtbl -> put_ShowRotation(This,bShow) ) 

#define IAcadWipeout_get_ScaleFactor(This,ScaleFactor)	\
    ( (This)->lpVtbl -> get_ScaleFactor(This,ScaleFactor) ) 

#define IAcadWipeout_put_ScaleFactor(This,ScaleFactor)	\
    ( (This)->lpVtbl -> put_ScaleFactor(This,ScaleFactor) ) 


#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAcadWipeout_INTERFACE_DEFINED__ */


/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

unsigned long             __RPC_USER  VARIANT_UserSize(     unsigned long *, unsigned long            , VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserMarshal(  unsigned long *, unsigned char *, VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserUnmarshal(unsigned long *, unsigned char *, VARIANT * ); 
void                      __RPC_USER  VARIANT_UserFree(     unsigned long *, VARIANT * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


